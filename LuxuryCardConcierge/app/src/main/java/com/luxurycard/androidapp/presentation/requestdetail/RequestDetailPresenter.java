package com.luxurycard.androidapp.presentation.requestdetail;

import com.luxurycard.androidapp.App;
import com.luxurycard.androidapp.common.constant.AppConstant;
import com.luxurycard.androidapp.datalayer.entity.askconcierge.ACResponse;
import com.luxurycard.androidapp.datalayer.preference.PreferencesStorage;
import com.luxurycard.androidapp.domain.model.Metadata;
import com.luxurycard.androidapp.domain.model.Profile;
import com.luxurycard.androidapp.domain.model.RequestDetailData;
import com.luxurycard.androidapp.domain.usecases.CreateRequest;
import com.luxurycard.androidapp.domain.usecases.GetAccessToken;
import com.luxurycard.androidapp.domain.usecases.LoadProfile;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.BiFunction;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by vinh.trinh on 9/13/2017.
 */

public class RequestDetailPresenter implements RequestDetail.Presenter {

    private CompositeDisposable compositeDisposable;
    private RequestDetail.View view;
    private GetAccessToken getAccessToken;
    private CreateRequest askConcierge;
    private LoadProfile loadProfile;
    private Metadata metadata;

    private final String transactionID;
    private RequestDetailData data;

    RequestDetailPresenter(RequestDetailData data, PreferencesStorage ps, CreateRequest cr,
                           GetAccessToken at, LoadProfile lp) {
        compositeDisposable = new CompositeDisposable();
        transactionID = data.transactionID;
        this.data = data;
        this.getAccessToken = at;
        this.loadProfile = lp;
        this.askConcierge = cr;
        try {
            this.metadata = ps.metadata();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    RequestDetailData getData() {
        return data;
    }

    @Override
    public void attach(RequestDetail.View view) {
        this.view = view;
    }

    @Override
    public void detach() {
        compositeDisposable.dispose();
        view = null;
    }

    @Override
    public void getRequestDetail() {
        view.showProgressDialog();
        compositeDisposable.add(Single.just(data)
                .map(data1 -> {
                    data1.buildJsonDetail();
                    this.data = data1;
                    DetailViewData viewData = new DetailViewData(data1);
                    viewData.details = detailText(data1, false);
                    return viewData;
                })
                .delay(2, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<DetailViewData>() {
                    @Override
                    public void onSuccess(DetailViewData data) {
                        view.showData(data);
                        view.dismissProgressDialog();
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.dismissProgressDialog();
                    }
                }));
    }

    @Override
    public String content(boolean create) {
        try {
            return detailText(data, create);
        } catch (JSONException e) {
            return "";
        }
    }

    @Override
    public void dismissRequest() {
        view.showProgressDialog();
        compositeDisposable.add(Single.zip(getAccessToken.execute(), loadProfile.buildUseCaseSingle(null), this::buildParams)
                .flatMap(params -> askConcierge.execute(params)).subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
        .subscribeWith(new DisposableSingleObserver<ACResponse>() {
            @Override
            public void onSuccess(ACResponse acResponse) {
                view.dismissProgressDialog();
                if(acResponse.getStatus() != null && acResponse.getStatus().isSuccess()) {
                    view.onCancelDone();
                } else {
                    view.showErrorMessage("");
                    trackFail();
                }
            }

            @Override
            public void onError(Throwable e) {
                view.dismissProgressDialog();
                view.showErrorMessage("");
                trackFail();
            }

            private void trackFail(){
                // Track GA with "create request failed"
                App.getInstance().track(AppConstant.GA_TRACKING_CATEGORY.REQUEST.getValue(),
                        AppConstant.GA_TRACKING_ACTION.SUBMIT.getValue(),
                        AppConstant.GA_TRACKING_LABEL.REQUEST_DENIED.getValue());
            }

        }));
    }

    private CreateRequest.Params buildParams(String accessToken, Profile profile) {
        CreateRequest.RequestContent requestContent = new CreateRequest.RequestContent(
                data.detail,
                data.responseMode.contains("Email"),
                data.responseMode.contains("Mobile")
        );
        CreateRequest.Params params = new CreateRequest.Params(
                accessToken,
                profile,
                metadata,
                requestContent
        );
        params.transactionID(transactionID);
        params.editType(CreateRequest.EDIT_TYPE.CANCEL);
        return params;
    }

    public String detailText(RequestDetailData data, boolean ignoreDate) throws JSONException {
        TextDetailBuilder textDetail = new TextDetailBuilder(data, data.detail(), ignoreDate);
        switch (data.requestType.toUpperCase()) {
            case "D RESTAURANT":
                return textDetail.dining();
            case "T HOTEL AND B&B":
                return textDetail.hotel();
            case "S GOLF":
                return textDetail.golf();
            case "T LIMO AND SEDAN":
                return textDetail.carRental();
            case "E CONCERT/THEATER":
                return textDetail.entertainment();
            case "C SIGHTSEEING/TOURS":
                return textDetail.tour();
            case "I AIRPORT SERVICES":
                return textDetail.flight();
            case "T CRUISE":
                return textDetail.cruise();
            case "G FLOWERS/GIFT BASKET":
                return textDetail.flower();
            case "T OTHER":
                return textDetail.privateJet();
            case "O CLIENT SPECIFIC":
                return textDetail.others();
        }
        return "";
    }

}
