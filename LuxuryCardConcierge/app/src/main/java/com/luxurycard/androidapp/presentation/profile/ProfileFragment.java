package com.luxurycard.androidapp.presentation.profile;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.SwitchCompat;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.method.PasswordTransformationMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.support.v7.widget.Toolbar;

import com.luxurycard.androidapp.R;
import com.luxurycard.androidapp.common.constant.AppConstant;
import com.luxurycard.androidapp.common.constant.CountryCode;
import com.luxurycard.androidapp.common.constant.IntentConstant;
import com.luxurycard.androidapp.common.logic.MaskerHelper;
import com.luxurycard.androidapp.common.logic.Validator;
import com.luxurycard.androidapp.domain.model.Profile;
import com.luxurycard.androidapp.presentation.base.BaseFragment;
import com.luxurycard.androidapp.presentation.base.CommonActivity;
import com.luxurycard.androidapp.presentation.info.MasterCardUtilityActivity;
import com.luxurycard.androidapp.presentation.widget.CustomSpinner;
import com.luxurycard.androidapp.presentation.widget.DropdownAdapter;
import com.luxurycard.androidapp.presentation.widget.PasswordInputFilter;
import com.luxurycard.androidapp.presentation.widget.TextPattern;
import com.luxurycard.androidapp.presentation.widget.ViewUtils;
import com.support.mylibrary.widget.ClickGuard;
import com.support.mylibrary.widget.ErrorIndicatorEditText;
import com.support.mylibrary.widget.LetterSpacingTextView;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnTouch;

/**
 * Created by vinh.trinh on 4/27/2017.
 */

public class ProfileFragment extends BaseFragment {

    enum PROFILE {CREATE, EDIT}

    private static final String ARG_WHICH = "which";
    @BindView(R.id.profile_rootview)
    LinearLayout llProfileView;
    @BindView(R.id.scroll_view)
    ScrollView mainScrollView;
    @BindView(R.id.llParent)
    LinearLayout llParent;
    @BindView(R.id.fakeViewToMoveButtonDown)
    View fakeViewToMoveButtonDown;
//    @BindView(R.id.tv_title)
//    AppCompatTextView tvTitle;
    @BindView(R.id.edt_first_name)
    ErrorIndicatorEditText edtFirstName;
    @BindView(R.id.edt_last_name)
    ErrorIndicatorEditText edtLastName;
    @BindView(R.id.edt_email)
    ErrorIndicatorEditText edtEmail;
    @BindView(R.id.flEmailMask)
    View emailMask;
    @BindView(R.id.edt_phone)
    ErrorIndicatorEditText edtPhone;
    @BindView(R.id.edt_pwd)
    ErrorIndicatorEditText edtPassword;
//    @BindView(R.id.edt_pwd_confirm)
//    ErrorIndicatorEditText edtConfirmPassword;
//    @BindView(R.id.switch_location)
//    SwitchCompat locationSwitchCompat;
    @BindView(R.id.checkbox_acknowledge)
    AppCompatCheckBox chbAcknowledge;
    @BindView(R.id.btn_submit)
    AppCompatButton btnSubmit;
    @BindView(R.id.btn_cancel)
    LetterSpacingTextView btnCancel;
    @BindView(R.id.content_wrapper)
    LinearLayout contentWrapper;
    @BindView(R.id.tvAcknow)
    LetterSpacingTextView tvAcknow;
    @BindView(R.id.tvSelectSalutation)
    LetterSpacingTextView tvSelectSalutation;
    @BindView(R.id.tvCountryCode)
    LetterSpacingTextView tvCountryCode;
    @BindView(R.id.edt_zip_code)
    ErrorIndicatorEditText edtZipCode;
    @BindView(R.id.edt_fisrt_six_digits)
    ErrorIndicatorEditText edtFirstSixDigits;
    @BindView(R.id.spCountryCode)
    Spinner spCountryCode;
    @BindView(R.id.vSalutation)
    View viewSalutation;
    @BindView(R.id.vCountryCode)
    View viewCountryCode;
    @BindView(R.id.title)
    LetterSpacingTextView title;
    @BindView(R.id.edt_title_first_six_digits)
    LetterSpacingTextView edtTitleFirstSixDigits;
    @BindView(R.id.edt_title_pwd)
    LetterSpacingTextView edtTitlePwd;
    @BindView(R.id.contentSubmit)
    LinearLayout contenSubmit;
    @BindView(R.id.password_must_be)
    AppCompatTextView tvPwdMustBe;
//    @BindView(R.id.edt_title_pwd_confirm)
//    LetterSpacingTextView edtTitlePwdConfirm;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private ProfileEventsListener listener;
    private Profile profile = new Profile();
    private ProfileComparator profileComparator;
    private Validator validator = new Validator();
    private FocusChangeListener firstNameMasker = new FocusChangeListener(MaskerHelper.INPUT.NAME);
    private FocusChangeListener lastNameMasker = new FocusChangeListener(MaskerHelper.INPUT.NAME);
    private FocusChangeListener emailMasker = new FocusChangeListener(MaskerHelper.INPUT.EMAIL);
    private FocusChangeListener phoneMasker = new FocusChangeListener(MaskerHelper.INPUT.PHONE);
    private FocusChangeListener zipCodeMasker = new FocusChangeListener(MaskerHelper.INPUT.ZIPCODE);
    private TextPattern phonePattern;
    private AppCompatEditText editText;
    private String errMessage;
    private int hasFocus;
    PROFILE which;
    boolean firstNameMask;
    private boolean isKeyboardShow;
    private CustomSpinner mSpinnerSalutation, mCountryCode;
    private boolean isEdtMode;
    private boolean profileFromUpdate = true;

    Button btnSave;

    public static ProfileFragment newInstance(PROFILE profile) {
        Bundle args = new Bundle();
        args.putString(ARG_WHICH, profile.name());
        ProfileFragment fragment = new ProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ProfileEventsListener) {
            listener = (ProfileEventsListener) context;
        } else {
            throw new ClassCastException(context.toString() + " must implement ProfileEventsListener.");
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        storeUp();
        outState.putParcelable("input", profile);
        if (profileComparator != null)
            outState.putParcelable("original", profileComparator.original);
        super.onSaveInstanceState(outState);
    }

    @Override
    public int setupCreateViewLayoutId() {
        return R.layout.fragment_profile;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        listener.onFragmentCreated();
        which = PROFILE.valueOf(getArguments().getString(ARG_WHICH));
        int currentIndexCountryCode = -1;
        if (which == PROFILE.EDIT) {
            isEdtMode = true;
            edtEmail.setTextColor(Color.parseColor("#99A1A8"));
            edtEmail.setInputType(InputType.TYPE_NULL);
            edtEmail.setCursorVisible(false);
            emailMask.setVisibility(View.VISIBLE);
            edtEmail.setEnabled(false);
            edtPassword.setVisibility(View.GONE);
            edtTitleFirstSixDigits.setVisibility(View.GONE);
            edtTitlePwd.setVisibility(View.GONE);
//            edtTitlePwdConfirm.setVisibility(View.GONE);
//            edtConfirmPassword.setVisibility(View.GONE);
            view.findViewById(R.id.checkbox_container).setVisibility(View.GONE);
//            tvTitle.setVisibility(View.GONE);
            btnSubmit.setText(getString(R.string.update));
            btnCancel.setVisibility(View.GONE);
            edtFirstSixDigits.setVisibility(View.GONE);
            contenSubmit.setVisibility(View.GONE);
            LetterSpacingTextView title = toolbar.findViewById(R.id.title);
            title.setText(getString(R.string.title_my_profile));
            btnSave = toolbar.findViewById(R.id.btn_save);
            FrameLayout layout = toolbar.findViewById(R.id.toolbar_btn_save);
            layout.setVisibility(View.VISIBLE);
            toolbar.setClickable(true);
            toolbar.setOnClickListener(v -> {
                if(getActivity().getCurrentFocus() != null) {
                    ViewUtils.hideSoftKey(getActivity().getCurrentFocus());
                }
            });
            tvPwdMustBe.setVisibility(View.GONE);
        } else {
            edtPhone.setImeOptions(EditorInfo.IME_ACTION_NEXT);
            InputFilter[] filtersPassword = new InputFilter[]{new PasswordInputFilter(), new InputFilter.LengthFilter(getResources().getInteger(R.integer.password_max_length_characters))};
            edtPassword.setFilters(filtersPassword);
            toolbar.setClickable(true);
            toolbar.setOnClickListener(v -> {
                if(getActivity().getCurrentFocus() != null) {
                    ViewUtils.hideSoftKey(getActivity().getCurrentFocus());
                }
            });
//            edtConfirmPassword.setFilters(filtersPassword);
            title.setText(R.string.text_sign_up);
            title.setAllCaps(false);
            currentIndexCountryCode = getCountryCode().indexOf("United States (USA) (1)");
            tvCountryCode.setText("USA (1)");
            setupTermOfUseClickable();

        }

        AppCompatImageButton back = toolbar.findViewById(android.R.id.home);
        back.setOnClickListener(v -> {
            ViewUtils.hideSoftKey(v);
            getActivity().onBackPressed();
        });
        mSpinnerSalutation = setUpCustomSpinner(tvSelectSalutation, getSalutation(), -1);
        tvSelectSalutation.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().equalsIgnoreCase(getString(R.string.my_profile_text_salutation_place_holder))) {
                    viewSalutation.setBackgroundColor(Color.WHITE);
                }
            }
        });

        mSpinnerSalutation.setNone(false);
        tvCountryCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().equalsIgnoreCase(getString(R.string.my_profile_text_salutation_place_holder))) {
                    viewCountryCode.setBackgroundColor(Color.WHITE);
                }
            }
        });
        mCountryCode = setUpCustomSpinner(tvCountryCode, getCountryCode(), currentIndexCountryCode);

        mCountryCode.setNone(false);
        edtFirstName.setOnFocusChangeListener(firstNameMasker);
        edtLastName.setOnFocusChangeListener(lastNameMasker);
        edtEmail.setOnFocusChangeListener(emailMasker);
        edtPhone.setOnFocusChangeListener(phoneMasker);
        edtZipCode.setOnFocusChangeListener(zipCodeMasker);
        edtFirstSixDigits.setInputType(InputType.TYPE_CLASS_PHONE | InputType.TYPE_NUMBER_VARIATION_PASSWORD);
        edtFirstSixDigits.setTransformationMethod(PasswordTransformationMethod.getInstance());
        initTextInteractListener();

//        phonePattern = new TextPattern(edtPhone,
//                "#-###-###-####",
//                2);
//        edtPhone.addTextChangedListener(phonePattern);
//        chbAcknowledge.setChecked(false);
        chbAcknowledge.setOnCheckedChangeListener((buttonView, isChecked) -> {
            btnSubmit.setEnabled(isChecked);

        });

//        locationSwitchCompat.setOnCheckedChangeListener((compoundButton, isChecked) -> {
//            boolean on = !isChecked;
//            profile.locationToggle(on);
//            if (which == PROFILE.EDIT && profileComparator != null) profileComparator.onChanged();
//            if (on) listener.onLocationSwitchOn();
//        });
        tvAcknow.setOnClickListener(view1 -> chbAcknowledge.setChecked(!chbAcknowledge.isChecked()));
        if (savedInstanceState != null) {
            loadProfile(savedInstanceState.getParcelable("original"));
        }
        ClickGuard.guard(btnSubmit, btnCancel);
    }

    private List<String> getSalutation() {
        return Arrays.asList(getResources().getStringArray(R.array.cate_salutations));
    }

    private List<String> getCountryCode() {
        //Todo
        return Arrays.asList(getResources().getStringArray(R.array.country_codes));
    }

    private CustomSpinner setUpCustomSpinner(TextView anchor, List<String> data, int currentSelected) {
        CustomSpinner customSpinner = new CustomSpinner(getContext(), anchor);

        DropdownAdapter dropdownAdapter = new DropdownAdapter(getContext(), R.layout.item_dropdown, data, currentSelected);
        customSpinner.setDropdownAdapter(dropdownAdapter);
        int maxHeight = data.size() * 40;
        if (maxHeight < 200) {
            customSpinner.setPobpupHeight(maxHeight);
        } else {
            customSpinner.setPobpupHeight(200);
        }
        customSpinner.setTextModifier(val -> {
            int last = val.lastIndexOf(')');
            int first = val.indexOf('(');
            if(first > -1 && last > -1) {
                int firstCloseParenthese = val.indexOf(')');
                return val.substring(first+1, firstCloseParenthese) + val.substring(firstCloseParenthese+1);
            }
            return val;
        });
        return customSpinner;
    }

    private void initTextInteractListener() {
//        CopyPasteHandler copyPasteHandler = new CopyPasteHandler(validator);
//        //everytime user paste text to edittext, onTextPasted get called.
//        edtFirstName.setTextInteractListener(copyPasteHandler);
//        edtLastName.setTextInteractListener(copyPasteHandler);
        llProfileView.getViewTreeObserver().addOnGlobalLayoutListener(() -> {
            Rect r = new Rect();
            llProfileView.getWindowVisibleDisplayFrame(r);
            int screenHeight = llProfileView.getRootView().getHeight();

            int keypadHeight = screenHeight - r.bottom;

            if (keypadHeight > screenHeight * 0.15) {
                if (!isKeyboardShow) {
                    isKeyboardShow = true;
                    // keyboard is opened
                    mainScrollView.smoothScrollTo(0, edtFirstName.getTop() + contentWrapper.getTop());
                    scaleView();
                    processAfterKeyboardToggle(true);
                }
            } else {
                // keyboard is closed
                if (isKeyboardShow) {
                    isKeyboardShow = false;
                    fakeViewToMoveButtonDown.setVisibility(View.VISIBLE);
                    processAfterKeyboardToggle(false);
                }
            }
        });
    }

    private void scaleView() {
        Animation anim = new ScaleAnimation(
                1f, 1f, // Start and end values for the X axis scaling
                fakeViewToMoveButtonDown.getY(),
                fakeViewToMoveButtonDown.getY() + fakeViewToMoveButtonDown.getHeight(), // Start and end values for the Y axis scaling
                Animation.RELATIVE_TO_SELF, 0f, // Pivot point of X scaling
                Animation.RELATIVE_TO_SELF, 1f); // Pivot point of Y scaling
        anim.setFillAfter(false); // Needed to keep the result of the animation
        anim.setDuration(0);
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                fakeViewToMoveButtonDown.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        fakeViewToMoveButtonDown.startAnimation(anim);
    }

    @OnClick(R.id.btn_submit)
    void onSubmit(View view) {
        profileFromUpdate = true;
        ViewUtils.hideSoftKey(view);
        storeUp();
        hasFocus = checkHasFocus();
        if (formValidation()) {
            listener.onProfileSubmit(profile, edtPassword.getText().toString());
        }
    }


    @OnClick(R.id.btn_save)
    void onSave(View view) {
        profileFromUpdate = true;
        ViewUtils.hideSoftKey(view);
        storeUp();
        hasFocus = checkHasFocus();
        if (formValidation()) {
            if(getActivity() instanceof MyProfileActivity){
                MyProfileActivity activity = (MyProfileActivity) this.getActivity();
                activity.dialogHelper.action(null,
                        getString(R.string.save_profile_confirm),
                        getString(R.string.text_cancel),
                        getString(R.string.text_yes),
                        null,
                        (dialog, which) -> listener.onProfileSubmit(profile, edtPassword.getText().toString()));
            }
        }

    }



    @OnClick(R.id.btn_cancel)
    void onCancel(View view) {
        profileFromUpdate = false;
        ViewUtils.hideSoftKey(view);
        hasFocus = checkHasFocus();
        resetErrorView();
        listener.onProfileCancel();
    }

    @OnClick({R.id.profile_rootview, R.id.content_wrapper, R.id.contentSubmit, R.id.flEmailMask})
    public void onClick(View view) {
        ViewUtils.hideSoftKey(view);
        if (view.getId() == R.id.flEmailMask) {
            edtEmail.setText(emailMasker.original);
        }
       }

    private void processAfterKeyboardToggle(boolean isKeyboardShow) {
        View currentView = getActivity().getCurrentFocus();
        if (currentView != null && currentView instanceof EditText) {
            ((EditText) currentView).setCursorVisible(isKeyboardShow);
            if (!isKeyboardShow) {
                currentView.clearFocus();
//                llProfileView.requestFocus();
            }
        }
    }

    private boolean formValidation() {
        boolean isValidated = checkout();
        if (!isValidated) {
            if (editText != null) {
                editText.requestFocus();
                editText.setError("");
                editText.setSelection(editText.getText().length());
            }
            listener.onProfileInputError(errMessage);
        }
        return isValidated;
    }

    private boolean checkout() {
        boolean isValidated = true;
        editText = null;
        StringBuilder messageBuilder = new StringBuilder();

        if (tvSelectSalutation.getText().toString().length() == 0) {
            viewSalutation.setBackgroundColor(Color.parseColor("#ffff4444"));
            if (messageBuilder.length() > 0) {
                messageBuilder.append("\n");
            }
            messageBuilder.append(getString(R.string.err_salutation));
            isValidated = false;
        } else {
            viewSalutation.setBackgroundColor(Color.parseColor("#99A1A8"));
        }

        if (!validator.name(profile.getFirstName())) {
            isValidated = false;
            if (editText == null) editText = edtFirstName;
            edtFirstName.setError("");
            if (profile.getFirstName().length() > 0) {
                if (messageBuilder.length() > 0) {
                    messageBuilder.append("\n");
                }
                messageBuilder.append(getString(R.string.input_err_invalid_name,
                        "first"));
            }

        } else if (!validator.specialChars(profile.getFirstName())) {
            isValidated = false;
            if (editText == null) editText = edtFirstName;
            edtFirstName.setError("");
            if (profile.getFirstName().length() > 0) {
                if (messageBuilder.length() > 0) {
                    messageBuilder.append("\n");
                }
                messageBuilder.append(getString(R.string.input_err_special_char_first_name));
            }
        } else {
            edtFirstName.setError(null);
        }

        if (!validator.name(profile.getLastName())) {
            isValidated = false;
            if (editText == null) editText = edtLastName;
            edtLastName.setError("");
            if (profile.getLastName().length() > 0) {
                if (messageBuilder.length() > 0) {
                    messageBuilder.append("\n");
                }
                messageBuilder.append(getString(R.string.input_err_invalid_name,
                        "last"));
            }

        } else if (!validator.specialChars(profile.getLastName())) {
            isValidated = false;
            if (editText == null) editText = edtLastName;
            edtLastName.setError("");
            if (profile.getLastName().length() > 0) {
                if (messageBuilder.length() > 0) {
                    messageBuilder.append("\n");
                }
                messageBuilder.append(getString(R.string.input_err_special_char_last_name));
            }
        } else {
            edtLastName.setError(null);
        }
        if (!validator.email(profile.getEmail())) {
            isValidated = false;
            if (editText == null) editText = edtEmail;
            edtEmail.setError("");
            if (profile.getEmail().length() > 0) {
                if (messageBuilder.length() > 0) {
                    messageBuilder.append("\n");
                }
                messageBuilder.append(getString(R.string.input_err_invalid_email));
            }
        } else {
            edtEmail.setError(null);
        }

        if (tvCountryCode.getText().toString().equals(getString(R.string.hint_country_code))) {
            isValidated = false;
            viewCountryCode.setBackgroundColor(Color.parseColor("#ffff4444"));
            if (messageBuilder.length() > 0) {
                messageBuilder.append("\n");
            }
            messageBuilder.append(getString(R.string.input_err_country_code));
        } else {
            viewCountryCode.setBackgroundColor(Color.parseColor("#99A1A8"));
        }

        if (!validator.phone(profile.getPhone())) {
            isValidated = false;
            if (editText == null) editText = edtPhone;
            edtPhone.setError("");
            if (profile.getPhone()
                    .length() > 0) {
                if (messageBuilder.length() > 0) {
                    messageBuilder.append("\n");
                }
                messageBuilder.append(getString(R.string.input_err_invalid_phone));
            }
        } else {
            edtPhone.setError(null);
        }

        if (!validator.zipCode(profile.getZipCode())) {
            isValidated = false;
            if (editText == null) editText = edtZipCode;
            edtZipCode.setError("");
            if (profile.getZipCode()
                    .length() > 0) {
                if (messageBuilder.length() > 0) {
                    messageBuilder.append("\n");
                }
                messageBuilder.append(getString(R.string.input_err_invalid_zipcode));
            }
        } else {
            edtZipCode.setError(null);
        }

        if (which == PROFILE.CREATE && !validator.binCode(profile.getFirstSixDigits())) {
            isValidated = false;
            if (editText == null) editText = edtFirstSixDigits;
            edtFirstSixDigits.setError("");
            if (profile.getFirstSixDigits()
                    .length() > 0) {
                if (messageBuilder.length() > 0) {
                    messageBuilder.append("\n");
                }
                messageBuilder.append(getString(R.string.input_err_invalid_bincode));
            }
        } else {
            edtFirstSixDigits.setError(null);
        }

        String password = edtPassword.getText().toString();
//        String reTypePwd = edtConfirmPassword.getText().toString();
        if (which == PROFILE.CREATE) {


            if (!validator.secretValidator(password)) {
                isValidated = false;
                if (editText == null) {
                    editText = edtPassword;
                }
                edtPassword.setError("");
                if (password.length() > 0) {
                    if (messageBuilder.length() > 0) {
                        messageBuilder.append("\n");
                    };
                    messageBuilder.append(getString(R.string.input_err_invalid_password));
                }
            } else {
                edtPassword.setError(null);
            }




//            if (!validator.secretValidator(password)) {
//                isValidated = false;
//                if (editText == null) {
//                    editText = edtPassword;
//                }
//                edtPassword.setError("");
//                if (password.length() > 0)
//                    messageBuilder.append("\n").append(getString(R.string.input_err_invalid_password));
//            }
//            else {
//                edtPassword.setError(null);
//            }

//            if (!validator.secretValidator(reTypePwd)) {
//                isValidated = false;
//                if (editText == null) {
//                    editText = edtConfirmPassword;
//                }
//                edtConfirmPassword.setError("");
//            }
//            boolean emptyPassword = password.length() == 0;
//            if (emptyPassword) {
//                if (!validator.password(password)) {
//                    isValidated = false;
//                    if (editText == null) {
//                        editText = edtPassword;
//                    }
//
//                    if (messageBuilder.length() > 0) {
//                        messageBuilder.append("\n");
//
//                    }
//
//                    messageBuilder.append(getString(R.string.input_err_invalid_password));
//                }
//            }
        }
        else {
            edtPassword.setError(null);
//            edtConfirmPassword.setError(null);
        }

        if (which == PROFILE.CREATE && editText == null && password.length() == 0/* && reTypePwd.length() == 0*/) {
            if (editText == null) editText = edtPassword;
//            editText = edtConfirmPassword;
        }
        if (which == PROFILE.EDIT && editText == null){
            if (editText == null) editText = edtEmail;
        }
//        if (editText != null) {
//            if (getActivity().getCurrentFocus() != null && getActivity().getCurrentFocus() instanceof ErrorIndicatorEditText) {
//                if (((ErrorIndicatorEditText) getActivity().getCurrentFocus()).isError()) {
//                    editText = (ErrorIndicatorEditText) getActivity().getCurrentFocus();
//                }
//            }
//        }
        if (editText != null) {
            String temp = messageBuilder.toString();
            messageBuilder = new StringBuilder("");
            if (which == PROFILE.CREATE && profile.anyEmpty(edtPassword.getText().toString())) {
                messageBuilder.append(getString(R.string.input_err_required));
            } else if (profile.anyIsEmpty()) {
                messageBuilder.append(getString(R.string.input_err_required));
            }

            if (messageBuilder.length() > 0) {
                messageBuilder.append("\n");
            }
            messageBuilder.append(temp);

        }
        errMessage = messageBuilder.toString();
        return isValidated;
    }

    class FocusChangeListener implements View.OnFocusChangeListener {

        MaskerHelper.INPUT input;
        String original = "";
        boolean hasFocus;

        FocusChangeListener(MaskerHelper.INPUT type) {
            input = type;
        }

        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            this.hasFocus = hasFocus;
            AppCompatEditText view = (AppCompatEditText) v;
//            if (input == MaskerHelper.INPUT.PHONE) {
//                if (hasFocus) {
//                    if (original.length() == 0) {
//                        view.removeTextChangedListener(phonePattern);
//                        original = "1-";
//                        view.setText(original);
//                        view.setSelection(2);
//                        view.addTextChangedListener(phonePattern);
//                        return;
//                    }
//                } else {
//                    String text = view.getText().toString();
//                    if (text.length() == 2) {
//                        original = "";
//                        view.removeTextChangedListener(phonePattern);
//                        view.setText(original);
//                        view.addTextChangedListener(phonePattern);
//                        return;
//                    }
//                }
//            }
            if (hasFocus) {
                if (firstNameMask) {
                    firstNameMask = false;
                    return;
                }
                view.setText(original);
                if (isEdtMode) {
                    edtEmail.setText(MaskerHelper.mask(MaskerHelper.INPUT.EMAIL, emailMasker.original));
                }

            } else {
                original = view.getText().toString().trim();
                view.setText(MaskerHelper.mask(input, original));
            }
            view.setCursorVisible(hasFocus);

        }
    }

    private void storeUp() {
        firstNameMasker.original = firstNameMasker.original.trim();
        lastNameMasker.original = lastNameMasker.original.trim();
        emailMasker.original = emailMasker.original.trim();
        phoneMasker.original = phoneMasker.original.trim();
        zipCodeMasker.original = zipCodeMasker.original.trim();

        String firstName = firstNameMasker.original;
        String lastName = lastNameMasker.original;
        String email = emailMasker.original;
        String phone = phoneMasker.original;

        profile.setFirstName(firstName);
        profile.setLastName(lastName);
        profile.setPhone(phone);
        profile.setEmail(email);
        profile.setSalutation(tvSelectSalutation.getText().toString().equalsIgnoreCase(getString(R.string.my_profile_text_salutation_place_holder)) ? "" : tvSelectSalutation.getText().toString());
        profile.setCountryCode(tvCountryCode.getText().toString().equalsIgnoreCase(getString(R.string.hint_country_code)) ? "" : tvCountryCode.getText().toString());
        profile.setZipCode(edtZipCode.getText().toString().trim());
        profile.setFirstSixDigits(edtFirstSixDigits.getText().toString());

    }

    private int checkHasFocus() {
        if (edtFirstName.hasFocus()) {
            profile.setFirstName(edtFirstName.getText().toString().trim());
            return 1;
        }
        if (edtLastName.hasFocus()) {
            profile.setLastName(edtLastName.getText().toString().trim());
            return 2;
        }
        if (edtEmail.hasFocus()) {
            profile.setEmail(edtEmail.getText().toString().trim());
            return 3;
        }
        if (edtPhone.hasFocus()) {
            profile.setPhone(edtPhone.getText().toString().trim());
            return 4;
        }
        profile.setZipCode(edtZipCode.getText().toString().trim());
        // zip code
        return 5;
    }

    void reloadProfileAfterUpdated(){
        loadProfile(profile);
    }

    void loadProfile(Profile profile) {
        firstNameMasker.original = profile.getFirstName();
        lastNameMasker.original = profile.getLastName();
        phoneMasker.original = profile.getPhone();
        emailMasker.original = profile.getEmail();
        zipCodeMasker.original = profile.getZipCode();
        edtFirstName.setText(profile.getFirstName());
        edtLastName.setText(profile.getLastName());
        edtEmail.setText(profile.getEmail());
//        edtPhone.setText(profile.getPhone());
        edtZipCode.setText(profile.getZipCode());

//        edtFirstSixDigits.setText(profile.getFirstSixDigits());
        /*edtFirstName.getOnFocusChangeListener().onFocusChange(edtFirstName, hasFocus == 1);
        edtLastName.getOnFocusChangeListener().onFocusChange(edtLastName, hasFocus == 2);
        edtEmail.getOnFocusChangeListener().onFocusChange(edtEmail, hasFocus == 3);
        edtPhone.getOnFocusChangeListener().onFocusChange(edtPhone, hasFocus == 4);*/
        String phone = profile.getPhone();
        if (phone.contains("-")) {
            String nPhone = phone.replace("-", "");
            edtPhone.setText(nPhone);
        } else {
            edtPhone.setText(phone);
        }

        if (profileFromUpdate) {
            edtFirstName.getOnFocusChangeListener().onFocusChange(edtFirstName, false);
            edtLastName.getOnFocusChangeListener().onFocusChange(edtLastName, false);
            edtPhone.getOnFocusChangeListener().onFocusChange(edtPhone, false);
            edtEmail.getOnFocusChangeListener().onFocusChange(edtEmail, false);
        } else {
            edtFirstName.getOnFocusChangeListener().onFocusChange(edtFirstName, hasFocus == 1);
            edtLastName.getOnFocusChangeListener().onFocusChange(edtLastName, hasFocus == 2);
            edtPhone.getOnFocusChangeListener().onFocusChange(edtPhone, hasFocus == 4);
            edtEmail.getOnFocusChangeListener().onFocusChange(edtEmail, hasFocus == 3);
        }


        if (profile.getSalutation() != null && !profile.getSalutation().equals("")) {
            if (getSalutation().contains(profile.getSalutation())) {
                mSpinnerSalutation.setSelectionPosition(getSalutation().indexOf(profile.getSalutation()));
            }
        }
        int countryCodeIndex = CountryCode.indexOf(profile.getCountryCode(), profile.getZipCode());
        if (countryCodeIndex > -1) {
            tvCountryCode.setText("");
            mCountryCode.setSelectionPosition(CountryCode.indexOf(profile.getCountryCode(), profile.getZipCode()));
        }


        profileComparator = new ProfileComparator(edtFirstName,
                edtLastName,
                edtEmail,
                edtPhone,
                btnSubmit,
                /*btnCancel,
                locationSwitchCompat,*/
                mSpinnerSalutation,
                mCountryCode,
                edtZipCode,
                edtFirstSixDigits,
                btnSave
        );
        profileComparator.original = profile;
        //locationSwitchCompat.setChecked(!profile.isLocationOn());

        resetErrorView();
        btnSave.setEnabled(false);
        btnSubmit.setEnabled(false);
        btnCancel.setEnabled(false);
        btnSave.setClickable(false);
        btnSubmit.setClickable(false);
        btnCancel.setClickable(false);
    }

    private String getCharZipCode(String zipCode) {
        String s = zipCode.substring(0, 3);
        return s;
    }

    void invokeSoftKey() {
        if (editText != null) {
            editText.requestFocus();
            editText.setError("");
            editText.setCursorVisible(true);
            editText.setSelection(editText.getText().toString().length());
            ViewUtils.invoSoftKey(editText);
        } else {
            ViewUtils.invoSoftKey(edtFirstName);
        }
    }



//    void switchOff() {
//        locationSwitchCompat.setChecked(true);
//        profile.locationToggle(false);
//        if (which == PROFILE.EDIT) {
//            profileComparator.onChanged();
//        }
//    }

    public interface ProfileEventsListener {
        void onProfileSubmit(Profile profile, String password);

        void onProfileInputError(String message);

        void onProfileCancel();

        void onFragmentCreated();

        void onLocationSwitchOn();
    }

    private void resetErrorView() {
        edtFirstName.setError(null);
        edtLastName.setError(null);
        edtEmail.setError(null);
        edtPhone.setError(null);
        edtPassword.setError(null);
        edtZipCode.setError(null);
        edtFirstSixDigits.setError(null);
    }

    public void showHighlightFieldEmail(){
        if(edtEmail != null){
            edtEmail.requestFocus();
            edtEmail.setError("");
            edtEmail.setCursorVisible(true);
            edtEmail.setSelection(edtEmail.getText().toString().length());
            ViewUtils.invoSoftKey(edtEmail);
        }
    }

    private void setupTermOfUseClickable() {

        String s= getString(R.string.acknowledge_sign_up);
        String termAndCondition = getString(R.string.term_of_use);
        int termStart = s.indexOf(termAndCondition);
        int termEnd = termStart + termAndCondition.length();
        String privacyPolicy = getString(R.string.privacy);
        int privacyStart = s.indexOf(privacyPolicy);
        int privacyEnd = privacyStart + privacyPolicy.length();


        SpannableString ss = new SpannableString(s);
        ClickableSpan span1 = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                openWebView(AppConstant.MASTERCARD_COPY_UTILITY.TermsOfUse);
            }
            @Override
            public void updateDrawState(final TextPaint textPaint) {
                textPaint.setColor(getResources().getColor(R.color.white));
                textPaint.setUnderlineText(true);
            }
        };

        ClickableSpan span2 = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                openWebView(AppConstant.MASTERCARD_COPY_UTILITY.Privacy);
            }
            @Override
            public void updateDrawState(final TextPaint textPaint) {
                textPaint.setColor(getResources().getColor(R.color.white));
                textPaint.setUnderlineText(true);
            }
        };

        ss.setSpan(span1, termStart, termEnd, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(span2, privacyStart, privacyEnd, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        tvAcknow.setText(ss);
        tvAcknow.setMovementMethod(LinkMovementMethod.getInstance());
    }

    void openWebView(AppConstant.MASTERCARD_COPY_UTILITY utilityType) {
        Intent intent = new Intent(this.getActivity(), MasterCardUtilityActivity.class);
        intent.putExtra(IntentConstant.MASTERCARD_COPY_UTILITY, utilityType);
        this.getActivity().startActivity(intent);
    }

}
