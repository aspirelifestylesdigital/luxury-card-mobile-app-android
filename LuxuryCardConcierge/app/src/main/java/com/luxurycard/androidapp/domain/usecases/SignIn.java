package com.luxurycard.androidapp.domain.usecases;

import com.luxurycard.androidapp.common.BackendException;
import com.luxurycard.androidapp.datalayer.entity.oauth.SignInRequest;
import com.luxurycard.androidapp.datalayer.entity.oauth.SignInResponse;
import com.luxurycard.androidapp.datalayer.preference.PreferencesStorage;
import com.luxurycard.androidapp.datalayer.retro2client.AppHttpClient;
import com.luxurycard.androidapp.domain.model.Metadata;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by vinh.trinh on 6/20/2017.
 */

public class SignIn extends UseCase<String, SignIn.Params> {

    private PreferencesStorage preferencesStorage;

    public SignIn(PreferencesStorage pref) {
        this.preferencesStorage = pref;
    }

    @Override
    Observable<String> buildUseCaseObservable(Params aVoid) {
        return null;
    }

    @Override
    public Single<String> buildUseCaseSingle(Params params) {
        return authentication(params.email, params.password)
                .flatMap(onlineMemberID -> {
                    Metadata metadata = new Metadata(onlineMemberID, "");
                    preferencesStorage.editor().metadata(metadata).build().save();
                    GetAccessToken getAccessToken = new GetAccessToken(preferencesStorage);
                    return getAccessToken.execute();
                });
    }

    /**
     * @return online member ID
     */
    private Single<String> authentication(String email, String password) {
        return Single.create(e -> {
            SignInRequest body = new SignInRequest(email, password);
            Call<ResponseBody> request = AppHttpClient.getInstance().userManagementApi().signIn(body);
            Response<ResponseBody> response = request.execute();
            if(response.isSuccessful()) {
                SignInResponse data = new SignInResponse(response.body().string());
                if(data.success) {
                    preferencesStorage.editor().hasForgotPwd(data.hasForgotPassword).build().save();
                    e.onSuccess(data.onlineMemberID);
                } else {
                    e.onError(new BackendException(data.message));
                }
            } else {
                e.onError(new Exception(response.errorBody().string()));
            }
        });
    }

    @Override
    Completable buildUseCaseCompletable(Params aVoid) {
        return null;
    }

    public static class Params {
        final String email;
        final String password;

        public Params(String email, String password) {
            this.email = email;
            this.password = password;
        }
    }
}
