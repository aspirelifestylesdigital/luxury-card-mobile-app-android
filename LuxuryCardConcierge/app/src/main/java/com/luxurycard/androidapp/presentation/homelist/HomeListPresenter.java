package com.luxurycard.androidapp.presentation.homelist;

import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;

import com.luxurycard.androidapp.App;
import com.luxurycard.androidapp.R;
import com.luxurycard.androidapp.common.constant.AppConstant;
import com.luxurycard.androidapp.common.constant.CityData;
import com.luxurycard.androidapp.common.logic.TimeFormats;
import com.luxurycard.androidapp.domain.model.HomeViewItem;
import com.luxurycard.androidapp.domain.model.Profile;
import com.luxurycard.androidapp.domain.usecases.GetHomeList;
import com.luxurycard.androidapp.domain.usecases.LoadProfile;
import com.luxurycard.androidapp.presentation.explore.ExploreFragment;
import com.luxurycard.androidapp.presentation.home.HomeNavBottomActivity;
import com.luxurycard.androidapp.presentation.homelist.dto.HomeContent;
import com.luxurycard.androidapp.presentation.infoweb.InfoWebFragment;
import com.luxurycard.androidapp.presentation.selectcity.SelectCityFragment;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class HomeListPresenter implements HomeList.Presenter {

    private HomeList.View view;
    private LoadProfile loadProfile;
    private GetHomeList homeList;

    private String mNameAccountWelcome = "";

    HomeListPresenter(LoadProfile loadProfile, GetHomeList homeList) {
        this.loadProfile = loadProfile;
        this.homeList = homeList;
    }

    @Override
    public void attach(HomeList.View view) {
        this.view = view;
    }

    @Override
    public void detach() {
        this.view = null;
    }

    @Override
    public void setUpDate() {
        String dateCurrent = "";
        try {
            dateCurrent = TimeFormats.timeCurrentOfUser();
        } catch (Exception e) {
            e.printStackTrace();
        }
        view.showDate(dateCurrent);
    }


    //<editor-fold desc="Profile">
    void resetProfileLocal(boolean isHomeRoot){
        mNameAccountWelcome = "";
        if (isHomeRoot) {
            getProfileLocal();
        }
    }

    void getProfileLocal() {
        if(TextUtils.isEmpty(mNameAccountWelcome)){
            loadProfile.buildUseCaseSingle(null)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(new GetProfileLocalObserver());
        }else {
            if (view != null) {
                view.showNameAccount(mNameAccountWelcome);
            }
        }
    }

    private final class GetProfileLocalObserver extends DisposableSingleObserver<Profile> {

        private String welcomeDefault = App.getInstance().getString(R.string.home_welcome);
        private String welcomeFormat = App.getInstance().getString(R.string.home_welcome_value);

        @Override
        public void onSuccess(Profile profile) {
            String result = welcomeDefault;
            if(!TextUtils.isEmpty(profile.getFirstName())){
                result = String.format(welcomeFormat, profile.getFirstName());
            }
            if(view != null){
                mNameAccountWelcome = result;
                view.showNameAccount(result);
            }
            dispose();
        }

        @Override
        public void onError(Throwable e) {
            if (view != null) {
                mNameAccountWelcome = welcomeDefault;
                view.showNameAccount(welcomeDefault);
                view.showGeneralErrorDialog();
            }
            dispose();
        }
    }
    //</editor-fold>

    //<editor-fold desc="Select City">
    @Override
    public void setSelectedCity(String cityName) {
        final String cityDefault = App.getInstance().getString(R.string.home_choose_a_destination);
        String resultCity = cityDefault;
        if(!TextUtils.isEmpty(cityName) && !cityName.equalsIgnoreCase(CityData.DEFAULT_CITY)){
            resultCity = cityName;
        }//else do nothing here

        int idImgCloseCity = cityDefault.equalsIgnoreCase(resultCity) ?
                R.drawable.ic_home_arrow_right : R.drawable.ic_home_close;

        if (view != null) {
            view.setTextButtonCity(resultCity, idImgCloseCity);
        }
    }
    //</editor-fold>

    //<editor-fold desc="GetHomeList">

    @Override
    public List<HomeViewItem> getHomeViewItemDefault() {
        String cityName;
        if(CityData.citySelected()){
            cityName = CityData.cityName();
        }else{
            cityName = CityData.DEFAULT_CITY;
        }
        loadHomeList(cityName);
        return new ArrayList<>();
    }

    @Override
    public void loadHomeList(String cityName) {
        if(!hasNetworkConnection()){
            view.noNetwork();
            return;
        }
        view.showLoadingPage();
        setSelectedCity(cityName);
        homeList.buildUseCaseSingle(new GetHomeList.Params(cityName))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new GetHomeListObserver());
    }

    private final class GetHomeListObserver extends DisposableSingleObserver<List<HomeViewItem>> {

        @Override
        public void onSuccess(List<HomeViewItem> homeListItem) {
            if(view != null){
                view.showListCity(homeListItem);
                view.hideLoadingPage();
            }
            dispose();
        }

        @Override
        public void onError(Throwable e) {
            if (view != null) {
                view.hideLoadingPage();
                view.showGeneralErrorDialog();
            }
            dispose();
        }
    }

    private boolean hasNetworkConnection(){
        return App.getInstance().hasNetworkConnection();
    }
    //</editor-fold>

    @Override
    public void onHomeItemClick(HomeViewItem homeViewItem, FragmentActivity activity) {
        if(activity instanceof HomeNavBottomActivity){
            HomeNavBottomActivity homeNavBottomActivity = (HomeNavBottomActivity) activity;

            String typeItem = homeViewItem.typeItem;
            if(HomeContent.TYPE_LUXURY_MAGAZINE.equalsIgnoreCase(typeItem)){

                homeNavBottomActivity.openWebViewTabCurrent(InfoWebFragment.newInstance(AppConstant.MASTERCARD_COPY_UTILITY.Luxury));

            }else if(HomeContent.TYPE_HOTEL.equalsIgnoreCase(typeItem)){

                homeNavBottomActivity.openWebViewTabCurrent(InfoWebFragment.newInstance(AppConstant.MASTERCARD_COPY_UTILITY.Hotel));

            }else if(HomeContent.TYPE_NORMAL.equalsIgnoreCase(typeItem)){
                //-- user don't choose city
                toExploreFragment(activity,"");

            }else if(HomeContent.TYPE_API.equalsIgnoreCase(typeItem)){

                String typeCategory = homeViewItem.linkCategory;
                if (TextUtils.isEmpty(typeCategory)) {
                    //-- all category explore
                    toExploreFragment(activity, "");
                }else if(typeCategory.contains("http://") || typeCategory.contains("https://")){

                    homeNavBottomActivity.openWebViewTabCurrent(InfoWebFragment.newInstance(homeViewItem.title, typeCategory));

                }else{
                    //filter follow category explore
                    toExploreFragment(activity, typeCategory);
                }

            }//-- else do nothing here wrong type)
        }
    }

    @Override
    public void toExploreFragment(FragmentActivity activity, String tyCategory) {
        if (activity instanceof HomeNavBottomActivity) {
            ((HomeNavBottomActivity) activity).toExploreFragment(tyCategory);
        }
    }

}
