package com.luxurycard.androidapp.domain.usecases;

import com.luxurycard.androidapp.domain.model.Profile;
import com.luxurycard.androidapp.domain.repository.ProfileRepository;

import java.util.regex.Pattern;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;

/**
 * Created by tung.phan on 5/18/2017.
 */

public class SaveProfile extends UseCase<Void, SaveProfile.Params> {

    private ProfileRepository profileRepository;

    public SaveProfile(ProfileRepository profileRepository) {
        super();
        this.profileRepository = profileRepository;
    }

    @Override
    Observable<Void> buildUseCaseObservable(Params params) {
        return null;
    }

    @Override
    Single<Void> buildUseCaseSingle(Params params) {
        return null;
    }

    @Override
    public Completable buildUseCaseCompletable(Params params) {
        params.profile.setCountryCode(getCountryName(params.profile.getCountryCode()));
        return profileRepository.saveProfile(params.profile, params.password);
    }

    private String getCountryName(String countryCode) {
        int last = countryCode.lastIndexOf(')');
        int first = countryCode.lastIndexOf('(');
        return countryCode.substring(first+1, last);
    }

    public static class Params {
        private final Profile profile;
        private final String password;

        public Params(Profile profile, String password) {
            this.profile = profile;
            this.password = password;
        }
    }
}
