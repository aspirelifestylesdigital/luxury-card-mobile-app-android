package com.luxurycard.androidapp.presentation.selectcity;

import com.luxurycard.androidapp.domain.model.CityRViewItem;
import com.luxurycard.androidapp.presentation.base.BasePresenter;

/**
 * Created by tung.phan on 5/8/2017.
 */

public interface SelectCity {
    interface View {
        void showViewNoNetwork();

        void showViewLoading();

        void hideViewLoading();

        void saveCitySuccessful(CityRViewItem cityItem);

        void showViewGeneralError();
    }

    interface Presenter extends BasePresenter<SelectCity.View> {
        void saveSelectCityServer(CityRViewItem cityName);
    }
}
