package com.luxurycard.androidapp.presentation.base;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;

import com.luxurycard.androidapp.R;

import butterknife.BindView;
import butterknife.OnClick;

public class BaseNavSubFragment extends BaseNavFragment {

    @BindView(R.id.title)
    protected AppCompatTextView tvTitle;

    @BindView(android.R.id.home)
    public AppCompatImageButton btnBack;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.toolbar_btn_call)
    public FrameLayout btnCallConcierge;


    @Override
    public int setupCreateViewLayoutId() {
        return 0;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    public void setTitle(String title) {
        this.tvTitle.setText(title);
    }

    public void setTitle(int titleId) {
        this.tvTitle.setText(getString(titleId));
    }

    public AppCompatImageButton getButtonBack() {
        return btnBack;
    }

    public Toolbar getToolbar() {
        return toolbar;
    }

    @OnClick(R.id.toolbar_btn_call)
    void onToolbarBtnCall(View view) {
        callConcierge();
    }

    public FrameLayout getBtnCallConcierge() {
        return btnCallConcierge;
    }

    public void onBackPressed() {
        if (getActivity() != null) {
            getActivity().onBackPressed();
        }
    }
}
