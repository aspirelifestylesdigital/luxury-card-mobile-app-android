package com.luxurycard.androidapp.presentation.selectcategory;

import android.content.Context;
import android.content.res.Resources;

import com.luxurycard.androidapp.R;
import com.luxurycard.androidapp.common.GPSChecker;
import com.luxurycard.androidapp.common.constant.CityData;
import com.luxurycard.androidapp.domain.model.Profile;
import com.luxurycard.androidapp.domain.usecases.LoadProfile;
import com.luxurycard.androidapp.presentation.selectcategory.adapter.CategoryTextAdapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by tung.phan on 5/31/2017.
 */

public class CategoryPresenter implements Category.Presenter {
    private CompositeDisposable disposables;
    private Category.View view;
    private LoadProfile loadProfile;
    private Profile profile;

    CategoryPresenter(LoadProfile loadProfile) {
        this.loadProfile = loadProfile;
    }

    @Override
    public void attach(Category.View view) {
        this.view = view;
        disposables = new CompositeDisposable();
    }

    @Override
    public void detach() {
        disposables.dispose();
        this.view = null;
    }

    @Override
    public void loadProfile() {
        loadProfile.param(null)
                .on(Schedulers.io(), AndroidSchedulers.mainThread())
                .execute(new DisposableSingleObserver<Profile>() {
                    @Override
                    public void onSuccess(Profile profile) {
                        CategoryPresenter.this.profile = profile;
                    }

                    @Override
                    public void onError(Throwable e) {}
                });
    }

    @Override
    public boolean isLocationProfileSettingOn() {
        return this.profile.isLocationOn();
    }

    @Override
    public void locationServiceCheck(Context context) {
        //--remove location
        /*if(GPSChecker.GPSEnable(context)) {
            view.proceedWithDiningCategory();
        } else {
            view.askForLocationSetting();
        }*/
    }

    public List<String> getListCategory(Resources resources) {
        ArrayList<String> arrCategory = new ArrayList<>(Arrays.asList(resources.getStringArray(R.array.category_texts)));
        String cityGuide = resources.getString(R.string.city_guide);
        if (!isCityGuideSupported()) {
            arrCategory.remove(cityGuide);
        }
        return arrCategory;
    }

    /**
     * @return true if currentSelectedCity is one of the city in city_guide_list
     */
    private boolean isCityGuideSupported() {
        return CityData.guideCode() > 0;
    }
}
