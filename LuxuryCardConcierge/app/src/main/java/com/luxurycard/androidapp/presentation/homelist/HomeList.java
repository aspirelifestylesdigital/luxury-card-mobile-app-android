package com.luxurycard.androidapp.presentation.homelist;

import android.support.v4.app.FragmentActivity;

import com.luxurycard.androidapp.domain.model.HomeViewItem;
import com.luxurycard.androidapp.presentation.base.BasePresenter;

import java.util.List;

public interface HomeList {

    interface View {

        void showGeneralErrorDialog();

        void showDate(String date);

        void showNameAccount(String nameAccount);

        void setTextButtonCity(String cityName, int idImgCloseCity);

        void showListCity(List<HomeViewItem> homeListItem);

        void noNetwork();

        void showLoadingPage();

        void hideLoadingPage();

    }

    interface Presenter extends BasePresenter<View> {
        void setUpDate();
        void setSelectedCity(String cityName);
        List<HomeViewItem> getHomeViewItemDefault();
        void loadHomeList(String cityName);

        void onHomeItemClick(HomeViewItem homeViewItem, FragmentActivity activity);

        void toExploreFragment(FragmentActivity activity,String category);
    }
}
