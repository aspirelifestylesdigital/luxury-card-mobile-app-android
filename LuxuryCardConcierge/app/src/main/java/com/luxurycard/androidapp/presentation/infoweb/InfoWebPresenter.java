package com.luxurycard.androidapp.presentation.infoweb;

import android.text.TextUtils;

import com.luxurycard.androidapp.App;
import com.luxurycard.androidapp.R;
import com.luxurycard.androidapp.common.constant.AppConstant;

import io.reactivex.disposables.CompositeDisposable;

public class InfoWebPresenter implements InfoWeb.Presenter {

    private InfoWeb.View view;
    private CompositeDisposable disposables;

    public InfoWebPresenter() {
        disposables = new CompositeDisposable();
    }

    @Override
    public void attach(InfoWeb.View view) {
        this.view = view;
    }

    @Override
    public void detach() {
        disposables.dispose();
        this.view = null;
    }


    @Override
    public String getTitleOfType(AppConstant.MASTERCARD_COPY_UTILITY type) {
        if (type == null) {
            return "";
        }
        String result;
        switch (type) {
            case Rewards:
                result = App.getInstance().getString(R.string.more_rewards);
                break;
            case Airport:
                result = App.getInstance().getString(R.string.more_airport);
                break;
            case Hotel:
                result = App.getInstance().getString(R.string.more_hotel);
                break;
            case Luxury:
                result = App.getInstance().getString(R.string.more_luxury_magazine).toUpperCase();
                break;
            case Account:
                result = App.getInstance().getString(R.string.info_account_title);
                break;
            default:
                result = "";
                break;
        }
        return result;
    }

    public AppConstant.GA_TRACKING_SCREEN_NAME getTypeTrack(AppConstant.MASTERCARD_COPY_UTILITY type) {
        if (type == null) {
            return null;
        }
        AppConstant.GA_TRACKING_SCREEN_NAME result;
        switch (type) {
            /*case Rewards: //-- don't have this case
                result = null*/
            case Airport:
                result = AppConstant.GA_TRACKING_SCREEN_NAME.AIRPORT_LOUNGE_ACCESS;
                break;
            case Hotel:
                result = AppConstant.GA_TRACKING_SCREEN_NAME.HOTEL_TRAVEL_MAGAZINE;
                break;
            case Luxury:
                result = AppConstant.GA_TRACKING_SCREEN_NAME.LUXURY_MAGAZINE;
                break;
            case Account:
                result = AppConstant.GA_TRACKING_SCREEN_NAME.ACCOUNT_MANAGEMENT;
                break;
            default:
                result = null;
                break;
        }
        return result;
    }

    public AppConstant.GA_TRACKING_SCREEN_NAME getTypeTrack(String titleData) {
        if (TextUtils.isEmpty(titleData)) {
            return null;
        }
        String title = titleData.toLowerCase();
        AppConstant.GA_TRACKING_SCREEN_NAME result = null;
        if(title.contains(AppConstant.MASTERCARD_COPY_UTILITY.Luxury.getValue().toLowerCase())){

            result = AppConstant.GA_TRACKING_SCREEN_NAME.LUXURY_MAGAZINE;

        }else if(title.contains(AppConstant.MASTERCARD_COPY_UTILITY.Hotel.getValue().toLowerCase())){

            result = AppConstant.GA_TRACKING_SCREEN_NAME.HOTEL_TRAVEL_MAGAZINE;

        }else if(title.contains(AppConstant.MASTERCARD_COPY_UTILITY.Airport.getValue().toLowerCase())){

            result = AppConstant.GA_TRACKING_SCREEN_NAME.AIRPORT_LOUNGE_ACCESS;

        }
        return result;
    }

    @Override
    public String getLinkUrl(AppConstant.MASTERCARD_COPY_UTILITY type) {
        if (type == null) {
            return "";
        }
        String result;
        switch (type) {
            case Rewards:
                result = "";
                break;
            case Airport:
                result = "https://www.prioritypass.com";
                break;
            case Hotel:
                result = "http://www.luxurymagazine.com/vipbook";
                break;
            case Luxury:
                result = "http://www.luxurymagazine.com";
                break;
            case Account:
                result = "https://www.myluxurycard.com";
                break;
            default:
                result = "";
                break;
        }
        return result;
    }
}
