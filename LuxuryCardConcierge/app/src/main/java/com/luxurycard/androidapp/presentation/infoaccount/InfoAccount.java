package com.luxurycard.androidapp.presentation.infoaccount;

import com.luxurycard.androidapp.presentation.base.BasePresenter;

public interface InfoAccount {

    interface View {

    }

    interface Presenter extends BasePresenter<View> {

    }
}
