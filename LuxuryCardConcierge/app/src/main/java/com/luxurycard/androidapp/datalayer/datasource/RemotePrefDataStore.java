package com.luxurycard.androidapp.datalayer.datasource;


import android.text.TextUtils;

import com.luxurycard.androidapp.common.BackendException;
import com.luxurycard.androidapp.datalayer.entity.preferences.GetPreferencesRequest;
import com.luxurycard.androidapp.datalayer.entity.preferences.PreferenceDetail;
import com.luxurycard.androidapp.datalayer.entity.preferences.PreferenceMember;
import com.luxurycard.androidapp.datalayer.entity.preferences.PreferenceValue;
import com.luxurycard.androidapp.datalayer.entity.preferences.UpdatePreferencesRequest;
import com.luxurycard.androidapp.datalayer.entity.preferences.UpdatePreferencesResponse;
import com.luxurycard.androidapp.datalayer.preference.PreferencesStorage;
import com.luxurycard.androidapp.datalayer.restapi.UserManagementApi;
import com.luxurycard.androidapp.datalayer.retro2client.AppHttpClient;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.Response;

public class RemotePrefDataStore {

    private PreferencesStorage preferencesStorage;

    public RemotePrefDataStore(PreferencesStorage preferencesStorage) {
        this.preferencesStorage = preferencesStorage;
    }

    public Single<PreferenceData> loadPreferences(String accessToken) {
        return Single.create(e -> {
            GetPreferencesRequest requestBody = new GetPreferencesRequest(accessToken, onlineMemberID());
            UserManagementApi userManagementApi = AppHttpClient.getInstance().userManagementApi();
            Call<List<PreferenceValue>> request = userManagementApi.getPreferences(requestBody);

            Response<List<PreferenceValue>> response = request.execute();
            if(response.isSuccessful()) {
                List<PreferenceValue> responseBody = response.body();
                String naValue = PreferenceData.NA_VALUE;
                String cuisine = naValue, hotel = naValue, vehicle = naValue;
                String cuisinePrefID = null, hotelPrefID = null, vehiclePrefID = null;
                String locationOn = null;
                String citySelected = null;
                for (PreferenceValue val : responseBody) {
                    switch (val.type()) {
                        case "Dining":
                            cuisine = val.value();
                            cuisinePrefID = val.myPreferencesID();
                            break;
                        case "Hotel":
                            hotel = val.value();
                            hotelPrefID = val.myPreferencesID();
                            locationOn = val.extraValue();
                            citySelected = val.cityNameSelected();
                            break;
                        case "Car Rental":
                            vehicle = val.value();
                            vehiclePrefID = val.myPreferencesID();
                            break;
                    }
                }
                PreferenceData preferenceData = new PreferenceData(cuisine, hotel, vehicle);
                preferenceData.preferenceIDs(cuisinePrefID, hotelPrefID, vehiclePrefID);
                preferenceData.extValue = locationOn;
                preferenceData.selectCity = citySelected;
                e.onSuccess(preferenceData);
            } else {
                e.onError(new Exception(response.errorBody().string()));
            }
        });
    }

    public Completable updatePreferences(PreferenceData preferenceData, String accessToken) {
        return Completable.create(e -> {
            PreferenceMember preferenceMember = new PreferenceMember(accessToken, onlineMemberID());
            List<PreferenceDetail> body = new ArrayList<>();
            PreferenceDetail hotel = new PreferenceDetail(PreferenceDetail.TYPE.HOTEL,
                    preferenceData.hotel, preferenceData.hotelPrefID, preferenceData.extValue, preferenceData.selectCity);
            body.add(hotel);
            if(!TextUtils.isEmpty(preferenceData.vehiclePrefID)) {
                PreferenceDetail transportation = new PreferenceDetail(PreferenceDetail.TYPE.TRANSPORTATION,
                        preferenceData.vehicle, preferenceData.vehiclePrefID);
                body.add(transportation);
            }
            if(!TextUtils.isEmpty(preferenceData.cuisinePrefID)) {
                PreferenceDetail cuisine = new PreferenceDetail(PreferenceDetail.TYPE.CUISINE,
                        preferenceData.cuisine, preferenceData.cuisinePrefID);
                body.add(cuisine);
            }

            UpdatePreferencesRequest requestBody = new UpdatePreferencesRequest(preferenceMember,
                    body.toArray(new PreferenceDetail[body.size()]));
            UserManagementApi userManagementApi = AppHttpClient.getInstance().userManagementApi();
            Call<UpdatePreferencesResponse> request = userManagementApi.updatePreferences(requestBody);
            Response<UpdatePreferencesResponse> response = request.execute();
            if(response.isSuccessful()) {
                UpdatePreferencesResponse responseBody = response.body();
                if(responseBody.isSuccess()) e.onComplete();
                else e.onError(new BackendException(responseBody.message()));
            } else {
                e.onError(new Exception(response.errorBody().string()));
            }
        });
    }

    public Completable createPreferences(PreferenceData preferenceData, String accessToken) {
        return Completable.create(e -> {
            PreferenceMember preferenceMember = new PreferenceMember(accessToken, onlineMemberID());
            List<PreferenceDetail> body = new ArrayList<>();
            if(TextUtils.isEmpty(preferenceData.hotelPrefID)) {
                PreferenceDetail hotel = new PreferenceDetail(PreferenceDetail.TYPE.HOTEL,
                        preferenceData.hotel, preferenceData.hotelPrefID, preferenceData.extValue, preferenceData.selectCity);
                body.add(hotel);
            }
            if(!PreferenceData.NA_VALUE.equals(preferenceData.vehicle)) {
                PreferenceDetail transportation = new PreferenceDetail(PreferenceDetail.TYPE.TRANSPORTATION,
                        preferenceData.vehicle, null);
                body.add(transportation);
            }
            if(!PreferenceData.NA_VALUE.equals(preferenceData.cuisine)) {
                PreferenceDetail cuisine = new PreferenceDetail(PreferenceDetail.TYPE.CUISINE,
                        preferenceData.cuisine, null);
                body.add(cuisine);
            }

            UpdatePreferencesRequest requestBody = new UpdatePreferencesRequest(preferenceMember,
                    body.toArray(new PreferenceDetail[body.size()]));
            UserManagementApi userManagementApi = AppHttpClient.getInstance().userManagementApi();
            Call<UpdatePreferencesResponse> request = userManagementApi.createPreferences(requestBody);
            Response<UpdatePreferencesResponse> response = request.execute();
            if(response.isSuccessful()) {
                UpdatePreferencesResponse responseBody = response.body();
                if(responseBody.isSuccess()) e.onComplete();
                else e.onError(new BackendException(responseBody.message()));
            } else {
                e.onError(new Exception(response.errorBody().string()));
            }
        });
    }

    private String onlineMemberID() throws JSONException {
        return preferencesStorage.metadata().onlineMemberID;
    }
}
