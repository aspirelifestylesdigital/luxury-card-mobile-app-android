package com.luxurycard.androidapp.presentation.base;

import android.content.Intent;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.LayoutRes;
import android.support.design.widget.AppBarLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.luxurycard.androidapp.R;
import com.luxurycard.androidapp.presentation.request.AskConciergeActivity;
import com.luxurycard.androidapp.presentation.widget.ViewUtils;
import com.support.mylibrary.widget.ClickGuard;
import com.support.mylibrary.widget.SwipeBackLayout;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;

/**
 * Created by vinh.trinh on 4/26/2017.
 * This activity using for all child activity with back button on toolbar.
 * Support back button navigate, swipe right to close.
 */

public abstract class CommonActivity extends BaseActivity implements SwipeBackLayout.SwipeBackListener {

    static {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        }
    }

    private final SwipeBackLayout.DragEdge DEFAULT_DRAG_EDGE = SwipeBackLayout.DragEdge.LEFT;
    @BindView(R.id.appbar)
    protected AppBarLayout appBar;
    @BindView(R.id.toolbar)
    protected Toolbar toolbar;
    @BindView(R.id.title)
    protected AppCompatTextView title;
    @BindView(android.R.id.home)
    protected AppCompatImageButton back;
    //Using this field to create swipe right to close child activity
    private SwipeBackLayout swipeBackLayout;
    //Shadow view when swipe right activity
    private ImageView ivShadow;
    protected boolean isSwipeBack = true;
    protected Rect homeUpRect;

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        if(isSwipeBack) {
            super.setContentView(getContainer());
            View view = LayoutInflater.from(this).inflate(layoutResID, null);
            swipeBackLayout.addView(view);
        }else{
            super.setContentView(layoutResID);
        }
        setup();
    }

    private View getContainer() {
        RelativeLayout container = new RelativeLayout(this);
        swipeBackLayout = new SwipeBackLayout(this);
        swipeBackLayout.setDragEdge(DEFAULT_DRAG_EDGE);
        swipeBackLayout.setOnSwipeBackListener(this);

//        swipeBackLayout.setEnableFlingBack(isSwipeBack);
//        swipeBackLayout.setEnablePullToBack(isSwipeBack);
        ivShadow = new ImageView(this);
        ivShadow.setBackgroundColor(getResources().getColor(R.color.shadow_black));
        RelativeLayout.LayoutParams params =
                new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT
                        , RelativeLayout.LayoutParams.MATCH_PARENT);
        container.addView(ivShadow, params);
        container.addView(swipeBackLayout);
        return container;
    }

    /**
     * enable swipe right to close activity
     */
    public void enableSwipe() {
        swipeBackLayout.setEnablePullToBack(true);
    }

    /**
     * disable swipe right to close activity
     */
    public void disableSwipe() {
        swipeBackLayout.setEnablePullToBack(false);
    }

    /**
     * set drag edge: LEFT, TOP, RIGHT, BOTTOM
     */
    public void setDragEdge(SwipeBackLayout.DragEdge dragEdge) {
        swipeBackLayout.setDragEdge(dragEdge);
    }

    @Override
    public void setContentView(View view) {
        super.setContentView(view);
        setup();
    }

    @Override
    public void setContentView(View view, ViewGroup.LayoutParams params) {
        super.setContentView(view, params);
        setup();
    }

    void setup() {
        ButterKnife.bind(this);
        if(toolbar == null) toolbar = findViewById(R.id.toolbar);
        if(appBar == null) appBar = findViewById(R.id.appbar);
        if(title == null) title = findViewById(R.id.title);
        if(back == null) back = findViewById(android.R.id.home);
        toolbar.setContentInsetsAbsolute(0, 0);
        toolbar.setContentInsetStartWithNavigation(0);
        toolbar.setContentInsetEndWithActions(0);

        back.setOnClickListener(view -> onBackPressed());

        setSupportActionBar(toolbar);
//        Drawable icBack = ContextCompat.getDrawable(this, R.drawable.ic_arrow_left);
//        ViewUtils.menuTintColors(this, icBack);
//        getSupportActionBar().setHomeAsUpIndicator(icBack);
        toolbar.addOnLayoutChangeListener(onLayoutChangeListener);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    @Override
    public void setTitle(CharSequence title) {
        this.title.setText(title);
    }

    @Override
    public void setTitle(int titleId) {
        title.setText(getString(titleId));
    }

    public void setToolbarColor(int color) {
        getSupportActionBar().setBackgroundDrawable(
                new ColorDrawable(ContextCompat.getColor(this, color))
        );
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        Drawable icConcierge = menu.findItem(R.id.action_ask_concierge).getIcon();
        ViewUtils.menuTintColors(this, icConcierge);
        return true;
    }

    boolean guarded = false;
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(!guarded) {
            View actionAsk = ButterKnife.findById(toolbar, R.id.action_ask_concierge);
            if (actionAsk != null) ClickGuard.guard(actionAsk);
            guarded = true;
        }
        int id = item.getItemId();
        /*if (id == android.R.id.home) {
            super.onBackPressed();
            return true;
        }*/
        if (id == R.id.action_ask_concierge) {
            Intent intent = new Intent(this, AskConciergeActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onViewPositionChanged(float fractionAnchor, float fractionScreen) {
        ivShadow.setAlpha(1 - fractionScreen);
    }

    final View.OnLayoutChangeListener onLayoutChangeListener = new View.OnLayoutChangeListener() {
        @Override
        public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft,
                                   int oldTop, int oldRight, int oldBottom) {
            View homeUpView = toolbar.getChildAt(toolbar.getChildCount()-1);
            if(homeUpView == null) return;
            int[] xy = new int[2];
            homeUpView.getLocationOnScreen(xy);
            homeUpRect = new Rect(xy[0], xy[1], xy[0] + homeUpView.getWidth(), xy[1] + homeUpView.getHeight());
            v.removeOnLayoutChangeListener(this);
        }
    };

    public Rect getHomeUpRect() {
        return homeUpRect;
    }
}
