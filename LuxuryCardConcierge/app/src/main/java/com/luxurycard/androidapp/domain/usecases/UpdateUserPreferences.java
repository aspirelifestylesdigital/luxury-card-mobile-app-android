package com.luxurycard.androidapp.domain.usecases;

import com.luxurycard.androidapp.datalayer.datasource.PreferenceData;
import com.luxurycard.androidapp.domain.repository.UserPreferencesRepository;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;

/**
 * Created by vinh.trinh on 7/27/2017.
 */

public class UpdateUserPreferences extends UseCase<Void, UpdateUserPreferences.Params> {

    private UserPreferencesRepository userPreferencesRepository;

    public UpdateUserPreferences(UserPreferencesRepository preferencesRepository) {
        this.userPreferencesRepository = preferencesRepository;
    }

    @Override
    Observable<Void> buildUseCaseObservable(Params preferenceData) {
        return null;
    }

    @Override
    Single<Void> buildUseCaseSingle(Params preferenceData) {
        return null;
    }

    @Override
    public Completable buildUseCaseCompletable(Params params) {
        return userPreferencesRepository.save(params.preferenceData, params.accessToken);
    }

    public static class Params {
        private final PreferenceData preferenceData;
        private final String accessToken;

        public Params(PreferenceData preferenceData, String accessToken) {
            this.preferenceData = preferenceData;
            this.accessToken = accessToken;
        }
    }
}
