package com.luxurycard.androidapp.presentation.preferences;


import com.luxurycard.androidapp.App;
import com.luxurycard.androidapp.common.BackendException;
import com.luxurycard.androidapp.common.constant.ErrCode;
import com.luxurycard.androidapp.datalayer.datasource.PreferenceData;
import com.luxurycard.androidapp.domain.usecases.GetAccessToken;
import com.luxurycard.androidapp.domain.usecases.GetUserPreferences;
import com.luxurycard.androidapp.domain.usecases.UpdateUserPreferences;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class UserPreferencesPresenter implements UserPreferences.Presenter {

    private CompositeDisposable disposables;
    private UserPreferences.View view;
    private GetUserPreferences getUserPreferences;
    private UpdateUserPreferences updateUserPreferences;
    private GetAccessToken getAccessToken;

    public UserPreferencesPresenter(GetUserPreferences getUserPreferences, UpdateUserPreferences updateUserPreferences,
                                    GetAccessToken getAccessToken) {
        disposables = new CompositeDisposable();
        this.getUserPreferences = getUserPreferences;
        this.updateUserPreferences = updateUserPreferences;
        this.getAccessToken = getAccessToken;
    }

    public void attach(UserPreferences.View view) {
        this.view = view;
    }

    public void detach() {
        disposables.dispose();
        view = null;
    }

    @Override
    public void savePreferences(PreferenceData preferenceData) {
        if(!App.getInstance().hasNetworkConnection()) {
            view.showErrorDialog(ErrCode.CONNECTIVITY_PROBLEM, null);
            return;
        }
        view.showLoading();
        getAccessToken.execute()
                .flatMapCompletable(accessToken -> updateUserPreferences
                        .buildUseCaseCompletable(new UpdateUserPreferences.Params(preferenceData, accessToken))
                )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new UpdatePreferencesObserver());
    }

    @Override
    public void loadPreferences() {
        view.showLoading();
        getUserPreferences.buildUseCaseSingle(null)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new LoadPreferencesObserver());
    }

    private class LoadPreferencesObserver extends DisposableSingleObserver<PreferenceData> {

        @Override
        public void onSuccess(PreferenceData preferenceData) {
            view.loadPreferencesOnUI(preferenceData);
            view.hideLoading();
        }

        @Override
        public void onError(Throwable e) {
            view.hideLoading();
        }
    }

    private class UpdatePreferencesObserver extends DisposableCompletableObserver {

        @Override
        public void onComplete() {
            view.hideLoading();
            view.showPreferenceSavedDialog();
        }

        @Override
        public void onError(Throwable e) {
            view.hideLoading();
            if(e instanceof BackendException) {
                view.showErrorDialog(ErrCode.API_ERROR, e.getMessage());
            } else {
                view.showErrorDialog(ErrCode.UNKNOWN_ERROR, e.getMessage());
            }
        }
    }
}
