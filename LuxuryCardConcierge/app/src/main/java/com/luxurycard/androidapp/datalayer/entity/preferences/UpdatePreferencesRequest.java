package com.luxurycard.androidapp.datalayer.entity.preferences;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by vinh.trinh on 7/27/2017.
 */

public class UpdatePreferencesRequest {

    @SerializedName("Member")
    private final PreferenceMember member;
    @SerializedName("PreferenceDetails")
    private final List<PreferenceDetail> preferenceDetails;

    public UpdatePreferencesRequest(PreferenceMember member, PreferenceDetail... preferenceDetails) {
        this.member = member;
        this.preferenceDetails = new ArrayList<>(Arrays.asList(preferenceDetails));
    }
}
