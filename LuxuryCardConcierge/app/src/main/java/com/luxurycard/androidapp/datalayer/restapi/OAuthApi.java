package com.luxurycard.androidapp.datalayer.restapi;

import com.luxurycard.androidapp.BuildConfig;
import com.luxurycard.androidapp.datalayer.entity.oauth.AccessTokenResponse;
import com.luxurycard.androidapp.datalayer.entity.oauth.RefreshTokenResponse;
import com.luxurycard.androidapp.datalayer.entity.oauth.ReqTokenResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

/**
 * Created by vinh.trinh on 6/19/2017.
 */

public interface OAuthApi {
    @Headers({"consumerKey:" + BuildConfig.WS_BCD_CONSUMER_KEY, "consumerSecret:" + BuildConfig.WS_BCD_CONSUMER_SECRET})
    @GET("OAuth/AuthoriseRequestToken?CallbackURL="+BuildConfig.WS_BCD_CALL_BACK_URL)
    Call<ReqTokenResponse> requestToken(@Query("MemberDeviceId") String deviceID, @Query("OnlineMemberId") String onlineMemberId);

    @GET("OAuth/GetAccessToken?CallbackURL="+BuildConfig.WS_BCD_CALL_BACK_URL+"&ConsumerKey=" + BuildConfig.WS_BCD_CONSUMER_KEY)
    Call<AccessTokenResponse> accessToken(@Query("RequestToken") String requestToken, @Query("OnlineMemberId") String onlineMemberId);

    @GET("OAuth/RefereshToken")
    Call<RefreshTokenResponse> refreshToken(@Query("RequestToken") String requestToken, @Query("AccessToken") String accessToken);
}