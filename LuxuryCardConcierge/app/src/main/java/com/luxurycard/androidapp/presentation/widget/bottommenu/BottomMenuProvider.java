package com.luxurycard.androidapp.presentation.widget.bottommenu;

import android.os.Bundle;

import com.luxurycard.androidapp.presentation.homelist.HomeListFragment;
import com.luxurycard.androidapp.presentation.infoaccount.InfoAccountFragment;
import com.luxurycard.androidapp.presentation.more.MoreFragment;
import com.luxurycard.androidapp.presentation.requestlist.parent.RequestFragment;

import java.util.HashMap;

public class BottomMenuProvider {

    private static HashMap<BottomMenu.Item, BottomMenuEntry> entries = new HashMap<>();
    static {
        BottomMenuEntry entry;

        entry = new BottomMenuEntry(HomeListFragment.class, null, null);
        entries.put(BottomMenu.Item.HOME, entry);

        entry = new BottomMenuEntry(RequestFragment.class, null, null);
        entries.put(BottomMenu.Item.REQUEST, entry);

        entry = new BottomMenuEntry(InfoAccountFragment.class, null, null);
        entries.put(BottomMenu.Item.ACCOUNT, entry);

        entry = new BottomMenuEntry(MoreFragment.class, null, null);
        entries.put(BottomMenu.Item.MORE, entry);
    }

    public static BottomMenuEntry getEntry(BottomMenu.Item itemEnum) {
        return entries.get(itemEnum);
    }

    public static class BottomMenuEntry {

        public Class<?> fragmentClass;
        public Bundle fragmentArguments;
        public String backStack;

        public BottomMenuEntry(Class<?> fragmentClass, Bundle fragmentArguments, String backStack) {
            this.fragmentClass = fragmentClass;
            this.fragmentArguments = fragmentArguments;
            this.backStack = backStack;
        }
    }
}
