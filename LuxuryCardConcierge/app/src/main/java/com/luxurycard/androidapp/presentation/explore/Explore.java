package com.luxurycard.androidapp.presentation.explore;

import com.luxurycard.androidapp.domain.model.LatLng;
import com.luxurycard.androidapp.domain.model.explore.ExploreRView;
import com.luxurycard.androidapp.domain.model.explore.ExploreRViewItem;
import com.luxurycard.androidapp.presentation.base.BasePresenter;
import com.luxurycard.androidapp.presentation.explore.ExploreDTO.ExploreParam;

import java.util.List;


/**
 * Created by tung.phan on 5/8/2017.
 */

public interface Explore {
    interface View {
        void showLoading(boolean more);
        void hideLoading(boolean more);
        void updateRecommendAdapter(List<ExploreRViewItem> datum);
        List<ExploreRViewItem> historyData();
        void onUpdateFailed(String message);
        void emptyData();
        void onSearchNoData();
        // repeat: true, keep opening SearchActivity
        void clearSearch(boolean repeat);
        void noInternetMessage();
        void cityGuideNotAvailable();

        void setDataHeaderTitle(String headerTitle);

        void handleNormalCategorySelected(String paramCategory);
        void handleSubCategorySelected(ExploreParam param);

        android.view.View getButtonCategory();
    }

    interface Presenter extends BasePresenter<Explore.View> {
        void getAccommodationData();
        void handleCitySelection();
        void handleCategorySelection(String category);
        void cityGuideCategorySelection(int index, String cityGuideSubCate, int subCategoryResId);
        void searchByTerm(String term, boolean withOffers);
        void clearSearch();
        void offHasOffers();
        void loadMore();
        ExploreRView detailItem(ExploreRView.ItemType type, int index);
        void applyDiningSortingCriteria(LatLng location, String cuisine);

        String getTextButtonSelectedCategory(String category);

        void handleGetCategoryCurrent(String paramCategory);

        void handleTooltip();

        boolean isFirstPageData();
    }
}
