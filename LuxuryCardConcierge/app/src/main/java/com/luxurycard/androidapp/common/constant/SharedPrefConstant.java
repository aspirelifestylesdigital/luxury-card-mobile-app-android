package com.luxurycard.androidapp.common.constant;

/**
 * Created by tung.phan on 5/5/2017.
 */

public interface SharedPrefConstant {

    String PROFILE = "ppfile";
    String PREFERENCES = "preferences";
    String METADATA = "pMetadata";
    String SELECTED_CITY = "cityNameSelected";
    String FORGOT_SECRET_KEY = "hasFP";
    String DETAIL_FORGOT_SECRET = "pDetailForgot";
    String FIRST_TIME_CHECK_PERMISSION = "pFIRST_TIME_CHECK_PERMISSION";
    String EXPLORE_SESSION = "explore_session";
    String TOOLTIP_CREATED = "tooltip_created";

}
