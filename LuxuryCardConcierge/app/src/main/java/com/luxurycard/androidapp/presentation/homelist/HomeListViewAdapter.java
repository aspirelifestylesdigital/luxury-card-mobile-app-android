package com.luxurycard.androidapp.presentation.homelist;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.luxurycard.androidapp.App;
import com.luxurycard.androidapp.R;
import com.luxurycard.androidapp.common.glide.GlideHelper;
import com.luxurycard.androidapp.domain.model.HomeViewItem;
import com.luxurycard.androidapp.presentation.homelist.dto.HomeContent;

import java.util.List;

public class HomeListViewAdapter extends RecyclerView.Adapter<HomeListViewAdapter.HomeItemViewHolder> {

    private final List<HomeViewItem> mValues;
    private final OnHomeListListener mListener;
    private int expectedImageWidth;
    private int imgDefault;

    public HomeListViewAdapter(List<HomeViewItem> items, OnHomeListListener listener) {
        mValues = items;
        mListener = listener;
        expectedImageWidth = App.getInstance().getResources().getDimensionPixelSize(R.dimen.home_item_img_width);
        imgDefault = R.drawable.home_rental_car;
    }

    @Override
    public HomeItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.home_list_item_content, parent, false);
        return new HomeItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final HomeItemViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        HomeViewItem itemDTO = holder.mItem;
        holder.mTitleView.setText(itemDTO.title);

        String typeItem = itemDTO.typeItem;
        if(HomeContent.TYPE_LUXURY_MAGAZINE.equalsIgnoreCase(typeItem)
                || HomeContent.TYPE_HOTEL.equalsIgnoreCase(typeItem)){

            holder.mImgView.setImageResource(itemDTO.imgIdDrawable);

        }else if(HomeContent.TYPE_NORMAL.equalsIgnoreCase(typeItem)){

            holder.mImgView.setImageResource(itemDTO.imgIdDrawable);

        }else if(HomeContent.TYPE_API.equalsIgnoreCase(typeItem)){
            String url = itemDTO.imgUrl;
            if (TextUtils.isEmpty(url)) {
                holder.mImgView.setImageResource(imgDefault);
            } else {
                GlideHelper.getInstance().loadImage(itemDTO.imgUrl, 0, holder.mImgView, expectedImageWidth);
            }
        }else {
            holder.mImgView.setImageResource(imgDefault);
        }

        holder.mView.setOnClickListener(v -> {
            if (null != mListener) {
                mListener.onHomeListInteraction(holder.mItem);
            }
        });
    }

    public void swapData(List<HomeViewItem> data) {
        this.mValues.clear();
        this.mValues.addAll(data);
        notifyDataSetChanged();
    }

    public void clearAllData() {
        this.mValues.clear();
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class HomeItemViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final ImageView mImgView;
        public final TextView mTitleView;
        public HomeViewItem mItem;

        public HomeItemViewHolder(View view) {
            super(view);
            mView = view;
            mImgView = (ImageView) view.findViewById(R.id.home_img);
            mTitleView = (TextView) view.findViewById(R.id.home_tv);
        }
    }


    interface OnHomeListListener {
        void onHomeListInteraction(HomeViewItem item);
    }
}
