package com.luxurycard.androidapp.datalayer.entity.profile;

import com.google.gson.annotations.SerializedName;
import com.luxurycard.androidapp.BuildConfig;

/**
 * Created by vinh.trinh on 6/19/2017.
 */
public class GetDetailsRequest {

    @SerializedName("AccessToken")
    private String accessToken;
    @SerializedName("ConsumerKey")
    private String consumerKey;
    @SerializedName("Functionality")
    private String functionality;
    @SerializedName("OnlineMemberId")
    private String onlineMemberID;

    public GetDetailsRequest(String accessToken, String onlineMemberID) {
        this.accessToken = accessToken;
        this.consumerKey = BuildConfig.WS_BCD_CONSUMER_KEY;
        this.functionality = "GetUserDetails";
        this.onlineMemberID = onlineMemberID;
    }
}
