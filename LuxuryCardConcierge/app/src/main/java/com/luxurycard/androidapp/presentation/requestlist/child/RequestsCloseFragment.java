package com.luxurycard.androidapp.presentation.requestlist.child;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.luxurycard.androidapp.R;

public class RequestsCloseFragment extends RequestsBaseFragment{

    public static RequestsCloseFragment newInstance() {
        Bundle args = new Bundle();
        RequestsCloseFragment fragment = new RequestsCloseFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.tvEmpty.setText(getString(R.string.request_text_empty_close));
    }

    @Override
    public RequestItemAdapter.TYPE getRequestsItemType() {
        return RequestItemAdapter.TYPE.CLOSE;
    }

}
