package com.luxurycard.androidapp.presentation.request;

import com.luxurycard.androidapp.common.constant.ErrCode;
import com.luxurycard.androidapp.presentation.base.BasePresenter;

/**
 * Created by vinh.trinh on 5/16/2017.
 */

public interface CreateNewConciergeCase {
    interface View {
        void onRequestSuccessfullySent();
        void showErrorDialog(ErrCode errCode, String extraMsg);
        void showProgressDialog();
        void dismissProgressDialog();
    }

    interface Presenter extends BasePresenter<View> {
        void sendRequest(String content, boolean email, boolean phone);
    }
}
