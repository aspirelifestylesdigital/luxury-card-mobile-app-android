package com.luxurycard.androidapp.presentation.explore;


import android.text.TextUtils;

import com.luxurycard.androidapp.App;
import com.luxurycard.androidapp.R;
import com.luxurycard.androidapp.common.constant.CityData;
import com.luxurycard.androidapp.common.logic.StringUtils;
import com.luxurycard.androidapp.datalayer.preference.PreferencesStorage;
import com.luxurycard.androidapp.domain.model.LatLng;
import com.luxurycard.androidapp.domain.model.explore.ExploreRView;
import com.luxurycard.androidapp.domain.model.explore.ExploreRViewItem;
import com.luxurycard.androidapp.domain.usecases.AccommodationSearch;
import com.luxurycard.androidapp.domain.usecases.GetAccommodationByCategories;
import com.luxurycard.androidapp.domain.usecases.GetCityGuides;
import com.luxurycard.androidapp.domain.usecases.GetDinningList;
import com.luxurycard.androidapp.domain.usecases.UseCase;
import com.luxurycard.androidapp.presentation.explore.ExploreDTO.ExploreParam;
import com.luxurycard.androidapp.presentation.explore.ExploreDTO.ExploreParamSubCategory;
import com.luxurycard.androidapp.presentation.explore.ExploreDTO.ExploreSession;
import com.luxurycard.androidapp.presentation.widget.tooltip.TooltipView;
import com.luxurycard.androidapp.presentation.widget.tooltip.data.TooltipConstant;
import com.luxurycard.androidapp.presentation.widget.tooltip.data.TooltipData;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by tung.phan on 5/8/2017.
 */

public class ExplorePresenter implements Explore.Presenter, ExploreConstant {

    public static final String TYPE_CATEGORY_ALL = "all";

    public static final int DEFAULT_PAGE = 1;
    private final Map<String, String[]> categoryMapper = new HashMap<>();

    private CompositeDisposable disposables;
    private GetAccommodationByCategories getAccommodationByCategories;
    private GetCityGuides getCityGuides;
    private GetDinningList getDinningList;
    private AccommodationSearch accommodationSearch;
    private Explore.View view;
    private UseCase executingUseCase;

    private Single<List<ExploreRViewItem>> historyData;
    private int paging = DEFAULT_PAGE;
    private boolean reachEnd = false;

    private final PreferencesStorage preferencesStorage;
    private ExploreSession session = new ExploreSession();

    private DiningSortingCriteria diningSortingCriteria = new DiningSortingCriteria((LatLng)null, null);
    private boolean isTooltipCreated = false;

    ExplorePresenter(GetAccommodationByCategories other, GetDinningList dining,
                     GetCityGuides cityGuides, AccommodationSearch search,
                     PreferencesStorage preferencesStorage) {
        disposables = new CompositeDisposable();
        getAccommodationByCategories = other;
        getDinningList = dining;
        getCityGuides = cityGuides;
        accommodationSearch = search;
        this.preferencesStorage = preferencesStorage;
        loadExploreSession();
        buildCategoryMapper();
    }

    boolean inSearchingMode() {
        return (!TextUtils.isEmpty(session.term) || session.withOffers);
    }

    String searchTerm() {
        return session.term;
    }

    boolean isWithOffers() {
        return session.withOffers;
    }

    @Override
    public void attach(Explore.View view) {
        this.view = view;
    }

    @Override
    public void detach() {
        disposables.dispose();
        this.view = null;
    }

    @Override
    public void getAccommodationData() {
        handleCategorySelection(session.selectedCategory);
    }

    @Override
    public void handleCitySelection() {
        if(session.lastSelectedCity.equals(CityData.cityName())) {
            return;
        }
        reachEnd = false;
        int diningCode = CityData.diningCode();
        session.lastSelectedCity = CityData.cityName();

        handleTitleView();

        if(inSearchingMode()) {
            view.clearSearch(false);
        }
        if(isCategorySelectedTypeDining()) {
            getDiningList(diningCode, DEFAULT_PAGE);
            return;
        }
        if(session.cityGuideIndex > -1) {
            getCityGuide(session.cityGuideIndex, DEFAULT_PAGE);
        } else if(isCategorySelectedTypeAll()) {
            getByAllCategories(DEFAULT_PAGE);
        } else {
            getAccommodationData(session.selectedCategory, DEFAULT_PAGE);
        }

    }
    @Override
    public void handleCategorySelection(String category) {
        reachEnd = false;
        session.cityGuideIndex = -1;
        session.selectedSubCategoryResId = 0;

        session.selectedSubCategory = "";
        session.selectedCategory = category;

        saveExploreSession();
        handleTitleView();

        if(inSearchingMode()) {
            view.clearSearch(false);
        }
        if(isCategorySelectedTypeDining()) {
            getDiningList(CityData.diningCode(), DEFAULT_PAGE);
        } else if(!isCategorySelectedTypeAll()) {
            getAccommodationData(session.selectedCategory, DEFAULT_PAGE);
        } else {
            getByAllCategories(DEFAULT_PAGE);
        }

    }

    @Override
    public void cityGuideCategorySelection(int index, String cityGuideSubCate, int subCategoryResId) {
        view.clearSearch(false);
        reachEnd = false;
        session.selectedCategory = "";

        session.cityGuideIndex = index;
        session.selectedSubCategory = cityGuideSubCate;
        session.selectedSubCategoryResId = subCategoryResId;

        saveExploreSession();
        handleTitleView();

        getCityGuide(session.cityGuideIndex, DEFAULT_PAGE);

    }

    private void handleTitleView() {
        String headerTitle;
        if(TextUtils.isEmpty(session.lastSelectedCity)){
            headerTitle = "";
        }else {
            boolean emptyCategory = TextUtils.isEmpty(session.selectedCategory);
            boolean emptySubCategory = TextUtils.isEmpty(session.selectedSubCategory);

            if(session.selectedCategory.equalsIgnoreCase(TYPE_CATEGORY_ALL)){
                headerTitle = session.lastSelectedCity;

            } else if (emptyCategory && !emptySubCategory) {

                headerTitle = session.lastSelectedCity + CONCAT_TITLE + session.selectedSubCategory;

            } else if (!emptyCategory && emptySubCategory) {

                headerTitle = session.lastSelectedCity + CONCAT_TITLE + session.selectedCategory;

            } else {

                headerTitle = session.lastSelectedCity;
            }
        }

        if (view != null) {
            view.setDataHeaderTitle(headerTitle);
        }
    }

    //hold get category
    String getTitleExploreDetail(String displayCategory){
        String headerTitle;
        if(TextUtils.isEmpty(session.lastSelectedCity)){
            headerTitle = "";
        }else if(TextUtils.isEmpty(displayCategory)){
            headerTitle = session.lastSelectedCity;
        }else {

            if(session.selectedCategory.equalsIgnoreCase(TYPE_CATEGORY_ALL)){

                headerTitle = session.lastSelectedCity + CONCAT_TITLE + displayCategory;

            } else {

                boolean emptyCategory = TextUtils.isEmpty(session.selectedCategory);
                boolean emptySubCategory = TextUtils.isEmpty(session.selectedSubCategory);

                if (emptyCategory && !emptySubCategory) {

                    headerTitle = session.lastSelectedCity + CONCAT_TITLE + session.selectedSubCategory;

                } else if (!emptyCategory && emptySubCategory) {

                    headerTitle = session.lastSelectedCity + CONCAT_TITLE + session.selectedCategory;

                } else {

                    headerTitle = session.lastSelectedCity;
                }
            }

        }

        return headerTitle;
    }

    @Override
    public void handleGetCategoryCurrent(String paramCategory) {
        if(TextUtils.isEmpty(paramCategory)){
            //- check category, subCategory previous action

            boolean emptyCategory = TextUtils.isEmpty(session.selectedCategory);
            boolean emptySubCategory = TextUtils.isEmpty(session.selectedSubCategory);

            if(emptyCategory && !emptySubCategory){
                //- current choose subCategory

                view.handleSubCategorySelected(new ExploreParam(
                        new ExploreParamSubCategory(
                                session.cityGuideIndex,
                                session.selectedSubCategory,
                                session.selectedSubCategoryResId)));

            }else if(!emptyCategory && emptySubCategory){
                //- current choose category
                view.handleNormalCategorySelected(session.selectedCategory);
            }else {
                view.handleNormalCategorySelected(paramCategory);
            }

        }else {
            // filter with type category
            view.handleNormalCategorySelected(paramCategory);
        }
    }

    //<editor-fold desc="Save/Load explore session">
    /** save cache data,
     * logic: after action choose category or subCategory need save cache data*/
    private void saveExploreSession() {
        if(session == null){
            return;
        }
        try {
            ExploreSession sessionDTO = new ExploreSession(session);
            this.preferencesStorage.editor().exploreSession(sessionDTO).build().saveAsync();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void loadExploreSession() {
        try {
            ExploreSession sessionCache = this.preferencesStorage.exploreSession();
            if (sessionCache != null) {
                session = sessionCache;
            }// else don't have previous category
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    //</editor-fold>

    @Override
    public void searchByTerm(String term, boolean withOffers) {
        reachEnd = false;
        session.cityGuideIndex = -1;
        session.term = term;
        session.withOffers = withOffers;
        handleSearchCategory();
    }

    @Override
    public void clearSearch() {
        session.term = "";
        session.withOffers = false;
    }

    @Override
    public void offHasOffers() {
        reachEnd = false;
        session.withOffers = false;
        if(TextUtils.isEmpty(session.term)) {
            view.clearSearch(true);
            return;
        }
        handleSearchCategory();
    }

    private void handleSearchCategory() {
        if (isCategorySelectedTypeAll() || isCategorySelectedTypeDining()) {
            searchByTerm(DEFAULT_PAGE, session.term, session.withOffers, session.lastSelectedCity, session.selectedCategory);
        } else {
            getAccommodationData(this.session.selectedCategory, DEFAULT_PAGE);
        }
    }

    @Override
    public void loadMore() {
        if(reachEnd || paging == DEFAULT_PAGE) return;
        if(executingUseCase == null && isCategorySelectedTypeAll()) {
            getByAllCategories(paging);
            return;
        }
        if(executingUseCase == null && isCategorySelectedTypeDining()) {
            getDiningList(CityData.diningCode(), paging);
            return;
        }
        if(executingUseCase instanceof GetCityGuides) {
            getCityGuide(session.cityGuideIndex, paging);
        } else if(executingUseCase instanceof GetAccommodationByCategories) {
//            getAccommodationData(selectedCategory, paging);
        } else if(executingUseCase instanceof AccommodationSearch) {
            searchByTerm(paging, session.term, session.withOffers, session.lastSelectedCity, session.selectedCategory);
        }
    }

    @Override
    public ExploreRView detailItem(ExploreRView.ItemType type, int index) {
        /*if(executingUseCase == null && !this.selectedCategory.equals("all")) return null;*/
        if(type == ExploreRView.ItemType.CITY_GUIDE) {
            return getCityGuides.getItemView(index);
        } else if(type == ExploreRView.ItemType.DINING) {
            return getDinningList.getItemView(index);
        } else if(type == ExploreRView.ItemType.SEARCH) {
            return accommodationSearch.getItemView(index);
        } else {
            return getAccommodationByCategories.getItemView(index);
        }
    }

    @Override
    public void applyDiningSortingCriteria(LatLng location, String cuisine) {
        diningSortingCriteria.setLocation(location);
        diningSortingCriteria.setCuisine(cuisine);
        diningSortingCriteria.setIsRegion(CityData.regions.contains(CityData.cityName()));
        if(isCategorySelectedTypeDining()) {
            List<ExploreRViewItem> viewData = view.historyData();
            if(viewData == null || viewData.size() == 0) return;
            Single.just(DiningSorting.sort(viewData, diningSortingCriteria))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new DisposableSingleObserver<List<ExploreRViewItem>>() {
                        @Override
                        public void onSuccess(List<ExploreRViewItem> exploreRViewItems) {
                            if (view != null) {
                                view.updateRecommendAdapter(exploreRViewItems);
                            }
                            dispose();
                        }

                        @Override
                        public void onError(Throwable e) {
                            dispose();
                        }
                    });
        }
    }

    /**
     * city guide API
     * @param cityGuideCategoryIndex cityGuideCategoryIndex
     */
    private void getCityGuide(int cityGuideCategoryIndex, int page) {
        if(!preExecute(page)) return;
        executingUseCase = getCityGuides;

        int code = CityData.specificCityGuideCode(cityGuideCategoryIndex);
        if(code == -1) {
            view.hideLoading(paging > DEFAULT_PAGE);
            view.emptyData();
            view.cityGuideNotAvailable();
            return;
        }
        Single<List<ExploreRViewItem>> executor = getCityGuides.buildUseCaseSingle(
                new GetCityGuides.Params(code).paging(page));
        executeAndMerge(executor);
    }

    /**
     * dining list API
     * @param diningCode selectedCategory ID
     */
    private void getDiningList(int diningCode, int page) {
        if(!preExecute(page)) return;
        executingUseCase = null;
        Single<List<ExploreRViewItem>> pureDiningList;
        if (diningCode > 0) {
            pureDiningList = getDinningList.buildUseCaseSingle(
                    new GetDinningList.Params(diningCode, page));
        } else {
            pureDiningList = Single.just(new ArrayList<ExploreRViewItem>());
        }

        Single<List<ExploreRViewItem>> ccaDiningList;
        if (page == DEFAULT_PAGE) {
            ccaDiningList = getAccommodationByCategories
                    .buildUseCaseSingle(new GetAccommodationByCategories.Params(
                            "dining",
                            page,
                            null,
                            categoryMapper.get("dining")
                    ));
        } else {
            ccaDiningList = Single.just(new ArrayList<ExploreRViewItem>());
        }
        Single<List<ExploreRViewItem>> executor = Single.zip(pureDiningList, ccaDiningList,
                (pureDiningItems, ccaDiningItems) -> {
                    pureDiningItems.addAll(ccaDiningItems);
                    return pureDiningItems;
                });
        executeAndMerge(executor);
    }

    private void getAccommodationData(String categoryName, int page) {
        categoryName = categoryName.toLowerCase();
        String[] categories = categoryMapper.get(categoryName);
        if(categories == null) {
            view.emptyData();
            return;
        }
        getAccommodationData(categoryName, page, categories);
    }

    /**
     *
     * @param categories all, flowers... except dining
     */
    private void getAccommodationData(String catName, int page, String... categories) {
        if(!preExecute(page)) return;
        executingUseCase = getAccommodationByCategories;
        Single<List<ExploreRViewItem>> executor = getAccommodationByCategories.buildUseCaseSingle(
                new GetAccommodationByCategories.Params(
                        catName,
                        page,
                        inSearchingMode() ? new SearchConditions(session.term, session.withOffers) : null,
                        categories));
        executeAndMerge(executor);
    }

    private void getByAllCategories(int page) {
        if(!preExecute(page)) return;
        executingUseCase = null;
        List<Single<List<ExploreRViewItem>>> executors = new ArrayList<>();

        // accommodation
        if(page == 1) {
            Single<List<ExploreRViewItem>> getAccommodations =
                    getAccommodationByCategories.buildUseCaseSingle(
                            new GetAccommodationByCategories.Params(
                                    "all",
                                    page,
                                    inSearchingMode() ? new SearchConditions(session.term, session.withOffers) : null,
                                    categoryMapper.get("all"))
                    );
            executors.add(getAccommodations);
        }

        // dining
        int diningCode = CityData.citySelected() ? CityData.diningCode() : 0;
        Single<List<ExploreRViewItem>> getDining;
        if(diningCode > 0) {
            getDining = getDinningList.buildUseCaseSingle(
                    new GetDinningList.Params(diningCode, page)
            );
            executors.add(getDining);
        }

        if(executors.size() == 0) {
            view.hideLoading(paging > DEFAULT_PAGE);
            return;
        }

        Single<List<ExploreRViewItem>> mainExecutor = Single.zip(executors, objects -> {
            List<ExploreRViewItem> result = new ArrayList<>();
            for(Object obj : objects) {
                List<ExploreRViewItem> eachList = (List<ExploreRViewItem>) obj;
                result.addAll(eachList);
            }
            return result;
        });
        executeAndMerge(mainExecutor);
    }

    private void searchByTerm(int page, String term, boolean withOffers, String city, String category) {
        if(!preExecute(page)) return;
        executingUseCase = accommodationSearch;
        AccommodationSearch.Params params = new AccommodationSearch.Params(term, page, withOffers, city, isCategorySelectedTypeDining(), category);
        disposables.add(accommodationSearch.buildUseCaseSingle(params)
                .zipWith(historyData, (searchResult, history) -> {
                    history.addAll(searchResult.exploreRViewItems);
                    if(isCategorySelectedTypeDining()) history = DiningSorting.sort(history, diningSortingCriteria);
                    else Collections.sort(history);
                    return new AccommodationSearch.SearchResult(history, searchResult.reachEnd);
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new SearchObserver()));
    }

    private void executeAndMerge(Single<List<ExploreRViewItem>> executor) {
        disposables.add(executor.zipWith(historyData, (exploreRViewItems, history) -> {
            if(exploreRViewItems.size() == 0) return exploreRViewItems;
            history.addAll(exploreRViewItems);
            if(isCategorySelectedTypeDining()) history = DiningSorting.sort(history, diningSortingCriteria);
            else Collections.sort(history);
            return history;
        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new AccommodationDataObserver()));
    }

    private boolean preExecute(int page) {
        if(!App.getInstance().hasNetworkConnection()) {
            view.noInternetMessage();
            view.emptyData();
            return false;
        }
        disposables.clear();
        if(page == DEFAULT_PAGE) {
            view.emptyData();
        }
        historyData = Single.just(view.historyData());
        paging = page;
        view.showLoading(page > DEFAULT_PAGE);
        return true;
    }

    private final class AccommodationDataObserver extends DisposableSingleObserver<List<ExploreRViewItem>> {

        @Override
        public void onSuccess(List<ExploreRViewItem> exploreRViewItems) {
            view.hideLoading(paging > DEFAULT_PAGE);
            if(exploreRViewItems.size()>0) {
                paging++;
            } else {
                if(inSearchingMode()) {
                    view.onSearchNoData();
                }
                reachEnd = true;
                return;
            }
            view.updateRecommendAdapter(exploreRViewItems);
            dispose();
        }

        @Override
        public void onError(Throwable e) {
            view.hideLoading(paging > DEFAULT_PAGE);
            view.onUpdateFailed(e.getMessage());
            dispose();
        }
    }

    private final class SearchObserver extends DisposableSingleObserver<AccommodationSearch.SearchResult> {

        private int pageCount = 1;

        @Override
        public void onSuccess(AccommodationSearch.SearchResult searchResult) {
            view.hideLoading(paging > DEFAULT_PAGE);
            int count = searchResult.exploreRViewItems.size();
            if(searchResult.reachEnd) {
                reachEnd = true;
            }
            if(searchResult.reachEnd && count == 0) {
                view.hideLoading(paging > DEFAULT_PAGE);
                view.onSearchNoData();
            } else {
                paging++;
                view.updateRecommendAdapter(searchResult.exploreRViewItems);
                if(count < pageCount*10) {
                    loadMore();
                } else {
                    pageCount++;
                }
            }
            dispose();
        }

        @Override
        public void onError(Throwable e) {
            view.hideLoading(paging > DEFAULT_PAGE);
            view.onSearchNoData();
            dispose();
        }
    }

    @Override
    public String getTextButtonSelectedCategory(String category) {
        String textTypeCategory;
        if(TYPE_CATEGORY_ALL.equalsIgnoreCase(category)){
            textTypeCategory = App.getInstance().getString(R.string.text_all_categories);
        }else {
            textTypeCategory = StringUtils.capitalWords(category);

        }
        return textTypeCategory;
    }

    private boolean isCategorySelectedTypeDining(){
        return session.selectedCategory.equalsIgnoreCase("dining");
    }

    private boolean isCategorySelectedTypeAll(){
        return session.selectedCategory.equalsIgnoreCase(TYPE_CATEGORY_ALL);
    }

    @Override
    public void handleTooltip() {
        if (view != null && !isTooltipCreated) {
            isTooltipCreated = true;
            if(TooltipData.isShouldShow(preferencesStorage, TooltipConstant.EXPLORE_BUTTON_CATEGORY)){
                TooltipView tooltipView = new TooltipView();
                tooltipView.showViewBottom(view.getButtonCategory(), TooltipConstant.EXPLORE_BUTTON_CATEGORY);
            }//else do nothing here
        }
    }

    @Override
    public boolean isFirstPageData() {
        return paging == DEFAULT_PAGE;
    }

    private void buildCategoryMapper() {
        categoryMapper.put("all", new String[] { "Specialty Travel", "Hotels", "Flowers", "Tickets",
                "Golf", "Golf Merchandise", "Vacation Packages", "Cruises", "VIP Travel Services",
                "Private Jet Travel", "Transportation","Wine", "Dining", "Retail Shopping" });
        categoryMapper.put("dining", new String[] {"Dining", "Specialty Travel"});
        categoryMapper.put("hotels", new String[] {"Hotels"});
        categoryMapper.put("flowers", new String[] {"Flowers"});
        categoryMapper.put("entertainment", new String[] {"Tickets", "Specialty Travel"});
        categoryMapper.put("golf", new String[] {"Golf", "Golf Merchandise", "Specialty Travel"});
        categoryMapper.put("vacation packages", new String[] {"Vacation Packages"});
        categoryMapper.put("cruise", new String[] {"Cruises"});
        categoryMapper.put("tours", new String[] {"VIP Travel Services"});
        categoryMapper.put("travel", new String[] {"Private Jet Travel"});
        categoryMapper.put("airport services", new String[] {"VIP Travel Services"});
        categoryMapper.put("travel services", new String[] {"VIP Travel Services"});
        categoryMapper.put("transportation", new String[] {"Transportation"});
        categoryMapper.put("shopping", new String[] {"Golf Merchandise", "Flowers", "Wine", "Retail Shopping"});
    }
}