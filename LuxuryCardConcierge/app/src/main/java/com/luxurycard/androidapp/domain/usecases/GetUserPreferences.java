package com.luxurycard.androidapp.domain.usecases;

import com.luxurycard.androidapp.datalayer.datasource.PreferenceData;
import com.luxurycard.androidapp.domain.repository.UserPreferencesRepository;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;

/**
 * Created by vinh.trinh on 7/27/2017.
 */

public class GetUserPreferences extends UseCase<PreferenceData, String> {

    private UserPreferencesRepository userPreferencesRepository;

    public GetUserPreferences(UserPreferencesRepository preferencesRepository) {
        userPreferencesRepository = preferencesRepository;
    }

    @Override
    Observable<PreferenceData> buildUseCaseObservable(String params) {
        return null;
    }

    @Override
    public Single<PreferenceData> buildUseCaseSingle(String accessToken) {
        return userPreferencesRepository.load(accessToken);
    }

    @Override
    Completable buildUseCaseCompletable(String params) {
        return null;
    }

}
