package com.luxurycard.androidapp.presentation.selectcategory;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.luxurycard.androidapp.App;
import com.luxurycard.androidapp.R;
import com.luxurycard.androidapp.common.constant.AppConstant;
import com.luxurycard.androidapp.common.constant.CityData;
import com.luxurycard.androidapp.common.constant.RequestCode;
import com.luxurycard.androidapp.datalayer.preference.PreferencesStorage;
import com.luxurycard.androidapp.datalayer.repository.ProfileDataRepository;
import com.luxurycard.androidapp.domain.usecases.LoadProfile;
import com.luxurycard.androidapp.presentation.base.BaseNavSubFragment;
import com.luxurycard.androidapp.presentation.cityguidecategory.SubCategoryFragment;
import com.luxurycard.androidapp.presentation.explore.ExploreDTO.ExploreParam;
import com.luxurycard.androidapp.presentation.explore.ExploreFragment;
import com.luxurycard.androidapp.presentation.selectcategory.adapter.CategoryTextAdapter;

import butterknife.BindView;

/**
 * Created by vinh.trinh on 5/3/2017.
 */

public class SelectCategoryFragment extends BaseNavSubFragment implements Category.View, CategoryTextAdapter.CategoryListener {

    @BindView(R.id.selection_recycle_view)
    RecyclerView recyclerView;

    private CategoryPresenter presenter;
    private CategoryTextAdapter adapter;

    private CategoryPresenter buildPresenter() {
        return new CategoryPresenter(new LoadProfile(
                new ProfileDataRepository(new PreferencesStorage(getContext()))
        ));
    }

    public static SelectCategoryFragment newInstance() {
        Bundle args = new Bundle();
        SelectCategoryFragment fragment = new SelectCategoryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = buildPresenter();
        presenter.attach(this);
        if(savedInstanceState == null){
            // Track GA
            App.getInstance().track(AppConstant.GA_TRACKING_SCREEN_NAME.CATEGORY_LIST.getValue());
        }
    }

    @Override
    public int setupCreateViewLayoutId() {
        return R.layout.common_recyclerview_activity_layout;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setTitle(R.string.category_title);
        getBtnCallConcierge().setVisibility(View.VISIBLE);
        getButtonBack().setOnClickListener(view1 -> this.onBackPressed());

        adapter = new CategoryTextAdapter(presenter.getListCategory(view.getContext().getResources()), this) ;
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }


    @Override
    public void onDestroy() {
        presenter.detach();
        super.onDestroy();
    }

    @Override
    public void onItemClick(String category) {
        //click on City Guide
        if(category.equalsIgnoreCase(App.getInstance().getString(R.string.city_guide))){
            //return the category id of the current city selected if that city support city guide
            int cityGuideCode = CityData.guideCode();
            addChildFragmentTabCurrent(SubCategoryFragment.newInstance(cityGuideCode));
        }else {
            handleReturnResultCategory(new ExploreParam(category));
        }
    }

    public void handleReturnResultCategory(ExploreParam exploreParam) {
        try {
            if (getParentFragment() != null) {

                // 1. reload page ExploreFragment

                Fragment baseExploreFr = getParentFragment().getChildFragmentManager().findFragmentByTag(ExploreFragment.class.getSimpleName());
                if(baseExploreFr instanceof ExploreFragment){
                    ((ExploreFragment) baseExploreFr).reloadData(RequestCode.SELECT_CATEGORY, exploreParam);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        //remove view Category current
        this.onBackPressed();
    }


    @Override
    public void proceedWithDiningCategory() {
        //dummy code here
//        returnResult("DINING");
    }

    @Override
    public void askForLocationSetting() {
        //dummy code here
        /*dialogHelper.action(null, "To enable, please go to Settings and turn on Location Service for this app.", "Setting", "Cancel",
                (dialogInterface, i) -> openGPSSetting(),
                (dialogInterface, i) -> returnResult("DINING"));*/
    }

    /*private void openGPSSetting() {
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivityForResult(intent, 11234);
    }*/

}
