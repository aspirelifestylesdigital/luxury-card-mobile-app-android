package com.luxurycard.androidapp.common.constant;

/**
 * Created by tung.phan on 6/1/2017.
 */

public interface SubCategory {

    String ACCOMMODATIONS = "Accommodations";
    String BARS = "Bars/Clubs";
    String CULTURE = "Culture";
    String DINING = "Dining";
    String SHOPPING = "Shopping";
    String SPAS = "Spas";

}
