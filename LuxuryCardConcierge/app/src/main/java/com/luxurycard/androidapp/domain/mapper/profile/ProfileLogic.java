package com.luxurycard.androidapp.domain.mapper.profile;

import android.text.TextUtils;


import com.api.aspire.data.entity.profile.ProfileAspireResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 7/4/18.
 */
public class ProfileLogic {

    //<editor-fold desc="UserAppPreference">
    public static void setAppUserPreference(List<ProfileAspireResponse.AppUserPreferences> appUserPreferences,
                                      String key, String value) {
        if (TextUtils.isEmpty(key) || appUserPreferences == null) {
            return;
        }

        boolean isExistPreference = false;

        for (ProfileAspireResponse.AppUserPreferences preference : appUserPreferences) {
            if (key.equals(preference.getPreferenceKey())) {
                preference.setPreferenceValue(value);
                isExistPreference = true;
                break;
            }
        }

        if (!isExistPreference) {
            ProfileAspireResponse.AppUserPreferences preference = new ProfileAspireResponse.AppUserPreferences(key, value);
            appUserPreferences.add(preference);
        }
    }

    public static String getAppUserPreference(List<ProfileAspireResponse.AppUserPreferences> appUserPreferences, String key) {
        if (TextUtils.isEmpty(key)) {
            return "";
        }
        if (appUserPreferences == null || appUserPreferences.isEmpty()) return "";


        for (ProfileAspireResponse.AppUserPreferences preference : appUserPreferences) {
            if (key.equals(preference.getPreferenceKey())) {
                return preference.getPreferenceValue();
            }
        }
        return "";
    }

    static List<ProfileAspireResponse.AppUserPreferences> isAppUserPrEmpty(List<ProfileAspireResponse.AppUserPreferences> appUserPrValue){
        return appUserPrValue == null ? new ArrayList<>() : appUserPrValue;
    }

    /**
     * default location status is: OFF
     */
    public static String isUserLocation(boolean locationOn) {
        return locationOn ? "ON" : "OFF";
    }

    public static boolean statusUserLocation(String locationValueOn) {
        return "ON".equalsIgnoreCase(locationValueOn);
    }

    //</editor-fold>

}
