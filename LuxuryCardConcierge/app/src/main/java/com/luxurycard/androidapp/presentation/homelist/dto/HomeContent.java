package com.luxurycard.androidapp.presentation.homelist.dto;

import com.luxurycard.androidapp.R;
import com.luxurycard.androidapp.domain.model.HomeViewItem;

import java.util.ArrayList;
import java.util.List;

public class HomeContent {

    public static final List<HomeViewItem> ITEMS = new ArrayList<>();
    private static final List<HomeViewItem> ITEMS_LIST_DEFAULT = new ArrayList<>();

    public static final String TYPE_LUXURY_MAGAZINE = "luxury_magazine";
    public static final String TYPE_HOTEL = "hotel";
    /** type normal create first time */
    public static final String TYPE_NORMAL = "normal";
    /** type get from api*/
    public static final String TYPE_API = "api";

    static {
        addItem(new HomeViewItem("0", TYPE_NORMAL, "", R.drawable.home_rental_car, "Need to arrange a \nrental car?", "0", ""));
        addItem(new HomeViewItem("1", TYPE_NORMAL, "", R.drawable.home_place_stay, "Looking for a \nplace to stay?", "1", ""));

        ITEMS_LIST_DEFAULT.add(new HomeViewItem("3", TYPE_LUXURY_MAGAZINE, "", R.drawable.home_luxury_magazine, "LUXURY MAGAZINE \nSpring 2018", "2", ""));
        ITEMS_LIST_DEFAULT.add(new HomeViewItem("4", TYPE_HOTEL, "", R.drawable.home_hotel_travel, "Hotel & Travel 2018","3",""));
        addLastItem();
    }

    public static void addItem(HomeViewItem item) {
        ITEMS.add(item);
    }

    public static void addLastItem() {
        ITEMS.addAll(ITEMS_LIST_DEFAULT);
    }

    public static List<HomeViewItem> getLastItem() {
        return ITEMS_LIST_DEFAULT;
    }

}
