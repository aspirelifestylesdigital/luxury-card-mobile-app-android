package com.luxurycard.androidapp.presentation.preferences;

import com.luxurycard.androidapp.common.constant.ErrCode;
import com.luxurycard.androidapp.datalayer.datasource.PreferenceData;
import com.luxurycard.androidapp.presentation.base.BasePresenter;

public interface UserPreferences {

    interface View {
        void loadPreferencesOnUI(PreferenceData data);
        void showPreferenceSavedDialog();
        void showErrorDialog(ErrCode errCode, String extraMsg);
        void hideLoading();
        void showLoading();
    }

    interface Presenter extends BasePresenter<View> {
        void savePreferences(PreferenceData preferenceData);
        void loadPreferences();
    }
}
