package com.luxurycard.androidapp.presentation.explore;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.luxurycard.androidapp.App;
import com.luxurycard.androidapp.R;
import com.luxurycard.androidapp.common.constant.AppConstant;
import com.luxurycard.androidapp.common.constant.CityData;
import com.luxurycard.androidapp.common.constant.CityGuide;
import com.luxurycard.androidapp.common.constant.RequestCode;
import com.luxurycard.androidapp.common.logic.StringUtils;
import com.luxurycard.androidapp.datalayer.datasource.AppGeoCoder;
import com.luxurycard.androidapp.datalayer.preference.PreferencesStorage;
import com.luxurycard.androidapp.datalayer.repository.B2CDataRepository;
import com.luxurycard.androidapp.domain.model.LatLng;
import com.luxurycard.androidapp.domain.model.explore.CityGuideDetailItem;
import com.luxurycard.androidapp.domain.model.explore.ExploreRView;
import com.luxurycard.androidapp.domain.model.explore.ExploreRViewItem;
import com.luxurycard.androidapp.domain.usecases.AccommodationSearch;
import com.luxurycard.androidapp.domain.usecases.GetAccommodationByCategories;
import com.luxurycard.androidapp.domain.usecases.GetCityGuides;
import com.luxurycard.androidapp.domain.usecases.GetDinningList;
import com.luxurycard.androidapp.presentation.base.BaseNavSubFragment;
import com.luxurycard.androidapp.presentation.cityguidecategory.SubCategoryFragment;
import com.luxurycard.androidapp.presentation.explore.ExploreDTO.ExploreParam;
import com.luxurycard.androidapp.presentation.explore.ExploreDTO.ExploreParamSearch;
import com.luxurycard.androidapp.presentation.explore.ExploreDTO.ExploreParamSubCategory;
import com.luxurycard.androidapp.presentation.search.SearchFragment;
import com.luxurycard.androidapp.presentation.selectcategory.SelectCategoryFragment;
import com.luxurycard.androidapp.presentation.selectcity.SelectCityFragment;
import com.luxurycard.androidapp.presentation.venuedetail.CityGuideDetailFragment;
import com.luxurycard.androidapp.presentation.venuedetail.DiningDetailFragment;
import com.luxurycard.androidapp.presentation.venuedetail.OtherExploreDetailFragment;
import com.luxurycard.androidapp.presentation.widget.ListItemDecoration;
import com.luxurycard.androidapp.presentation.widget.OnItemClickListener;
import com.luxurycard.androidapp.presentation.widget.RecyclerViewScrollListener;
import com.luxurycard.androidapp.presentation.widget.ViewUtils;
import com.support.mylibrary.common.Utils;
import com.support.mylibrary.widget.ClickGuard;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.OnClick;

public class ExploreFragment extends BaseNavSubFragment implements Explore.View, ExploreConstant,
        OnItemClickListener {

    private ExplorePresenter presenter;
    @BindView(R.id.recommend_view)
    RecyclerView recommendView;
    @BindView(R.id.location_btn)
    Button btnLocation;
    @BindView(R.id.category_btn)
    Button btnCategory;
    @BindView(R.id.search_btn)
    Button btnSearch;
    @BindView(R.id.search_box)
    FrameLayout searchBox;
    @BindView(R.id.edt_search)
    EditText edtSearch;
    @BindView(R.id.btn_no_data_request)
    LinearLayout btnNewRequest;
    @BindView(R.id.offer_wrapper)
    LinearLayout withOffersWrapper;
    @BindView(R.id.tv_no_data)
    TextView tvNoData;
    @BindView(R.id.tv_no_data_search_title)
    TextView tvNoDataSearchTitle;
    @BindView(R.id.linear_no_data_search_container)
    ScrollView scrollViewNoDataContainer;
    @BindView(R.id.loading)
    ProgressBar loadingProgressBar;
    @BindString(R.string.text_search)
    String valTextSearch;
    @BindString(R.string.text_discover_more)
    String valTextDiscoverMore;
    @BindString(R.string.empty_search_format)
    String valTextFormatSearchEmpty;
    @BindString(R.string.text_category_all_logic)
    String valTextCategoryAll;

    private int lastSelected = -1;
    private String selectedSubCategoryName;
    private int selectedSubCategoryResId;

    private String previousPage = "";

    private ExploreRViewAdapter exploreRViewAdapter;
    private ListItemDecoration dividerItemDecoration;

    /** typeCategory: empty: get All Category, otherwise filter follow category*/
    public static ExploreFragment newInstance(String typeCategory, LatLng location, String cuisine) {
        Bundle args = new Bundle();
        args.putParcelable("sorting_criteria_1", location);
        args.putString("sorting_criteria_2", cuisine);
        args.putString("type_category", typeCategory);
        ExploreFragment fragment = new ExploreFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private ExplorePresenter buildPresenter() {
        PreferencesStorage preferencesStorage = new PreferencesStorage(this.getContext());
        B2CDataRepository b2CDataRepository = new B2CDataRepository();
        AppGeoCoder appGeoCoder = new AppGeoCoder();
        GetAccommodationByCategories other = new GetAccommodationByCategories(b2CDataRepository);
        GetCityGuides cityGuides = new GetCityGuides(b2CDataRepository);
        GetDinningList dinningList = new GetDinningList(b2CDataRepository, appGeoCoder);
        AccommodationSearch search = new AccommodationSearch(b2CDataRepository, appGeoCoder);
        return new ExplorePresenter(other, dinningList, cityGuides, search, preferencesStorage);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = buildPresenter();
        presenter.attach(this);
        if(savedInstanceState == null){
            // Track GA
            App.getInstance().track(AppConstant.GA_TRACKING_SCREEN_NAME.EXPLORE.getValue());
        }
    }

    @Override
    public int setupCreateViewLayoutId() {
        return R.layout.fragment_explore;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recommendView.setLayoutManager(layoutManager);
        dividerItemDecoration = new ListItemDecoration(recommendView.getContext());
        recommendView.addItemDecoration(dividerItemDecoration);
        exploreRViewAdapter = new ExploreRViewAdapter(getContext(), new ArrayList<>(), this);
        recommendView.setAdapter(exploreRViewAdapter);
        dividerItemDecoration.setAdapterItemCount(0);

        getBtnCallConcierge().setVisibility(View.VISIBLE);
        getButtonBack().setOnClickListener(v -> this.onBackPressed());

        setSizeHeightSpaceTab(btnLocation, R.drawable.ic_explore_location);
        setSizeHeightSpaceTab(btnCategory, R.drawable.ic_explore_menu);
        setSizeHeightSpaceTab(btnSearch, R.drawable.ic_explore_search);

        Drawable searchDrawableEnd = ContextCompat.getDrawable(view.getContext(), R.drawable.ic_clear);
        Drawable searchDrawableStart = ContextCompat.getDrawable(view.getContext(), R.drawable.ic_search_small);
        edtSearch.setCompoundDrawablesWithIntrinsicBounds(searchDrawableStart,null,searchDrawableEnd,null);

        setupLoadMore();
        edtSearch.setOnTouchListener((v, event) -> {
            if(event.getAction() == MotionEvent.ACTION_UP) {
                if(event.getRawX() >= (edtSearch.getRight() - edtSearch.getCompoundDrawables()[2].getBounds().width()- 20)) {
                    // your action here
                    clearSearch(true);
                    /*// - new flow search
                    shouldReload = true;
                    handleSearchInputResult(null);*/
                    return true;
                }else{
                    // Search with the given keyword
                    searchWithGivenKeyword();
                    return true;
                }
            }
            return false;
        });
        ClickGuard.guard(btnLocation, btnCategory, btnSearch);

        if (getArguments() != null) {
            String paramCategory = getArguments().getString("type_category", "");
            if(CityData.citySelected()) {
                handleSelectedCityResult();
                presenter.handleGetCategoryCurrent(paramCategory);

                //<editor-fold desc="Logic temp don't sort dining code (no have location)">
                //sort with category dining
                /*LatLng location = getArguments().getParcelable("sorting_criteria_1");
                String cuisine = getArguments().getString("sorting_criteria_2");
                //add temp condition logic
                if(location != null && cuisine != null){
                    preferencesChange(location, cuisine);
                }*/
                //</editor-fold>
            }
        }

        presenter.handleTooltip();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        presenter.detach();
        super.onDestroy();
    }

    private void setSizeHeightSpaceTab(Button button, int idDrawable){
        ViewUtils.drawableTop(button, idDrawable);
    }

    @Override
    public void setDataHeaderTitle(final String headerTitle) {
        if(tvTitle != null){
            setTitle(headerTitle);
            /*logic view text long will be show 2 line line1: city > | line2: category*/
            tvTitle.postDelayed(() -> {
                if(tvTitle != null && tvTitle.getLineCount() >= 2 && headerTitle.contains(CONCAT_TITLE)){
                    String titleShowLogic = headerTitle.replace(CONCAT_TITLE, CONCAT_LINE_TITLE);
                    setTitle(titleShowLogic);
                }//-- do nothing here
            },50);
        }
    }

    @Override
    public void onBackPressed() {
        // logic previous page
        if (!TextUtils.isEmpty(previousPage)) {
            if (SelectCityFragment.class.getSimpleName().equalsIgnoreCase(previousPage)) {

                addChildFragmentTabCurrent(SelectCityFragment.newInstance(false));

            } else if (SelectCategoryFragment.class.getSimpleName().equalsIgnoreCase(previousPage)) {

                addChildFragmentTabCurrent(SelectCategoryFragment.newInstance());

            } else {
                super.onBackPressed();
            }

            //reset previous page
            previousPage = "";

        } else {
            //reset previous page
            previousPage = "";
            super.onBackPressed();
        }
    }

    @OnClick(R.id.location_btn)
    public void clickBtnLocation(View view) {
        previousPage = SelectCityFragment.class.getSimpleName();
        addChildFragmentTabCurrent(SelectCityFragment.newInstance(false));
    }

    @OnClick(R.id.category_btn)
    public void clickBtnCategory(View view) {
        previousPage = SelectCategoryFragment.class.getSimpleName();
        addChildFragmentTabCurrent(SelectCategoryFragment.newInstance());
    }

    @OnClick(R.id.search_btn)
    public void clickBtnSearch(Button btnSearch) {
        //handle case btn Discover more pressed
        String valBtnSearch = btnSearch.getText().toString();
        if (valBtnSearch.equalsIgnoreCase(valTextDiscoverMore) && CityData.guideCode()>0) {
            //start sub category select
            addChildFragmentTabCurrent(SubCategoryFragment.newInstance(CityData.guideCode()));

        } else if(valBtnSearch.equalsIgnoreCase(valTextSearch)) {

            addChildFragmentTabCurrent(SearchFragment.newInstance());
        }
    }

    @OnClick(R.id.btn_clear_offers)
    public void clearOffers() {
        presenter.offHasOffers();
        withOffersWrapper.setVisibility(View.GONE);
    }

    @OnClick(R.id.btn_no_data_request)
    public void clickNewRequest() {
        openNewRequestActivity();
    }

    @Override
    public void hideLoading(boolean more) {
        if(more) {
            exploreRViewAdapter.showLoading(false);
        } else {
            loadingProgressBar.setVisibility(View.GONE);
        }
        scrollListener.loadDone();
    }

    @Override
    public void showLoading(boolean more) {
        if(more) {
            exploreRViewAdapter.showLoading(true);
        } else {
            loadingProgressBar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void updateRecommendAdapter(List<ExploreRViewItem> data) {
        hideViewNoHaveData();
        exploreRViewAdapter.setSearchAction(presenter.inSearchingMode());
        exploreRViewAdapter.swapData(data);
        dividerItemDecoration.setAdapterItemCount(exploreRViewAdapter.getItemCount());
        //scroll to top row
        if (presenter != null) {
            boolean isFirstPage = presenter.isFirstPageData();
            try {
                if (isFirstPage) {
                    if (recommendView.getLayoutManager() != null) {
                        recommendView.getLayoutManager().scrollToPosition(0);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onUpdateFailed(String message) {
        showGeneralErrorActivity();
    }

    @Override
    public void emptyData() {
        exploreRViewAdapter.clear();
    }

    @Override
    public void onSearchNoData() {
        if(exploreRViewAdapter.isEmpty()) {
            showViewNoHaveData();
        }
    }

    @Override
    public List<ExploreRViewItem> historyData() {
        return exploreRViewAdapter.getData();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void reloadData(int requestCode, ExploreParam param){
        if(requestCode == RequestCode.SELECT_CITY) {
            handleSelectedCityResult();
        } else if (requestCode == RequestCode.SELECT_CATEGORY) {
            handleCategoryDataFragment(param);
        } else if (requestCode == RequestCode.SELECT_SUB_CATEGORY) {
            handleSubCategorySelected(param);
        } else if (requestCode == RequestCode.SEARCH_INPUT) {
            handleSearchInputResult(param);
        }
    }

    private void handleCategoryDataFragment(ExploreParam param) {
        if (param == null) {
            return;
        }

        if (param.isPageSubCategory()) {
            handleSubCategorySelected(param);
        } else {
            handleNormalCategorySelected(param.getSelectedCategory());
        }
    }

    @Override
    public void handleNormalCategorySelected(String category) {
        if(TextUtils.isEmpty(category) || valTextCategoryAll.equalsIgnoreCase(category)) {
            category = valTextCategoryAll;
            exploreRViewAdapter.setShowCategoryAll();
        }else {
            exploreRViewAdapter.setShowCategoryType();
        }

        if(category.equalsIgnoreCase("dining")) {
            requestLocation();
        }

        String selectedCategory = presenter.getTextButtonSelectedCategory(category);
        btnCategory.setText(selectedCategory);

        presenter.handleCategorySelection(category);

        btnSearch.setText(valTextSearch);
        btnSearch.setPadding(0, (int) getResources().getDimension(R.dimen.padding_medium), 0, 0);
        setSizeHeightSpaceTab(btnSearch, R.drawable.ic_explore_search);

        // Track GA "select category"
        //<editor-fold desc="Track">
        if(selectedCategory.equalsIgnoreCase(App.getInstance().getString(R.string.text_all_categories))){
            selectedCategory = StringUtils.capitalWords(ExplorePresenter.TYPE_CATEGORY_ALL); //-- All
        }
        App.getInstance().track(AppConstant.GA_TRACKING_CATEGORY.CATEGORY_SELECTION.getValue(),
                AppConstant.GA_TRACKING_ACTION.SELECT.getValue(),
                selectedCategory);
        //</editor-fold>

    }

    private void handleSelectedCityResult() {
        String selectedCity = CityData.cityName();
        btnLocation.setText(selectedCity);
        presenter.handleCitySelection();
        requestLocation();
    }

    @Override
    public void handleSubCategorySelected(ExploreParam param) {
        if (param == null || param.getSubCategoryDTO() == null) {
            return;
        }
        exploreRViewAdapter.setShowCategoryType();
        btnCategory.setText(getString(R.string.city_guide));
        ExploreParamSubCategory subCateDTO = param.getSubCategoryDTO();

        int categoryIndex = subCateDTO.subCategoryIndex;
        selectedSubCategoryName = subCateDTO.subCategoryName;
        selectedSubCategoryResId = subCateDTO.subCategoryResId;
        presenter.cityGuideCategorySelection(categoryIndex, selectedSubCategoryName, selectedSubCategoryResId);

        //reset text for btn Search and also remove the icon
        btnSearch.setText(valTextDiscoverMore);
        ViewUtils.emptyDrawable(btnSearch);
        btnSearch.setPadding(0,Utils.dip2px(getContext(), 32),0,(int) getResources().getDimension(R.dimen.padding_smallest));

        // Track GA city guide category
        //<editor-fold desc="Track">
        String trackingLabel = "";
        switch (categoryIndex){
            case CityGuide.BAR_LIST_INDEX:
                trackingLabel = AppConstant.GA_TRACKING_LABEL.CITY_GUIDE_BAR_CLUB.getValue();
                break;
            case CityGuide.CULTURE_LIST_INDEX:
                trackingLabel = AppConstant.GA_TRACKING_LABEL.CITY_GUIDE_CULTURE.getValue();
                break;
            case CityGuide.DINNING_LIST_INDEX:
                trackingLabel = AppConstant.GA_TRACKING_LABEL.CITY_GUIDE_DINING.getValue();
                break;
            case CityGuide.SHOPPING_LIST_INDEX:
                trackingLabel = AppConstant.GA_TRACKING_LABEL.CITY_GUIDE_SHOPPING.getValue();
                break;
            case CityGuide.SPA_LIST_INDEX:
                trackingLabel = AppConstant.GA_TRACKING_LABEL.CITY_GUIDE_SPAS.getValue();
                break;
            default:
                break;
        }
        if(!TextUtils.isEmpty(trackingLabel)){
            App.getInstance().track(AppConstant.GA_TRACKING_CATEGORY.CATEGORY_SELECTION.getValue(),
                    AppConstant.GA_TRACKING_ACTION.SELECT.getValue(),
                    trackingLabel);
        }
        //</editor-fold>

    }

    private void handleSearchInputResult(ExploreParam data) {
        if(data != null && data.getSearchDTO() != null) {
            emptyData();
            ExploreParamSearch search = data.getSearchDTO();
            String term = search.term;
            boolean withOffers = search.withOffers;
            showSearchBox(term, withOffers);
            presenter.searchByTerm(term, withOffers);
        } else if(!presenter.inSearchingMode() && shouldReload) {
            presenter.getAccommodationData();
            shouldReload = false;
        }
    }

    @Override
    public void onItemClick(int pos) {
        // highlight selected item
        if(lastSelected > -1) exploreRViewAdapter.notifyItemChanged(lastSelected);
        exploreRViewAdapter.notifyItemChanged(pos);
        lastSelected = pos;

        ExploreRViewItem listItem = exploreRViewAdapter.getItem(pos);

        ExploreRView detailViewData = presenter.detailItem(listItem.getItemType(), listItem.dataListIndex);
        if(detailViewData == null) return;
        switch (detailViewData.getItemType()) {
            case DINING:
                String titleDining = presenter.getTitleExploreDetail(listItem.getDisplayCategory());
                addChildFragmentTabCurrent(DiningDetailFragment.newInstance(titleDining, detailViewData));
                break;
            case CITY_GUIDE:
                if(detailViewData instanceof CityGuideDetailItem) {
                    ((CityGuideDetailItem) detailViewData)
                            .setSubCategoryName(selectedSubCategoryName)
                            .setImageResId(selectedSubCategoryResId);
                }
                String titleCityGuide = presenter.getTitleExploreDetail(selectedSubCategoryName);
                addChildFragmentTabCurrent(CityGuideDetailFragment.newInstance(titleCityGuide, detailViewData));
                break;
            case NORMAL:
                String titleNormal = presenter.getTitleExploreDetail(listItem.getDisplayCategory());
                addChildFragmentTabCurrent(OtherExploreDetailFragment.newInstance(titleNormal, detailViewData));
                break;
            default:
                throw new IllegalStateException("type must be one of above");
        }
    }

    private RecyclerViewScrollListener scrollListener;
    private void setupLoadMore() {
        scrollListener = new RecyclerViewScrollListener() {

            @Override
            public void onScrollUp() {}

            @Override
            public void onScrollDown() {}

            @Override
            public void onLoadMore() {
                presenter.loadMore();
            }
        };
        recommendView.addOnScrollListener(scrollListener);
    }

    private void showSearchBox(String term, boolean withOffers) {
        hideViewNoHaveData();
        searchBox.setVisibility(View.VISIBLE);
        edtSearch.setText(term);
        SpannableStringBuilder str;
        int startInd, endInd;
        if(TextUtils.isEmpty(term)) {
            String valTextOffers = App.getInstance().getString(R.string.text_offers);
            String text = getString(R.string.search_text_result, valTextOffers);
            str = new SpannableStringBuilder(text);
            startInd = text.lastIndexOf(valTextOffers);
            endInd = startInd + valTextOffers.length();
        } else {
            String text = getString(R.string.search_text_result, term);
            str = new SpannableStringBuilder(text);
            startInd = text.lastIndexOf(term);
            endInd = startInd + term.length();
        }
        str.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), startInd, endInd, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        edtSearch.setText(str);


        if(withOffers)
            withOffersWrapper.setVisibility(View.VISIBLE);
        else
            withOffersWrapper.setVisibility(View.GONE);
    }

    private boolean shouldReload = false;
    @Override
    public void clearSearch(boolean repeat) {
        hideViewNoHaveData();
        searchBox.setVisibility(View.GONE);
        withOffersWrapper.setVisibility(View.GONE);
        presenter.clearSearch();
        emptyData();
        if(repeat) {
            shouldReload = true;
            addChildFragmentTabCurrent(SearchFragment.newInstance());
        }
    }

    private void searchWithGivenKeyword() {
        // Go to Search screen
        addChildFragmentTabCurrent(SearchFragment.newInstance(presenter.searchTerm(), presenter.isWithOffers()));
    }

    public void preferencesChange(LatLng latLng, String cuisine) {
        presenter.applyDiningSortingCriteria(latLng, cuisine);
    }

    @Override
    public void noInternetMessage() {
        showErrorNoNetwork();
    }

    @Override
    public void cityGuideNotAvailable() {
        showViewNoHaveData();
    }

    private void showViewNoHaveData() {
        recommendView.setVisibility(View.GONE);
        if (presenter.inSearchingMode()) {

            tvNoData.setVisibility(View.GONE);
            scrollViewNoDataContainer.setVisibility(View.VISIBLE);
            tvNoDataSearchTitle.setText(
                    String.format(valTextFormatSearchEmpty, presenter.searchTerm()));
        } else {
            tvNoDataSearchTitle.setText("");
            scrollViewNoDataContainer.setVisibility(View.GONE);
            tvNoData.setText(getString(R.string.empty_result_notify));
            tvNoData.setVisibility(View.VISIBLE);
        }
    }

    private void hideViewNoHaveData(){
        recommendView.setVisibility(View.VISIBLE);
        //reset title search empty
        tvNoDataSearchTitle.setText("");
        scrollViewNoDataContainer.setVisibility(View.GONE);
        tvNoData.setVisibility(View.GONE);
    }

    private void requestLocation(){
        // TODO: 4/11/2018 no request location
        /*if (getActivity() instanceof HomeNavBottomActivity) {
            ((HomeNavBottomActivity) getActivity()).requestLocation();
        }*/
    }

    @Override
    public View getButtonCategory() {
        return btnCategory;
    }
}