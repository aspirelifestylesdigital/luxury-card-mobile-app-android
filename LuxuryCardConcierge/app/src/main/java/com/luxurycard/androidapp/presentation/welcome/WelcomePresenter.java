package com.luxurycard.androidapp.presentation.welcome;

import com.luxurycard.androidapp.App;
import com.luxurycard.androidapp.common.constant.ErrCode;
import com.luxurycard.androidapp.domain.model.GalleryViewPagerItem;
import com.luxurycard.androidapp.domain.usecases.GetGalleries;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Den on 3/20/18.
 */

public class WelcomePresenter implements Welcome.Presenter {

    private CompositeDisposable disposables;
    private Welcome.View view;
    private GetGalleries getGalleries;


    WelcomePresenter(GetGalleries getGalleries) {
        disposables = new CompositeDisposable();
        this.getGalleries = getGalleries;
    }

    @Override
    public void attach(Welcome.View view) {
        this.view = view;
    }

    @Override
    public void detach() {
        disposables.dispose();
        this.view = null;
    }

    @Override
    public void getGalleryList() {
        if(!App.getInstance().hasNetworkConnection()) {
            view.dismissProgressDialog();
            view.showErrorMessage(ErrCode.CONNECTIVITY_PROBLEM);
            return;
        }
        disposables
                .add(getGalleries.param(null)
                        .on(Schedulers.io(), AndroidSchedulers.mainThread())
                        .execute(new WelcomePresenter.GetGalleryObserver()));
    }


    private final class GetGalleryObserver extends DisposableSingleObserver<List<GalleryViewPagerItem>> {
        @Override
        public void onSuccess(List<GalleryViewPagerItem> exploreRViewItemList) {
            view.onGetGalleryListFinished(exploreRViewItemList);
            dispose();
        }

        @Override
        public void onError(Throwable e) {
            if (view != null) {
                view.dismissProgressDialog();
                view.showErrorMessage(ErrCode.UNKNOWN_ERROR);
            }
            dispose();
        }
    }
}
