package com.luxurycard.androidapp.presentation.requestlist;

import android.view.View;

import com.luxurycard.androidapp.domain.model.RequestDetailData;
import com.luxurycard.androidapp.domain.model.RequestItemView;
import com.luxurycard.androidapp.presentation.base.BasePresenter;

import java.util.List;

/**
 * Created by vinh.trinh on 8/30/2017.
 */

public interface RequestList {

    interface View {
        void showProgressDialog();
        void dismissProgressDialog();
        void showListLoadMore(boolean open);
        void hideListLoadMore(boolean open);
        int openCount();
        int closeCount();
        void loadData(RequestListPresenter.RequestData data);
        void onFetchError();
        void doneCancel();
        void resetLoadDuplicate();

        void isRequestsEmpty();

        void resetViewRequestsData();

        android.view.View getButtonNewRequest();
    }

    interface Presenter extends BasePresenter<View>{
        void fetchList();
        void loadMore();
        void refresh();
        void cancelRequest(RequestDetailData data);
        void handleTooltip();
    }

}