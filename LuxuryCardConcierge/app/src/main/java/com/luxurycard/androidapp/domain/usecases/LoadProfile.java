package com.luxurycard.androidapp.domain.usecases;

import com.luxurycard.androidapp.datalayer.datasource.LocalProfileDataStore;
import com.luxurycard.androidapp.datalayer.datasource.PreferenceData;
import com.luxurycard.androidapp.domain.model.Profile;
import com.luxurycard.androidapp.domain.repository.ProfileRepository;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;

/**
 * Created by tung.phan on 5/18/2017.
 */

public class LoadProfile extends UseCase<Profile, String> {

    private ProfileRepository profileRepository;

    public LoadProfile(ProfileRepository profileRepository) {
        this.profileRepository = profileRepository;
    }

    @Override
    Observable<Profile> buildUseCaseObservable(String params) {
        return null;
    }

    @Override
    public Single<Profile> buildUseCaseSingle(String accessToken) {
        return profileRepository.loadProfile(accessToken);
    }

    @Override
    Completable buildUseCaseCompletable(String params)  {
        return null;
    }

    /** Get data location from preference update into profile data */
    public LocalProfileDataStore.ParamSaveLocation saveLocationInProfile(Profile profile,
                                                                         PreferenceData preferenceData){
        return profileRepository.saveLocationProfileLocal(profile, preferenceData);
    }

}
