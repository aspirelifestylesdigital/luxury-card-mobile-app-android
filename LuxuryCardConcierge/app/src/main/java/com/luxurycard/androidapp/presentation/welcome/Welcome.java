package com.luxurycard.androidapp.presentation.welcome;

import com.luxurycard.androidapp.common.constant.ErrCode;
import com.luxurycard.androidapp.domain.model.GalleryViewPagerItem;
import com.luxurycard.androidapp.presentation.base.BasePresenter;
import com.luxurycard.androidapp.presentation.inspiregallery.GetGallery;

import java.util.List;

/**
 * Created by Den on 3/20/18.
 */

public interface Welcome {
    interface View {
        void onGetGalleryListFinished(List<GalleryViewPagerItem> galleryItemList);
        void dismissProgressDialog();
        void showErrorMessage(ErrCode errCode);
    }

    interface Presenter extends BasePresenter<Welcome.View> {
        void getGalleryList();
    }
}
