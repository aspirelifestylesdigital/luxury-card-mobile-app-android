package com.luxurycard.androidapp.datalayer.datasource;


import com.luxurycard.androidapp.common.constant.CityData;
import com.luxurycard.androidapp.datalayer.preference.PreferencesStorage;

import io.reactivex.Completable;
import io.reactivex.Single;

public class LocalPrefDataStore {

    private PreferencesStorage preferencesStorage;

    public LocalPrefDataStore(PreferencesStorage preferencesStorage) {
        this.preferencesStorage = preferencesStorage;
    }

    public Single<PreferenceData> loadPreferences() {
        return Single.create(emitter -> {
            if(preferencesStorage.preferencesCreated()) {
                PreferenceData preferenceData = preferencesStorage.userPreferences();
                emitter.onSuccess(preferenceData);
            } else {
                emitter.onSuccess(PreferenceData.plain());
            }
        });
    }

    public Completable savePreferences(PreferenceData preferenceData) {
        return Completable.create(e -> {
            PreferencesStorage.Editor editor = preferencesStorage.editor().preferences(preferenceData);
            final String selectCity = preferenceData.selectCity;
            if(CityData.isCityInApp(selectCity)){
                editor.selectedCity(selectCity);
            }
            editor.build().save();
            e.onComplete();
        });
    }
}
