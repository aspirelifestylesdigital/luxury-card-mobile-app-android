package com.luxurycard.androidapp.presentation.inspiregallery;

import com.luxurycard.androidapp.common.constant.ErrCode;
import com.luxurycard.androidapp.domain.model.GalleryViewPagerItem;
import com.luxurycard.androidapp.presentation.base.BasePresenter;

import java.util.List;

/**
 * Created by Thu Nguyen on 6/13/2017.
 */

public interface GetGallery {

    interface View {
        void onGetGalleryListFinished(List<GalleryViewPagerItem> galleryItemList);
        void dismissProgressDialog();
        void showErrorMessage(ErrCode errCode);
    }

    interface Presenter extends BasePresenter<View> {
        void getGalleryList();
    }

}
