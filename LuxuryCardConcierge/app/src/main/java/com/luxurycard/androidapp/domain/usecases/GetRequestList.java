package com.luxurycard.androidapp.domain.usecases;

import com.luxurycard.androidapp.common.BackendException;
import com.luxurycard.androidapp.common.constant.RequestTypeConstant;
import com.luxurycard.androidapp.datalayer.entity.askconcierge.ACRequestItem;
import com.luxurycard.androidapp.datalayer.entity.askconcierge.GetACRequest;
import com.luxurycard.androidapp.domain.model.Metadata;
import com.luxurycard.androidapp.domain.model.RequestDetailData;
import com.luxurycard.androidapp.domain.model.RequestItemView;
import com.luxurycard.androidapp.domain.repository.ACRepository;
import com.luxurycard.androidapp.presentation.requestdetail.RequestDetail;
import com.luxurycard.androidapp.presentation.requestdetail.TextDetailBuilder;

import org.json.JSONException;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;

/**
 * Created by vinh.trinh on 8/30/2017.
 */

public class GetRequestList extends UseCase<List<RequestItemView>, GetRequestList.Params> {

    private ACRepository acRepository;
    private List<ACRequestItem> data;
//    private SimpleDateFormat compareFormat;
    private List<SimpleDateFormat> simpleDateFormats;
    private SimpleDateFormat datePrinter;
    private TextDetailBuilder textDetailBuilder;
    private final long fiveMinutesMs;

    public GetRequestList(ACRepository acRepository) {
        this.acRepository = acRepository;
        simpleDateFormats = new ArrayList<>();
        data = new ArrayList<>();
        SimpleDateFormat firstDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.US);
        firstDateFormat.setTimeZone(TimeZone.getTimeZone("GMT+0"));
        SimpleDateFormat secondDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
        secondDateFormat.setTimeZone(TimeZone.getTimeZone("GMT+0"));
        simpleDateFormats.add(firstDateFormat);
        simpleDateFormats.add(secondDateFormat);
//        compareFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.US);
//        compareFormat.setTimeZone(TimeZone.getTimeZone("GMT+0"));
        datePrinter = new SimpleDateFormat("MM/dd/yyyy | h:mm a", Locale.US);
        fiveMinutesMs = (60 * 5)*1000;
        textDetailBuilder = new TextDetailBuilder();
    }

    @Override
    Observable<List<RequestItemView>> buildUseCaseObservable(Params params) {
        return null;
    }

    @Override
    public Single<List<RequestItemView>> buildUseCaseSingle(Params params) {
        return Single.create(e -> {
            GetACRequest requestBody = new GetACRequest(params.accessToken, params.metadata.onlineMemberID);
            requestBody.list(params.rowStart, params.rowEnd);
            try {
                int startIndex = this.data.size();
                List<ACRequestItem> data = acRepository.getRequestList(requestBody);
                this.data.addAll(data);
                List<RequestItemView> viewData = new ArrayList<>(data.size());

                Calendar current = Calendar.getInstance(Locale.US);
                for (int i = 0; i < data.size(); i++) {
                    //fix here
                    viewData.add(bindView(data.get(i), startIndex + i, current.getTimeInMillis()));
                }
                e.onSuccess(viewData);
            } catch (BackendException | IOException | JSONException exception) {
                e.onError(exception);
            }
        });
    }

    @Override
    Completable buildUseCaseCompletable(Params params) {
        return null;
    }

    Date tryParse(String dateString, List<SimpleDateFormat> formatStrings)
    {
        for (SimpleDateFormat formatString : formatStrings)
        {
            try
            {
                return formatString.parse(dateString);
            }
            catch (ParseException e) {}
        }

        return null;
    }

    private RequestItemView bindView(ACRequestItem item, int index, long currentTime) throws
            ParseException, JSONException {
        Date date = tryParse(item.createdDate(), simpleDateFormats);
        if (date == null) {
            throw new ParseException("Error parse date time", 0);
        }

//        Date compareDate = compareFormat.parse(item.createdDate());

        RequestDetailData requestDetail = new RequestDetailData(
                item.EPCCaseID(),
                item.transactionID(),
                item.type(),
                item.functionality(),
                datePrinter.format(date),
                item.eventDate(),
                item.pickupDate(),
                item.startDate(),
                item.endDate(),
                item.createdDate(),
                item.numberOfAdults(),
                item.numberOfChildren(),
                item.details(),
                item.prefResponse(),
                !"closed".equals(item.status()),
                requestType(item.status(), item.mode(), date, currentTime),
                item.prefResponse(),
                item.mode(),
                item.status()
        ).setCity(item.city()).setState(item.state()).setCountry(item.country());
        return new RequestItemView(
                textDetailBuilder.getName(item.type(), item.details()),
                item.EPCCaseID(),
                formatDateView(date),
                index,
                viewStatus(item.status(), item.mode(), date, currentTime),
                requestDetail.getNameRequestType(),
                requestDetail.getContentRequestItem(),
                item.transactionID(),
                requestDetail,
                requestDetail.status
                );
    }


    private String formatDateView(Date date) {
        return datePrinter.format(date);
    }

    private RequestItemView.STATUS viewStatus(String requestStatus, String requestMode, Date date, long currentTime) {
//        if(!open) return RequestItemView.STATUS.CLOSED;
//
//        if (currentTime - date.getTime()  <= fiveMinutesMs) {
//            if ("cancel".equalsIgnoreCase(requestMode)) {
//                return RequestItemView.STATUS.OPEN_CANCELED;
//            } else {
//                return RequestItemView.STATUS.PENDING;
//            }
//        } else {
//            if ("inprogress".equalsIgnoreCase(requestStatus)) {
//                return RequestTypeConstant.IN_PROGRESS;
//            }
//            if ("cancel".equalsIgnoreCase(requestMode)) {
//                return RequestItemView.STATUS.OPEN_CANCELED;
//            } else {
//                return RequestItemView.STATUS.OPEN;
//            }
//        }

        if ("closed".equalsIgnoreCase(requestStatus)) {
            return RequestItemView.STATUS.CLOSED;
        }
        if (currentTime - date.getTime()  <= fiveMinutesMs) {
            if ("cancel".equalsIgnoreCase(requestMode)) {
                return RequestItemView.STATUS.OPEN_CANCELED;
            } else {
                return  RequestItemView.STATUS.PENDING;
            }
        } else {

            if ("cancel".equalsIgnoreCase(requestMode)) {
                    return  RequestItemView.STATUS.OPEN_CANCELED;
            } else {
                    return  RequestItemView.STATUS.OPEN;
                }

        }

    }

    private String requestType(String requestStatus, String requestMode, Date date, long currentTime) {
        if ("closed".equalsIgnoreCase(requestStatus)) {
            return RequestTypeConstant.CLOSED;
        }
        if (currentTime - date.getTime()  <= fiveMinutesMs) {
            if ("cancel".equalsIgnoreCase(requestMode)) {
                return  RequestTypeConstant.OPEN;
            } else {
                return  RequestTypeConstant.PENDING;
            }
        } else {
            if ("inprogress".equalsIgnoreCase(requestStatus)) {
                return RequestTypeConstant.IN_PROGRESS;
            } else {
                return RequestTypeConstant.OPEN;
            }
        }
    }




    public RequestDetailData getDatum(int index) throws ParseException {
        ACRequestItem item = data.get(index);
//        Date date = dateFormat.parse(item.createdDate());
        Date date = tryParse(item.createdDate(), simpleDateFormats);
        if (date == null) {
            throw new ParseException("Error parse date time", 0);
        }
        Calendar current = Calendar.getInstance(Locale.US);
        long currentTime = current.getTimeInMillis();
        return new RequestDetailData(
                item.EPCCaseID(),
                item.transactionID(),
                item.type(),
                item.functionality(),
                datePrinter.format(date),
                item.eventDate(),
                item.pickupDate(),
                item.startDate(),
                item.endDate(),
                item.createdDate(),
                item.numberOfAdults(),
                item.numberOfChildren(),
                item.details(),
                item.prefResponse(),
                !"closed".equals(item.status()),
                requestType(item.status(), item.mode(), date, currentTime),
                item.prefResponse(),
                item.mode(),
                item.status()
        ).setCity(item.city()).setState(item.state()).setCountry(item.country()
        );
    }

    public void reset() {
        data.clear();
    }

    public static class Params {
        private String accessToken;
        private Metadata metadata;
        private int rowStart;
        private int rowEnd;

        public Params(String accessToken, Metadata metadata, int rowStart, int rowEnd) {
            this.accessToken = accessToken;
            this.metadata = metadata;
            this.rowStart = rowStart;
            this.rowEnd = rowEnd;
        }
    }
}
