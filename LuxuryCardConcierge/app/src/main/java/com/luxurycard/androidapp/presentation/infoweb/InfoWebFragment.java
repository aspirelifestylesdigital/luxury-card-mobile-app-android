package com.luxurycard.androidapp.presentation.infoweb;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.luxurycard.androidapp.App;
import com.luxurycard.androidapp.R;
import com.luxurycard.androidapp.common.constant.AppConstant;
import com.luxurycard.androidapp.common.constant.ErrCode;
import com.luxurycard.androidapp.common.constant.IntentConstant;
import com.luxurycard.androidapp.presentation.base.BaseFragment;
import com.luxurycard.androidapp.presentation.base.BaseNavSubFragment;
import com.luxurycard.androidapp.presentation.home.HomeNavBottomActivity;
import com.luxurycard.androidapp.presentation.home.adapter.BaseParentFragment;
import com.luxurycard.androidapp.presentation.more.MoreFragment;
import com.support.mylibrary.widget.CustomWebView;

import butterknife.BindView;

public class InfoWebFragment extends BaseNavSubFragment implements InfoWeb.View {

    @BindView(R.id.pbLoading)
    ProgressBar pbLoading;
    @BindView(R.id.content)
    CustomWebView wbContent;

    @BindView(R.id.view_link_empty)
    View viewLinkEmpty;

    private InfoWebPresenter presenter;

    public static InfoWebFragment newInstance(AppConstant.MASTERCARD_COPY_UTILITY utilityType) {
        Bundle args = new Bundle();
        args.putSerializable(IntentConstant.MASTERCARD_COPY_UTILITY, utilityType);
        InfoWebFragment fragment = new InfoWebFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static InfoWebFragment newInstance(String titlePage, String linkUrl) {
        Bundle args = new Bundle();
        args.putString("info_title_page", titlePage);
        args.putString("info_link_url", linkUrl);
        InfoWebFragment fragment = new InfoWebFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private InfoWebPresenter buildPresenter() {
        return new InfoWebPresenter();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public int setupCreateViewLayoutId() {
        return R.layout.fragment_info_web;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        wbContent.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
        showViewLoading();
        wbContent.setListenerWebView(new CustomWebView.OnListenerWebView() {
            @Override
            public void showDialogLoading() {

            }

            @Override
            public void hideDialogLoading() {
                hideViewLoading();
            }
        });

        btnBack.setOnClickListener(view1 -> this.onBackPressed());

        presenter = buildPresenter();
        presenter.attach(this);

        if (getArguments() != null) {
            AppConstant.MASTERCARD_COPY_UTILITY type = (AppConstant.MASTERCARD_COPY_UTILITY) getArguments().getSerializable(IntentConstant.MASTERCARD_COPY_UTILITY);
            String titlePage = getArguments().getString("info_title_page","");
            String linkUrl = getArguments().getString("info_link_url","");
            if (type != null) {

                handleGetLink(type);

                //-- Track GA
                AppConstant.GA_TRACKING_SCREEN_NAME typeTrack = presenter.getTypeTrack(type);
                track(savedInstanceState, typeTrack);

            }else if(!TextUtils.isEmpty(titlePage) && !TextUtils.isEmpty(linkUrl)){

                handleGetLinkServer(titlePage, linkUrl);

                //-- Track GA
                AppConstant.GA_TRACKING_SCREEN_NAME titleTrack = presenter.getTypeTrack(titlePage);
                track(savedInstanceState, titleTrack);
            }
        }

    }

    private void track(Bundle savedInstanceState, AppConstant.GA_TRACKING_SCREEN_NAME type){
        if(type == null){
            return;
        }
        if(savedInstanceState == null){
            // Track GA
            App.getInstance().track(type.getValue());
        }
    }

    @Override
    public void onBackPressed() {
//        if(getActivity() instanceof HomeNavBottomActivity){
//            //check inside tab more show current tab More
//            ((HomeNavBottomActivity) getActivity()).resetViewCurrentTabFragment();
//        }
        super.onBackPressed();
    }

    private void handleGetLinkServer(String titlePage, String linkUrl) {
        setTitle(titlePage);
        wbContent.loadUrl(linkUrl);
    }

    private void handleGetLink(AppConstant.MASTERCARD_COPY_UTILITY type) {
        String title = presenter.getTitleOfType(type);
        setTitle(title);

        if (!TextUtils.isEmpty(title)) {

            if (!App.getInstance().hasNetworkConnection()) {
                showErrorMessage(ErrCode.CONNECTIVITY_PROBLEM);
            } else {

                String link = presenter.getLinkUrl(type);
                if (TextUtils.isEmpty(link)) {
                    wbContent.setVisibility(View.GONE);
                    viewLinkEmpty.setVisibility(View.VISIBLE);

                } else {
                    wbContent.loadUrl(link);
                }
            }
        }//-- do nothing
    }

    @Override
    public void onResume() {
        hideBottomMenuOnFragment();
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        if (wbContent != null) {
            wbContent.stopLoading();
        }
        showBottomMenuOnFragment();
        super.onStop();
    }

    @Override
    public void onDestroy() {
        presenter.detach();
        super.onDestroy();
    }

    @Override
    public void showErrorMessage(ErrCode errCode) {
        showErrorActivity(errCode);
    }

    private void showViewLoading() {
        if (pbLoading != null) {
            pbLoading.setVisibility(View.VISIBLE);
        }
    }

    private void hideViewLoading(){
        if (pbLoading != null && pbLoading.getVisibility() == View.VISIBLE) {
            pbLoading.setVisibility(View.GONE);
        }
    }
}
