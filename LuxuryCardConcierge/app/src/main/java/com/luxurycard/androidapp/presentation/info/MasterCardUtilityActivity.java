package com.luxurycard.androidapp.presentation.info;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.luxurycard.androidapp.R;
import com.luxurycard.androidapp.common.constant.AppConstant;
import com.luxurycard.androidapp.common.constant.IntentConstant;
import com.luxurycard.androidapp.presentation.base.BaseActivity;

public class MasterCardUtilityActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mastercard_copy_utility);
        // Get mastercard utility type
        AppConstant.MASTERCARD_COPY_UTILITY type = (AppConstant.MASTERCARD_COPY_UTILITY) getIntent().getSerializableExtra(IntentConstant.MASTERCARD_COPY_UTILITY);
        if(savedInstanceState == null) {
            MasterCardUtilityFragment masterCardUtilityFragment = MasterCardUtilityFragment
                    .newInstance(type);

            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragment_place_holder
                            , masterCardUtilityFragment
                            , MasterCardUtilityFragment.class.getSimpleName())
                    .commit();
        }


    }

    @Override
    public void onBackPressed() {
        finish();
    }

}
