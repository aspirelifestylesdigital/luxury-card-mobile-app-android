package com.luxurycard.androidapp.presentation.more;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.luxurycard.androidapp.App;
import com.luxurycard.androidapp.R;
import com.luxurycard.androidapp.common.constant.AppConstant;
import com.luxurycard.androidapp.presentation.base.BaseNavFragment;
import com.luxurycard.androidapp.presentation.home.HomeNavBottomActivity;
import com.luxurycard.androidapp.presentation.info.MasterCardUtilityFragment;
import com.luxurycard.androidapp.presentation.infoweb.InfoWebFragment;

import butterknife.BindView;
import butterknife.OnClick;

public class MoreFragment extends BaseNavFragment implements More.View{

    @BindView(R.id.tvDate)
    TextView tvDate;

    private MorePresenter presenter;

    public static MoreFragment newInstance() {
        Bundle args = new Bundle();
        MoreFragment fragment = new MoreFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private MorePresenter buildPresenter() {
        return new MorePresenter();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = buildPresenter();
        presenter.attach(this);
    }

    @Override
    public int setupCreateViewLayoutId() {
        return R.layout.fragment_more;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void track() {
        //dummy code here not track GA this page
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //set date
        presenter.setUpDate();
    }

    @Override
    public void onDestroy() {
        presenter.detach();
        super.onDestroy();
    }

    @Override
    public void showDate(String date) {
        tvDate.setText(date);
    }

    //<editor-fold desc="click item">

    @OnClick(R.id.tvMySettings)
    public void clickMySettings() {
        this.replaceChildFragmentTabCurrent(MySettingsFragment.newInstance());
    }

    @OnClick(R.id.tvRewards)
    public void clickRewards() {
        openInfoWeb(AppConstant.MASTERCARD_COPY_UTILITY.Rewards);
    }

    /* logic tab open web view*/
    /*private void hideColorTabMore() {
        if(getActivity() instanceof HomeNavBottomActivity){
            ((HomeNavBottomActivity) getActivity()).navBottomMenu.hideColorTabMore();
        }
    }

    public void showColorTabMore() {
        if(getActivity() instanceof HomeNavBottomActivity){
            ((HomeNavBottomActivity) getActivity()).navBottomMenu.showColorTabMore();
        }
    }*/

    @OnClick(R.id.tvAirport)
    public void clickAirport() {
        openInfoWeb(AppConstant.MASTERCARD_COPY_UTILITY.Airport);
    }

    @OnClick(R.id.tvHotel)
    public void clickHotel() {
        openInfoWeb(AppConstant.MASTERCARD_COPY_UTILITY.Hotel);
    }

    @OnClick(R.id.tvLuxuryMagazine)
    public void clickLuxuryMagazine() {
        openInfoWeb(AppConstant.MASTERCARD_COPY_UTILITY.Luxury);
    }

    @OnClick(R.id.tvAbout)
    public void clickAbout() {
        openMasterCardUtility(AppConstant.MASTERCARD_COPY_UTILITY.About);
    }

    @OnClick(R.id.tvTerm)
    public void clickTerm() {
        openMasterCardUtility(AppConstant.MASTERCARD_COPY_UTILITY.TermsOfUse);
    }

    @OnClick(R.id.tvPrivacyPolicy)
    public void clickPrivacyPolicy() {
        openMasterCardUtility(AppConstant.MASTERCARD_COPY_UTILITY.Privacy);
    }

    @OnClick(R.id.tvSignOut)
    public void clickSignOut() {
        if(getActivity() instanceof HomeNavBottomActivity){
            // Track "Sign out"
            //<editor-fold desc="Track">
            App.getInstance().track(AppConstant.GA_TRACKING_CATEGORY.SIGN_OUT.getValue(),
                    AppConstant.GA_TRACKING_ACTION.CLICK.getValue(),
                    AppConstant.GA_TRACKING_LABEL.SIGN_OUT.getValue());
            //</editor-fold>
            presenter.toSignOut((HomeNavBottomActivity)getActivity());
        }
    }

    private void openInfoWeb(AppConstant.MASTERCARD_COPY_UTILITY type) {
        if (getActivity() instanceof HomeNavBottomActivity) {
            ((HomeNavBottomActivity) getActivity()).openWebViewTabCurrent(InfoWebFragment.newInstance(type));
        }
    }

    private void openMasterCardUtility(AppConstant.MASTERCARD_COPY_UTILITY type) {
        replaceChildFragmentTabCurrent(MasterCardUtilityFragment.newInstance((type)));
    }

    //</editor-fold>
}
