package com.luxurycard.androidapp.presentation.selectcity;

import android.content.res.Resources;

import com.luxurycard.androidapp.App;
import com.luxurycard.androidapp.R;
import com.luxurycard.androidapp.common.constant.CityData;
import com.luxurycard.androidapp.datalayer.preference.PreferencesStorage;
import com.luxurycard.androidapp.datalayer.repository.PreferencesDataRepository;
import com.luxurycard.androidapp.domain.model.CityRViewItem;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.schedulers.Schedulers;

public class SelectCityPresenter implements SelectCity.Presenter {

    private SelectCity.View view;

    private CityRViewAdapter.CityRViewAdapterListener eventSelectCity;

    private List<CityRViewItem> cityItems = new ArrayList<>();

    private PreferencesStorage preferencesStorage;
    private PreferencesDataRepository preferencesDataRepository;

    public SelectCityPresenter(CityRViewAdapter.CityRViewAdapterListener eventSelectCity, PreferencesStorage preferencesStorage, PreferencesDataRepository preferencesDataRepository) {
        this.preferencesStorage = preferencesStorage;
        this.eventSelectCity = eventSelectCity;
        this.preferencesDataRepository = preferencesDataRepository;
    }

    @Override
    public void attach(SelectCity.View view) {
        this.view = view;
    }

    @Override
    public void detach() {
        this.view = null;
    }


    public CityRViewAdapter fakeCityData() {
        Resources res = App.getInstance().getResources();
        String[] cities = res.getStringArray(R.array.city_list);
        String[] citiesDescriptions = res.getStringArray(R.array.city_locate_list);
        for (int i = 0; i < cities.length; i++) {
            cityItems.add(new CityRViewItem(
                    cities[i], citiesDescriptions[i]));
        }
        return new CityRViewAdapter(cityItems, eventSelectCity);
    }


    public CityRViewItem getCityItemSelected(int pos) {
        return cityItems.get(pos);
    }

    @Override
    public void saveSelectCityServer(CityRViewItem cityItem) {
        if(!App.getInstance().hasNetworkConnection()) {
            view.showViewNoNetwork();
            return;
        }
        final String cityNameVal = cityItem.getCity();
        if(CityData.isCityInApp(cityNameVal)){
            view.showViewLoading();
            preferencesDataRepository.saveSelectCity(cityNameVal)
                    .andThen(Completable.create(e -> {
                        CityData.setSelectedCity(cityNameVal);
                        preferencesStorage.editor().selectedCity(cityNameVal).build().saveAsync();
                        e.onComplete();
                    }))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(new SaveSelectCityObserver(cityItem));
        }else{
            if(view != null){
                view.showViewGeneralError();
            }
        }
    }

    private final class SaveSelectCityObserver extends DisposableCompletableObserver {

        CityRViewItem cityItem;

        SaveSelectCityObserver(CityRViewItem cityItem) {
            this.cityItem = cityItem;
        }

        @Override
        public void onComplete() {
            if (view != null) {
                view.hideViewLoading();
                view.saveCitySuccessful(cityItem);
            }
            dispose();
        }

        @Override
        public void onError(Throwable e) {
            if (view != null) {
                view.hideViewLoading();
                view.showViewGeneralError();
            }
            dispose();
        }
    }
}
