package com.luxurycard.androidapp.presentation.selectcity;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.luxurycard.androidapp.App;
import com.luxurycard.androidapp.R;
import com.luxurycard.androidapp.common.constant.AppConstant;
import com.luxurycard.androidapp.common.constant.CityData;
import com.luxurycard.androidapp.common.constant.RequestCode;
import com.luxurycard.androidapp.datalayer.preference.PreferencesStorage;
import com.luxurycard.androidapp.datalayer.repository.PreferencesDataRepository;
import com.luxurycard.androidapp.domain.model.CityRViewItem;
import com.luxurycard.androidapp.presentation.base.BaseNavSubFragment;
import com.luxurycard.androidapp.presentation.explore.ExploreDTO.ExploreParam;
import com.luxurycard.androidapp.presentation.explore.ExploreFragment;
import com.luxurycard.androidapp.presentation.home.HomeNavBottomActivity;
import com.luxurycard.androidapp.presentation.homelist.HomeListFragment;

import butterknife.BindView;

/**
 * Created by vinh.trinh on 5/3/2017.
 */

public class SelectCityFragment extends BaseNavSubFragment implements SelectCity.View, CityRViewAdapter.CityRViewAdapterListener {

    @BindView(R.id.selection_recycle_view)
    RecyclerView selectCityRView;

    private SelectCityPresenter presenter;

    private CityRViewAdapter cityRViewAdapter;

    boolean isGoPageExplore = false;

    private SelectCityPresenter buildPresenter() {
        PreferencesStorage preferencesStorage = new PreferencesStorage(getContext());
        PreferencesDataRepository preferencesDataRepository = new PreferencesDataRepository(preferencesStorage);
        return new SelectCityPresenter(this, preferencesStorage, preferencesDataRepository);
    }

    /* true: go to page explore, otherwise back to page*/
    public static SelectCityFragment newInstance(boolean isGoPageExplore) {
        Bundle args = new Bundle();
        args.putBoolean("isGoPageExplore", isGoPageExplore);
        SelectCityFragment fragment = new SelectCityFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = buildPresenter();
        presenter.attach(this);
        if(savedInstanceState == null){
            // Track GA
            App.getInstance().track(AppConstant.GA_TRACKING_SCREEN_NAME.CITY_LIST.getValue());
        }
    }

    @Override
    public int setupCreateViewLayoutId() {
        return R.layout.common_recyclerview_activity_layout;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setTitle(R.string.title_choose_city);
        getBtnCallConcierge().setVisibility(View.VISIBLE);


        if (getArguments() != null) {
             this.isGoPageExplore = getArguments().getBoolean("isGoPageExplore", false);
        }


        //setup city data
        cityRViewAdapter = presenter.fakeCityData();

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        selectCityRView.setLayoutManager(layoutManager);
        selectCityRView.setAdapter(cityRViewAdapter);
        //selectCityRView.addItemDecoration(new DividerItemDecoration(this, R.drawable.line_divider_black));
        layoutManager.scrollToPosition(CityData.selectedCity());

        getButtonBack().setOnClickListener(view1 -> {
            this.onBackPressed();
        });

    }


    @Override
    public void onDestroy() {
        presenter.detach();
        super.onDestroy();
    }

    @Override
    public void onItemClick(int pos) {
        String currentCity = CityData.cityName();

        CityRViewItem itemSelected = presenter.getCityItemSelected(pos);

        // Track GA "select city"
        App.getInstance().track(AppConstant.GA_TRACKING_CATEGORY.CITY_SELECTION.getValue(),
                AppConstant.GA_TRACKING_ACTION.SELECT.getValue(),
                itemSelected.getCity());

        if (currentCity.equalsIgnoreCase(itemSelected.getCity())) {
            //same city do nothing here
            Log.d("SelectCityFragment", "onItemClick: same city");
            if(isGoPageExplore){
                toPageExploreHoldPreviousPage();
            } else {
                this.onBackPressed();
            }
        } else {
            presenter.saveSelectCityServer(itemSelected);
        }
    }

    @Override
    public void showViewNoNetwork() {
        showErrorNoNetwork();
    }

    @Override
    public void showViewLoading() {
        showLoading();
    }

    @Override
    public void hideViewLoading() {
        hideLoading();
    }

    @Override
    public void showViewGeneralError() {
        showGeneralErrorActivity();
    }

    @Override
    public void saveCitySuccessful(CityRViewItem cityItem) {
        if(getActivity() instanceof HomeNavBottomActivity) {
            HomeNavBottomActivity activity = (HomeNavBottomActivity) getActivity();
            if(isGoPageExplore){
                //-- hold page for back on previous from page Explore
                toPageExploreHoldPreviousPage();
            }else {
                //open from Page HomeList or Explore
                this.onBackPressed();
                handleTabHome(cityItem);
            }
        }else{
            this.onBackPressed();
        }
    }

    /* hold page for back on previous from page Explore */
    private void toPageExploreHoldPreviousPage(){
        if (getActivity() instanceof HomeNavBottomActivity) {
            ((HomeNavBottomActivity) getActivity()).toExploreFragment("");
        }
    }

    private void handleTabHome(CityRViewItem cityItem) {
        try {
            if (getParentFragment() != null) {
                // 1. reload page HomeListFragment
                // 2. reload page ExploreFragment

                Fragment fragment = getParentFragment().getChildFragmentManager().findFragmentByTag(HomeListFragment.class.getSimpleName());
                if(fragment instanceof HomeListFragment){
                    ((HomeListFragment) fragment).loadSelectCity(cityItem.getCity());
                }

                Fragment baseExploreFr = getParentFragment().getChildFragmentManager().findFragmentByTag(ExploreFragment.class.getSimpleName());
                if(baseExploreFr instanceof ExploreFragment){
                    ((ExploreFragment) baseExploreFr).reloadData(RequestCode.SELECT_CITY, null);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
