package com.luxurycard.androidapp.common.constant;

/**
 * Created by Den on 3/30/18.
 */

public interface RequestTypeConstant {
    String OPEN = "Open";
    String CLOSED = "Closed";
    String PENDING = "Pending";
    String OPEN_CANCELED = "OpenCanceled";
    String IN_PROGRESS = "In Progress";
    String NEW_REQUEST_PENDING = "New Pending Request";
}

