package com.luxurycard.androidapp.presentation.base;

import android.os.Bundle;

import com.luxurycard.androidapp.common.constant.ErrCode;
import com.luxurycard.androidapp.presentation.home.HomeNavBottomActivity;
import com.luxurycard.androidapp.presentation.widget.DialogHelper;

public class BaseNavFragment extends BaseFragment {

    @Override
    public int setupCreateViewLayoutId() {
        return 0; //-- dummy layoutID here. get id from extend from child class
    }

    //<editor-fold desc="Dialog">
    public void showGeneralErrorActivity() {
        showErrorActivity(ErrCode.GENERAL_ERROR);
    }

    public void showErrorNoNetwork(){
        showErrorActivity(ErrCode.CONNECTIVITY_PROBLEM);
    }

    public void showErrorActivity(ErrCode errCode) {
        showErrorActivity(errCode,null);
    }

    public void showErrorActivity(ErrCode errCode, String extraMsg) {
        if (getActivity() instanceof HomeNavBottomActivity) {
            ((HomeNavBottomActivity) getActivity()).showErrorDialog(errCode, extraMsg);
        }
    }

    public void showLoading() {
        if (getActivity() instanceof HomeNavBottomActivity) {
            ((HomeNavBottomActivity) getActivity()).showProgressDialog();
        }
    }

    public void hideLoading() {
        if (getActivity() instanceof HomeNavBottomActivity) {
            ((HomeNavBottomActivity) getActivity()).hideProgressDialog();
        }
    }

    public void callConcierge() {
        if (getActivity() instanceof HomeNavBottomActivity) {
            ((HomeNavBottomActivity) getActivity()).onCallConcierge();
        }
    }

    public DialogHelper getActionDialog(){
        if(getActivity() instanceof HomeNavBottomActivity){
            return ((HomeNavBottomActivity) getActivity()).mDialogHelper;
        }
        return null;
    }
    //</editor-fold>

    public void replaceChildFragmentTabCurrent(BaseNavSubFragment fragment){
        if(getActivity() instanceof HomeNavBottomActivity){
            ((HomeNavBottomActivity) getActivity()).replaceChildFragment(fragment);
        }
    }

    public void addChildFragmentTabCurrent(BaseNavSubFragment fragment){
        if(getActivity() instanceof HomeNavBottomActivity){
            ((HomeNavBottomActivity) getActivity()).addChildFragment(fragment);
        }
    }

    public void showBottomMenuOnFragment(){
        if(getActivity() instanceof HomeNavBottomActivity){
            ((HomeNavBottomActivity) getActivity()).showBottomMenu();
        }
    }

    public void hideBottomMenuOnFragment(){
        if(getActivity() instanceof HomeNavBottomActivity){
            ((HomeNavBottomActivity) getActivity()).hideBottomMenu();
        }
    }

    public void openNewRequestActivity(){
        openNewRequestDetailActivity(null);
    }

    public void openNewRequestDetailActivity(Bundle bundle){
        if(getActivity() instanceof HomeNavBottomActivity){
            ((HomeNavBottomActivity) getActivity()).openAskConciergeActivity(bundle);
        }
    }

    public void showDialogNewRequestSuccessActivity(){
        if(getActivity() instanceof HomeNavBottomActivity) {
            ((HomeNavBottomActivity) getActivity()).showDialogNewRequestSuccess();
        }
    }

    /** track GA*/
    public void track(){
       //dummy code here
    }

}
