package com.luxurycard.androidapp.presentation.profile;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.luxurycard.androidapp.App;
import com.luxurycard.androidapp.R;
import com.luxurycard.androidapp.common.constant.AppConstant;
import com.luxurycard.androidapp.common.constant.ErrCode;
import com.luxurycard.androidapp.common.constant.IntentConstant;
import com.luxurycard.androidapp.common.constant.ResultCode;
import com.luxurycard.androidapp.common.logic.Validator;
import com.luxurycard.androidapp.datalayer.preference.ForgotPasswordStorage;
import com.luxurycard.androidapp.domain.model.DetailForgotPassword;
import com.luxurycard.androidapp.domain.usecases.RetrievePassword;
import com.luxurycard.androidapp.presentation.base.BaseActivity;
import com.luxurycard.androidapp.presentation.base.CommonActivity;
import com.luxurycard.androidapp.presentation.checkout.SignInActivity;
import com.luxurycard.androidapp.presentation.request.ThankYouFragment;
import com.luxurycard.androidapp.presentation.widget.DialogHelper;
import com.luxurycard.androidapp.presentation.widget.ViewUtils;
import com.support.mylibrary.widget.ErrorIndicatorEditText;
import com.support.mylibrary.widget.LetterSpacingTextView;

import org.json.JSONException;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by vinh.trinh on 7/24/2017.
 */

public class ForgotPasswordActivity extends BaseActivity implements ForgotPassword.View {

    @BindView(R.id.edt_email)
    ErrorIndicatorEditText edtEmail;
    @BindView(R.id.content_wrapper)
    LinearLayout contentWrapper;
    @BindView(R.id.scrollView)
    ScrollView scroll;
    @BindView(R.id.btn_submit)
    Button btnSubmit;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.title)
    LetterSpacingTextView title;

    private ForgotPasswordPresenter presenter;
    private DialogHelper dialogHelper;
    private Validator validator;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        ButterKnife.bind(this);
        btnSubmit.setEnabled(false);
        final Handler handler = new Handler();
        presenter = new ForgotPasswordPresenter(new RetrievePassword());
        presenter.attach(this);
        dialogHelper = new DialogHelper(this);
        validator = new Validator();
        title.setText(getString(R.string.forgot_password_title));

        edtEmail.setOnFocusChangeListener((view1, b) -> {
            if (!b) {
                ViewUtils.hideSoftKey(view1);
            } else {
                new Thread(() -> {
                    try {
                        Thread.sleep(350);
                    } catch (InterruptedException e) {}
                    handler.post(() -> scroll.scrollTo(0, scroll.getBottom()));
                }).start();
            }
        });

        edtEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void afterTextChanged(Editable editable) {
                btnSubmit.setEnabled(editable.toString().length() > 0);
            }
        });
        if(savedInstanceState == null){
            // Track GA
            App.getInstance().track(AppConstant.GA_TRACKING_SCREEN_NAME.FORGOT_PASSWORD.getValue());
        }
    }

    @OnClick(android.R.id.home)
    public void onButtonBack() {
        onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            ViewUtils.hideSoftKey(edtEmail);
            super.onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.btn_submit)
    void onSubmit(View view) {
        ViewUtils.hideSoftKey(edtEmail);
        String email = edtEmail.getText().toString();
        edtEmail.setError(null);
        if (!validator.email(email)) {
            edtEmail.setError("");
            dialogHelper.profileDialog(getString(R.string.input_err_invalid_email), dialog -> {
                if(view != null) {
                    view.postDelayed(() -> invokeSoftKey(view), 100);
                }
            });
        } else {
            showProgressDialog();
            presenter.retrievePassword(email);
        }
    }

    @Override
    public void showProgressDialog() {
        dialogHelper.showProgress();
    }

    @Override
    public void dismissProgressDialog() {
        dialogHelper.dismissProgress();
    }

    @Override
    public void showErrorMessage(ErrCode errCode, String extraMsg) {
        if (dialogHelper.networkUnavailability(errCode, extraMsg)) return;
        if(errCode == ErrCode.API_ERROR) {
            dialogHelper.alert(getResources().getString(R.string.input_err_fields),
                    getResources().getString(R.string.input_err_invalid_email));
        } else {
            dialogHelper.showGeneralError();
        }
    }

    @Override
    public void showSuccessMessage() {
//        mDialogHelper.alert(null, getString(R.string.retrieve_password_message), dialog -> onBackPressed());

//        getSupportFragmentManager()
//                .beginTransaction()
//                .replace(R.id.holder, RetrievePasswordFragment.newInstance())
//                        .addToBackStack( RetrievePasswordFragment.class.getSimpleName())
//                .commit();
        saveForgotPassInfo(edtEmail.getText().toString());
        setResult(ResultCode.RESULT_OK, null);
        Intent intent = new Intent(this, RetrievePasswordActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
    }

    private void saveForgotPassInfo(String email) {
        ForgotPasswordStorage passwordStorage = new ForgotPasswordStorage(this);
        try {
            long timeLimit = 24 * 60 * 60000l; //24 hours
            Long timeExpiry = System.currentTimeMillis() + timeLimit;
            ArrayList<DetailForgotPassword> arrayList = passwordStorage.detailForgotPassword();
            DetailForgotPassword detailForgotPassword = new DetailForgotPassword(email, timeExpiry);
            if (arrayList == null) {
                arrayList = new ArrayList<>();
                arrayList.add(detailForgotPassword);
            } else {
                if (getIndexByProperty(email, arrayList) != -1 ) {
                    int index = getIndexByProperty(email, arrayList);
                    arrayList.set(index, detailForgotPassword);
                } else {
                    arrayList.add(detailForgotPassword);
                }
            }
            passwordStorage.editor().detailForgotPass(arrayList).build().save();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private int getIndexByProperty(String email, ArrayList<DetailForgotPassword> arrayList) {
        for (int i = 0; i < arrayList.size(); i++) {
            if (arrayList.get(i) !=null && arrayList.get(i).getEmail().equals(email)) {
                return i;
            }
        }
        return -1;// not there is list
    }

    void invokeSoftKey(View view) {
        if(edtEmail != null) {
            edtEmail.requestFocus();
            edtEmail.setCursorVisible(true);
            edtEmail.setSelection(edtEmail.getText().toString().length());
        }
        ViewUtils.invoSoftKey(view);
    }

//    @Override
//    public void onViewPositionChanged(float fractionAnchor, float fractionScreen) {
//        if(fractionScreen == 1.0) ViewUtils.hideSoftKey(edtEmail);
//        super.onViewPositionChanged(fractionAnchor, fractionScreen);
//    }

    @Override
    public void onBackPressed() {
        ViewUtils.hideSoftKey(getCurrentFocus());
        super.onBackPressed();
    }

//    @Override
//    public void toSignIn() {
//        setResult(ResultCode.RESULT_OK, null);
//        Intent intent = new Intent(this, SignInActivity.class);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//        startActivity(intent);
//    }
}
