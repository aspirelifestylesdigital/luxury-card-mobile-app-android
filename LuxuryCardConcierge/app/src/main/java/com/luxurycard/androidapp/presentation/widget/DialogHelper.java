package com.luxurycard.androidapp.presentation.widget;

import android.app.Activity;
import android.support.v7.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.view.MotionEvent;

import com.luxurycard.androidapp.R;
import com.luxurycard.androidapp.common.constant.ErrCode;
import com.luxurycard.androidapp.presentation.base.CommonActivity;
import com.luxurycard.androidapp.presentation.home.HomeNavBottomActivity;

/**
 * Created by vinh.trinh on 4/26/2017.
 */

public class DialogHelper {

    private Activity activity;
    private ProgressDialog progressDialog;

    public DialogHelper(Activity activity) {
        this.activity = activity;
    }

    public void alert(String title, String message) {
        alert(title, message,null);
    }

    public void alert(String title, String message, DialogInterface.OnDismissListener dismissListener) {
//        Dialog dialog = new MyDialogBuilder(activity, title, message)
//                .positiveText(R.string.text_ok)
//                .onDismiss(dismissListener)
//                .single(true)
//                .build();
//        dialog.show();


        AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setOnDismissListener(dismissListener);
        builder.setPositiveButton(R.string.text_ok, null);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public void action(String title, String message, String positiveText, String negativeText,
                       DialogInterface.OnClickListener takeAction) {

        action(title, message, positiveText, negativeText, takeAction, true);

    }

    public void action(String title, String message, String positiveText, String negativeText,
                       DialogInterface.OnClickListener takeAction, boolean isCancelOutside) {
//        Dialog dialog = new MyDialogBuilder(activity, title, message)
//                .positiveText(positiveText)
//                .negativeText(negativeText)
//                .onPositive(takeAction)
//                .build();
//        dialog.show();

        AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(positiveText, null);
        builder.setNegativeButton(negativeText, takeAction);
        builder.setCancelable(isCancelOutside);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }


    public void action(String title, String message, String positiveText,String neutralText , String negativeText,
                       DialogInterface.OnClickListener takeAction, DialogInterface.OnClickListener neutralAction, DialogInterface.OnClickListener cancelAction) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(negativeText, cancelAction);
        builder.setNeutralButton(neutralText, neutralAction);
        builder.setNegativeButton(positiveText, takeAction);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }

    public void action(String title, String message, String positiveText, String negativeText,
                       DialogInterface.OnClickListener takeAction, DialogInterface.OnClickListener cancelAction) {
//        Dialog dialog = new MyDialogBuilder(activity, title, message)
//                .positiveText(positiveText)
//                .negativeText(negativeText)
//                .onPositive(takeAction)
//                .onNegative(cancelAction)
//                .build();

//        dialog.show();
        AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(negativeText, cancelAction);
        builder.setNegativeButton(positiveText, takeAction);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }

    public void showProgress() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        } else {
            progressDialog = createProgressDialog();
        }
        progressDialog.show();
        progressDialog.setContentView(R.layout.layout_progress_loading);
    }

    public void dismissProgress() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    public MyDialogBuilder createBuilder(int title, int content, int positiveText, int negativeText) {
        return new MyDialogBuilder(activity, title, content)
                .positiveText(positiveText)
                .negativeText(negativeText);
    }

    public void showDialogNewRequestSuccess() {
        if (activity instanceof HomeNavBottomActivity) {
            String title = activity.getString(R.string.request_thank_you_title);
            String message = activity.getString(R.string.request_thank_you_message);

            action(title, message,
                    activity.getString(R.string.text_ok),
                    activity.getString(R.string.call_concierge),
                    (dialog, which) -> {
                        //--ok
                        dialog.dismiss();
                    },
                    (dialog, which) -> {
                        //--call
                        dialog.dismiss();
                        ((HomeNavBottomActivity) activity).onCallConcierge();
                    });
        }
    }

    public void showLeavingAlert(String url) {

        AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(R.string.text_alert);
        builder.setMessage(R.string.text_leaving_message);
        builder.setNegativeButton(R.string.text_yes_all_caps, (dialog, which) -> {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                    Uri.parse(url));
            activity.startActivity(browserIntent);
        });
        builder.setPositiveButton(R.string.text_cancel, null);
        builder.create().show();


    }

    public void showLeavingAlert(DialogInterface.OnClickListener onClickListener){
        AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(R.string.text_alert);
        builder.setMessage(R.string.text_leaving_message);
        builder.setPositiveButton(R.string.your_benefits, onClickListener);
        builder.setNegativeButton(R.string.text_cancel, null);
        builder.create().show();

    }

    public void profileDialog(String message, DialogInterface.OnDismissListener dismissListener) {
//        final Dialog customDialog = new Dialog(activity, R.style.AppDialogTheme);
//        customDialog.setContentView(R.layout.custom_alert_dialog);
//        TextView titleView = ButterKnife.findById(customDialog, R.id.title);
//        TextView messageView = ButterKnife.findById(customDialog, R.id.message);
//        Button positiveBtn = ButterKnife.findById(customDialog, R.id.positive_btn);
//        Button negativeBtn = ButterKnife.findById(customDialog, R.id.negative_btn);
//
//        positiveBtn.setText(R.string.text_ok);
//        titleView.setText(R.string.input_err_fields);
//        messageView.setText(message);
//        messageView.setGravity(Gravity.START);
//        negativeBtn.setVisibility(View.GONE);
//        positiveBtn.setBackground(null);
//        positiveBtn.setOnClickListener(v -> customDialog.dismiss());
//        customDialog.setCancelable(true);
//        customDialog.setCanceledOnTouchOutside(true);
//        customDialog.setOnDismissListener(dismissListener);
//        customDialog.show();



        AlertDialog.Builder builder =  new AlertDialog.Builder(activity, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(R.string.input_err_fields);
        builder.setMessage(message);
        builder.setPositiveButton(R.string.text_ok, null);
        builder.setCancelable(true);
        builder.setOnDismissListener(dismissListener);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();


    }

    private ProgressDialog createProgressDialog() {
        ProgressDialog progressDialog = new ProgressDialog(activity) {
            @Override
            public boolean onTouchEvent(@NonNull MotionEvent event) {
                if(!(activity instanceof CommonActivity)) return false;
                Rect homeUpRect = ((CommonActivity)activity).getHomeUpRect();
                if(homeUpRect != null && homeUpRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    activity.onBackPressed();
                }
                return false;
            }
        };
        progressDialog.getWindow().setDimAmount(0.2f);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        progressDialog.setIndeterminate(true);
        return progressDialog;
    }

    public boolean networkUnavailability(ErrCode errCode, String extraMessage) {
        if(errCode == ErrCode.CONNECTIVITY_PROBLEM || (extraMessage != null && extraMessage.contains("Unable to resolve host"))) {
//            Dialog dialog = new MyDialogBuilder(activity,"Cannot Get Data",
//                    "Turn on cellular data or use Wi-Fi to access Mobile Concierge.")
//                    .positiveText("SETTINGS")
//                    .negativeText("OK")
//                    .onPositive((dialogInterface, i) -> activity.startActivity(new Intent(Settings.ACTION_SETTINGS)))
//                    .onNegative((dialogInterface, i) -> dialogInterface.dismiss())
//                    .autoDismiss(false)
//                    .build();
//            dialog.show();
            AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.AppCompatAlertDialogStyle);
            builder.setTitle(R.string.can_not_get_data);
            builder.setMessage(R.string.turn_on_cellular);
            builder.setPositiveButton(R.string.text_setting, (dialog, which) -> {
                activity.startActivity(new Intent(Settings.ACTION_SETTINGS));
            });
            builder.setNegativeButton(R.string.text_ok, null);
            builder.create().show();
            return true;
        }
        return false;
    }

    public void showGeneralError() {

        AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(R.string.can_not_get_data);
        builder.setMessage(R.string.text_connection);
        builder.setPositiveButton(R.string.text_setting, (dialog, which) -> {
            activity.startActivity(new Intent(Settings.ACTION_SETTINGS));
        });
        builder.setNegativeButton(R.string.text_ok, null);
        builder.create().show();

//        Dialog dialog = new MyDialogBuilder(activity, "Cannot Get Data",
//                "An error has occurred. Check your internet settings or try again.")
//                .onPositive((dialog1, which) -> {
//                    // Go to wifi settings
//                    activity.startActivity(new Intent(Settings.ACTION_SETTINGS));
//                })
//                .onNegative((dialogInterface, i) -> dialogInterface.dismiss())
//                .positiveText("SETTINGS")
//                .negativeText("OK")
//                .autoDismiss(false)
//                .build();
//        dialog.show();
    }
}
