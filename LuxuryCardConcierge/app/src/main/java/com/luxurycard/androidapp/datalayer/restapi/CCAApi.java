package com.luxurycard.androidapp.datalayer.restapi;

import com.luxurycard.androidapp.datalayer.entity.b2ctile.B2CTilesRequest;
import com.luxurycard.androidapp.datalayer.entity.b2ctile.B2CTilesResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * for all other categories business except Dining
 */

public interface CCAApi {
    @POST("conciergecontent/GetTiles")
    Call<B2CTilesResponse> getTiles(@Body B2CTilesRequest body);
}
