package com.luxurycard.androidapp.presentation.cityguidecategory;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.luxurycard.androidapp.R;
import com.luxurycard.androidapp.common.constant.RequestCode;
import com.luxurycard.androidapp.domain.model.CityRViewItem;
import com.luxurycard.androidapp.domain.model.SubCategoryItem;
import com.luxurycard.androidapp.presentation.base.BaseNavSubFragment;
import com.luxurycard.androidapp.presentation.explore.ExploreDTO.ExploreParam;
import com.luxurycard.androidapp.presentation.explore.ExploreDTO.ExploreParamSubCategory;
import com.luxurycard.androidapp.presentation.explore.ExploreFragment;
import com.luxurycard.androidapp.presentation.selectcategory.SelectCategoryFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

import static com.luxurycard.androidapp.common.constant.IntentConstant.CATEGORY_ID;

public class SubCategoryFragment extends BaseNavSubFragment implements SubCategory.View, SubCategoryAdapter.OnCategoryItemClickListener {

    @BindView(R.id.selection_recycle_view)
    RecyclerView recyclerView;

    private SubCategoryPresenter presenter;
    private SubCategoryAdapter adapter;

    private SubCategoryPresenter buildPresenter() {
        return new SubCategoryPresenter();
    }

    public static SubCategoryFragment newInstance(int cityGuideCode) {
        Bundle args = new Bundle();
        args.putInt(CATEGORY_ID, cityGuideCode);
        SubCategoryFragment fragment = new SubCategoryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = buildPresenter();
        presenter.attach(this);
    }

    @Override
    public int setupCreateViewLayoutId() {
        return R.layout.common_recyclerview_activity_layout;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setTitle(R.string.city_guide_category_title);
        getBtnCallConcierge().setVisibility(View.VISIBLE);
        getButtonBack().setOnClickListener(view1 -> this.onBackPressed());

        adapter = new SubCategoryAdapter(new ArrayList<>(), this);
        recyclerView.setAdapter(adapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        if (getArguments() != null) {
            int categoryId = getArguments().getInt(CATEGORY_ID, 0);
            if (categoryId != 0) {
                presenter.getSubCategories(categoryId);
            }
        }

    }


    @Override
    public void onDestroy() {
        presenter.detach();
        super.onDestroy();
    }

    @Override
    public void showLoadingView() {
        showLoading();
    }

    @Override
    public void hideLoadingView() {
        hideLoading();
    }

    @Override
    public void updateSubCategoryRViewAdapter(List<SubCategoryItem> subCategoryItems) {
        if (adapter != null) {
            adapter.swapData(subCategoryItems);
        }
    }

    @Override
    public void onItemClick(int index) {

        // RequestCode.SELECT_SUB_CATEGORY, selectedCategory = category
        SubCategoryItem item = adapter.getItem(index);
        ExploreParam exploreParam = new ExploreParam(new ExploreParamSubCategory(index,
                item.getSubCategoryName(),
                item.getImageResources()));

        handleReturnResultSubCategory(exploreParam);
    }

    private void handleReturnResultSubCategory(ExploreParam exploreParam) {
        try {
            if (getParentFragment() != null) {

                /* case open this Fragment ( 2 case )
                 *  1. Explore > Select Category > SubCategory
                 *      => reload page ExploreFragment
                 *
                 *  2. Explore > SubCategory
                 *      => reload page ExploreFragment
                 *  */

                FragmentManager fragmentManager = getParentFragment().getChildFragmentManager();
                int sizeFragmentStack = fragmentManager.getBackStackEntryCount();

                // -2: (-1 current page + -1 previous page)
                if (sizeFragmentStack >= 2) {

                    Fragment fragmentPrevious = fragmentManager.getFragments().get(sizeFragmentStack - 2);
                    if(fragmentPrevious instanceof SelectCategoryFragment){

                        ((SelectCategoryFragment) fragmentPrevious).handleReturnResultCategory(exploreParam);

                    }else if(fragmentPrevious instanceof ExploreFragment){
                        ((ExploreFragment) fragmentPrevious).reloadData(RequestCode.SELECT_SUB_CATEGORY, exploreParam);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        //- final remove view Category current
        this.onBackPressed();
    }

}
