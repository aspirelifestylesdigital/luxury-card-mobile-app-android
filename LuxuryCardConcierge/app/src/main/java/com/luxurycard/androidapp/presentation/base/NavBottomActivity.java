package com.luxurycard.androidapp.presentation.base;


import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.text.TextUtils;
import android.util.Log;
import android.view.ViewGroup;

import com.luxurycard.androidapp.R;
import com.luxurycard.androidapp.common.constant.ErrCode;
import com.luxurycard.androidapp.presentation.widget.DialogHelper;
import com.luxurycard.androidapp.presentation.widget.ViewUtils;

import java.util.HashMap;
import java.util.Stack;

import static android.support.v4.app.FragmentManager.POP_BACK_STACK_INCLUSIVE;

public abstract class NavBottomActivity extends BaseActivity {

    static {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        }
    }

    public DialogHelper mDialogHelper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDialogHelper = new DialogHelper(this);
    }

    /** cache fragment when click back, use in cache data */
    public void addFragment(@NonNull FragmentManager fragmentManager, @NonNull BaseFragment fragment,
                                   int frameId, String tag, Boolean backStack){

        startFragment(false, fragmentManager, fragment, frameId, tag, backStack);

    }

    /** will reload all fragment when click back, use don't cache data */
    public void replaceFragment(@NonNull FragmentManager fragmentManager, @NonNull BaseFragment fragment,
                                   int frameId, String tag, Boolean backStack){

        startFragment(true, fragmentManager, fragment, frameId, tag, backStack);
    }

    /**
     * The {@code fragment} is replaced to the container view with id {@code frameId}. The operation is
     * performed by the {@code fragmentManager}.
     */
    private void startFragment(boolean isReplace, @NonNull FragmentManager fragmentManager, @NonNull BaseFragment fragment,
                              int frameId, String tag, Boolean backStack) {
        try {
            //check keyboard
            if (getCurrentFocus() != null) {
                ViewUtils.hideSoftKey(getCurrentFocus());
            }

            if (TextUtils.isEmpty(tag)) {
                tag = fragment.getClass().getSimpleName();
            }

            Fragment f = fragmentManager.findFragmentById(frameId);

            /* This check same fragment showing. */
            if (f != null && tag.equals(f.getTag())) {
                Log.i("TAG", "same fragment " + fragment);
                return;
            }

            boolean fragmentPopped = fragmentManager.popBackStackImmediate(tag, 0);
            /* Prevent duplicate fragment in back stack */
            if (fragmentPopped || fragmentManager.findFragmentByTag(tag) != null) {
                Log.i("TAG", tag + " popped");
                return;
            }

            FragmentTransaction ft = fragmentManager.beginTransaction();
            if (isReplace) {
                ft.replace(frameId, fragment, tag);
            } else {
                ft.add(frameId, fragment, tag);
            }

            if (backStack) {
                ft.addToBackStack(tag);
            }

            ft.commit();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
