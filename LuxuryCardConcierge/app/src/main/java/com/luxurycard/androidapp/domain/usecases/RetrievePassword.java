package com.luxurycard.androidapp.domain.usecases;

import com.luxurycard.androidapp.common.BackendException;
import com.luxurycard.androidapp.datalayer.entity.profile.ForgotPasswordRequest;
import com.luxurycard.androidapp.datalayer.entity.profile.ForgotPasswordResponse;
import com.luxurycard.androidapp.datalayer.retro2client.AppHttpClient;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by vinh.trinh on 7/24/2017.
 */

public class RetrievePassword extends UseCase<Void, String> {

    @Override
    Observable<Void> buildUseCaseObservable(String s) {
        return null;
    }

    @Override
    Single<Void> buildUseCaseSingle(String s) {
        return null;
    }

    @Override
    Completable buildUseCaseCompletable(String email) {
        return Completable.create(e -> {
            ForgotPasswordRequest body = new ForgotPasswordRequest(email);
            Call<ResponseBody> request = AppHttpClient.getInstance().userManagementApi().forgotPassword(body);
            Response<ResponseBody> response = request.execute();
            if(response.isSuccessful()) {
                ForgotPasswordResponse forgotPwdResponse = new ForgotPasswordResponse(response.body().string());
                if(forgotPwdResponse.success) e.onComplete();
                else e.onError(new BackendException(forgotPwdResponse.message));
            } else {
                e.onError(new Exception(response.errorBody().string()));
            }
        });
    }
}
