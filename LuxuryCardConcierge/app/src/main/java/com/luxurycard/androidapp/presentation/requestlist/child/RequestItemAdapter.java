package com.luxurycard.androidapp.presentation.requestlist.child;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.luxurycard.androidapp.R;
import com.luxurycard.androidapp.common.constant.RequestTypeConstant;
import com.luxurycard.androidapp.domain.model.RequestDetailData;
import com.luxurycard.androidapp.domain.model.RequestItemView;
import com.luxurycard.androidapp.presentation.requestdetail.RequestDetail;
import com.luxurycard.androidapp.presentation.widget.FooterLoaderAdapter;
import com.luxurycard.androidapp.presentation.widget.OnItemClickListener;

import org.json.JSONException;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by vinh.trinh on 8/31/2017.
 */

public class RequestItemAdapter extends FooterLoaderAdapter<RequestItemAdapter.ViewHolder> {

    protected enum TYPE { OPEN, CLOSE }
    private List<RequestItemView> dataView;
    private TYPE type;
    private RequestItemListener listener;

    private int selectedPosition = -1;

    RequestItemAdapter(Context context, TYPE type) {
        super(context);
        dataView = new ArrayList<>();
        this.type = type;
    }

    @Override
    public long getYourItemId(int position) {
        return position;
    }



    @Override
    public RequestItemAdapter.ViewHolder getYourItemViewHolder(LayoutInflater inflater, ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.request_list_item, parent, false));
    }

    @Override
    public void bindYourViewHolder(RecyclerView.ViewHolder holder, int position) {
        holder.itemView.setSelected(position == selectedPosition);
        RequestItemView datum = dataView.get(position);
        RequestItemAdapter.ViewHolder viewHolder = (ViewHolder) holder;
        String requestType =  datum.getRequestType().equalsIgnoreCase(RequestTypeConstant.OPEN_CANCELED)
                ? RequestTypeConstant.OPEN
                : datum.getRequestType();
        viewHolder.vTitle.setText("Status: "+ requestType);
        if (requestType.equals(RequestTypeConstant.PENDING)) {
            viewHolder.vRequestID.setText(RequestTypeConstant.NEW_REQUEST_PENDING);
        } else {
            viewHolder.vRequestID.setText("Request " + datum.requestDetailData.getEPCCaseIDCCaseID());
        }
        //viewHolder.vRequestType.setText(datum.requestType);
        viewHolder.vContent.setText(datum.requestDetailData.getStringDetailContent());


//        viewHolder.vDate.setText(datum.date);
//        viewHolder.vLabelDate.setText(datum.dateLabel);

            switch (datum.getStatus()) {
                case OPEN:
                    viewHolder.container.setVisibility(View.VISIBLE);
                    viewHolder.btnCancel.setVisibility(View.VISIBLE);
                    break;
                case CLOSED:
                    viewHolder.container.setVisibility(View.VISIBLE);
                    viewHolder.btnCancel.setVisibility(View.GONE);
                    break;
                case PENDING:
                    viewHolder.container.setVisibility(View.GONE);
                    break;
                case OPEN_CANCELED:
                    viewHolder.container.setVisibility(View.VISIBLE);
                    viewHolder.btnCancel.setVisibility(View.GONE);
                    break;
            }



//        if (datum.status == RequestItemView.STATUS.CLOSED) {
//            viewHolder.btnCancel.setVisibility(View.GONE);
//        } else {
//            if (datum.status == RequestItemView.STATUS.PENDING) {
//                viewHolder.container.setVisibility(View.GONE);
//            } else {
//                viewHolder.container.setVisibility(View.VISIBLE);
//            }
//        }

//        viewHolder..setVisibility(datum.status == RequestItemView.STATUS.PENDING ?
//                View.INVISIBLE : View.VISIBLE);
    }

    public void setItemClickListener(RequestItemListener listener) {
        this.listener = listener;
    }

    public void clear() {
        selectedPosition = -1;
        setItemCount(0);
        dataView.clear();
        notifyDataSetChanged();
    }

    public void append(List<RequestItemView> newData) {
        dataView.addAll(newData);
        updateItemCount();
        notifyDataSetChanged();
    }

    public void refreshDataCache(List<RequestItemView> newData) {
        selectedPosition = -1;
        dataView.clear();
        dataView.addAll(newData);
        updateItemCount();
        notifyDataSetChanged();
    }

    RequestItemView getItem(int pos) {
        return dataView.get(pos);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private View itemView;
        private TextView vTitle, vRequestID, vRequestType, vContent;
        private Button btnDetail, btnCancel;
        private LinearLayout container;

        ViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            vTitle = itemView.findViewById(R.id.tv_status);
            vRequestID = itemView.findViewById(R.id.tv_request_id);
            //vRequestType = itemView.findViewById(R.id.tv_request_type);
            vContent = itemView.findViewById(R.id.tv_content);
            btnCancel = itemView.findViewById(R.id.btn_cancel);
            btnDetail = itemView.findViewById(R.id.btn_detail);
            container = itemView.findViewById(R.id.linear_container);
//            vDate = itemView.findViewById(R.id.request_item_date);
//            vLabelDate = itemView.findViewById(R.id.label_above);
//            icon = itemView.findViewById(R.id.icon_arrow);


            btnDetail.setOnClickListener(v -> {
                if (listener != null)
                listener.onItemClick(getAdapterPosition());
            });

            btnCancel.setOnClickListener(v -> {
                if (listener != null) listener.onCancelClick(getAdapterPosition());
            });

        }
    }

    private void updateItemCount() {
        setItemCount(this.dataView.size());
    }

    public List<RequestItemView> getRequestItemView(){
        return dataView;
    }

    interface RequestItemListener extends OnItemClickListener{
        void onCancelClick(int pos);
    }

}
