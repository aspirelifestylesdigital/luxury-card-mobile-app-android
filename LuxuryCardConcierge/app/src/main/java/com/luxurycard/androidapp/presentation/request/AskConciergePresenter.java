package com.luxurycard.androidapp.presentation.request;

import android.text.TextUtils;

import com.luxurycard.androidapp.App;
import com.luxurycard.androidapp.common.constant.ErrCode;
import com.luxurycard.androidapp.datalayer.entity.askconcierge.ACResponse;
import com.luxurycard.androidapp.datalayer.preference.PreferencesStorage;
import com.luxurycard.androidapp.domain.model.Metadata;
import com.luxurycard.androidapp.domain.usecases.CreateRequest;
import com.luxurycard.androidapp.domain.usecases.GetAccessToken;
import com.luxurycard.androidapp.domain.usecases.LoadProfile;
import com.luxurycard.androidapp.presentation.widget.ViewUtils;

import org.json.JSONException;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by vinh.trinh on 5/16/2017.
 */

public class AskConciergePresenter implements CreateNewConciergeCase.Presenter {

    private CompositeDisposable compositeDisposable;
    private CreateNewConciergeCase.View view;
    private GetAccessToken getAccessToken;
    private CreateRequest askConcierge;
    private LoadProfile loadProfile;
    private Metadata metadata;
    private String transactionID;
    private boolean isDuplicate;

    AskConciergePresenter(PreferencesStorage ps, GetAccessToken at, LoadProfile lp, CreateRequest as) {
        this.compositeDisposable = new CompositeDisposable();
        this.getAccessToken = at;
        this.loadProfile = lp;
        this.askConcierge = as;
        try {
            this.metadata = ps.metadata();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    void setTransactionID(String val) {
        transactionID = val;
    }

    void setDuplicate(boolean val) {
        isDuplicate = val;
    }

    @Override
    public void attach(CreateNewConciergeCase.View view) {
        this.view = view;
    }

    @Override
    public void detach() {
        compositeDisposable.dispose();
        this.view = null;
    }

    @Override
    public void sendRequest(String content, boolean email, boolean phone) {
        if(!App.getInstance().hasNetworkConnection()) {
            view.showErrorDialog(ErrCode.CONNECTIVITY_PROBLEM, null);
            return;
        }
        view.showProgressDialog();
        compositeDisposable.add(
                Single.zip(loadProfile.buildUseCaseSingle(null), getAccessToken.execute(),
                        (profile, accessToken) -> {
                            CreateRequest.RequestContent rContent = new CreateRequest.RequestContent(
                                    content,
                                    email,
                                    phone);
                            CreateRequest.Params params = new CreateRequest.Params(
                                    accessToken,
                                    profile,
                                    metadata,
                                    rContent);
                            if(!TextUtils.isEmpty(transactionID) && !isDuplicate) {
                                params.transactionID(transactionID);
                                params.editType(CreateRequest.EDIT_TYPE.AMEND);
                            }
                            return params;
                        })
                        .flatMap(params -> askConcierge.execute(params))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new AskConciergeObserver()));
    }

    private final class AskConciergeObserver extends DisposableSingleObserver<ACResponse> {

        @Override
        public void onSuccess(ACResponse acResponse) {
            view.dismissProgressDialog();
            if(acResponse.getTransactionId() == null && acResponse.getStatus() == null) {
                view.showErrorDialog(ErrCode.UNKNOWN_ERROR, "Failed to retrieve your profile.");
            } else if(acResponse.getStatus().isSuccess()) {
                view.onRequestSuccessfullySent();
            } else {
                view.showErrorDialog(ErrCode.UNKNOWN_ERROR, acResponse.getStatus().getMessage());
            }
            dispose();
        }

        @Override
        public void onError(Throwable e) {
            view.dismissProgressDialog();
            view.showErrorDialog(ErrCode.UNKNOWN_ERROR, e.getMessage());
            dispose();
        }
    }
}
