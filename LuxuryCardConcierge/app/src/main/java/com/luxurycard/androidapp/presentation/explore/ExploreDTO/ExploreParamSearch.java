package com.luxurycard.androidapp.presentation.explore.ExploreDTO;

public final class ExploreParamSearch {

    public String term;
    public boolean withOffers = false;

    public ExploreParamSearch(String term, boolean withOffers) {
        this.term = term;
        this.withOffers = withOffers;
    }

}
