package com.luxurycard.androidapp.presentation.changepass;

import com.luxurycard.androidapp.App;
import com.luxurycard.androidapp.common.constant.ErrCode;
import com.luxurycard.androidapp.domain.model.Profile;
import com.luxurycard.androidapp.domain.usecases.LoadProfile;
import com.luxurycard.androidapp.domain.usecases.ResetPassword;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by vinh.trinh on 4/26/2017.
 */

public class ChangePasswordPresenter
        implements ChangePassword.Presenter {

    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private ChangePassword.View view;
    private ResetPassword changePassword;
    private LoadProfile loadProfile;
    private Profile profile;

    ChangePasswordPresenter(ResetPassword changePassword, LoadProfile loadProfile) {
        compositeDisposable = new CompositeDisposable();
        this.changePassword = changePassword;
        this.loadProfile = loadProfile;
    }

    @Override
    public void attach(ChangePassword.View view) {
        this.view = view;
    }

    @Override
    public void detach() {
        compositeDisposable.dispose();
        view = null;
    }

    @Override
    public void loadProfile() {
        compositeDisposable.add(loadProfile.param(null)
                .on(Schedulers.io(), AndroidSchedulers.mainThread())
                .execute(new ChangePasswordPresenter.LoadProfileObserver()));
    }

    @Override
    public void doChangePassword(String oldPassword, String newPassword) {
        if(!App.getInstance().hasNetworkConnection()) {
            view.showErrorDialog(ErrCode.CONNECTIVITY_PROBLEM, null);
            return;
        }
        view.showProgressDialog();
        compositeDisposable.add(
                changePassword.param(new ResetPassword.Params(profile.getEmail(),oldPassword,newPassword))
                                .on(Schedulers.io(), AndroidSchedulers.mainThread())
                                .execute(new DisposableCompletableObserver() {
                                    @Override
                                    public void onComplete() {
                                        view.dismissProgressDialog();
                                        view.showSuccessMessage();
                                    }

                                    @Override
                                    public void onError(Throwable e) {
                                        view.dismissProgressDialog();
                                        view.showErrorDialog(ErrCode.API_ERROR, e.getMessage());
                                    }
                                })
                       );

    }

    @Override
    public void abort() {
        compositeDisposable.clear();
    }

    private final class LoadProfileObserver extends DisposableSingleObserver<Profile> {

        @Override
        public void onSuccess(@NonNull Profile profile) {
            ChangePasswordPresenter.this.profile = profile;
        }

        @Override
        public void onError(@NonNull Throwable e) {
        }
    }


}
