package com.luxurycard.androidapp.datalayer.datasource;


import android.text.TextUtils;

import org.json.JSONException;
import org.json.JSONObject;

public class PreferenceData {

    public static final String  NA_VALUE = "na";

    public final String cuisine;
    public final String hotel;
    public final String vehicle;
    String cuisinePrefID;
    String hotelPrefID;
    String vehiclePrefID;
    public String extValue;
    public String selectCity;

    public PreferenceData(String cuisine, String hotel, String vehicle) {
        this.cuisine = cuisine;
        this.hotel = hotel;
        this.vehicle = vehicle;
    }

    public PreferenceData() {
        String temp = "";
        cuisine = temp;
        hotel = temp;
        vehicle = temp;
    }

    public PreferenceData(String jsonString) throws JSONException {
        JSONObject json = new JSONObject(jsonString);
        this.cuisine = json.getString("Cuisine");
        this.hotel = json.getString("Hotel");
        this.vehicle = json.getString("Vehicle");
        this.cuisinePrefID = json.optString("cID");
        this.hotelPrefID = json.optString("hID");
        this.vehiclePrefID = json.optString("vID");
        this.extValue = json.optString("extVal", "No");
        this.selectCity = json.optString("selectCity", "");
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject json = new JSONObject();
        json.put("Cuisine", cuisine);
        json.put("Hotel", hotel);
        json.put("Vehicle", vehicle);
        json.put("cID", cuisinePrefID);
        json.put("hID", hotelPrefID);
        json.put("vID", vehiclePrefID);
        json.put("extVal", extValue);
        json.put("selectCity", selectCity);
        return json;
    }

    void preferenceIDs(String cuisine, String hotel, String vehicle) {
        this.cuisinePrefID = cuisine;
        this.hotelPrefID = hotel;
        this.vehiclePrefID = vehicle;
    }

    public boolean hasToBeCreated() {
        return TextUtils.isEmpty(hotelPrefID) || (!NA_VALUE.equals(cuisine) && TextUtils.isEmpty(cuisinePrefID))
                || (!NA_VALUE.equals(vehicle) && TextUtils.isEmpty(vehiclePrefID));
    }

    public static PreferenceData plain() {
        return new PreferenceData(NA_VALUE, NA_VALUE, NA_VALUE);
    }

    public PreferenceData applyChanges(String cuisine, String hotel, String vehicle) {
        PreferenceData change = new PreferenceData(cuisine, hotel, vehicle);
        change.preferenceIDs(cuisinePrefID, hotelPrefID, vehiclePrefID);
        return change;
    }
}
