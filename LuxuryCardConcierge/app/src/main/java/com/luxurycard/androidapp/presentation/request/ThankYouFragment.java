package com.luxurycard.androidapp.presentation.request;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.luxurycard.androidapp.R;
import com.luxurycard.androidapp.presentation.base.BaseFragment;

import butterknife.OnClick;

/**
 * Created by vinh.trinh on 5/24/2017.
 */

public class ThankYouFragment extends BaseFragment {

    private ViewEventsListener listener;

    public static ThankYouFragment newInstance() {
        Bundle args = new Bundle();
        ThankYouFragment fragment = new ThankYouFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ViewEventsListener) {
            listener = (ViewEventsListener) context;
        } else {
            throw new ClassCastException(context.toString()
                    + " must implement ThankYouFragment.ViewEventsListener.");
        }
    }

    @Override
    public int setupCreateViewLayoutId() {
        return R.layout.fragment_ask_thankyou;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @OnClick(R.id.btn_another)
    public void anotherRequestClick(View view) {
        listener.anotherRequest();
    }

    @OnClick(R.id.btn_explore)
    public void exploreClick(View view) {
        listener.toExplore();
    }

    interface ViewEventsListener {
        void anotherRequest();
        void toExplore();
    }
}
