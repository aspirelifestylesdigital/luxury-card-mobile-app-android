package com.luxurycard.androidapp.presentation.home.adapter;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.luxurycard.androidapp.R;
import com.luxurycard.androidapp.presentation.base.BaseFragment;
import com.luxurycard.androidapp.presentation.base.BaseNavFragment;
import com.luxurycard.androidapp.presentation.home.HomeNavBottomActivity;
import com.luxurycard.androidapp.presentation.homelist.HomeListFragment;
import com.luxurycard.androidapp.presentation.infoaccount.InfoAccountFragment;
import com.luxurycard.androidapp.presentation.more.MoreFragment;
import com.luxurycard.androidapp.presentation.requestlist.parent.RequestFragment;

import butterknife.BindView;


/* only use for view pager with fragment */
public class BaseParentFragment extends BaseNavFragment {

    @BindView(R.id.fragment_container)
    FrameLayout frameLayout;

    int position = -1;

    public static BaseParentFragment newInstance(int position) {
        Bundle bundle = new Bundle(1);
        bundle.putInt("position", position);
        BaseParentFragment fragment = new BaseParentFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public int setupCreateViewLayoutId() {
        return R.layout.fragment_base_parent;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getArguments() != null) {
            position = getArguments().getInt("position", -1);
        }

        /* 4 fragment Root with track when user click on Bottom Menu*/
        switch (position) {
            case 0:
                replaceFragment(HomeListFragment.newInstance(), false);
                break;
            case 1:
                replaceFragment(RequestFragment.newInstance(), false);
                break;
            case 2:
                replaceFragment(InfoAccountFragment.newInstance(), false);
                break;
            case 3:
                replaceFragment(MoreFragment.newInstance(), false);
                break;
            default:
                replaceFragment(HomeListFragment.newInstance(), false);
                break;
        }
    }

    public void replaceFragment(BaseFragment fragment, boolean backStack) {
        showFragment(true, fragment, backStack);
    }

    public void addFragment(BaseFragment fragment, boolean backStack) {
        showFragment(false, fragment, backStack);
    }

    public void showFragment(boolean isReplace, BaseFragment fragment, boolean backStack) {
        if (getActivity() instanceof HomeNavBottomActivity) {
            HomeNavBottomActivity activity = (HomeNavBottomActivity) getActivity();
            int idFrameLayout = frameLayout.getId();
            String tag = fragment.getClass().getSimpleName();
            if (isReplace) {
                //-- replace fragment
                activity.replaceFragment(getChildFragmentManager(), fragment, idFrameLayout, tag, backStack);
            } else {
                //-- add fragment
                activity.addFragment(getChildFragmentManager(), fragment, idFrameLayout, tag, backStack);
            }
        }
    }
}
