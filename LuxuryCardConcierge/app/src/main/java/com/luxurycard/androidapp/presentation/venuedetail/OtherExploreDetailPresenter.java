package com.luxurycard.androidapp.presentation.venuedetail;

import com.luxurycard.androidapp.App;
import com.luxurycard.androidapp.datalayer.preference.PreferencesStorage;
import com.luxurycard.androidapp.domain.model.explore.OtherExploreDetailItem;
import com.luxurycard.androidapp.domain.usecases.GetContentFull;
import com.luxurycard.androidapp.presentation.widget.tooltip.TooltipView;
import com.luxurycard.androidapp.presentation.widget.tooltip.data.TooltipConstant;
import com.luxurycard.androidapp.presentation.widget.tooltip.data.TooltipData;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by vinh.trinh on 6/13/2017.
 */

public class OtherExploreDetailPresenter implements OtherExploreDetail.Presenter {

    private GetContentFull getContentFull;
    private OtherExploreDetail.View view;
    private CompositeDisposable disposables;
    private PreferencesStorage preferencesStorage;

    OtherExploreDetailPresenter(PreferencesStorage preferencesStorage, GetContentFull getContentFull) {
        disposables = new CompositeDisposable();
        this.getContentFull = getContentFull;
        this.preferencesStorage = preferencesStorage;
    }


    @Override
    public void attach(OtherExploreDetail.View view) {
        this.view = view;
    }

    @Override
    public void detach() {
        disposables.dispose();
        this.view = null;
    }

    @Override
    public void getContent(Integer contentId) {
        if(!App.getInstance().hasNetworkConnection()) {
            view.hideLoadingFragment();
            view.noInternetMessage();
            return;
        }
        view.showLoadingFragment();
        disposables.add(getContentFull.param(new GetContentFull.Params(contentId))
                .on(Schedulers.io(), AndroidSchedulers.mainThread())
                .execute(new OtherExploreDetailPresenter.GetContentFullObserver()));
    }

    private final class GetContentFullObserver extends DisposableSingleObserver<OtherExploreDetailItem> {

        @Override
        public void onSuccess(OtherExploreDetailItem exploreRViewItem) {
            view.onGetContentFullFinished(exploreRViewItem);
            dispose();
        }

        @Override
        public void onError(Throwable e) {
            view.hideLoadingFragment();
            dispose();
        }
    }

    @Override
    public void handleTooltip() {
        if (view != null) {
            if(TooltipData.isShouldShow(preferencesStorage, TooltipConstant.EXPLORE_DETAIL_BUTTON_BOOK)){
                TooltipView tooltipView = new TooltipView();
                tooltipView.showViewTop(view.getButtonBook(), TooltipConstant.EXPLORE_DETAIL_BUTTON_BOOK);
            }//else do nothing here
        }
    }


}
