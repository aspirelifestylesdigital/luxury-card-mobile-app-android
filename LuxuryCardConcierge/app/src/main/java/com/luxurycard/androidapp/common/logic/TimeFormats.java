package com.luxurycard.androidapp.common.logic;

import android.annotation.SuppressLint;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeFormats {

    public static String timeCurrentOfUser() throws Exception {
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("EEE, MMMM d");
        return sdf.format(new Date());
    }

}
