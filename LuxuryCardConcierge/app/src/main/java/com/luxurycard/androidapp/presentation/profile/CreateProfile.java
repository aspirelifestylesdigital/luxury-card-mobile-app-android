package com.luxurycard.androidapp.presentation.profile;

import com.luxurycard.androidapp.common.constant.ErrCode;
import com.luxurycard.androidapp.domain.model.Profile;
import com.luxurycard.androidapp.presentation.base.BasePresenter;

/**
 * Created by vinh.trinh on 4/27/2017.
 */

public interface CreateProfile {

    interface View {
        void showProfileCreatedDialog();
        void showErrorDialog(ErrCode errCode, String extraMsg);
        void showProgressDialog();
        void dismissProgressDialog();
    }

    interface Presenter extends BasePresenter<View> {
        void createProfile(Profile profile, String password);
        void abort();
    }

}
