package com.luxurycard.androidapp.common.logic;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by vinh.trinh on 4/28/2017.
 */

public final class Validator {
    public final int NAME_MIN_LENGTH = 2;
    public final int NAME_MAX_LENGTH = 25;
    public final int PHONE_MAX_LENGTH = 10;
    public final int EMAIL_MAX_LENGTH = 100;
    public final int ZIP_CODE_LENGTH_1 = 5;
    public final int ZIP_CODE_LENGTH_10 = 10;// include "-"
    public final String BIN_CODE = "545212";

    private final String EMAIL_REGEX = "(?:[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-zA-Z0-9-]*[a-zA-Z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";

    public boolean email(String email) {
        boolean isEmailValid = email.matches(EMAIL_REGEX);
        if(isEmailValid){
            if(email.contains(".")) {
                String passEmail = email.substring(email.lastIndexOf("."));
                if(passEmail.length()>4){
                    isEmailValid = false;
                }
            }
        }
        return isEmailValid ;
    }

    public boolean phone(String phone) {//length?
//        return phone.length() >= PHONE_MAX_LENGTH;
        Pattern pattern;
        Matcher matcher;
        if (phone != null) {
            final String SECRET_PATTERN = "^[0-9]*$";
            pattern = Pattern.compile(SECRET_PATTERN);
            matcher = pattern.matcher(phone);
            return matcher.matches() && phone.length() >= PHONE_MAX_LENGTH;
        }else {
            return false;
        }
    }

    public boolean name(String name) {
        return name.length() >= NAME_MIN_LENGTH;
    }

    public boolean password(String pwd, String confirmPwd) {
        return pwd.length() > 0 && confirmPwd.equals(pwd);
    }
    public boolean secretValidator(String secretWord) {
        /*Pattern pattern;
        Matcher matcher;
        if(secretWord!=null) {
            final String SECRET_PATTERN =
                    "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\\$%\\^&\\*\\[\\]\"\\';:_\\-<>\\., =\\+\\/\\\\]).{10,25}";
            pattern = Pattern.compile(SECRET_PATTERN);
            matcher = pattern.matcher(secretWord);
            return matcher.matches();
        } else {
          return false;
        }*/
        return secretWord.length() > 0 && secretWord.length() < 25;
    }

    public boolean specialChars(String specialChars){
        Pattern pattern;
        Matcher matcher;
        if(specialChars!=null ) {
            final  String SECRET_PATTERN =
                    "^[\\\\sa-zA-Z0-9- ]*$";
            pattern = Pattern.compile(SECRET_PATTERN);
            matcher = pattern.matcher(specialChars);
            return matcher.matches();
        } else {
            return false;
        }

    }

    public boolean zipCode(String zipCode) {//5 or 10
//        return zipCode.length() >= ZIP_CODE_LENGTH_1;
        if (zipCode.length() < ZIP_CODE_LENGTH_1) {
            return false;
        }
        Pattern pattern;
        Matcher matcher;
        if (zipCode != null) {
            final String SECRET_PATTERN = "^[\\\\sa-zA-Z0-9- ]*$";
            pattern = Pattern.compile(SECRET_PATTERN);
            matcher = pattern.matcher(zipCode);
            return matcher.matches() ;
        }else {
            return false;
        }
    }

    public boolean binCode(String binCode) {return  binCode.equals(BIN_CODE) ;}
}
