package com.luxurycard.androidapp.datalayer.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.luxurycard.androidapp.BuildConfig;

/**
 * Created by vinh.trinh on 6/19/2017.
 */
public class Member {
    @Expose
    @SerializedName("ConsumerKey")
    private String consumerKey;
    @Expose
    @SerializedName("Functionality")
    private String functionality;
    @Expose
    @SerializedName("Email")
    private String email;
    @Expose
    @SerializedName("FirstName")
    private String firstName;
    @Expose
    @SerializedName("LastName")
    private String lastName;
    @Expose
    @SerializedName("MobileNumber")
    private String phoneNumber;
    @Expose
    @SerializedName("Password")
    private String secretKey;
    @Expose
    @SerializedName("Salutation")
    private String salutation;
    @Expose
    @SerializedName("MemberDeviceID")
    private String memberDeviceID;
    @Expose
    @SerializedName("onlineMemberID")
    private String onlineMemberID;

    @Expose
    @SerializedName("PostalCode")
    private String zipCode;

    public Member(String email, String firstName, String lastName,
                  String phoneNumber, String secretKey, String salutation, String zipCode) {
        this.consumerKey = BuildConfig.WS_BCD_CONSUMER_KEY;
        this.functionality = "Registration";
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.secretKey = secretKey;
        this.salutation = salutation;
        this.zipCode = zipCode;
        this.memberDeviceID = BuildConfig.WS_BCD_MEMBER_DEVICE_ID;
    }

    public Member(String email, String firstName, String lastName, String phoneNumber,
                  String secretKey, String salutation, String zipCode, String onlineMemberID) {
        this.consumerKey = BuildConfig.WS_BCD_CONSUMER_KEY;
        this.functionality = "Registration";
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.secretKey = secretKey;
        this.salutation = salutation;
        this.zipCode = zipCode;
        this.memberDeviceID = BuildConfig.WS_BCD_MEMBER_DEVICE_ID;
        this.onlineMemberID = onlineMemberID;
    }
}