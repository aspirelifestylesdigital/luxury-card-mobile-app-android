package com.luxurycard.androidapp.domain.usecases;

import android.text.TextUtils;

import com.luxurycard.androidapp.datalayer.entity.askconcierge.ACRequest;
import com.luxurycard.androidapp.datalayer.entity.askconcierge.ACResponse;
import com.luxurycard.androidapp.domain.model.Metadata;
import com.luxurycard.androidapp.domain.model.Profile;
import com.luxurycard.androidapp.domain.repository.ACRepository;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by vinh.trinh on 5/17/2017.
 */

public class CreateRequest extends UseCase<ACResponse, CreateRequest.Params> {

    public enum EDIT_TYPE { AMEND, CANCEL }
    private ACRepository repository;

    public CreateRequest(ACRepository repository) {
        this.repository = repository;
    }

    @Override
    Observable<ACResponse> buildUseCaseObservable(Params params) {
        return null;
    }

    @Override
    Single<ACResponse> buildUseCaseSingle(Params params) {
        return Single.create(e -> {
//            ACRequest requestBody = createRequestBody(params);
            ACRequest requestBody = requestBody(params);
            Call<ACResponse> request = TextUtils.isEmpty(params.transactionID)? repository.createNewConciergeCase(requestBody) :
                                    repository.updateConciergeCase(requestBody);
            Response<ACResponse> response = request.execute();
            if(response.isSuccessful()) {
                e.onSuccess(response.body());
            } else {
                e.onError(new Exception(""));
            }
        });
    }

    @Override
    Completable buildUseCaseCompletable(Params params) {
        return null;
    }

    public Single<ACResponse> execute(Params params) {
        return params.profile == null ? Single.just(ACResponse.empty()) : buildUseCaseSingle(params);
    }

    private ACRequest requestBody(Params params) {
        return TextUtils.isEmpty(params.transactionID)? new ACRequest(params.accessToken, params.metadata.onlineMemberID, params.profile,
                params.content.content, buildPrefResponse(params)) :
                new ACRequest(params.accessToken, params.transactionID, params.metadata.onlineMemberID,
                        params.profile, params.content.content, buildPrefResponse(params), params.editType);
    }

    private String buildPrefResponse(Params params) {
        StringBuilder stringBuilder = new StringBuilder();

        boolean hasEmail = params.content.email, hasPhone = params.content.phone;
        if(hasEmail && hasPhone){
            stringBuilder.append("Email, Mobile");
        }else{
            if(hasEmail){
                stringBuilder.append("Email");
            }else{
                stringBuilder.append("Mobile");
            }
        }

        return stringBuilder.toString();
    }

    public static final class Params {

        public final String accessToken;
        public final Profile profile;
        public final Metadata metadata;
        public final RequestContent content;
        private String transactionID;
        private String editType;

        public Params(String accessToken, Profile profile, Metadata metadata, RequestContent requestContent) {
            this.accessToken = accessToken;
            this.profile = profile;
            this.metadata = metadata;
            this.content = requestContent;
        }

        public void transactionID(String transactionID) {
            this.transactionID = transactionID;
        }

        public void editType(EDIT_TYPE editType) {
            this.editType = editType.name();
        }
    }

    public static final class RequestContent {
        public final String content;

        public final boolean email;
        public final boolean phone;

        public RequestContent(String content, boolean email, boolean phone) {
            this.content = content;
            this.email = email;
            this.phone = phone;
        }
    }
}
