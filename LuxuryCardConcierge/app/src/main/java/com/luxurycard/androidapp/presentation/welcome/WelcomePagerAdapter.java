package com.luxurycard.androidapp.presentation.welcome;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.view.PagerAdapter;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.DrawableTypeRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;
import com.luxurycard.androidapp.App;
import com.luxurycard.androidapp.R;
import com.luxurycard.androidapp.common.glide.GlideHelper;
import com.luxurycard.androidapp.domain.model.GalleryViewPagerItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;

/**
 * Created by Den on 3/20/18.
 */

public class WelcomePagerAdapter extends PagerAdapter {


    private LayoutInflater mLayoutInflater;

    private List<GalleryViewPagerItem> pagerItems = new ArrayList<>();
    private Context context;

    public WelcomePagerAdapter(Context context, List<GalleryViewPagerItem> pagerItems) {
        this.context = context;
        this.pagerItems = pagerItems;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }



    public WelcomePagerAdapter(final Context context) {
        mLayoutInflater = LayoutInflater.from(context);
    }


    @Override
    public Object instantiateItem(final ViewGroup container, final int position) {
        final View view = mLayoutInflater.inflate(R.layout.welcome_item_layout, container, false);
        ImageView galleryImage = ButterKnife.findById(view, R.id.imageViewWelcome);
        //View vFirstLayer = ButterKnife.findById(view, R.id.background_first_layer);
        //View vSecondLayer = ButterKnife.findById(view, R.id.background_second_layer);
        GalleryViewPagerItem item = pagerItems.get(position);
//        GlideHelper.getInstance().loadImage(item.imageURL,
//                0, galleryImage, 0);


        DrawableTypeRequest drawableTypeRequest = Glide.with(App.getInstance().getApplicationContext()).load(item.imageURL);

        drawableTypeRequest.override(App.getInstance().getResources().getDisplayMetrics().widthPixels, Target.SIZE_ORIGINAL);



        drawableTypeRequest
                .asBitmap()
                .centerCrop()
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {



                        DisplayMetrics displayMetrics = App.getInstance().getApplicationContext().getResources().getDisplayMetrics();
                        int width = (int) (displayMetrics.widthPixels  * 0.76);

                        ViewGroup.LayoutParams params = galleryImage.getLayoutParams();
                        params.width = width;
                        params.height = resource.getHeight() * width / resource.getWidth();
                        galleryImage.requestLayout();

//                        ViewGroup.LayoutParams layerParams = vFirstLayer.getLayoutParams();
//                        layerParams.width = width;
//                        layerParams.height = resource.getHeight() * width / resource.getWidth();
//                        vFirstLayer.requestLayout();

//                        ViewGroup.LayoutParams layerSecondParams = vSecondLayer.getLayoutParams();
//                        layerSecondParams.width = width;
//                        layerSecondParams.height = resource.getHeight() * width / resource.getWidth();
//                        vSecondLayer.requestLayout();

                        galleryImage.setImageBitmap(resource);

                    }
                });

        container.addView(view);
        return view;
    }

    @Override
    public int getItemPosition(final Object object) {
        return POSITION_NONE;
    }

    @Override
    public int getCount() {
        return pagerItems.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void destroyItem(final ViewGroup container, final int position, final Object object) {
        container.removeView((View) object);
    }

}
