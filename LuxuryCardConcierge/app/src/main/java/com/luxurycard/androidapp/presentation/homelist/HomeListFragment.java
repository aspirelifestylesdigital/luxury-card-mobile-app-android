package com.luxurycard.androidapp.presentation.homelist;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.luxurycard.androidapp.App;
import com.luxurycard.androidapp.R;
import com.luxurycard.androidapp.common.constant.AppConstant;
import com.luxurycard.androidapp.common.constant.CityData;
import com.luxurycard.androidapp.datalayer.preference.PreferencesStorage;
import com.luxurycard.androidapp.datalayer.repository.B2CDataRepository;
import com.luxurycard.androidapp.datalayer.repository.ProfileDataRepository;
import com.luxurycard.androidapp.domain.model.HomeViewItem;
import com.luxurycard.androidapp.domain.repository.B2CRepository;
import com.luxurycard.androidapp.domain.repository.ProfileRepository;
import com.luxurycard.androidapp.domain.usecases.GetHomeList;
import com.luxurycard.androidapp.domain.usecases.LoadProfile;
import com.luxurycard.androidapp.presentation.base.BaseNavFragment;
import com.luxurycard.androidapp.presentation.selectcity.SelectCityFragment;
import com.luxurycard.androidapp.presentation.widget.ListItemDecoration;
import com.support.mylibrary.widget.ClickGuard;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by vinh.trinh on 5/3/2017.
 */

public class HomeListFragment extends BaseNavFragment implements HomeList.View, HomeListViewAdapter.OnHomeListListener {

    @BindView(R.id.home_list_root)
    CoordinatorLayout rootView;

    @BindView(R.id.tv_date)
    TextView tvDate;
    @BindView(R.id.tv_name_account)
    TextView tvNameAccount;

    @BindView(R.id.home_list_item)
    RecyclerView rcvView;

    @BindView(R.id.home_list_app_bar)
    AppBarLayout appBarLayout;

    @BindView(R.id.btn_choose_destination)
    Button btnChooseCity;

    @BindView(R.id.btn_home_call)
    FrameLayout frameCall;

    @BindView(R.id.frameBtnContainer)
    FrameLayout frameContainerButton;

    @BindView(R.id.collapsingToolbar)
    CollapsingToolbarLayout collapsingContainer;

    @BindView(R.id.linearHeaderTitles)
    LinearLayout linearHeaderTitles;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.imgCloseCity)
    ImageView imgCloseCity;

    @BindView(R.id.frameCloseCityContainer)
    FrameLayout frameCloseCityContainer;

    private HomeListPresenter presenter;
    private final String TAG = HomeListFragment.class.getSimpleName();
    private HomeListViewAdapter adapter;

    //<editor-fold desc="logic scroll toolbar with button">
    private int heightToolBar;
    private int fullWidthScreen;
    private int widthButtonCall;
    /** width margin left  8dp + right 8dp */
    private int widthSizeSpaceButtonCall;
    private int widthFrameChooseCityButton;
    //</editor-fold>

    private HomeListPresenter buildPresenter() {
        PreferencesStorage preferencesStorage = new PreferencesStorage(App.getInstance().getApplicationContext());
        ProfileRepository profileRepository = new ProfileDataRepository(preferencesStorage);
        B2CRepository b2CRepository = new B2CDataRepository();
        LoadProfile loadProfile = new LoadProfile(profileRepository);
        GetHomeList homeList = new GetHomeList(b2CRepository);
        return new HomeListPresenter(loadProfile, homeList);
    }

    public static HomeListFragment newInstance() {
        Bundle args = new Bundle();
        HomeListFragment fragment = new HomeListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = buildPresenter();
        presenter.attach(this);
    }

    @Override
    public int setupCreateViewLayoutId() {
        return R.layout.home_list_item_layout;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ClickGuard.guard(
                view.findViewById(R.id.btn_choose_destination),
                view.findViewById(R.id.btn_home_call),
                frameCloseCityContainer);

        if (adapter == null || adapter.getItemCount() == 0) {
            adapter = new HomeListViewAdapter(initHomeViewItem(), this);
        }
        rcvView.setLayoutManager(new LinearLayoutManager(getContext()));
        rcvView.addItemDecoration(new ListItemDecoration(getContext()));
        rcvView.setAdapter(adapter);

        setViewButtonScroll(view);

        showViewToolbarExpand();
        //set date
        presenter.setUpDate();
        //-- select city
        presenter.setSelectedCity(CityData.cityName());
        //set profile
        presenter.getProfileLocal();

        //-- default first time load Bottom Menu default Track Home
        if(savedInstanceState == null){
            // Track GA
            track();
        }
    }

    @Override
    public void track() {
        //- don't track GA in this func. (replace Fragment) run again lifecycle fragment
        //-- tracked onViewCreated()
        App.getInstance().track(AppConstant.GA_TRACKING_SCREEN_NAME.HOME.getValue());
    }

    public void reloadProfileLocal(){
        if (presenter != null) {
            boolean isHomeListShowing = isShowCurrentTab();
            presenter.resetProfileLocal(isHomeListShowing);
        }
    }

    //check current tab home, screen HomeList showing
    private boolean isShowCurrentTab() {
        boolean isHomeListShowing = false;
        try {
            if (getParentFragment() != null) {
                isHomeListShowing = getParentFragment().getChildFragmentManager().getBackStackEntryCount() == 0;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isHomeListShowing;
    }

    public void showViewToolbarExpand() {
        if (appBarLayout != null) {
            appBarLayout.setExpanded(true, false);
        }
    }

    //<editor-fold desc="logic scroll toolbar with button">
    private void setViewButtonScroll(View view) {
        Resources resources = view.getResources();
        DisplayMetrics displayMetrics = resources.getDisplayMetrics();
        fullWidthScreen = displayMetrics.widthPixels;
        frameCall.post(() -> widthButtonCall = frameCall.getWidth());
        frameContainerButton.post(() -> {
            widthFrameChooseCityButton = frameContainerButton.getWidth();
        });
        toolbar.post(() -> heightToolBar = toolbar.getHeight());

        //-- left(8dp) + right(8dp) = 16dp
        widthSizeSpaceButtonCall = (int) resources.getDimension(R.dimen.size_scale_button_call);
        //-- button with call = 14dp
        widthSizeSpaceButtonCall += (int) resources.getDimension(R.dimen.space_10);
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = true;
            int lastStatusSetWidth = 0;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                int valHeight = scrollRange + verticalOffset;

                try {
                    boolean checkIsShow = true;
                    if (scrollRange + verticalOffset == 0) {
                        isShow = false;
//                        Log.d(TAG, "isShow: No show");
                        if (lastStatusSetWidth == 0) {
                            int collageWidthButton = fullWidthScreen - widthButtonCall - widthSizeSpaceButtonCall;
                            animWidthSize(collageWidthButton);
//                            Log.d(TAG, "onOffsetChanged: 1");
                            lastStatusSetWidth = 1;
                        }
                        checkIsShow = false;

                    } else if (!isShow) {
//                        Log.d(TAG, "isShow: Show");
                        isShow = true;
                        if (lastStatusSetWidth == 1) {
                            animWidthSize(widthFrameChooseCityButton);
//                            Log.d(TAG, "onOffsetChanged: 2");
                            lastStatusSetWidth = 0;
                        }
                        checkIsShow = false;

                    }

                    if (checkIsShow) {
                        Log.d(TAG, "collapsingHeight: " + scrollRange + " linear: " + valHeight + " height: " + heightToolBar);
                        if (valHeight < heightToolBar) {
                            if (lastStatusSetWidth == 0) {
                                int collageWidthButton = fullWidthScreen - widthButtonCall - widthSizeSpaceButtonCall;
                                animWidthSize(collageWidthButton);
                                lastStatusSetWidth = 1;
//                                Log.d(TAG, "onOffsetChanged: 3");
                            }
                        } else {
                            if (lastStatusSetWidth == 1) {
                                animWidthSize(widthFrameChooseCityButton);
                                lastStatusSetWidth = 0;
//                                Log.d(TAG, "onOffsetChanged: 4");
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void animWidthSize(int toX) {
        ValueAnimator anim = ValueAnimator.ofInt(frameContainerButton.getWidth(), toX);
        anim.addUpdateListener(valueAnimator -> {
            int val = (Integer) valueAnimator.getAnimatedValue();
            ViewGroup.LayoutParams layoutParams = frameContainerButton.getLayoutParams();
            layoutParams.width = val;
            frameContainerButton.setLayoutParams(layoutParams);
        });
        anim.setDuration(70);
        anim.start();
    }
    //</editor-fold>

    @Override
    public void onDestroy() {
        presenter.detach();
        super.onDestroy();
    }

    private List<HomeViewItem> initHomeViewItem() {
        return presenter.getHomeViewItemDefault();
    }

    @Override
    public void showDate(String date) {
        tvDate.setText(date);
    }

    @Override
    public void showNameAccount(String nameAccount) {
        if (tvNameAccount != null) {
            tvNameAccount.setText(nameAccount);
        }
    }

    @OnClick(R.id.btn_home_call)
    public void onClickCallConcierge() {
        callConcierge();
    }

    @OnClick(R.id.btn_choose_destination)
    public void onClickChooseCity() {
        if (presenter != null) {
            presenter.toExploreFragment(getActivity(), "");
        }
    }

    @OnClick(R.id.frameCloseCityContainer)
    public void onClickCloseCity() {
        replaceChildFragmentTabCurrent(SelectCityFragment.newInstance(true));
    }

    @Override
    public void setTextButtonCity(String cityName, int idImgCloseCity) {
        if (btnChooseCity != null) {
            btnChooseCity.setText(cityName);
            imgCloseCity.setImageResource(idImgCloseCity);
            frameCloseCityContainer.setClickable(idImgCloseCity == R.drawable.ic_home_close);
        }
    }

    @Override
    public void showGeneralErrorDialog() {
        showGeneralErrorActivity();
    }

    @Override
    public void showListCity(List<HomeViewItem> homeListItem) {
        adapter.swapData(homeListItem);
    }

    public void loadSelectCity(String cityName) {
        if (presenter != null) {
            if(isShowCurrentTab()){
                //reload call api
                presenter.loadHomeList(cityName);
            }else {
                //wait reload when run open fragment HomeList
                if (adapter != null) {
                    adapter.clearAllData();
                }
            }
        }
    }

    @Override
    public void noNetwork() {
        showErrorNoNetwork();
    }

    @Override
    public void showLoadingPage() {
        showLoading();
    }

    @Override
    public void hideLoadingPage() {
        hideLoading();
    }

    @Override
    public void onHomeListInteraction(HomeViewItem item) {
        if (presenter != null) {
            presenter.onHomeItemClick(item, getActivity());
        }
    }
}
