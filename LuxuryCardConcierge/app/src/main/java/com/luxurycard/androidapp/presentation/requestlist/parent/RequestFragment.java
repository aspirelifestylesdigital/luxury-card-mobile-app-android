package com.luxurycard.androidapp.presentation.requestlist.parent;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.luxurycard.androidapp.App;
import com.luxurycard.androidapp.R;
import com.luxurycard.androidapp.common.constant.AppConstant;
import com.luxurycard.androidapp.datalayer.preference.PreferencesStorage;
import com.luxurycard.androidapp.datalayer.repository.ProfileDataRepository;
import com.luxurycard.androidapp.datalayer.repository.RequestRepository;
import com.luxurycard.androidapp.domain.model.RequestItemView;
import com.luxurycard.androidapp.domain.usecases.CreateRequest;
import com.luxurycard.androidapp.domain.usecases.GetAccessToken;
import com.luxurycard.androidapp.domain.usecases.GetRequestList;
import com.luxurycard.androidapp.domain.usecases.LoadProfile;
import com.luxurycard.androidapp.presentation.base.BaseNavFragment;
import com.luxurycard.androidapp.presentation.home.HomeNavBottomActivity;
import com.luxurycard.androidapp.presentation.requestlist.RequestList;
import com.luxurycard.androidapp.presentation.requestlist.RequestListPresenter;
import com.luxurycard.androidapp.presentation.requestlist.child.RequestsCloseFragment;
import com.luxurycard.androidapp.presentation.requestlist.child.RequestsOpenFragment;
import com.luxurycard.androidapp.presentation.widget.DialogHelper;
import com.luxurycard.androidapp.presentation.widget.tooltip.TooltipView;
import com.luxurycard.androidapp.presentation.widget.tooltip.data.TooltipConstant;
import com.luxurycard.androidapp.presentation.widget.tooltip.data.TooltipData;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;


public class RequestFragment extends BaseNavFragment implements RequestList.View{

    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.request_rdoGroup)
    RadioGroup rdoGroup;

    @BindView(R.id.rdBtnOpen)
    RadioButton rdoBtnOpen;

    @BindView(R.id.rdBtnClose)
    RadioButton rdoBtnClose;

    @BindView(R.id.btn_new_request)
    LinearLayout linearNewRequest;

    @BindView(R.id.viewpager)
    ViewPager viewPager;
    RequestsViewPagerAdapter vpAdapter;

    /* prevent load page request many time */
    boolean onTabRequestDuplicate = false;

    private RequestListPresenter presenter;

    /** true: is Tab Open, otherwise false: is Tab Close
     * default first time Tab Open */
    private boolean isTabOpen = true;

    public static RequestFragment newInstance() {
        Bundle args = new Bundle();
        RequestFragment fragment = new RequestFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private RequestListPresenter buildPresenter() {
        PreferencesStorage ps = new PreferencesStorage(App.getInstance().getApplicationContext());
        GetAccessToken getAccessToken = new GetAccessToken(ps);
        GetRequestList getRequestList = new GetRequestList(new RequestRepository());
        ProfileDataRepository profileDataRepository = new ProfileDataRepository(ps);
        LoadProfile lp = new LoadProfile(profileDataRepository);
        CreateRequest cr = new CreateRequest(new RequestRepository());

        return new RequestListPresenter(ps, getAccessToken, getRequestList,cr, lp);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        vpAdapter = new RequestsViewPagerAdapter(getChildFragmentManager());
        presenter = buildPresenter();
        presenter.attach(this);
    }

    @Override
    public void track() {
        // Track GA
        if(isTabOpen){
            App.getInstance().track(AppConstant.GA_TRACKING_SCREEN_NAME.OPEN_REQUESTS.getValue());
        }else{
            App.getInstance().track(AppConstant.GA_TRACKING_SCREEN_NAME.CLOSED_REQUESTS.getValue());
        }
    }

    @Override
    public int setupCreateViewLayoutId() {
        return R.layout.fragment_request;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        title.setText(getText(R.string.request_titles));
        setupViews();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        presenter.detach();
        super.onDestroy();
    }

    private void setupViews() {
        viewPager.setAdapter(vpAdapter);
        viewPager.setOffscreenPageLimit(vpAdapter.getCount());
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                if (presenter != null) {
                    isTabOpen = position == 0;
                    //- track GA
                    track();
                    presenter.switchList(isTabOpen);
                    if ((position == 0 && openCount() < 10)
                            || (position == 1 && closeCount() < 10)) {
                        presenter.fetchList();
                    }
                    rdoGroup.check(isTabOpen ? R.id.rdBtnOpen : R.id.rdBtnClose);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    @OnClick({R.id.rdBtnOpen, R.id.rdBtnClose})
    public void onClickTabOpenClose(View view) {
        if (viewPager == null)
            return;

        switch (view.getId()) {
            case R.id.rdBtnOpen:
                //check with current tab
                if (viewPager.getCurrentItem() == 0) {
                    Log.d("TAB", "onClickTabOpenOpen: in current");

                } else {
                    viewPager.setCurrentItem(0, true);
                }
                break;
            case R.id.rdBtnClose:
                if (viewPager.getCurrentItem() == 1) {
                    Log.d("TAB", "onClickTabOpenClose: in current");

                } else {
                    viewPager.setCurrentItem(1, true);
                }
                break;
            default:
                break;
        }
    }

    @OnClick(R.id.btn_new_request)
    public void onClickNewRequest(View view) {
        openNewRequestActivity();
    }

    @OnClick(R.id.btn_home_call)
    public void onClickCallConcierge(View view) {
        callConcierge();
    }

    public void showDialogNewRequestSuccess() {
        refreshLoadNewData();
        showDialogNewRequestSuccessActivity();
    }

    private void refreshLoadNewData() {
        if (vpAdapter == null) {
            return;
        }
        //reset all item old
        RequestsOpenFragment requestOpen = vpAdapter.getRequestOpen();
        if (requestOpen != null) {
            requestOpen.clear();
        }//-- do nothing here

        RequestsCloseFragment requestClose = vpAdapter.getRequestClose();
        if (requestClose != null) {
            requestClose.clear();
        }//-- do nothing here

        if (presenter != null) {
            presenter.refresh();
        }
    }

    @Override
    public void showProgressDialog() {
        showLoading();
    }

    @Override
    public void dismissProgressDialog() {
        hideLoading();
    }

    @Override
    public void showListLoadMore(boolean open) {
        boolean shouldShowViewProcess = false;
        if (open) {
            RequestsOpenFragment requestOpen = vpAdapter.getRequestOpen();
            if (requestOpen != null) {
                requestOpen.showLoadMore();
                shouldShowViewProcess = requestOpen.itemCount() == 0;
            }//-- do nothing here
        } else {
            RequestsCloseFragment requestClose = vpAdapter.getRequestClose();
            if (requestClose != null) {
                requestClose.showLoadMore();
                shouldShowViewProcess = requestClose.itemCount() == 0;
            }//-- do nothing here
        }

        if(shouldShowViewProcess){
            showProgressDialog();
        }
    }

    @Override
    public void hideListLoadMore(boolean open) {
        if (open) {
            RequestsOpenFragment requestOpen = vpAdapter.getRequestOpen();
            if (requestOpen != null) {
                requestOpen.stopLoadMore();
            }//-- do nothing here
        } else {
            RequestsCloseFragment requestClose = vpAdapter.getRequestClose();
            if (requestClose != null) {
                requestClose.stopLoadMore();
            }//-- do nothing here
        }
    }

    @Override
    public int openCount() {
        RequestsOpenFragment requestOpen = vpAdapter.getRequestOpen();
        if (requestOpen != null) {
            return requestOpen.itemCount();
        } else {
            return 0;
        }
    }

    @Override
    public int closeCount() {
        RequestsCloseFragment requestClose = vpAdapter.getRequestClose();
        if (requestClose != null) {
            return requestClose.itemCount();
        } else {
            return 0;
        }
    }

    @Override
    public void isRequestsEmpty() {
        RequestsOpenFragment requestOpen = vpAdapter.getRequestOpen();
        if (requestOpen != null) {
            int sizeOpen = requestOpen.itemCount();
            if(sizeOpen == 0){
                requestOpen.onViewEmptyData();
            }else {
                requestOpen.onViewHaveData();
            }
        }//-- do nothing here

        RequestsCloseFragment requestClose = vpAdapter.getRequestClose();
        if (requestClose != null) {
            int sizeOpen = requestClose.itemCount();
            if(sizeOpen == 0){
                requestClose.onViewEmptyData();
            }else {
                requestClose.onViewHaveData();
            }
        }//-- do nothing here
    }

    @Override
    public void resetViewRequestsData() {
        RequestsOpenFragment requestOpen = vpAdapter.getRequestOpen();
        if (requestOpen != null) {
            requestOpen.onViewHaveData();
        }//-- do nothing here

        RequestsCloseFragment requestClose = vpAdapter.getRequestClose();
        if (requestClose != null) {
            requestClose.onViewHaveData();
        }//-- do nothing here
    }

    @Override
    public void loadData(RequestListPresenter.RequestData data) {
        List<RequestItemView> openList = data.getOpenList();
        List<RequestItemView> closeList = data.getCloseList();
        RequestsOpenFragment requestOpen = vpAdapter.getRequestOpen();
        if (requestOpen != null) {
            requestOpen.loadList(openList);
        }//-- do nothing here

        RequestsCloseFragment requestClose = vpAdapter.getRequestClose();
        if (requestClose != null) {
            requestClose.loadList(closeList);
        }//-- do nothing here
    }

    @Override
    public void onFetchError() {
        showErrorNoNetwork();
    }

    @Override
    public void doneCancel() {
        Log.d("", "doneCancel: ");
        //-- move to close tab refresh data
        refreshLoadNewData();

        //- 5/10/2018 Track GA cancel request is request success
        // Track GA with "create request successfully"
        App.getInstance().track(AppConstant.GA_TRACKING_CATEGORY.REQUEST.getValue(),
                AppConstant.GA_TRACKING_ACTION.SUBMIT.getValue(),
                AppConstant.GA_TRACKING_LABEL.REQUEST_ACCEPTED.getValue());

    }

    @Override
    public void resetLoadDuplicate() {
        onTabRequestDuplicate = false;
    }

    public void onCancelClick(final RequestItemView data) {
        if (getActivity() instanceof HomeNavBottomActivity) {

            ((HomeNavBottomActivity) getActivity()).mDialogHelper.action(
                    "",
                    getString(R.string.confirm_cancel_request),
                    getString(R.string.call_concierge),
                    getString(R.string.cancel_anyway),
                    (dialog, which) -> callConcierge(),
                    (dialogInterface, i) -> presenter.cancelRequest(data.requestDetailData));

//            ((HomeNavBottomActivity) getActivity()).mDialogHelper.action("",
//                    getString(R.string.confirm_cancel_request),
//                    getString(R.string.text_continue_request),
//                    getString(R.string.text_call_concierge),
//                    getString(R.string.text_cancel_request),
//                    null,
//                    (dialog, which) -> callConcierge(),
//                    (dialogInterface, i) -> presenter.cancelRequest(data.requestDetailData));
        }

    }

    public void onHomeActivityRequestFragment(){
        if (presenter != null) {
            if (!onTabRequestDuplicate) {
                onTabRequestDuplicate = true;
                refreshLoadNewData();
            }
            presenter.handleTooltip();
        }
    }

    @Override
    public View getButtonNewRequest() {
        return linearNewRequest;
    }

    long threshold = 0;
    public void handleLoadRequestsMore() {
        long current = System.currentTimeMillis();
        if (current - threshold > 300) {
            threshold = current;
            if (presenter != null) {
                presenter.loadMore();
            }
        }
    }

}
