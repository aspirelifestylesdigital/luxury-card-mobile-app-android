package com.luxurycard.androidapp.datalayer.preference;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.luxurycard.androidapp.common.constant.SharedPrefConstant;
import com.luxurycard.androidapp.domain.model.DetailForgotPassword;

import org.json.JSONException;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Created by Den on 3/23/18.
 */

public class ForgotPasswordStorage {
    private Context context;
    private String jsonForgotPWD;

    public ForgotPasswordStorage(Context context) {
        this.context = context;
    }

    private ForgotPasswordStorage(Context context, String jsonForgotPWD) {
        this.context = context;
        this.jsonForgotPWD = jsonForgotPWD;
    }


    public ArrayList<DetailForgotPassword> detailForgotPassword() throws JSONException {
        SharedPreferences preferences = getInstance();
        Gson gson = new Gson();
        String json = preferences.getString(SharedPrefConstant.DETAIL_FORGOT_SECRET, null);
        Type type = new TypeToken<ArrayList<DetailForgotPassword>>() {}.getType();
        ArrayList<DetailForgotPassword> arrayList = gson.fromJson(json, type);
        return arrayList;
    }
    public void save() {
        SharedPreferences preferences = getInstance();
        SharedPreferences.Editor editor = preferences.edit();
        if(jsonForgotPWD != null) {
            editor.putString(SharedPrefConstant.DETAIL_FORGOT_SECRET, jsonForgotPWD);
        }
        editor.commit();
    }

    public Boolean isExpired(String email) {
        try {
            ArrayList<DetailForgotPassword> arrayList = detailForgotPassword();

            if (arrayList == null) {
                return false;
            } else {
                if (getIndexByProperty(email, arrayList) != -1 ) {
                    int index = getIndexByProperty(email, arrayList);
                    long currentTime = System.currentTimeMillis() - arrayList.get(index).getTime();
                    if (currentTime > 0) {
                        return  true;
                    } else {
                        arrayList.remove(index);
                        editor().detailForgotPass(arrayList).build().save();
                        return false;
                    }
                } else {
                    return false;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }

    private int getIndexByProperty(String email, ArrayList<DetailForgotPassword> arrayList) {
        for (int i = 0; i < arrayList.size(); i++) {
            if (arrayList.get(i) !=null && arrayList.get(i).getEmail().equals(email)) {
                return i;
            }
        }
        return -1;// not there is list
    }


    private SharedPreferences getInstance() {
        return PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
    }

    public ForgotPasswordStorage.Editor editor() {
        return new ForgotPasswordStorage.Editor(context);
    }

    public static class Editor {
        private Context context;
        private String jsonForgotPasswords;


        public Editor (Context context) {
            this.context = context;
        }

        public Editor detailForgotPass(ArrayList<DetailForgotPassword> arrayList) throws JSONException {
            Gson gson = new Gson();
            this.jsonForgotPasswords = gson.toJson(arrayList);
            return this;
        }

        public ForgotPasswordStorage build() {
            return new ForgotPasswordStorage(context, jsonForgotPasswords);
        }

    }
}
