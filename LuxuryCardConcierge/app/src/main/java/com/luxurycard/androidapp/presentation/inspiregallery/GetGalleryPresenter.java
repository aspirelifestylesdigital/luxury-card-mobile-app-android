package com.luxurycard.androidapp.presentation.inspiregallery;

import com.luxurycard.androidapp.App;
import com.luxurycard.androidapp.common.constant.ErrCode;
import com.luxurycard.androidapp.domain.model.GalleryViewPagerItem;
import com.luxurycard.androidapp.domain.usecases.GetGalleries;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by vinh.trinh on 6/13/2017.
 */

public class GetGalleryPresenter implements GetGallery.Presenter {

    private CompositeDisposable disposables;
    private GetGallery.View view;
    private GetGalleries getGalleries;

    GetGalleryPresenter(GetGalleries getGalleries) {
        disposables = new CompositeDisposable();
        this.getGalleries = getGalleries;
    }

    @Override
    public void attach(GetGallery.View view) {
        this.view = view;
    }

    @Override
    public void detach() {
        disposables.dispose();
        this.view = null;
    }

    @Override
    public void getGalleryList() {
        if(!App.getInstance().hasNetworkConnection()) {
            view.dismissProgressDialog();
            view.showErrorMessage(ErrCode.CONNECTIVITY_PROBLEM);
            return;
        }
        disposables
                .add(getGalleries.param(null)
                .on(Schedulers.io(), AndroidSchedulers.mainThread())
                .execute(new  GetGalleryObserver()));
    }

    private final class GetGalleryObserver extends DisposableSingleObserver<List<GalleryViewPagerItem>> {
        @Override
        public void onSuccess(List<GalleryViewPagerItem> exploreRViewItemList) {
            view.onGetGalleryListFinished(exploreRViewItemList);
            dispose();
        }

        @Override
        public void onError(Throwable e) {
            view.dismissProgressDialog();
            view.showErrorMessage(ErrCode.UNKNOWN_ERROR);
            dispose();
        }
    }
}
