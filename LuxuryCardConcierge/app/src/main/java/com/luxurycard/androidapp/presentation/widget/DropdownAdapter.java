package com.luxurycard.androidapp.presentation.widget;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.luxurycard.androidapp.R;
import com.support.mylibrary.widget.LetterSpacingTextView;

import java.util.List;

/**
 * Created by anh.trinh on 8/8/2017.
 */

public class DropdownAdapter
        extends ArrayAdapter<String> {
    private List<String> asr;
    LayoutInflater inflater;
    private Context context;

    public boolean isNone() {
        return isNone;
    }

    public void setNone(final boolean none) {
        isNone = none;
    }

    boolean isNone = true;

    public int getCurrentSelected() {
        return currentSelected;
    }

    public void setCurrentSelected(final int currentSelected) {
        this.currentSelected = currentSelected;
    }

    public int currentSelected = -1;

    public DropdownAdapter(@NonNull final Context context,
                           final int resource) {
        super(context,
                resource);
    }

    public DropdownAdapter(@NonNull final Context context,
                           final int resource,
                           @NonNull final List<String> objects, int currentSelected) {
        super(context,
                resource,
                objects);
        this.asr = objects;
        this.context = context;
        this.currentSelected = currentSelected;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }



    public DropdownAdapter(@NonNull final Context context,
                           final int resource,
                           @NonNull final List<String> objects, boolean isNone) {
        super(context,
                resource,
                objects);
        this.isNone = isNone;
        this.asr = objects;
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public View getView(final int position,
                        @Nullable View convertView,
                        @NonNull final ViewGroup parent) {

        convertView = LayoutInflater.from(getContext())
                .inflate(R.layout.item_dropdown,
                        parent,
                        false);
        LetterSpacingTextView txt = (LetterSpacingTextView) convertView.findViewById(R.id.title);
        View view = convertView.findViewById(R.id.view);
        txt.setGravity(Gravity.CENTER_VERTICAL);
        txt.setTextColor(Color.parseColor("#000000"));
        txt.setEllipsize(TextUtils.TruncateAt.MIDDLE);

        if (position == 0 && isNone) {
            txt.setText("None");
        } else {
            txt.setText(asr.get(position)
                    .toString());
            if (position == asr.size() - 1) {
                view.setVisibility(View.GONE);
            }
        }
        if (position == currentSelected) {
            if ((position > 0 || !isNone)) {
                txt.setTextColor(Color.BLACK);
                convertView.setBackgroundColor(Color.parseColor("#E5EBEE"));
            }
        }
        return convertView;
    }

    @Override
    public int getCount() {
        return asr.size();
    }
}
