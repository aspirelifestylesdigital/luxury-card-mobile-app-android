package com.luxurycard.androidapp.presentation.info;

import com.luxurycard.androidapp.common.constant.AppConstant;
import com.luxurycard.androidapp.common.constant.ErrCode;
import com.luxurycard.androidapp.datalayer.entity.GetClientCopyResult;
import com.luxurycard.androidapp.presentation.base.BasePresenter;

/**
 * Created by vinh.trinh on 6/13/2017.
 */

public interface MasterCardUtility {

    interface View {
        void onGetMasterCardCopy(GetClientCopyResult result);
        void loadEmptyContent();
        void showProcessLoading();
        void showErrorMessage(ErrCode errCode);
    }

    interface Presenter extends BasePresenter<View> {
        void getMasterCardCopy(String type);

        String getTitleOfType(AppConstant.MASTERCARD_COPY_UTILITY type);
    }

}
