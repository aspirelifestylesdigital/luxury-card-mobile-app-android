package com.luxurycard.androidapp.presentation.preferences;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.luxurycard.androidapp.R;
import com.luxurycard.androidapp.datalayer.datasource.PreferenceData;
import com.luxurycard.androidapp.presentation.base.BaseFragment;
import com.luxurycard.androidapp.presentation.widget.CustomSpinner;
import com.luxurycard.androidapp.presentation.widget.DropdownAdapter;
import com.support.mylibrary.widget.ButtonTextSpacing;
import com.support.mylibrary.widget.LetterSpacingTextView;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class UserPreferencesFragment extends BaseFragment {

    @BindView(R.id.content_wrapper_prefer)
    LinearLayout contentWrapper;
    @BindView(R.id.btn_update)
    ButtonTextSpacing btnUpdate;
    @BindView(R.id.btn_cancel)
    ButtonTextSpacing btnCancel;
    @BindView(R.id.tvSelectDining)
    LetterSpacingTextView tvSelectDining;
    @BindView(R.id.tvSelectHotel)
    LetterSpacingTextView tvSelectHotel;
    @BindView(R.id.tvSelectTransportation)
    LetterSpacingTextView tvSelectTransportation;

    private MyPreferences myPreferences;
    private PreferencesListener listener;

    private CustomSpinner mSpinnerDining;
    private CustomSpinner mSpinnerHotel;
    private CustomSpinner mSpinnerTransportation;

    public static UserPreferencesFragment newInstance() {
        Bundle args = new Bundle();
        UserPreferencesFragment fragment = new UserPreferencesFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof PreferencesListener) {
            listener = (PreferencesListener) context;
        } else {
            throw new ClassCastException(context.toString() + " must implement PreferencesListener.");
        }
    }

    @Override
    public int setupCreateViewLayoutId() {
        return R.layout.fragment_preferences;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadViewData();
        listener.onFragmentCreated();
    }

    private List<String> getCuisines() {
        return Arrays.asList(getResources().getStringArray(R.array.cate_cuisine));
    }

    private List<String> getVehicles() {
        return Arrays.asList(getResources().getStringArray(R.array.cate_preferred_vehicle));
    }

    private List<String> getHotels() {
        return Arrays.asList(getResources().getStringArray(R.array.cate_rating_hotels));
    }

    private CustomSpinner setUpCustomSpinner(TextView anchor, List<String> data,
                                             CustomSpinner.SpinnerListener spinnerListener) {
        CustomSpinner customSpinner = new CustomSpinner(getContext(), anchor);
        DropdownAdapter dropdownAdapter = new DropdownAdapter(getContext(), R.layout.item_dropdown, data, -1);
        customSpinner.setDropdownAdapter(dropdownAdapter);
        customSpinner.setmSpinnerListener(spinnerListener);
        int maxHeight = data.size() * 40;
        if (maxHeight < 200) {
            customSpinner.setPobpupHeight(maxHeight);
        } else {
            customSpinner.setPobpupHeight(200);
        }
        return customSpinner;
    }

    private void loadViewData() {
        myPreferences = new MyPreferences();
        String[] cuisines = getResources().getStringArray(R.array.cate_cuisine);
        String[] hotels = getResources().getStringArray(R.array.cate_rating_hotels);
        String[] vehicle = getResources().getStringArray(R.array.cate_preferred_vehicle);
        myPreferences.load(cuisines, hotels, vehicle);
        mSpinnerDining = setUpCustomSpinner(tvSelectDining,
                getCuisines(),
                new CustomSpinner.SpinnerListener() {
                    @Override
                    public void onArchorViewClick() {
                    }

                    @Override
                    public void onItemSelected(final int index) {
                        if (originalData() == null) return;
                        cuisineChanged = index != myPreferences.cuisine(originalData().cuisine);
                        enableButton(cuisineChanged || hotelChanged || vehicleChanged);
                    }
                });
        mSpinnerHotel = setUpCustomSpinner(tvSelectHotel,
                getHotels(),
                new CustomSpinner.SpinnerListener() {
                    @Override
                    public void onArchorViewClick() {
                    }

                    @Override
                    public void onItemSelected(final int index) {
                        if (originalData() == null) return;
                        hotelChanged = index != myPreferences.hotel(originalData().hotel);
                        enableButton(cuisineChanged || hotelChanged || vehicleChanged);
                    }
                });
        mSpinnerTransportation = setUpCustomSpinner(tvSelectTransportation,
                getVehicles(),
                new CustomSpinner.SpinnerListener() {
                    @Override
                    public void onArchorViewClick() {
                    }

                    @Override
                    public void onItemSelected(final int index) {
                        if (originalData() == null) return;
                        vehicleChanged = index != myPreferences.vehicle(originalData().vehicle);
                        enableButton(cuisineChanged || hotelChanged || vehicleChanged);
                    }
                });

    }

    private void enableButton(boolean isEnable) {
        if(btnCancel != null && btnUpdate != null) {
            btnUpdate.setEnabled(isEnable);
            btnCancel.setEnabled(isEnable);
        }
    }

    void load(PreferenceData data) {
        if (data.cuisine.equals(PreferenceData.NA_VALUE) && data.hotel.equals(PreferenceData.NA_VALUE) && data.vehicle.equals(PreferenceData.NA_VALUE)) {
            mSpinnerDining.setSelectionPosition(0);
            mSpinnerHotel.setSelectionPosition(0);
            mSpinnerTransportation.setSelectionPosition(0);
        } else {
            mSpinnerDining.setSelectionPosition(myPreferences.cuisine(data.cuisine));
            mSpinnerHotel.setSelectionPosition(myPreferences.hotel(data.hotel));
            mSpinnerTransportation.setSelectionPosition(myPreferences.vehicle(data.vehicle));
        }
        cuisineChanged = false;
        hotelChanged = false;
        vehicleChanged = false;
        enableButton(false);
    }

    public interface PreferencesListener {
        void onFragmentCreated();

        void onPreferencesSubmitted(String cuisine, String hotel, String transportation);

        void onPreferencesCancel();
    }

    @OnClick(R.id.btn_update)
    public void updateClick() {
        if (btnUpdate.isEnabled()) {
            listener.onPreferencesSubmitted(
                    myPreferences.cuisine(mSpinnerDining.getSelectionPosition()),
                    myPreferences.hotel(mSpinnerHotel.getSelectionPosition()),
                    myPreferences.vehicle(mSpinnerTransportation.getSelectionPosition())
            );
        }
    }

    @OnClick(R.id.btn_cancel)
    public void onCancel() {
        listener.onPreferencesCancel();
    }

    private boolean cuisineChanged = false, hotelChanged = false, vehicleChanged = false;

    private PreferenceData originalData() {
        return ((UserPreferencesActivity) getActivity()).original;
    }
}
