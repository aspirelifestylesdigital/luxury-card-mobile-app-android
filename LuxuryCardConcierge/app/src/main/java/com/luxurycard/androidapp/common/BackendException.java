package com.luxurycard.androidapp.common;

/**
 * Created by vinh.trinh on 8/23/2017.
 */

public class BackendException extends Exception {

    public BackendException(String message) {
        super(message);
    }
}
