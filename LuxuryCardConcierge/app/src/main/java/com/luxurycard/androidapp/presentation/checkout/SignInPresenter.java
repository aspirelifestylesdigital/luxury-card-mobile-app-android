package com.luxurycard.androidapp.presentation.checkout;

import com.luxurycard.androidapp.App;
import com.luxurycard.androidapp.common.BackendException;
import com.luxurycard.androidapp.common.constant.CityData;
import com.luxurycard.androidapp.common.constant.ErrCode;
import com.luxurycard.androidapp.datalayer.preference.PreferencesStorage;
import com.luxurycard.androidapp.domain.model.Profile;
import com.luxurycard.androidapp.domain.usecases.GetUserPreferences;
import com.luxurycard.androidapp.domain.usecases.LoadProfile;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by vinh.trinh on 4/26/2017.
 */

public class SignInPresenter implements SignIn.Presenter {

    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private SignIn.View view;
    private com.luxurycard.androidapp.domain.usecases.SignIn signIn;
    private LoadProfile loadProfile;
    private GetUserPreferences getUserPreferences;
    private PreferencesStorage preferencesStorage;

    SignInPresenter(com.luxurycard.androidapp.domain.usecases.SignIn signIn, LoadProfile loadProfile,
                    GetUserPreferences getUserPreferences,
                    PreferencesStorage preferencesStorage) {
        this.signIn = signIn;
        this.loadProfile = loadProfile;
        this.getUserPreferences = getUserPreferences;
        this.preferencesStorage = preferencesStorage;
    }

    @Override
    public void attach(SignIn.View view) {
        this.view = view;
    }

    @Override
    public void detach() {
        compositeDisposable.dispose();
        view = null;
    }

    @Override
    public void doSignIn(String email, String password) {
        if(!App.getInstance().hasNetworkConnection()) {
            view.showErrorDialog(ErrCode.CONNECTIVITY_PROBLEM, null);
            return;
        }
        view.showProgressDialog();
        signIn.buildUseCaseSingle(new com.luxurycard.androidapp.domain.usecases.SignIn.Params(email, password))
                .flatMap(accessToken -> loadProfile.buildUseCaseSingle(accessToken)
                        .zipWith(getUserPreferences.buildUseCaseSingle(accessToken), (profile, preferenceData) -> {
                            profile.locationToggle("Yes".equalsIgnoreCase(preferenceData.extValue));
                            PreferencesStorage.Editor editor = preferencesStorage.editor().profile(profile);
                            if (CityData.setSelectedCity(preferenceData.selectCity)) {
                                //save local
                                editor.selectedCity(preferenceData.selectCity);
                            }
                            editor.build().save();
                            return profile;
                }))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<Profile>() {
                    @Override
                    public void onSuccess(Profile profile) {
                        view.dismissProgressDialog();
                        view.proceedToHome();
                        dispose();
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.dismissProgressDialog();
                        if(e instanceof BackendException) {
                            view.showErrorDialog(ErrCode.API_ERROR, e.getMessage());
                        } else {
                            view.showErrorDialog(ErrCode.UNKNOWN_ERROR, e.getMessage());
                        }
                        dispose();
                    }
                });
    }

    @Override
    public void abort() {
        compositeDisposable.clear();
    }

}
