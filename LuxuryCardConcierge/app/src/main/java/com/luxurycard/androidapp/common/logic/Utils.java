package com.luxurycard.androidapp.common.logic;

import android.os.Build;

/**
 * Created by vinh.trinh on 5/15/2017.
 */

public final class Utils {

    private Utils() {

    }

    public static boolean isHigherThanM() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M;
    }

    public static boolean isHigherThanLolipop() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
    }
}
