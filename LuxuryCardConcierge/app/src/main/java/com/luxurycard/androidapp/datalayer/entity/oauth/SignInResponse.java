package com.luxurycard.androidapp.datalayer.entity.oauth;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by vinh.trinh on 7/18/2017.
 */

public class SignInResponse {

    public final Boolean hasForgotPassword;
    public final String onlineMemberID;
    public final String message;
    public final Boolean success;

    public SignInResponse(String rawJson) throws JSONException {
        JSONObject json = new JSONObject(rawJson);
        success = json.getBoolean("success");
        if(success) {
            hasForgotPassword = json.getBoolean("HasForgotPassword");
            onlineMemberID = json.getString("OnlineMemberID");
            message = null;
        } else {
            hasForgotPassword = false;
            onlineMemberID = null;
            JSONArray messages = json.optJSONArray("message");
            if(null != messages) {
                JSONObject error = messages.length() > 0 ? messages.getJSONObject(0) : null;
                if(error != null) {
                    String code = error.getString("code");
                    switch (code) {
                        case "ENR70-1":
                            message = "Please confirm that the value entered is correct.";
                            break;
                        default:
                            message = error.getString("message");
                            break;
                    }
                } else {
                    message = null;
                }
            } else {
                message = json.optString("message");
            }
        }
    }
}
