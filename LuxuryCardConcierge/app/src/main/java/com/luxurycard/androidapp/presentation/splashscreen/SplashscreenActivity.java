package com.luxurycard.androidapp.presentation.splashscreen;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.luxurycard.androidapp.App;
import com.luxurycard.androidapp.R;
import com.luxurycard.androidapp.common.constant.AppConstant;
import com.luxurycard.androidapp.datalayer.preference.PreferencesStorage;
import com.luxurycard.androidapp.presentation.base.BaseActivity;
import com.luxurycard.androidapp.presentation.checkout.SignInActivity;
import com.luxurycard.androidapp.presentation.home.HomeNavBottomActivity;
import com.luxurycard.androidapp.presentation.request.AskConciergeActivity;
import com.luxurycard.androidapp.presentation.welcome.WelcomeActivity;
import com.luxurycard.androidapp.presentation.widget.DialogHelper;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class SplashscreenActivity extends BaseActivity {

    @BindView(R.id.fullscreen_content)
    View contentView;
    DialogHelper dialogHelper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);
        ButterKnife.bind(this);

        /* Possible work around for market launches. See https://issuetracker.google.com/issues/36907463
            for more details. Essentially, the market launches the main activity on top of other activities.
            we never want this to happen. Instead, we check if we are the root and if not, we finish. */
        if (!isTaskRoot()) {
            final Intent intent = getIntent();
            if (intent != null && intent.hasCategory(Intent.CATEGORY_LAUNCHER) && Intent.ACTION_MAIN.equals(intent.getAction())) {
                //"Main Activity is not the root.  Finishing Main Activity instead of launching."
                finish();
                return;
            }
        }

        hide();
        dialogHelper = new DialogHelper(this);
        if (isRootedDevice()) {
            dialogHelper.alert(null, getString(R.string.text_device_rooted_error), dialogInterface -> finish());
        } else {
            navigate();
        }

        // Track GA
        App.getInstance().track(AppConstant.GA_TRACKING_SCREEN_NAME.SPLASH.getValue());

    }

    void navigate() {
        final Handler handler = new Handler();
        handler.postDelayed(() -> {
            PreferencesStorage preferencesStorage = new PreferencesStorage(SplashscreenActivity.this);
            new LoadDataWorker().load(preferencesStorage);
            boolean profileCreated = preferencesStorage.profileCreated();
            boolean hasForgotPwd = preferencesStorage.hasForgotPwd();
            Intent intent;

            if (profileCreated) {
                if (hasForgotPwd) {
                    intent = new Intent(this, WelcomeActivity.class);
                } else {
                    intent = new Intent(this, HomeNavBottomActivity.class);
                }
            } else {
                //-- don't have account local
//                if (hasForgotPwd) {
//                    intent = new Intent(this, SignInActivity.class);
//                } else {
                    intent = new Intent(this, WelcomeActivity.class);
//                }
            }
            startActivity(intent);
            finish();
        }, 3000);
    }

    private void hide() {
        // Hide UI first
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        if (contentView != null) {
            contentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        }
    }

    private boolean isRootedDevice() {
        boolean found = false;
        String[] places = {"/sbin/su", "/system/bin/su", "/system/xbin/su", "/system/xbin/sudo",
                "/data/local/xbin/su", "/data/local/bin/su",
                "/system/sd/xbin/su", "/system/bin/failsafe/su", "/data/local/su",
                "/magisk/.core/bin/su", "/su/bin/su"};
        for (String where : places) {
            if (new File(where).exists()) {
                found = true;
                break;
            }
        }
        return found;
    }
}