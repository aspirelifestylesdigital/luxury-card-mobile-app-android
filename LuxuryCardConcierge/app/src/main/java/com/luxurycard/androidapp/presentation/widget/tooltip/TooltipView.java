package com.luxurycard.androidapp.presentation.widget.tooltip;

import android.content.res.Resources;
import android.graphics.Typeface;
import android.support.v4.content.res.ResourcesCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.luxurycard.androidapp.App;
import com.luxurycard.androidapp.R;
import com.luxurycard.androidapp.presentation.widget.tooltip.data.TooltipConstant;
import com.luxurycard.androidapp.presentation.widget.tooltip.data.TooltipData;

import it.sephiroth.android.library.tooltip.Tooltip;

/**
 * Created on 4/15/18.
 */
public class TooltipView {

    private String TAG = TooltipView.class.getSimpleName();

    private Tooltip.Builder tooltipBuild;

    public TooltipView() {
        tooltipBuild = buildTooltipBuild();
    }

    private Tooltip.Builder buildTooltipBuild() {
        Tooltip.Callback eventTooltip = new Tooltip.Callback() {
            @Override
            public void onTooltipClose(Tooltip.TooltipView tooltipView, boolean b, boolean b1) {
                Log.d(TAG, "onTooltipClose: close");
            }

            @Override
            public void onTooltipFailed(Tooltip.TooltipView tooltipView) {
            }

            @Override
            public void onTooltipShown(Tooltip.TooltipView tooltipView) {
            }

            @Override
            public void onTooltipHidden(Tooltip.TooltipView tooltipView) {
            }
        };

        //-- set font
        Typeface typefaceFont = null;
        try {
            typefaceFont = ResourcesCompat.getFont(App.getInstance().getApplicationContext(), R.font.roboto_regular_font);
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }

        return new Tooltip.Builder()
                .closePolicy(new Tooltip.ClosePolicy()
                        .insidePolicy(true, false)
                        .outsidePolicy(true, false), 0L)
                .withOverlay(false)
                .typeface(typefaceFont)
                .withStyleId(R.style.ToolTipAppStyle)
                .withArrow(true)
                .withCallback(eventTooltip);
    }

    private void show(View view) {
        Tooltip.make(view.getContext(),tooltipBuild).show();
    }

    /**
     * @param tooltipConstant: value {@link TooltipConstant} */
    public void showViewTop(View view, String tooltipConstant) {
        String message = TooltipData.getMessage(tooltipConstant);

        if(TextUtils.isEmpty(message))
            return;

        tooltipBuild
                .anchor(view, Tooltip.Gravity.TOP)
                .text(message)
                .build();

        show(view);
    }

    /**
     * @param tooltipConstant: value {@link TooltipConstant} */
    public void showViewBottom(View view, String tooltipConstant) {
        String message = TooltipData.getMessage(tooltipConstant);

        if(TextUtils.isEmpty(message))
            return;

        tooltipBuild
                .anchor(view, Tooltip.Gravity.BOTTOM)
                .text(message)
                .build();

        show(view);
    }

}
