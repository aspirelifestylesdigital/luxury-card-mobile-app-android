package com.luxurycard.androidapp.datalayer.restapi;

import com.luxurycard.androidapp.datalayer.entity.b2cutility.B2CGetMasterCardCopyRequest;
import com.luxurycard.androidapp.datalayer.entity.b2cutility.B2CGetMasterCardCopyResponse;
import com.luxurycard.androidapp.datalayer.entity.search.SearchRequest;
import com.luxurycard.androidapp.datalayer.entity.search.SearchResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * for all other categories business except Dining
 */
public interface B2CUtilityApi {

    @POST("utility/GetClientCopy")
    Call<B2CGetMasterCardCopyResponse> getMasterCardCopy(@Body B2CGetMasterCardCopyRequest body);

    @POST("utility/SearchIAAndCCA")
    Call<SearchResponse> searchByKeyword(@Body SearchRequest body);
}
