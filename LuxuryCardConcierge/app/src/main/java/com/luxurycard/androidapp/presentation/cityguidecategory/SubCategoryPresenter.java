package com.luxurycard.androidapp.presentation.cityguidecategory;

import android.util.Log;

import com.luxurycard.androidapp.domain.model.SubCategoryItem;
import com.luxurycard.androidapp.domain.usecases.StaticCityGuideCategories;
import com.luxurycard.androidapp.presentation.explore.ExplorePresenter;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by tung.phan on 5/31/2017.
 */

public class SubCategoryPresenter implements SubCategory.Presenter {
    private static final String TAG = ExplorePresenter.class.getSimpleName();
    private CompositeDisposable disposables;
    private StaticCityGuideCategories staticCityGuideCategories;
    private SubCategory.View view;

    @Override
    public void attach(SubCategory.View view) {
        this.view = view;
        disposables = new CompositeDisposable();
        staticCityGuideCategories = new StaticCityGuideCategories();
    }

    @Override
    public void detach() {
        disposables.dispose();
        this.view = null;
    }

    @Override
    public void getSubCategories(int categoryId) {
        view.showLoadingView();
        disposables.add(staticCityGuideCategories.param(new StaticCityGuideCategories.Params(categoryId))
                .on(Schedulers.io(), AndroidSchedulers.mainThread())
                .execute(new GetSubCategoryObserver()));
    }

    private final class GetSubCategoryObserver extends DisposableSingleObserver<List<SubCategoryItem>> {

        @Override
        public void onSuccess(List<SubCategoryItem> subCategoryItems) {
            if (view != null) {
                view.hideLoadingView();
                view.updateSubCategoryRViewAdapter(subCategoryItems);
            }
        }

        @Override
        public void onError(Throwable e) {
            if (view != null) {
                view.hideLoadingView();
            }
            Log.e(TAG, e.getMessage());
        }
    }
}
