package com.luxurycard.androidapp.presentation.infoaccount;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.luxurycard.androidapp.App;
import com.luxurycard.androidapp.R;
import com.luxurycard.androidapp.common.constant.AppConstant;
import com.luxurycard.androidapp.common.constant.ErrCode;
import com.luxurycard.androidapp.presentation.base.BaseNavFragment;
import com.luxurycard.androidapp.presentation.infoweb.InfoWeb;
import com.luxurycard.androidapp.presentation.infoweb.InfoWebPresenter;
import com.support.mylibrary.widget.CustomWebView;

import butterknife.BindView;

public class InfoAccountFragment extends BaseNavFragment implements InfoWeb.View {

    @BindView(R.id.title)
    TextView tvTitle;
    @BindView(R.id.pbLoading)
    ProgressBar pbLoading;
    @BindView(R.id.content)
    CustomWebView tvContent;

    private InfoWebPresenter presenter;

    public static InfoAccountFragment newInstance() {
        Bundle args = new Bundle();
        InfoAccountFragment fragment = new InfoAccountFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private InfoWebPresenter buildPresenter() {
        return new InfoWebPresenter();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = buildPresenter();
        presenter.attach(this);
    }

    @Override
    public int setupCreateViewLayoutId() {
        return R.layout.fragment_info_account;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvTitle.setText(getContext().getString(R.string.info_account_title));
        tvContent.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));

        handleGetLink(AppConstant.MASTERCARD_COPY_UTILITY.Account);

    }

    @Override
    public void track() {
        // Track GA
        App.getInstance().track(AppConstant.GA_TRACKING_SCREEN_NAME.ACCOUNT_MANAGEMENT.getValue());
    }

    private void handleGetLink(AppConstant.MASTERCARD_COPY_UTILITY type) {
        if (!App.getInstance().hasNetworkConnection()) {
            showErrorMessage(ErrCode.CONNECTIVITY_PROBLEM);
        } else {
            String link = presenter.getLinkUrl(type);
            if (TextUtils.isEmpty(link)) {

            } else {
                tvContent.loadUrl(link);
            }
        }

    }

    @Override
    public void onDestroy() {
        presenter.detach();
        super.onDestroy();
    }

    @Override
    public void showErrorMessage(ErrCode errCode) {
        showErrorActivity(errCode);
    }
}
