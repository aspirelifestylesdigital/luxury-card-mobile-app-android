package com.luxurycard.androidapp.presentation.inspiregallery;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import com.luxurycard.androidapp.App;
import com.luxurycard.androidapp.R;
import com.luxurycard.androidapp.common.constant.AppConstant;
import com.luxurycard.androidapp.common.constant.ErrCode;
import com.luxurycard.androidapp.datalayer.repository.B2CDataRepository;
import com.luxurycard.androidapp.domain.model.GalleryViewPagerItem;
import com.luxurycard.androidapp.domain.usecases.GetGalleries;
import com.luxurycard.androidapp.presentation.base.BaseFragment;
import com.support.mylibrary.widget.CircleIndicator;

import java.util.List;

import butterknife.BindView;

/**
 * Created by vinh.trinh on 5/3/2017.
 */

public class InspireGalleryFragment extends BaseFragment implements GetGallery.View {

    @BindView(R.id.viewpager)
    ViewPager viewPager;
    @BindView(R.id.parent_view)
    FrameLayout parentView;
    @BindView(R.id.indicator)
    CircleIndicator indicator;
    @BindView(R.id.pbLoading)
    ProgressBar pbLoading;
    private final Integer LIMIT_PAGE = 15;

    GetGalleryPresenter presenter;
    List<GalleryViewPagerItem> galleryItemList;
    private InspireGalleryEventListener inspireGalleryEventListener;
    private int currentPosition = 0;

    private ViewPager.OnPageChangeListener onPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

        @Override
        public void onPageSelected(int position) {
            currentPosition = position;
            setTheHoldPageColor(position);
        }

        @Override
        public void onPageScrollStateChanged(int state) {}
    };

    public static InspireGalleryFragment newInstance() {
        Bundle args = new Bundle();
        InspireGalleryFragment fragment = new InspireGalleryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof InspireGalleryEventListener) {
            inspireGalleryEventListener = (InspireGalleryEventListener) context;
        } else {
            throw new ClassCastException(context.toString() + " must implement ProfileEventsListener.");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        presenter.detach();
    }

    @Override
    public void onResume() {
        super.onResume();
        consumeTheBackgroundColor();
    }

    private void consumeTheBackgroundColor() {
        if (galleryItemList != null) {
            //update toolbar + status bar color after back from other screen
            setTheHoldPageColor(viewPager.getCurrentItem());
        }
    }

    @Override
    public int setupCreateViewLayoutId() {
        return R.layout.fragment_inspiration_gallery;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter = new GetGalleryPresenter(new GetGalleries(new B2CDataRepository()));
        presenter.attach(this);
        // Begin call API
        presenter.getGalleryList();
    }

    private void setTheHoldPageColor(int position) {
        int color = galleryItemList.get(position).color;
        color = color != 0 ? color : ContextCompat.getColor(getActivity(), R.color.colorPrimary);
        inspireGalleryEventListener.onPageColorChanged(color);
        parentView.setBackgroundColor(color);
    }

    private void initViewPager() {
        InspireGalleryPagerAdapter pagerAdapter = new InspireGalleryPagerAdapter(getContext(), galleryItemList) {
            @Override
            public int getItemPosition(Object object) {
                return POSITION_NONE;
            }
        };
        viewPager.setAdapter(pagerAdapter);
        viewPager.addOnPageChangeListener(onPageChangeListener);
        //init indicator
        indicator.setViewPager(viewPager);
        pagerAdapter.registerDataSetObserver(indicator.getDataSetObserver());
        //set first page color
        setTheHoldPageColor(viewPager.getCurrentItem());
    }

    @Override
    public void onGetGalleryListFinished(List<GalleryViewPagerItem> galleryItemList) {
        parentView.setBackground(null);
        pbLoading.setVisibility(View.GONE);
        if (galleryItemList != null && galleryItemList.size() > 0) {
            viewPager.setVisibility(View.VISIBLE);
            indicator.setVisibility(View.VISIBLE);
            this.galleryItemList = galleryItemList;

            initViewPager();
            viewPager.setOffscreenPageLimit(LIMIT_PAGE);
        }
    }

    @Override
    public void dismissProgressDialog() {
        pbLoading.setVisibility(View.GONE);
    }

    @Override
    public void showErrorMessage(ErrCode errCode) {
//        if(errCode == ErrCode.CONNECTIVITY_PROBLEM)
//            ((HomeActivity)getActivity()).dialogHelper.networkUnavailability(ErrCode.CONNECTIVITY_PROBLEM,null);
//        else ((HomeActivity)getActivity()).dialogHelper.showGeneralError();
    }

    public interface InspireGalleryEventListener {
        void onPageColorChanged(int color);
    }

}
