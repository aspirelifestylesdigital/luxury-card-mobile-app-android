package com.luxurycard.androidapp.domain.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by vinh.trinh on 4/27/2017.
 */
public class Profile implements Parcelable {

    private String firstName;
    private String lastName;
    private String email;
    private String phone;
    private String salutation;
    private boolean locationOn;
    private String countryCode;
    private String zipCode;
    private String firstSixDigits;

    public boolean anyIsEmpty() {
        return firstName.length() == 0 || lastName.length() == 0 || email.length() == 0 ||
                phone.length() == 0 || zipCode.length() == 0;
    }

    public boolean allIsEmpty() {
        return firstName.length() == 0 && lastName.length() == 0 && email.length() == 0 &&
                phone.length() == 0 && zipCode.length() == 0 && firstSixDigits.length() == 0;
    }

    public boolean anyEmpty(String editTextPwd) {
        return editTextPwd.length() == 0 ||
                firstSixDigits.length() == 0 || anyIsEmpty();
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public boolean isLocationOn() {
        return locationOn;
    }

    public void locationToggle(boolean on) {
        this.locationOn = on;
    }

    public String getSalutation() {
        return salutation;
    }

    public void setSalutation(String salutation) {
        this.salutation = salutation;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getFirstSixDigits() {
        return firstSixDigits;
    }

    public void setFirstSixDigits(String firstSixDigits) {
        this.firstSixDigits = firstSixDigits;
    }

    public Profile() {
        String temp = "";
        firstName = temp;
        lastName = temp;
        email = temp;
        phone = temp;
        salutation = temp;
        countryCode = temp;
        zipCode = temp;
        firstSixDigits = temp;
    }

    public Profile(String jsonString) throws JSONException {
        JSONObject json = new JSONObject(jsonString);
        this.firstName = json.getString("firstName");
        this.lastName = json.getString("lastName");
        this.email = json.getString("email");
        this.phone = json.getString("phone");
        this.salutation = json.getString("salutation");
        this.locationOn = json.optBoolean("locationToggle");
        this.countryCode = json.optString("countryCode");
        this.zipCode = json.optString("postalCode");
        this.firstSixDigits = json.optString("firstSixDigits");
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject json = new JSONObject();
        json.put("firstName", firstName);
        json.put("lastName", lastName);
        json.put("email", email);
        json.put("phone", phone);
        json.put("salutation", salutation);
        json.put("locationToggle", locationOn);
        json.put("countryCode", countryCode);
        json.put("postalCode", zipCode);
        json.put("firstSixDigits", firstSixDigits);
        return json;
    }

    private Profile(Parcel in) {
        firstName = in.readString();
        lastName = in.readString();
        email = in.readString();
        phone = in.readString();
        locationOn = in.readInt() == 1;
        countryCode = in.readString();
        zipCode = in.readString();
        firstSixDigits = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(firstName);
        dest.writeString(lastName);
        dest.writeString(email);
        dest.writeString(phone);
        dest.writeInt(locationOn ? 1 : 0);
        dest.writeString(countryCode);
        dest.writeString(zipCode);
        dest.writeString(firstSixDigits);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Profile> CREATOR = new Creator<Profile>() {
        @Override
        public Profile createFromParcel(Parcel in) {
            return new Profile(in);
        }

        @Override
        public Profile[] newArray(int size) {
            return new Profile[size];
        }
    };
}
