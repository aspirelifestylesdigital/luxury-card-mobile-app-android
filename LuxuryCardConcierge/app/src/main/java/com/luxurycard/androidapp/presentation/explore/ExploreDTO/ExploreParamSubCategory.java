package com.luxurycard.androidapp.presentation.explore.ExploreDTO;

public final class ExploreParamSubCategory {

    public int subCategoryIndex = 0;
    public String subCategoryName;
    public int subCategoryResId = 0;

    public ExploreParamSubCategory(int subCategoryIndex, String subCategoryName, int subCategoryResId) {
        this.subCategoryIndex = subCategoryIndex;
        this.subCategoryName = subCategoryName;
        this.subCategoryResId = subCategoryResId;
    }
}
