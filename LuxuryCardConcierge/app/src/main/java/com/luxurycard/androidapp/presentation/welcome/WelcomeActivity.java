package com.luxurycard.androidapp.presentation.welcome;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import com.gigamole.infinitecycleviewpager.HorizontalInfiniteCycleViewPager;
import com.luxurycard.androidapp.App;
import com.luxurycard.androidapp.R;
import com.luxurycard.androidapp.common.constant.AppConstant;
import com.luxurycard.androidapp.common.constant.ErrCode;
import com.luxurycard.androidapp.datalayer.repository.B2CDataRepository;
import com.luxurycard.androidapp.domain.model.GalleryViewPagerItem;
import com.luxurycard.androidapp.domain.usecases.GetGalleries;
import com.luxurycard.androidapp.presentation.base.BaseActivity;
import com.luxurycard.androidapp.presentation.checkout.SignInActivity;
import com.luxurycard.androidapp.presentation.profile.SignUpActivity;
import com.luxurycard.androidapp.presentation.widget.DialogHelper;
import com.support.mylibrary.widget.CircleIndicator;
import com.support.mylibrary.widget.LetterSpacingTextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class WelcomeActivity extends BaseActivity implements Welcome.View {

    @BindView(R.id.infiniteCycleView)
    HorizontalInfiniteCycleViewPager viewPager;
    @BindView(R.id.ciWelcome)
    CircleIndicator indicator;
    @BindView(R.id.pbLoading)
    ProgressBar pbLoading;
    @BindView(R.id.tvWelcomeDescription)
    LetterSpacingTextView tvWelcomeDescription;
    @BindView(R.id.tvWelcomeTitle)
    LetterSpacingTextView tvWelcomeTitle;

    WelcomePagerAdapter pagerAdapter;

    WelcomePresenter presenter;
    List<GalleryViewPagerItem> galleryItemList;

    public DialogHelper dialogHelper;
    int viewPagerPosition = 0;

    private ViewPager.OnPageChangeListener pageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            updateContent(position);
        }

        @Override
        public void onPageScrollStateChanged(int state) {
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        ButterKnife.bind(this);
        setupPresenter();

        if(savedInstanceState == null){
            // Track GA
            App.getInstance().track(AppConstant.GA_TRACKING_SCREEN_NAME.WELCOME_SCREENS.getValue());
        }
//        viewPager.setMaxPageScale(1.0F);
//        viewPager.setMinPageScale(0.8F);
//        viewPager.setMinPageScaleOffset(-40.0F);
    }

    private void setupPresenter(){
        dialogHelper = new DialogHelper(this);

        presenter = new WelcomePresenter(new GetGalleries(new B2CDataRepository()));
        presenter.attach(this);
        // Begin call API
        presenter.getGalleryList();
    }

    private void initViewPager() {
        pagerAdapter = new WelcomePagerAdapter(this, galleryItemList);
        viewPager.setAdapter(pagerAdapter);
        viewPager.addOnPageChangeListener(pageChangeListener);
        indicator.configureIndicator();
        indicator.setViewPager(viewPager);
        pagerAdapter.registerDataSetObserver(indicator.getDataSetObserver());
//        viewPager.setOnInfiniteCyclePageTransformListener(new OnInfiniteCyclePageTransformListener() {
//            @Override
//            public void onPreTransform(View page, float position) {
//                View vShow = ButterKnife.findById(page, R.id.background_second_layer);
//                vShow.setAlpha(-position);
//            }
//
//            @Override
//            public void onPostTransform(View page, float position) {
//                View vShow = ButterKnife.findById(page, R.id.background_first_layer);
//                vShow.setAlpha(position);
//            }
//        });
    }

    @Override
    public void onGetGalleryListFinished(List<GalleryViewPagerItem> galleryItemList) {
        pbLoading.setVisibility(View.GONE);
        if (galleryItemList != null && galleryItemList.size() > 0) {
            viewPager.setVisibility(View.VISIBLE);
            indicator.setVisibility(View.VISIBLE);
            this.galleryItemList = galleryItemList;
            initViewPager();
            updateContent(0);
        }
    }

    @Override
    public void dismissProgressDialog() {
        pbLoading.setVisibility(View.GONE);
    }

    @Override
    public void showErrorMessage(ErrCode errCode) {
        if(errCode == ErrCode.CONNECTIVITY_PROBLEM)
            this.dialogHelper.networkUnavailability(ErrCode.CONNECTIVITY_PROBLEM,null);
        else this.dialogHelper.showGeneralError();
    }


    private void updateContent(int position) {

        int currentPosition = position % galleryItemList.size();
        GalleryViewPagerItem item = galleryItemList.get(currentPosition);
        tvWelcomeTitle.setText(item.title);
        tvWelcomeDescription.setText(item.summary);
        this.viewPagerPosition = position % galleryItemList.size();
    }

    @OnClick(R.id.btn_welcome_sign_in)
    public void signInClick(View view) {
        Intent intent = new Intent(this, SignInActivity.class);
        startActivity(intent);
    }


    @OnClick(R.id.btn_welcome_sign_up)
    public void signUpClick(View view) {
        Intent intent = new Intent(this, SignUpActivity.class);
        startActivity(intent);

    }
}
