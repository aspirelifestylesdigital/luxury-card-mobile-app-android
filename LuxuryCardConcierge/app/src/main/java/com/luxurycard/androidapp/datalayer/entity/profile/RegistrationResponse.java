package com.luxurycard.androidapp.datalayer.entity.profile;

import com.luxurycard.androidapp.App;
import com.luxurycard.androidapp.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by vinh.trinh on 6/19/2017.
 */

public class RegistrationResponse {
    public final String[] onlineMemberDetailIDs;
    public final String onlineMemberID;
    public final String message;
    public final boolean success;

    public RegistrationResponse(String rawJson) throws JSONException {
        JSONObject json = new JSONObject(rawJson);

        // success
        success = json.getBoolean("success");
        if(success) {
            // onlineMemberDetailIDs
            JSONArray jsonDetailIDS = json.getJSONArray("OnlineMemberDetailIDs");
            onlineMemberDetailIDs = new String[jsonDetailIDS.length()];
            for (int i = 0; i < jsonDetailIDS.length(); i++) {
                onlineMemberDetailIDs[i] = jsonDetailIDS.getString(i);
            }

            // onlineMemberID
            onlineMemberID = json.getString("OnlineMemberID");

            message = null;
        } else {
            onlineMemberDetailIDs = null;
            onlineMemberID = null;
            JSONArray messages = json.optJSONArray("message");
            if(null != messages) {
                JSONObject error = messages.length() > 0 ? messages.getJSONObject(0) : null;
                if(error != null) {
                    String code = error.getString("code");
                    switch (code) {
                        case "ENR68-2":
                            message = App.getInstance().getString(R.string.errorAccountEmailExists);
                            break;
                        default:
                            message = error.getString("message");
                            break;
                    }
                } else {
                    message = null;
                }
            } else {
                message = json.optString("message");
            }
        }
    }
}
