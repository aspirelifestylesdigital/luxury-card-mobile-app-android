package com.luxurycard.androidapp.datalayer.restapi;

import com.luxurycard.androidapp.datalayer.entity.oauth.SignInRequest;
import com.luxurycard.androidapp.datalayer.entity.preferences.GetPreferencesRequest;
import com.luxurycard.androidapp.datalayer.entity.preferences.PreferenceValue;
import com.luxurycard.androidapp.datalayer.entity.preferences.UpdatePreferencesRequest;
import com.luxurycard.androidapp.datalayer.entity.preferences.UpdatePreferencesResponse;
import com.luxurycard.androidapp.datalayer.entity.profile.ChangePasswordRequest;
import com.luxurycard.androidapp.datalayer.entity.profile.ForgotPasswordRequest;
import com.luxurycard.androidapp.datalayer.entity.profile.GetDetailsRequest;
import com.luxurycard.androidapp.datalayer.entity.profile.RegistrationRequest;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by vinh.trinh on 6/19/2017.
 */

public interface UserManagementApi {

    @POST("ManageUsers/Registration")
    Call<ResponseBody> registration(@Body RegistrationRequest request);

    @POST("ManageUsers/UpdateRegistration")
    Call<ResponseBody> updateRegistration(@Body RegistrationRequest request);

    @POST("ManageUsers/GetUserDetails")
    Call<ResponseBody> userDetail(@Body GetDetailsRequest request);

    @POST("ManageUsers/Login")
    Call<ResponseBody> signIn(@Body SignInRequest request);

    @POST("ManageUsers/ForgotPassword")
    Call<ResponseBody> forgotPassword(@Body ForgotPasswordRequest request);

    @POST("ManageUsers/ChangePassword")
    Call<ResponseBody> changePassword(@Body ChangePasswordRequest request);

    @POST("ManageUsers/GetPreference")
    Call<List<PreferenceValue>> getPreferences(@Body GetPreferencesRequest request);

    @POST("ManageUsers/AddPreference")
    Call<UpdatePreferencesResponse> createPreferences(@Body UpdatePreferencesRequest request);

    @POST("ManageUsers/UpdatePreference")
    Call<UpdatePreferencesResponse> updatePreferences(@Body UpdatePreferencesRequest request);

}