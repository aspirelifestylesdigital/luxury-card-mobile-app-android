package com.luxurycard.androidapp.presentation.checkout;

import com.luxurycard.androidapp.common.constant.ErrCode;
import com.luxurycard.androidapp.presentation.base.BasePresenter;

/**
 * Created by vinh.trinh on 4/26/2017.
 */

public interface SignIn {

    interface View {
        void proceedToHome();
        void showErrorDialog(ErrCode errCode, String extraMsg);
        void showProgressDialog();
        void dismissProgressDialog();
    }

    interface Presenter extends BasePresenter<View> {
        void doSignIn(String email, String password);
        void abort();
    }

}
