package com.luxurycard.androidapp.datalayer.entity.profile;

import android.text.TextUtils;

import com.luxurycard.androidapp.common.constant.AppConstant;
import com.luxurycard.androidapp.common.constant.CountryCode;
import com.luxurycard.androidapp.presentation.profile.ProfileFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.List;

/**
 * Created by vinh.trinh on 6/19/2017.
 */
public class GetDetailsResponse {

    public final Boolean success;
    public final String message;
    public final String firstName;
    public final String lastName;
    public String mobile;
    public String countryCode;
    public final String email;
    public final String salutation;
    public final String zipCode;
    public final String onlineMemberDetailID;

    public GetDetailsResponse(String rawJson) throws JSONException {
        JSONObject json = new JSONObject(rawJson);
        JSONObject jsonMember = json.optJSONObject("Member");
        JSONArray memberDetails = json.optJSONArray("MemberDetails");
        if (jsonMember == null || memberDetails == null) {
            success = false;
            JSONArray messages = json.optJSONArray("message");
            if (null != messages) {
                message = messages.length() > 0 ? messages.getJSONObject(0).getString("message") : "";
            } else {
                message = json.getString("message");
            }
            firstName = null;
            lastName = null;
            mobile = null;
            email = null;
            countryCode = null;
            salutation = null;
            zipCode = null;
            onlineMemberDetailID = null;
        } else {
            success = true;
            message = null;
            firstName = jsonMember.getString("FIRSTNAME");
            lastName = jsonMember.getString("LASTNAME");
            zipCode = jsonMember.getString("POSTALCODE");
            String _mobile = jsonMember.getString("MOBILENUMBER");
            int fiacc = firstIndexAfterCountryCode(_mobile);
            if (fiacc == -1) {
                mobile = _mobile;
                countryCode = null;
            } else {
                mobile = _mobile.substring(fiacc);
                countryCode = _mobile.substring(0, fiacc);
            }


            email = jsonMember.getString("EMAIL");
            salutation = jsonMember.getString("SALUTATION");
            onlineMemberDetailID = memberDetails.length() == 0 ? "" : memberDetails.getJSONObject(0)
                    .getString("ONLINEMEMBERDETAILID");
        }
    }

    private int firstIndexAfterCountryCode(String val) {
        int length = val.length() < 4 ? val.length() : 4;
        for (int i = length; i >= 1; i--) {
            String countryCode = val.substring(0, i);
            if (CountryCode.contains(countryCode)) return i;
        }
        return -1;
    }
}