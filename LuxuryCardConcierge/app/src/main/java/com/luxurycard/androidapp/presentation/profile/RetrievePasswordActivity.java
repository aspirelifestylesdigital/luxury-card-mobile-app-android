package com.luxurycard.androidapp.presentation.profile;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.luxurycard.androidapp.R;
import com.luxurycard.androidapp.presentation.base.BaseActivity;
import com.luxurycard.androidapp.presentation.base.CommonActivity;
import com.luxurycard.androidapp.presentation.checkout.SignInActivity;
import com.luxurycard.androidapp.presentation.widget.CustomTypefaceSpan;
import com.luxurycard.androidapp.presentation.widget.ViewUtils;
import com.support.mylibrary.widget.LetterSpacingTextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by son.ho on 11/8/2017.
 */

public class RetrievePasswordActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.title)
    LetterSpacingTextView title;
    @BindView(R.id.forgot_pas_description)
    LetterSpacingTextView description;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_retrieve_password);
        ButterKnife.bind(this);
        title.setText(getString(R.string.forgot_password_title));


        String firstWord = getResources().getString(R.string.retrieve_password_message);
        String secondWord = getResources().getString(R.string.retrive_pass_phone_number);

        Spannable spannable = new SpannableString(firstWord+secondWord);

        spannable.setSpan( new CustomTypefaceSpan("roboto", Typeface.DEFAULT), 0, firstWord.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannable.setSpan( new CustomTypefaceSpan("roboto", Typeface.DEFAULT_BOLD), firstWord.length(), firstWord.length() + secondWord.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        description.setText( spannable );
    }

    @OnClick(R.id.btn_return_to_sign_in)
    public void toSignInView() {
        Intent intent = new Intent(this, SignInActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
    }

    @OnClick(android.R.id.home)
    public void onButtonBack() {
        onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            super.onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        ViewUtils.hideSoftKey(getCurrentFocus());
        super.onBackPressed();
    }
}
