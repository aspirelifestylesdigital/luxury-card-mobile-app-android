package com.luxurycard.androidapp.presentation.cityguidecategory;

import com.luxurycard.androidapp.domain.model.SubCategoryItem;
import com.luxurycard.androidapp.presentation.base.BasePresenter;

import java.util.List;


/**
 * Created by tung.phan on 5/31/2017.
 */

public interface SubCategory {

    interface View {
        void showLoadingView();

        void hideLoadingView();

        void updateSubCategoryRViewAdapter(List<SubCategoryItem> subCategoryItems);
    }

    interface Presenter extends BasePresenter<SubCategory.View> {

        void getSubCategories(int categoryId);
    }
}
