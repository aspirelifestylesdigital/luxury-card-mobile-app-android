package com.luxurycard.androidapp.common.logic;

/**
 * Created by Thu Nguyen on 6/30/2017.
 */

public class StringUtils {
    private static final String CONTACT_US_1 = "Contact us for more information.";
    private static final String CONTACT_US_2 = "Contact us for more information";
    private static final String BROWSE_AND_BOOK_1 = "Browse and Book Sightseeing & Tours Worldwide.";
    private static final String BROWSE_AND_BOOK_2 = "Browse and Book Sightseeing & Tours Worldwide";

    public static String removeRedundantToken(String text){
        return text.replace(CONTACT_US_1, "").replace(CONTACT_US_2, "")
                .replace(BROWSE_AND_BOOK_1, "").replace(BROWSE_AND_BOOK_2, "");
    }

    public static String capitalWords(String text) {
        char[] chars = text.toCharArray();
        try {
            for (int i=chars.length-1; i >= 1; i--) {
                chars[i] = Character.toLowerCase(chars[i]);
                if(chars[i] == ' ' && i-1>=0) {
                    chars[i+1] = Character.toUpperCase(chars[i+1]);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new String(chars);
    }
}
