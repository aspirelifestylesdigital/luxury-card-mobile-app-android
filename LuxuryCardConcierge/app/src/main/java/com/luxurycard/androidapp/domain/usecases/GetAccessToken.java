package com.luxurycard.androidapp.domain.usecases;

import com.luxurycard.androidapp.common.BackendException;
import com.luxurycard.androidapp.datalayer.entity.oauth.AccessTokenResponse;
import com.luxurycard.androidapp.datalayer.entity.oauth.ReqTokenResponse;
import com.luxurycard.androidapp.datalayer.preference.PreferencesStorage;
import com.luxurycard.androidapp.datalayer.restapi.OAuthApi;
import com.luxurycard.androidapp.datalayer.retro2client.AppHttpClient;
import com.luxurycard.androidapp.domain.model.Metadata;

import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by vinh.trinh on 7/27/2017.
 */

public class GetAccessToken {

    private PreferencesStorage preferencesStorage;

    public GetAccessToken(PreferencesStorage preferencesStorage) {
        this.preferencesStorage = preferencesStorage;
    }

    public Single<String> execute() {
        return Single.create(e -> {
            Metadata metadata = preferencesStorage.metadata();
            OAuthApi oAuthApi = AppHttpClient.getInstance().oAuthApi();
            // request token
            Call<ReqTokenResponse> requestTokenCall = oAuthApi.requestToken(
                    metadata.uuid, metadata.onlineMemberID);
            Response<ReqTokenResponse> requestTokenResponse = requestTokenCall.execute();
            if(!requestTokenResponse.isSuccessful()) {
                e.onError(new Exception(requestTokenResponse.errorBody().string()));
                return;
            }
            if(requestTokenResponse.body().status() == null || !requestTokenResponse.body().status().equals("Valid")) {
                String message = requestTokenResponse.body().message();
                e.onError(new BackendException(message));
                return;
            }
            String requestToken = requestTokenResponse.body().getRequestToken();
            // access token
            Call<AccessTokenResponse> accessTokenCall = oAuthApi.accessToken(requestToken, metadata.onlineMemberID);
            Response<AccessTokenResponse> accessTokenResponse = accessTokenCall.execute();
            if(!accessTokenResponse.isSuccessful()) {
                e.onError(new Exception(accessTokenResponse.errorBody().string()));
                return;
            }
            if(accessTokenResponse.body().message() != null || !accessTokenResponse.body().status()) {
                String message = accessTokenResponse.body().message();
                e.onError(new BackendException(message));
                return;
            }
            e.onSuccess(accessTokenResponse.body().accessToken());
        });
    }
}
