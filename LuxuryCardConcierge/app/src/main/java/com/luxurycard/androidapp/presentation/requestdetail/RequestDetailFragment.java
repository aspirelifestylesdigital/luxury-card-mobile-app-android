package com.luxurycard.androidapp.presentation.requestdetail;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.luxurycard.androidapp.App;
import com.luxurycard.androidapp.R;
import com.luxurycard.androidapp.common.constant.AppConstant;
import com.luxurycard.androidapp.common.constant.IntentConstant;
import com.luxurycard.androidapp.datalayer.preference.PreferencesStorage;
import com.luxurycard.androidapp.datalayer.repository.ProfileDataRepository;
import com.luxurycard.androidapp.datalayer.repository.RequestRepository;
import com.luxurycard.androidapp.domain.model.RequestDetailData;
import com.luxurycard.androidapp.domain.usecases.CreateRequest;
import com.luxurycard.androidapp.domain.usecases.GetAccessToken;
import com.luxurycard.androidapp.domain.usecases.LoadProfile;
import com.luxurycard.androidapp.presentation.base.BaseNavSubFragment;
import com.luxurycard.androidapp.presentation.home.HomeNavBottomActivity;
import com.luxurycard.androidapp.presentation.requestlist.parent.RequestFragment;
import com.luxurycard.androidapp.presentation.widget.DialogHelper;
import com.support.mylibrary.widget.LetterSpacingTextView;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Den on 4/4/18.
 */

public class RequestDetailFragment extends BaseNavSubFragment implements RequestDetail.View {


    @BindView(R.id.request_detail_recyclerview)
    RecyclerView requestDetailRView;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tv_title)
    LetterSpacingTextView tvTitle;
    @BindView(R.id.tv_status)
    LetterSpacingTextView tvStatus;

    @BindView(R.id.btn_edit)
    Button btnEdit;

    @BindView(R.id.btn_duplicate)
    Button btnDuplicate;

    @BindView(R.id.btn_cancel)
    Button btnCancel;

    private RequestDetailData data;
    private RequestDetailAdapter detailAdapter;
    private String requestID;

    DialogHelper dialogHelper;

    private RequestDetailPresenter presenter;

    private RequestDetailPresenter createPresenter(RequestDetailData rdData) {
        PreferencesStorage ps = new PreferencesStorage(this.getContext());
        ProfileDataRepository profileDataRepository = new ProfileDataRepository(ps);
        GetAccessToken at = new GetAccessToken(ps);
        LoadProfile lp = new LoadProfile(profileDataRepository);
        CreateRequest cr = new CreateRequest(new RequestRepository());
        return new RequestDetailPresenter(rdData, ps, cr, at, lp);
    }

    public static RequestDetailFragment newInstance(RequestDetailData data, String requestID) {
        Bundle args = new Bundle();
        RequestDetailFragment fragment = new RequestDetailFragment();
        fragment.data = data;
        fragment.requestID = requestID;
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dialogHelper = new DialogHelper(this.getActivity());
        presenter = createPresenter(data);
        presenter.attach(this);
        presenter.getRequestDetail();
        if(savedInstanceState == null){
            // Track GA
            App.getInstance().track(AppConstant.GA_TRACKING_SCREEN_NAME.REQUEST_DETAILS.getValue());
        }

    }

    @Override
    public int setupCreateViewLayoutId() {
        return R.layout.fragment_request_detail;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @OnClick(R.id.btn_back)
    void backClick(View view) {
        this.onBackPressed();
    }

    @OnClick(R.id.request_toolbar_btn_call)
    void onCallConcierge(View view) {
        callConcierge();
    }

    @OnClick(R.id.btn_duplicate)
    void duplicateClick(View view) {
        openNewRequestDetailActivity(initData(data, true));
    }

    @OnClick(R.id.btn_edit)
    void editClick(View view) {
        openNewRequestDetailActivity(initData(data, false));
    }

    private Bundle initData(RequestDetailData data, boolean isUpdate){
        Bundle bundle = new Bundle();
        bundle.putParcelable(IntentConstant.AC_REQUEST_DETAIL,data);
        bundle.putBoolean(IntentConstant.AC_BOOL_DUPLICATE, isUpdate);
        return bundle;
    }

    @OnClick(R.id.btn_cancel)
    void cancelClick(View view) {

//        ((HomeNavBottomActivity) getActivity()).mDialogHelper.action("",
//                getString(R.string.confirm_cancel_request),
//                getString(R.string.text_continue_request),
//                getString(R.string.text_call_concierge),
//                getString(R.string.text_cancel_request),
//                null,
//                (dialog, which) -> callConcierge(),
//                (dialogInterface, i) -> presenter.dismissRequest());

        dialogHelper.action(
                null,
                getString(R.string.confirm_cancel_request),
                getString(R.string.call_concierge),
                getString(R.string.cancel_anyway),
                (dialog, which) -> callConcierge(),
                (dialogInterface, i) -> presenter.dismissRequest());
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupView();
        getToolbar().setVisibility(View.GONE);

    }

    @Override
    public void onDestroy() {
        presenter.detach();
        super.onDestroy();
    }

    @Override
    public void showProgressDialog() {
        if (getActivity() instanceof HomeNavBottomActivity) {
            ((HomeNavBottomActivity) getActivity()).showProgressDialog();
        }
    }

    @Override
    public void dismissProgressDialog() {
        if (getActivity() instanceof HomeNavBottomActivity) {
            ((HomeNavBottomActivity) getActivity()).hideProgressDialog();
        }
    }

    @Override
    public void showData(DetailViewData data) {
        if (detailAdapter == null) {
            detailAdapter = new RequestDetailAdapter(data.contents);
            LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
            requestDetailRView.setLayoutManager(layoutManager);
            requestDetailRView.setAdapter(detailAdapter);
        }
        detailAdapter.notifyDataSetChanged();
    }

    @Override
    public void showErrorMessage(String message) {
        dialogHelper.alert("Error!", "Operation failed.");
    }

    @Override
    public void onCancelDone() {
        this.onBackPressed();

        if (getParentFragment() != null) {
            Fragment fragment = getParentFragment()
                    .getChildFragmentManager().findFragmentByTag(RequestFragment.class.getSimpleName());

            if (fragment instanceof RequestFragment) {
                ((RequestFragment) fragment).doneCancel();
            }

        }
    }

    private void setupView() {
        if ("closed".equalsIgnoreCase(data.requestStatus)) {
            btnCancel.setEnabled(false);
            btnEdit.setEnabled(false);
        } else {
            if (data.requestMode.equalsIgnoreCase("cancel")) {
                btnCancel.setEnabled(false);
                btnEdit.setEnabled(false);
            }
        }
        tvTitle.setText("Request " + data.getEPCCaseIDCCaseID());
//        String status = data.status.equals(RequestTypeConstant.OPEN) ? "Open" : "Closed";
        tvStatus.setText("Status: " + data.status);
    }
}
