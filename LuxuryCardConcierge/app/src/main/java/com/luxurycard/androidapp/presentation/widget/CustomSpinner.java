package com.luxurycard.androidapp.presentation.widget;

import android.content.Context;
import android.graphics.Color;
import android.util.TypedValue;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListPopupWindow;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.luxurycard.androidapp.R;

/**
 * Created by anh.trinh on 8/8/2017.
 */

public class CustomSpinner {
    private TextView anchorView;
    private ListPopupWindow listPopupWindow;
    private Context context;
    private DropdownAdapter dropdownAdapter;

    public boolean isNone() {
        return isNone;
    }

    public void setNone(final boolean none) {
        isNone = none;
        this.dropdownAdapter.setNone(isNone);
        this.dropdownAdapter.notifyDataSetChanged();
    }

    boolean isNone = true;

    public int getSelectionPosition() {
        return selectionPosition;
    }

    public void setSelectionPosition(final int selectionPosition) {
        this.selectionPosition = selectionPosition;
        this.listPopupWindow.setSelection(this.selectionPosition);
        this.dropdownAdapter.setCurrentSelected(selectionPosition);
        if (selectionPosition == 0 && isNone) {
            anchorView.setTextColor(Color.parseColor("#ffffff"));
//            anchorView.setBackgroundColor(Color.parseColor("#000000"));
        } else {
            anchorView.setTextColor(Color.parseColor("#ffffff"));
//            anchorView.setBackgroundColor(Color.parseColor("#000000"));
        }
        String val = dropdownAdapter.getItem(selectionPosition);
        anchorView.setText(textModifier == null ? val : textModifier.modify(val));
    }

    /*private String countryCodeText(String val) {z
        int last = val.lastIndexOf(')');
        int first = val.indexOf('(');
        if(first > -1 && last > -1) {
            int firstCloseParenthese = val.indexOf(')');
            return val.substring(first+1, firstCloseParenthese) + val.substring(firstCloseParenthese+1);
        }
        return val;
    }*/

    private int selectionPosition;

    public interface SpinnerListener {
        void onArchorViewClick();

        void onItemSelected(int index);
    }

    public interface TextModifier {
        String modify(String val);
    }

    public void setTextModifier(TextModifier tm) {
        this.textModifier = tm;
    }

    public void setmSpinnerListener(final SpinnerListener mSpinnerListener) {
        this.mSpinnerListener = mSpinnerListener;
    }

    private TextModifier textModifier;
    private SpinnerListener mSpinnerListener;

    public CustomSpinner(Context context,
                         final TextView anchorView) {
        this.context = context;
        this.anchorView = anchorView;
        listPopupWindow = new ListPopupWindow(anchorView.getContext());
        anchorView.setOnClickListener(view -> {
            if (mSpinnerListener != null) {
                mSpinnerListener.onArchorViewClick();
            }
            if(listPopupWindow.isShowing()){
                listPopupWindow.dismiss();
                anchorView.setBackgroundResource(R.drawable.bg_spinner_hide);
            }else {
                listPopupWindow.show();
                anchorView.setBackgroundResource(R.drawable.bg_spinner_show);
            }
        });
        listPopupWindow.setModal(true);
        listPopupWindow.setAnchorView(anchorView);
        listPopupWindow.setOnDismissListener(() -> anchorView.setBackgroundResource(R.drawable.bg_spinner_hide));
        listPopupWindow.setOnItemClickListener((adapterView, view, i, l) -> {
            if (mSpinnerListener != null) {
                mSpinnerListener.onItemSelected(i);
            }
            if (i == 0 &&isNone) {
                anchorView.setTextColor(Color.parseColor("#ffffff"));
//                anchorView.setBackgroundColor(Color.parseColor("#000000"));
            } else {
                anchorView.setTextColor(Color.parseColor("#ffffff"));
//                anchorView.setBackgroundColor(Color.parseColor("#000000"));
            }
            String val = adapterView.getItemAtPosition(i).toString();
            anchorView.setText(textModifier == null ? val : textModifier.modify(val));
            selectionPosition = i;
            dropdownAdapter.setCurrentSelected(i);
            listPopupWindow.dismiss();

        });
    }

    private int convertDpToPx(int dp, Context context) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                                               dp,
                                               context.getResources()
                                                      .getDisplayMetrics());
    }

    public void setPobpupHeight(int popupHeight) {
        listPopupWindow.setHeight(convertDpToPx(popupHeight, context));
    }


    public DropdownAdapter getDropdownAdapter() {
        return dropdownAdapter;
    }

    public void setDropdownAdapter(final DropdownAdapter dropdownAdapter) {
        this.dropdownAdapter = dropdownAdapter;
        this.dropdownAdapter.setNone(isNone);
        listPopupWindow.setAdapter(dropdownAdapter);
    }


}
