package com.luxurycard.androidapp.datalayer.datasource;

import android.text.TextUtils;

import com.luxurycard.androidapp.common.BackendException;
import com.luxurycard.androidapp.datalayer.entity.profile.GetDetailsRequest;
import com.luxurycard.androidapp.datalayer.entity.profile.GetDetailsResponse;
import com.luxurycard.androidapp.datalayer.entity.profile.RegistrationRequest;
import com.luxurycard.androidapp.datalayer.entity.profile.RegistrationResponse;
import com.luxurycard.androidapp.datalayer.preference.PreferencesStorage;
import com.luxurycard.androidapp.datalayer.repository.PreferencesDataRepository;
import com.luxurycard.androidapp.datalayer.restapi.UserManagementApi;
import com.luxurycard.androidapp.datalayer.retro2client.AppHttpClient;
import com.luxurycard.androidapp.domain.model.Metadata;
import com.luxurycard.androidapp.domain.model.Profile;
import com.luxurycard.androidapp.domain.usecases.GetAccessToken;

import io.reactivex.Completable;
import io.reactivex.Single;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class RemoteProfileDataStore {

    private PreferencesStorage preferencesStorage;

    public RemoteProfileDataStore(PreferencesStorage pref) {
        this.preferencesStorage = pref;
    }

    public Single<Profile> loadProfile(String accessToken) {
        return Single.create(e -> {
            Metadata metadata = preferencesStorage.metadata();
            GetDetailsRequest requestBody = new GetDetailsRequest(accessToken, metadata.onlineMemberID);

            Call<ResponseBody> call = AppHttpClient.getInstance().userManagementApi()
                    .userDetail(requestBody);
            Response<ResponseBody> response = call.execute();
            if(!response.isSuccessful()) {
                e.onError(new Exception(response.errorBody().string()));
                return;
            }
            GetDetailsResponse responseBody = new GetDetailsResponse(response.body().string());
            if(responseBody.success) {
                Profile profile = new Profile();
                profile.setFirstName(responseBody.firstName);
                profile.setLastName(responseBody.lastName);
                profile.setEmail(responseBody.email);
                profile.setPhone(responseBody.mobile);
                profile.setCountryCode(responseBody.countryCode);
                profile.setSalutation(responseBody.salutation);
                profile.setZipCode(responseBody.zipCode);
                Metadata metadata1 = new Metadata(metadata.onlineMemberID, responseBody.onlineMemberDetailID);
                preferencesStorage.editor()
                        .metadata(metadata1)
                        .build().save();

                e.onSuccess(profile);
            } else {
                e.onError(new BackendException(responseBody.message));
            }
        });
    }

    public Completable saveProfile(Profile profile, String password) {
        if(TextUtils.isEmpty(password)) {
            return save(profile, password).mergeWith(saveLocationSetting(profile.isLocationOn()));
        } else {
            return save(profile, password);
        }
    }

    private Completable save(Profile profile, String password) {
        return Completable.create(e -> {

            UserManagementApi userManagementApi = AppHttpClient.getInstance().userManagementApi();
            Metadata metadata = preferencesStorage.metadata();
            boolean registration = metadata == null;

            com.luxurycard.androidapp.datalayer.entity.Member member;
            com.luxurycard.androidapp.datalayer.entity.MemberDetail memberDetail;
            if(metadata == null) { // being create
                member = new com.luxurycard.androidapp.datalayer.entity.Member(
                        profile.getEmail(),
                        profile.getFirstName(),
                        profile.getLastName(),
                        profile.getCountryCode()+profile.getPhone(),
                        password,
                        profile.getSalutation(),
                        profile.getZipCode());
                memberDetail = new com.luxurycard.androidapp.datalayer.entity.MemberDetail();
            } else {
                String onlineMemberID = metadata.onlineMemberID;
                String onlineMemberDetailID = metadata.onlineMemberDetailID;

                member = new com.luxurycard.androidapp.datalayer.entity.Member(
                        profile.getEmail(),
                        profile.getFirstName(),
                        profile.getLastName(),
                        profile.getCountryCode()+profile.getPhone(),
                        null,
                        profile.getSalutation(),
                        profile.getZipCode(),
                        onlineMemberID);

                memberDetail = new com.luxurycard.androidapp.datalayer.entity.MemberDetail(onlineMemberDetailID);


            }
            RegistrationRequest requestBody = new RegistrationRequest(member, memberDetail);
            Call<ResponseBody> call = registration ? userManagementApi.registration(requestBody) :
                    userManagementApi.updateRegistration(requestBody);
            Response<ResponseBody> response = call.execute();
            if(!response.isSuccessful()) {
                e.onError(new Exception(response.errorBody().string()));
                return;
            }
            RegistrationResponse registrationResponse = new RegistrationResponse(response.body().string());
            if(registrationResponse.success) {
                Metadata metadata1 = new Metadata(
                        registrationResponse.onlineMemberID,
                        registrationResponse.onlineMemberDetailIDs[0]);
                preferencesStorage.editor().metadata(metadata1).build().save();
                e.onComplete();
            } else {
                e.onError(new BackendException(registrationResponse.message));
            }
        });
    }

    private Completable saveLocationSetting(boolean on) {
        GetAccessToken getAccessToken = new GetAccessToken(preferencesStorage);
        PreferencesDataRepository preferencesDataRepository = new PreferencesDataRepository(preferencesStorage);
        return getAccessToken.execute().flatMapCompletable(accessToken -> {
            PreferenceData preferenceData = preferencesStorage.userPreferences();
            preferenceData.extValue = on ? "Yes" : "No";
            return preferencesDataRepository.save(preferenceData, accessToken);
        });
    }
}
