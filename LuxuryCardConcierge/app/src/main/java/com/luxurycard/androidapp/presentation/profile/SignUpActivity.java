package com.luxurycard.androidapp.presentation.profile;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;

import com.luxurycard.androidapp.App;
import com.luxurycard.androidapp.R;
import com.luxurycard.androidapp.common.GPSChecker;
import com.luxurycard.androidapp.common.constant.AppConstant;
import com.luxurycard.androidapp.common.constant.ErrCode;
import com.luxurycard.androidapp.common.constant.IntentConstant;
import com.luxurycard.androidapp.common.constant.RequestCode;
import com.luxurycard.androidapp.datalayer.preference.PreferencesStorage;
import com.luxurycard.androidapp.datalayer.repository.PreferencesDataRepository;
import com.luxurycard.androidapp.datalayer.repository.ProfileDataRepository;
import com.luxurycard.androidapp.domain.model.Profile;
import com.luxurycard.androidapp.domain.usecases.CreatePlainUserPreferences;
import com.luxurycard.androidapp.domain.usecases.GetAccessToken;
import com.luxurycard.androidapp.domain.usecases.GetUserPreferences;
import com.luxurycard.androidapp.domain.usecases.SaveProfile;
import com.luxurycard.androidapp.presentation.LocationPermissionHandler;
import com.luxurycard.androidapp.presentation.base.BaseActivity;
import com.luxurycard.androidapp.presentation.checkout.SignInActivity;
import com.luxurycard.androidapp.presentation.home.HomeNavBottomActivity;
import com.luxurycard.androidapp.presentation.widget.DialogHelper;
import com.luxurycard.androidapp.presentation.widget.ViewUtils;

/**
 * Created by vinh.trinh on 4/27/2017.
 */

public class SignUpActivity extends BaseActivity implements ProfileFragment.ProfileEventsListener, CreateProfile.View {

    private CreateProfilePresenter presenter;
    private DialogHelper dialogHelper;
    ProfileFragment fragment;
//    LocationPermissionHandler locationPermissionHandler = new LocationPermissionHandler();

    private boolean isWelcomeOpen = true;

    private CreateProfilePresenter presenter() {
        final PreferencesStorage preferencesStorage = new PreferencesStorage(getApplicationContext());
        final ProfileDataRepository profileDataRepository = new ProfileDataRepository(preferencesStorage);
        final PreferencesDataRepository preferencesDataRepository = new PreferencesDataRepository(preferencesStorage);
        SaveProfile saveProfile = new SaveProfile(profileDataRepository);
        GetAccessToken getAccessToken = new GetAccessToken(preferencesStorage);
        CreatePlainUserPreferences createPlainUserPreferences = new CreatePlainUserPreferences(preferencesDataRepository);
        GetUserPreferences getUserPreferences = new GetUserPreferences(preferencesDataRepository);
        return new CreateProfilePresenter(saveProfile, getAccessToken, createPlainUserPreferences,
                getUserPreferences);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_fragment_holder);
        presenter = presenter();
        presenter.attach(this);

        dialogHelper = new DialogHelper(this);
        if (savedInstanceState == null) {
            fragment = ProfileFragment.newInstance(ProfileFragment.PROFILE.CREATE);
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragment_place_holder,
                            fragment,
                            ProfileFragment.class.getSimpleName())
                    .commit();
            // Track GA
            App.getInstance().track(AppConstant.GA_TRACKING_SCREEN_NAME.SIGN_UP.getValue());
        }

        if(getIntent() != null){
            isWelcomeOpen = getIntent().getBooleanExtra(IntentConstant.PAGE_WELCOME_OPEN, true);
        }
    }

    @Override
    protected void onDestroy() {
        presenter.detach();
        super.onDestroy();
    }

    @Override
    public void onProfileSubmit(Profile profile, String password) {
        presenter.createProfile(profile, password);
    }

    @Override
    public void showProfileCreatedDialog() {
        dialogHelper.alert(null, getString(R.string.profile_created_message), dialog -> proceedToHome());
    }

    private void proceedToHome() {
        ViewUtils.hideSoftKey(getCurrentFocus());
        Intent intent = new Intent(this, HomeNavBottomActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);

        // Track GA with "Sign up" event
        App.getInstance().track(AppConstant.GA_TRACKING_CATEGORY.AUTHENTICATION.getValue(),
                AppConstant.GA_TRACKING_ACTION.CLICK.getValue(),
                AppConstant.GA_TRACKING_LABEL.SIGN_UP.getValue());
    }

    @Override
    public void showErrorDialog(ErrCode errCode, String extraMsg) {
        if(!dialogHelper.networkUnavailability(errCode, extraMsg)){
            if(errCode == ErrCode.API_ERROR && extraMsg.equalsIgnoreCase(getResources().getString(R.string.errorAccountEmailExists))){
                //error exist email
                dialogHelper.alert(getString(R.string.dialogTitleError), extraMsg, dialog -> {
                    if(fragment != null && fragment.getView() != null) {
                        fragment.getView().postDelayed(() -> fragment.showHighlightFieldEmail(), 100);
                    }
                });
            }else{
                dialogHelper.alert(getString(R.string.dialogTitleError), extraMsg);
            }
        }
    }

    @Override
    public void showProgressDialog() {
        dialogHelper.showProgress();
    }

    @Override
    public void dismissProgressDialog() {
        dialogHelper.dismissProgress();
    }

    @Override
    public void onProfileCancel() {
        // goto SignIn
        if(getCurrentFocus() != null){
            ViewUtils.hideSoftKey(getCurrentFocus());
        }
        if(isWelcomeOpen){
            Intent intent = new Intent(this, SignInActivity.class);
            intent.putExtra(IntentConstant.PAGE_WELCOME_OPEN, false);
            startActivity(intent);
        }else {
            this.onBackPressed();
        }
    }

    @Override
    public void onProfileInputError(String message) {
        dialogHelper.profileDialog(message,dialog ->{
            if(fragment != null) {
                fragment.getView().postDelayed(() -> fragment.invokeSoftKey(), 100);
            }
        } );
    }

    @Override
    public void onFragmentCreated() {}

    @Override
    public void onLocationSwitchOn() {
        /*if(!locationPermissionHandler.hasPermission(this)) {
            locationPermissionHandler.requestPermission(this);
        } else{
            if(!GPSChecker.GPSEnable(getApplicationContext())) {
                dialogHelper.action(null,"To enable, please go to Settings and turn on Location Service for this app.",
                        "Setting","Cancel",
                        (dialogInterface, i) -> GPSChecker.openGPSSetting(this));
            }
        }*/
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        boolean granted = locationPermissionHandler.handlingPermissionResult(requestCode, grantResults);
//        if(!granted) fragment.switchOff();
//        else if (!GPSChecker.GPSEnable(getApplicationContext())) {
//            dialogHelper.action(null, "To enable, please go to Settings and turn on Location Service for this app.",
//                    "Setting", "Cancel",
//                    (dialogInterface, i) -> GPSChecker.openGPSSetting(this));
//        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
