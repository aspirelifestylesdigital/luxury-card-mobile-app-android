package com.luxurycard.androidapp.presentation.checkout;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.luxurycard.androidapp.App;
import com.luxurycard.androidapp.R;
import com.luxurycard.androidapp.common.constant.AppConstant;
import com.luxurycard.androidapp.common.constant.ErrCode;
import com.luxurycard.androidapp.common.constant.IntentConstant;
import com.luxurycard.androidapp.common.logic.Validator;
import com.luxurycard.androidapp.datalayer.preference.ForgotPasswordStorage;
import com.luxurycard.androidapp.datalayer.preference.PreferencesStorage;
import com.luxurycard.androidapp.datalayer.repository.PreferencesDataRepository;
import com.luxurycard.androidapp.datalayer.repository.ProfileDataRepository;
import com.luxurycard.androidapp.domain.repository.ProfileRepository;
import com.luxurycard.androidapp.domain.repository.UserPreferencesRepository;
import com.luxurycard.androidapp.domain.usecases.GetUserPreferences;
import com.luxurycard.androidapp.domain.usecases.LoadProfile;
import com.luxurycard.androidapp.presentation.base.BaseActivity;
import com.luxurycard.androidapp.presentation.changepass.ChangePasswordActivity;
import com.luxurycard.androidapp.presentation.home.HomeNavBottomActivity;
import com.luxurycard.androidapp.presentation.profile.ForgotPasswordActivity;
import com.luxurycard.androidapp.presentation.profile.SignUpActivity;
import com.luxurycard.androidapp.presentation.widget.DialogHelper;
import com.luxurycard.androidapp.presentation.widget.PasswordInputFilter;
import com.luxurycard.androidapp.presentation.widget.ViewKeyboardListener;
import com.luxurycard.androidapp.presentation.widget.ViewUtils;
import com.support.mylibrary.widget.ButtonTextSpacing;
import com.support.mylibrary.widget.ErrorIndicatorEditText;
import com.support.mylibrary.widget.LetterSpacingTextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignInActivity extends BaseActivity implements SignIn.View {

    @BindView(R.id.edt_email)
    ErrorIndicatorEditText edtEmail;
    @BindView(R.id.edt_password)
    ErrorIndicatorEditText edtPassword;
    @BindView(R.id.btn_sign_in)
    ButtonTextSpacing btnSignIn;
    //    @BindView(R.id.img_background)
//    ImageView imgBackground;
    @BindView(R.id.content_wrapper)
    LinearLayout contentWrapper;
    @BindView(R.id.scrollView)
    ScrollView scroll;
    @BindView(R.id.holder)
    FrameLayout llRootView;
    @BindView(R.id.llParent)
    LinearLayout llParent;
    DialogHelper dialogHelper;
    SignInPresenter presenter;
    Validator validator;
    String errMessage;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.title)
    LetterSpacingTextView title;
    @BindView(R.id.tv_sign_up)
    LetterSpacingTextView tvSignUp;
    private boolean isWelcomeOpen = true;

    private SignInPresenter signInPresenter() {
        PreferencesStorage preferencesStorage = new PreferencesStorage(getApplicationContext());
        ProfileRepository profileRepository = new ProfileDataRepository(preferencesStorage);
        UserPreferencesRepository preferencesRepository = new PreferencesDataRepository(preferencesStorage);
        com.luxurycard.androidapp.domain.usecases.SignIn signIn =
                new com.luxurycard.androidapp.domain.usecases.SignIn(preferencesStorage);
        LoadProfile loadProfile = new LoadProfile(profileRepository);
        GetUserPreferences getUserPreferences = new GetUserPreferences(preferencesRepository);
        return new SignInPresenter(signIn, loadProfile, getUserPreferences, preferencesStorage);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);
        ButterKnife.bind(this);
        dialogHelper = new DialogHelper(this);
        presenter = signInPresenter();
        presenter.attach(this);
        validator = new Validator();
        title.setText("Sign In");
        title.setAllCaps(false);

        toolbar.setOnClickListener(v -> {
            if(getCurrentFocus() != null) {
                ViewUtils.hideSoftKey(getCurrentFocus());
            }
        });

        //String text = "<font color=#FFFFFF>Don't have an account?</font> <font color=#f7eab9>Sign up here</font>";
        //tvSignUp.setText(Html.fromHtml(text));
        keyboardInteractListener();

        edtEmail.setOnFocusChangeListener((view1, b) -> {
            if (!b && !edtPassword.isFocused()) {
                ViewUtils.hideSoftKey(edtEmail);
            } else {
                scroll.postDelayed(() -> {
                    scroll.scrollTo(0, btnSignIn.getBottom());
                }, 150);
            }
            edtEmail.setCursorVisible(b);
        });

        edtPassword.setOnFocusChangeListener((view1, b) -> {
            if (!b && !edtEmail.isFocused()) {
                ViewUtils.hideSoftKey(edtPassword);
            } else {
                scroll.postDelayed(() -> {
                    scroll.scrollTo(0, btnSignIn.getBottom());
                }, 200);
            }
            edtPassword.setCursorVisible(b);
        });

        edtEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                btnSignIn.setEnabled(editable.toString().trim().length() > 0
                        && edtPassword.getText().toString().trim().length() > 0);
            }
        });

        edtPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                btnSignIn.setEnabled(editable.toString().trim().length() > 0
                        && edtEmail.getText().toString().trim().length() > 0);
            }
        });
        edtPassword.setFilters(new InputFilter[]{new PasswordInputFilter(), new InputFilter.LengthFilter(getResources().getInteger(R.integer.password_max_length_characters))});

        if(getIntent() != null){
            isWelcomeOpen = getIntent().getBooleanExtra(IntentConstant.PAGE_WELCOME_OPEN, true);
        }

        if(savedInstanceState == null){
            // Track GA
            App.getInstance().track(AppConstant.GA_TRACKING_SCREEN_NAME.SIGN_IN.getValue());
        }
    }

    /**
     * handle click on back hide keyboard close cursor
     */
    private void keyboardInteractListener() {
        ViewKeyboardListener.KeyboardEvent event = new ViewKeyboardListener.KeyboardEvent() {
            @Override
            public void showKeyboard() {
                //dummy do nothing
            }

            @Override
            public void hideKeyboard() {
                //dummy do nothing
            }

            @Override
            public View getCurrentFocus() {
                return SignInActivity.this.getCurrentFocus();
            }
        };
        //-- add listen keyboard
        ViewKeyboardListener keyboardListener = new ViewKeyboardListener(llRootView, event);
        keyboardListener.setRootViewFocusChangeListener();
    }

    @Override
    protected void onDestroy() {
        presenter.detach();
        super.onDestroy();
    }

    @OnClick(R.id.btn_sign_in)
    public void signInClick(View view) {
        ViewUtils.hideSoftKey(view);
        String email = edtEmail.getText().toString();
        String password = edtPassword.getText().toString();

        //-- clean all error highlight
        edtEmail.setError(null);
        edtPassword.setError(null);
        final SignInValidate validate = validateFields(email, password);
        if (validate.isNoHaveErrorInput()) {
            presenter.doSignIn(email, password);
        } else {
            final ErrorIndicatorEditText edt = validate.errorValidate;
            edt.setError("");
            dialogHelper.profileDialog(validate.errorMgs, dialog -> {
                final ErrorIndicatorEditText edtError = validate.errorValidate;
                if (edtError != null) {
                    edtError.postDelayed(() -> {
                        edtError.requestFocus();
                        edtError.setCursorVisible(true);
                        edtError.setSelection(edtError.getText().toString().length());


                        ViewUtils.invoSoftKey(edtError);
                    }, 100);
                }
            });
        }
    }

    @OnClick(R.id.tv_sign_up)
    public void toSignUpView() {
        hideAllEdtCursor();
        clearForm();
        if (isWelcomeOpen) {
            Intent intent = new Intent(this, SignUpActivity.class);
            intent.putExtra(IntentConstant.PAGE_WELCOME_OPEN, false);
            startActivity(intent);
        }else {
            this.onBackPressed();
        }
    }

    @OnClick(android.R.id.home)
    public void onButtonBack() {
        onBackPressed();
    }

    @OnClick(R.id.tv_forgot_pw)
    public void forgotPasswordClick() {
        clearForm();
        startActivity(new Intent(this, ForgotPasswordActivity.class));
    }

    @OnClick({R.id.llParent, R.id.toolbar})
    public void onClickOutsideView(View view) {
        hideAllEdtCursor();
    }

    private void hideAllEdtCursor() {
        if(getCurrentFocus() != null){
            ViewUtils.hideSoftKey(getCurrentFocus());
        }
        edtEmail.setCursorVisible(false);
        edtPassword.setCursorVisible(false);
    }

    // presenter
    @Override
    public void proceedToHome() {
        boolean hasForgotPwd = new PreferencesStorage(getApplicationContext()).hasForgotPwd();
        Intent intent;
        if (hasForgotPwd) {
            if (isExpiredPassword()) {
                PreferencesStorage preferencesStorage = new PreferencesStorage(this);
                preferencesStorage.clear();
                dialogHelper.action("alert", "Your temporary password has expired. " +
                                "Enter your email address again to receive a new temporary password.",
                        "OK",
                        "Cancel",
                        (dialogInterface, i) -> forgotPasswordClick());
            } else {
                intent = new Intent(this, ChangePasswordActivity.class);
                intent.putExtra(Intent.EXTRA_REFERRER, "a");
                startActivity(intent);
                finish();
            }
        } else {
            intent = new Intent(this, HomeNavBottomActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }

        // Track GA with "Sign in" event
        App.getInstance().track(AppConstant.GA_TRACKING_CATEGORY.AUTHENTICATION.getValue(),
                AppConstant.GA_TRACKING_ACTION.CLICK.getValue(),
                AppConstant.GA_TRACKING_LABEL.SIGN_IN.getValue());
    }


    // presenter
    @Override
    public void showErrorDialog(ErrCode errCode, String extraMsg) {
        if (dialogHelper.networkUnavailability(errCode, extraMsg)) return;
        if (errCode == ErrCode.API_ERROR) {
            dialogHelper.alert("ERROR!", extraMsg);
        } else dialogHelper.showGeneralError();
    }

    // presenter
    @Override
    public void showProgressDialog() {
        dialogHelper.showProgress();
    }

    // presenter
    @Override
    public void dismissProgressDialog() {
        dialogHelper.dismissProgress();
    }

    private SignInValidate validateFields(String email, String password) {
        ErrorIndicatorEditText valid = null;
        StringBuilder stringBuilder = new StringBuilder();
        if (TextUtils.isEmpty(email.trim()) || TextUtils.isEmpty(password.trim())) {
            if (TextUtils.isEmpty(email)) edtEmail.setError("");
            if (TextUtils.isEmpty(password)) edtPassword.setError("");
            else edtPassword.setError(null);
            stringBuilder.append(getString(R.string.input_err_required));
            valid = edtEmail;
        }
        if (!TextUtils.isEmpty(email) && !validator.email(email)) {
            edtEmail.setError("");
            if (stringBuilder.length() > 0) stringBuilder.append("\n");
            stringBuilder.append(getString(R.string.input_err_invalid_email));
            valid = edtEmail;
        } else {
            edtEmail.setError(null);
        }

        if (TextUtils.isEmpty(password) || password.length() < 0) {
            edtPassword.setError("");
            if (stringBuilder.length() > 0) stringBuilder.append("\n");
            stringBuilder.append(getString(R.string.input_err_old_pwd_failed));
            valid = edtEmail;
        } else  {
            edtEmail.setError(null);
        }
        errMessage = stringBuilder.toString();
        return new SignInValidate(valid, errMessage);
    }

    private boolean isExpiredPassword() {
        String email = edtEmail.getText().toString();
        ForgotPasswordStorage passwordStorage = new ForgotPasswordStorage(this);
        return passwordStorage.isExpired(email);
    }


    private void clearForm() {
        edtEmail.setText("");
        edtPassword.setText("");
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private class SignInValidate {
        ErrorIndicatorEditText errorValidate = null;
        String errorMgs = "";

        SignInValidate(ErrorIndicatorEditText errorValidate, String errorMgs) {
            this.errorValidate = errorValidate;
            this.errorMgs = errorMgs;
        }

        boolean isNoHaveErrorInput() {
            return errorValidate == null;
        }
    }
}
