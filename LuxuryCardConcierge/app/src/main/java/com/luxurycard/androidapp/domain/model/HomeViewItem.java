package com.luxurycard.androidapp.domain.model;

import android.support.annotation.NonNull;

public class HomeViewItem implements Comparable<HomeViewItem>{

    public final String id;
    public final String imgUrl;
    /*imgIdDrawable == 0: get image from url*/
    public final int imgIdDrawable;
    public final String title;
    public final String typeItem;
    public final String textSort;
    public final String linkCategory;

    public HomeViewItem(String id, String typeItem, String imgUrl, int imgIdDrawable, String title, String textSort, String linkCategory) {
        this.id = id;
        this.typeItem = typeItem;
        this.imgUrl = imgUrl;
        this.imgIdDrawable = imgIdDrawable;
        this.title = title;
        this.textSort = textSort;
        this.linkCategory = linkCategory;
    }

    @Override
    public int compareTo(@NonNull HomeViewItem o) {
        return this.textSort.compareToIgnoreCase(o.textSort);
    }
}
