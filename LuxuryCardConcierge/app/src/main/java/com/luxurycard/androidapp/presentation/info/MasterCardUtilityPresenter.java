package com.luxurycard.androidapp.presentation.info;

import com.luxurycard.androidapp.App;
import com.luxurycard.androidapp.R;
import com.luxurycard.androidapp.common.constant.AppConstant;
import com.luxurycard.androidapp.common.constant.ErrCode;
import com.luxurycard.androidapp.common.logic.StringUtils;
import com.luxurycard.androidapp.datalayer.entity.GetClientCopyResult;
import com.luxurycard.androidapp.domain.usecases.GetMasterCardCopy;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Thu Nguyen on 6/13/2017.
 */

public class MasterCardUtilityPresenter implements MasterCardUtility.Presenter {

    private GetMasterCardCopy getMasterCardCopy;
    private MasterCardUtility.View view;
    private CompositeDisposable disposables;

    public MasterCardUtilityPresenter(GetMasterCardCopy getMasterCardCopy) {
        disposables = new CompositeDisposable();
        this.getMasterCardCopy = getMasterCardCopy;
    }

    @Override
    public void attach(MasterCardUtility.View view) {
        this.view = view;
    }

    @Override
    public void detach() {
        disposables.dispose();
        this.view = null;
    }

    //<editor-fold desc="load menu About">
    @Override
    public void getMasterCardCopy(String type) {
        if (!App.getInstance().hasNetworkConnection()) {
            view.loadEmptyContent();
            view.showErrorMessage(ErrCode.CONNECTIVITY_PROBLEM);
            return;
        }
        view.showProcessLoading();
        disposables.add(getMasterCardCopy.param(type)
                .on(Schedulers.io(), AndroidSchedulers.mainThread())
                .execute(new MasterCardUtilityPresenter.GetContentFullObserver()));
    }

    private final class GetContentFullObserver extends DisposableSingleObserver<GetClientCopyResult> {

        @Override
        public void onSuccess(GetClientCopyResult result) {
            view.onGetMasterCardCopy(result);
            dispose();
        }

        @Override
        public void onError(Throwable e) {
            view.loadEmptyContent();
            view.showErrorMessage(ErrCode.UNKNOWN_ERROR);
            dispose();
        }
    }
    //</editor-fold>


    @Override
    public String getTitleOfType(AppConstant.MASTERCARD_COPY_UTILITY type) {
        if (type == null) {
            return "";
        }
        String result;
        switch (type) {
            case TermsOfUse:
                result = App.getInstance().getString(R.string.more_term_of_use);
                break;
            case Privacy:
                result = App.getInstance().getString(R.string.more_privacy_policy);
                break;
            case About:
                /*
                String titleAbout = App.getInstance().getString(R.string.more_about);
                result = StringUtils.capitalWords(titleAbout); //-- set title: 'About This App'
                */
                result = App.getInstance().getString(R.string.more_about_title_header);
                break;
            default:
                result = "";
                break;
        }
        return result;
    }
}
