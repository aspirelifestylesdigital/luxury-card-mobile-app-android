package com.luxurycard.androidapp.common.constant;

/**
 * Created by tung.phan on 5/23/2017.
 */

public interface IntentConstant {

    String SELECTED_CATEGORY = "selected_category";
    String SELECTED_CITY = "selected_city";
    String CATEGORY_ID = "category_id";
    String CATEGORY_NAME = "category_name";
    String EXPLORE_DETAIL = "explore_detail";
    String SEARCH_ITEM_DETAIL = "search_item_detail";
    String MASTERCARD_COPY_UTILITY = "mastercard_copy_utility";
    String INVOKE_KEYBOARD = "invoke_keyboard";
    String RESET_EXPLORE_PAGE = "reset_explore_page";

    String AC_SUGGESTED_CONCIERGE = "suggested_concierge";
    String AC_PREF_RESPONSE = "pref_response";
    String AC_TRANSACTION_ID = "transaction_id";
    String PAGE_WELCOME_OPEN = "page_welcome_open";
    String AC_REQUEST_DETAIL = "request_detail";
    String AC_BOOL_DUPLICATE = "boolean_duplicate";

}
