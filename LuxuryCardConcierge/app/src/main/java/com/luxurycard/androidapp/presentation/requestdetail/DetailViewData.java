package com.luxurycard.androidapp.presentation.requestdetail;

import com.luxurycard.androidapp.domain.model.RequestDetailData;
import com.luxurycard.androidapp.domain.model.explore.RequestDetailContent;

import org.json.JSONException;

import java.util.List;

/**
 * Created by vinh.trinh on 9/19/2017.
 */

class DetailViewData {
    final String title;
    final String date;
    final boolean isOpening;
    String details;
    List<RequestDetailContent> contents;

    DetailViewData(RequestDetailData data) throws JSONException {
        TextDetailBuilder textDetailBuilder = new TextDetailBuilder();
        this.isOpening = data.open;
        this.title = textDetailBuilder.nameMapping(data.requestType);
        this.date = data.date;
        this.contents = data.getDetailContent();
    }
}
