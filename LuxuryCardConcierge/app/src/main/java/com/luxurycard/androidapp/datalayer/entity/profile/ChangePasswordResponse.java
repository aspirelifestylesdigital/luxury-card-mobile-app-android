package com.luxurycard.androidapp.datalayer.entity.profile;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by vinh.trinh on 7/24/2017.
 */

public class ChangePasswordResponse {

    public final String message;
    public final boolean success;

    public ChangePasswordResponse(String rawJson) throws JSONException {
        JSONObject json = new JSONObject(rawJson);

        // success
        success = json.getBoolean("success");
        if(success) {
            message = null;
        } else {
            JSONArray messages = json.optJSONArray("message");
            if(null != messages) {
                JSONObject error = messages.getJSONObject(0);
                message = error.getString("message");
            } else {
                message = json.optString("message");
            }
        }
    }
}