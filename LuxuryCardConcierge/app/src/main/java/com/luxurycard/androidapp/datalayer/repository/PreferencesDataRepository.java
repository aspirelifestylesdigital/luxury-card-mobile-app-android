package com.luxurycard.androidapp.datalayer.repository;

import android.text.TextUtils;

import com.luxurycard.androidapp.datalayer.datasource.LocalPrefDataStore;
import com.luxurycard.androidapp.datalayer.datasource.PreferenceData;
import com.luxurycard.androidapp.datalayer.datasource.RemotePrefDataStore;
import com.luxurycard.androidapp.datalayer.preference.PreferencesStorage;
import com.luxurycard.androidapp.domain.repository.UserPreferencesRepository;
import com.luxurycard.androidapp.domain.usecases.GetAccessToken;

import io.reactivex.Completable;
import io.reactivex.Single;

/**
 * Created by vinh.trinh on 7/26/2017.
 */

public class PreferencesDataRepository implements UserPreferencesRepository {

    private LocalPrefDataStore local;
    private RemotePrefDataStore remote;
    private PreferencesStorage preferencesStorage;

    public PreferencesDataRepository(PreferencesStorage preferencesStorage) {
        this.preferencesStorage = preferencesStorage;
        local = new LocalPrefDataStore(preferencesStorage);
        remote = new RemotePrefDataStore(preferencesStorage);
    }

    @Override
    public Single<PreferenceData> load(String accessToken) {
        if(TextUtils.isEmpty(accessToken)) {
            return local.loadPreferences();
        } else {
            return remote.loadPreferences(accessToken)
                    .flatMap(userPreferences -> local.savePreferences(userPreferences)
                                    .andThen(local.loadPreferences())
                    );
        }
    }

    @Override
    public Completable save(PreferenceData data, String accessToken) {
        if(data.hasToBeCreated()) {
            return remote.createPreferences(data, accessToken)
                    .andThen(remote.loadPreferences(accessToken))
                    .flatMapCompletable(pData1 -> {
                        pData1 = pData1.applyChanges(data.cuisine, data.hotel, data.vehicle);
                        pData1.extValue = data.extValue;
                        pData1.selectCity = data.selectCity;
                        return prerequisiteStep(pData1, accessToken);
                    });
        } else {
            return prerequisiteStep(data, accessToken);
        }
    }

    private Completable prerequisiteStep(PreferenceData preferenceData, String accessToken) {
        return remote.updatePreferences(preferenceData, accessToken)
                .andThen(local.savePreferences(preferenceData));
    }

    public Completable saveSelectCity(String nameCity){
        GetAccessToken getAccessToken = new GetAccessToken(preferencesStorage);
        return getAccessToken.execute().flatMapCompletable(accessToken -> {
            PreferenceData preferenceData = preferencesStorage.userPreferences();
            preferenceData.selectCity = TextUtils.isEmpty(nameCity) ? "" : nameCity;
            return save(preferenceData, accessToken);
        });
    }
}
