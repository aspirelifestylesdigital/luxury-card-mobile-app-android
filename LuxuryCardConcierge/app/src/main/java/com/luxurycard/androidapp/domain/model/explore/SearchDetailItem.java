package com.luxurycard.androidapp.domain.model.explore;

import android.os.Parcel;

import com.luxurycard.androidapp.common.constant.AppConstant;

/**
 * Created by vinh.trinh on 6/23/2017.
 */

public class SearchDetailItem extends ExploreRView {

    public final Integer ID;
    public final Integer secondaryID;
    public final String title;
    public final String category; // IA, ??
    public int cityGuideSubCategoryIndex = -1;

    public SearchDetailItem(Integer ID, Integer secondaryID, String title, String category) {
        this.ID = ID;
        this.secondaryID = secondaryID;
        this.title = title;
        this.category = category;
    }

    protected SearchDetailItem(Parcel in) {
        this.ID = in.readInt();
        this.secondaryID = in.readInt();
        this.title = in.readString();
        this.category = in.readString();
        this.cityGuideSubCategoryIndex = in.readInt();
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getDescription() {
        return null;
    }

    @Override
    public String getImageUrl() {
        return null;
    }

    @Override
    public Integer getId() {
        return ID;
    }

    @Override
    public String getSuggestedToAC() {
        return null;
    }

    @Override
    public String getRequestTypeName() {
        AppConstant.EXPLORE_CATEGORY categoryEnum = AppConstant.EXPLORE_CATEGORY.getEnum(category);
        if(categoryEnum != null){
            switch (categoryEnum){
                case DINING: // Dining
                case CULINARY_EXPERIENCE:
                    return "Restaurant Request";
                case HOTEL: // Hotel
                    return "HOTEL AND B&B Request";
                case GOLF: // Golf
                case GOLF_EXPERIENCE:
                case GOLF_MERCHANDISE:
                    return "Golf Request";
                case CAR_RENTAL:
                case CAR_TRANSFER:
                    return "Limo and Sedan Request";
                case ENTERTAINMENT:
                case ENTERTAINMENT_EXPERIENCE:
                case ENTERTAINMENT_MAJOR_SPORTS_EVENTS:
                    return "Entertainment";
                case TOUR_SUBCAT:
                    return "Sightseeing/Tours Request";
                case AIRPORT_SERVICES_SUBCAT:
                    return "Airport Services Request";
                case CRUISE:
                    return "Cruise Request";
                case FLOWER:
                    return "Flowers/Gift Basket Request";
                case TRAVEL:
                    return "Private Jet Request";
                case VACATION_PACKAGES:
                    return "Other Request";
                default:
                    return "Other Request";
            }
        }else{
            return "";
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(ID);
        dest.writeInt(secondaryID);
        dest.writeString(title);
        dest.writeString(category);
        dest.writeInt(cityGuideSubCategoryIndex);
    }

    public static final Creator<SearchDetailItem> CREATOR = new Creator<SearchDetailItem>() {
        @Override
        public SearchDetailItem createFromParcel(Parcel in) {
            return new SearchDetailItem(in);
        }

        @Override
        public SearchDetailItem[] newArray(int size) {
            return new SearchDetailItem[size];
        }
    };
}
