package com.luxurycard.androidapp.presentation.requestdetail;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.luxurycard.androidapp.R;
import com.luxurycard.androidapp.domain.model.explore.RequestDetailContent;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;

/**
 * Created by Den on 4/4/18.
 */

public class RequestDetailAdapter extends RecyclerView.Adapter<RequestDetailAdapter.RequestDetailViewHolder>  {


    private List<RequestDetailContent> data;

    RequestDetailAdapter(List<RequestDetailContent> data) {
        this.data = new ArrayList<>(data);
    }

    @Override
    public RequestDetailViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.request_detail_item, parent, false);
        return new RequestDetailViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RequestDetailViewHolder holder, int position) {
        RequestDetailContent datum = data.get(position);
        holder.tvTitle.setText(datum.getTitle());
        if (TextUtils.isEmpty(datum.getDescription())) {
            holder.tvContent.setVisibility(View.GONE);
        } else {
            holder.tvContent.setVisibility(View.VISIBLE);
            holder.tvContent.setText(datum.getDescription());
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class RequestDetailViewHolder extends RecyclerView.ViewHolder {
//        private TextView cityRegionName, StateCountry;
//        private RelativeLayout parentView;
        private TextView tvTitle, tvContent;

        RequestDetailViewHolder(View view) {
            super(view);
            tvTitle = ButterKnife.findById(view, R.id.tv_title);
            tvContent = ButterKnife.findById(view, R.id.tv_content);
        }
    }
}
