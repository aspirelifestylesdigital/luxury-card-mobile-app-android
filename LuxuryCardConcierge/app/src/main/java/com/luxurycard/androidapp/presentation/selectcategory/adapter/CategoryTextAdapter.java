package com.luxurycard.androidapp.presentation.selectcategory.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.luxurycard.androidapp.App;
import com.luxurycard.androidapp.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;

public class CategoryTextAdapter extends RecyclerView.Adapter<CategoryTextAdapter.CategoryViewHolder> {
    private final List<String> data;
    private CategoryListener listener;
    private String textCityGuide;

    public CategoryTextAdapter(List<String> data, CategoryListener listener) {
        this.data = new ArrayList<>(data);
        this.listener = listener;
        textCityGuide = App.getInstance().getString(R.string.city_guide);
    }

    @Override
    public CategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.explore_category_item, parent, false);
        return new CategoryViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CategoryViewHolder holder, int position) {
        final String cityRViewItem = data.get(position);
        bindView(holder, cityRViewItem);
    }

    private void bindView(CategoryViewHolder holder, String category) {
        holder.categoryName.setText(category);

        if(category.equalsIgnoreCase(textCityGuide)){
            holder.cateSubArrowRight.setVisibility(View.VISIBLE);
        }else {
            holder.cateSubArrowRight.setVisibility(View.GONE);
        }
    }

    public String getItem(int pos) {
        return data.get(pos);
    }

    public void add(String datum) {
        this.data.add(datum);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class CategoryViewHolder extends RecyclerView.ViewHolder {
        private TextView categoryName;
        private ImageView cateSubArrowRight;

        CategoryViewHolder(View view) {
            super(view);
            categoryName = ButterKnife.findById(view, R.id.item_name);
            cateSubArrowRight = ButterKnife.findById(view, R.id.explore_cate_arrow_left);
            view.setOnClickListener(v ->
                    {
                        if (listener != null) {
                            listener.onItemClick(categoryName.getText().toString());
                        }
                    }

            );
        }
    }

    public interface CategoryListener {
        void onItemClick(String category);
    }
}
