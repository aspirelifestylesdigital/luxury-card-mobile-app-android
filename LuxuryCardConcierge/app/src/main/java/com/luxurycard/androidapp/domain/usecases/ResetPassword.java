package com.luxurycard.androidapp.domain.usecases;

import com.luxurycard.androidapp.datalayer.entity.profile.ChangePasswordRequest;
import com.luxurycard.androidapp.datalayer.entity.profile.ChangePasswordResponse;
import com.luxurycard.androidapp.datalayer.preference.ForgotPasswordStorage;
import com.luxurycard.androidapp.datalayer.preference.PreferencesStorage;
import com.luxurycard.androidapp.datalayer.retro2client.AppHttpClient;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by tung.phan on 5/18/2017.
 */

public class ResetPassword
        extends UseCase<Void, ResetPassword.Params> {

    private PreferencesStorage preferencesStorage;

    public ResetPassword(PreferencesStorage preferencesStorage) {
        this.preferencesStorage = preferencesStorage;
    }

    @Override
    Observable<Void> buildUseCaseObservable(Params params) {
        return null;
    }

    @Override
    Single<Void> buildUseCaseSingle(Params params) {
        return null;
    }

    @Override
    public Completable buildUseCaseCompletable(Params params) {
        return Completable.create(e -> {
            ChangePasswordRequest body = new ChangePasswordRequest(params.email,params.newPassword,params.oldPassword);
            Call<ResponseBody> request = AppHttpClient.getInstance().userManagementApi().changePassword(body);
            Response<ResponseBody> response = request.execute();
            if(response.isSuccessful()) {
                ChangePasswordResponse
                        changePasswordResponse = new ChangePasswordResponse(response.body().string());
                if(changePasswordResponse.success) {
                    preferencesStorage.editor().hasForgotPwd(false).build().save();
                    e.onComplete();
                } else {
                    e.onError(new Exception(changePasswordResponse.message));
                }
            } else {
                e.onError(new Exception(response.errorBody().string()));
            }
        });
    }

    public static class Params {
        private final String email;
        private final String oldPassword;
        private final String newPassword;

        public Params(String email,String oldPassword, String newPassword) {
            this.email = email;
            this.oldPassword = oldPassword;
            this.newPassword = newPassword;
        }
    }
}
