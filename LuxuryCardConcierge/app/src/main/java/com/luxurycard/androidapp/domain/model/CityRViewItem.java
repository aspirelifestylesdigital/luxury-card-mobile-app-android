package com.luxurycard.androidapp.domain.model;

/**
 * Created by vinh.trinh on 5/12/2017.
 */

public class CityRViewItem {

    private String city;//can also be region name
    private String state;//can also be country

    public CityRViewItem(String city, String state) {
        this.city = city;
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public String getState() {
        return state;
    }

}
