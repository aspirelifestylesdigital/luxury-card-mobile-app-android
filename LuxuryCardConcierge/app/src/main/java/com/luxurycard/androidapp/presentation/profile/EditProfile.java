package com.luxurycard.androidapp.presentation.profile;

import com.luxurycard.androidapp.common.constant.ErrCode;
import com.luxurycard.androidapp.domain.model.Profile;
import com.luxurycard.androidapp.presentation.base.BasePresenter;

/**
 * Created by vinh.trinh on 5/11/2017.
 */

public interface EditProfile {

    interface View {
        void showProfile(Profile profile, boolean fromRemote);
        void showErrorDialog(ErrCode errCode, String extraMsg);
        void showProgressDialog();
        void dismissProgressDialog();
        void profileUpdated();
    }

    interface Presenter extends BasePresenter<View> {
        void getProfile();
        void updateProfile(Profile profile);
        void abort();
    }
}
