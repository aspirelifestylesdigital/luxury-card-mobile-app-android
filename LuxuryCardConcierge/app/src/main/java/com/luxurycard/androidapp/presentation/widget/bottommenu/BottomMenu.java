package com.luxurycard.androidapp.presentation.widget.bottommenu;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.luxurycard.androidapp.R;
import com.support.mylibrary.widget.ClickGuard;

import butterknife.ButterKnife;

public class BottomMenu extends FrameLayout {

    public enum Item {HOME, REQUEST, ACCOUNT, MORE}

    private FrameLayout home;
    private FrameLayout request;
    private FrameLayout account;
    private FrameLayout more;

    private TextView requestBadge;
    private TextView homeBadge;
    private TextView accountBadge;
    private TextView moreBadge;

    /**
     * last item bottom menu selected
     */
    private FrameLayout currentTab;

    public interface OnMenuItemClickListener {
        void onMenuItemClick(int position);

        void onBackRootFragment();
    }

    OnMenuItemClickListener callback;

    public BottomMenu(Context context) {
        super(context);
        init(context);
    }

    public BottomMenu(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    /**
     * Find views and set listeners and tags
     *
     * @param context
     */
    private void init(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.view_bottom_menu, this, true);
        ButterKnife.bind(this, view);

        home = ButterKnife.findById(view, R.id.bottom_menu_home);
        home.setOnClickListener(onClickListener);
        home.setTag(Item.HOME);

        request = ButterKnife.findById(view, R.id.bottom_menu_request);
        request.setOnClickListener(onClickListener);
        request.setTag(Item.REQUEST);

        account = ButterKnife.findById(view, R.id.bottom_menu_account);
        account.setOnClickListener(onClickListener);
        account.setTag(Item.ACCOUNT);

        more = ButterKnife.findById(view, R.id.bottom_menu_more);
        more.setOnClickListener(onClickListener);
        more.setTag(Item.MORE);

        homeBadge = ButterKnife.findById(view, R.id.bottom_menu_home_badge);
        requestBadge = ButterKnife.findById(view, R.id.bottom_menu_request_badge);
        accountBadge = ButterKnife.findById(view, R.id.bottom_menu_account_badge);
        moreBadge = ButterKnife.findById(view, R.id.bottom_menu_more_badge);

        ClickGuard.guard(500L,home,request, account,more);
    }

    public void setCallback(OnMenuItemClickListener callback) {
        this.callback = callback;
    }

    private OnClickListener onClickListener = v -> {
        Item itemEnum = (Item) v.getTag();
        if (callback != null) {
            FrameLayout onClickTab = getButton(itemEnum);
            if(currentTab == onClickTab){
                //-- open webView hide color tab. otherwise enable color
                if(more == onClickTab && !more.isSelected()){
                    more.setSelected(true);
                }
                callback.onBackRootFragment();
            }else{
                buttonActive(itemEnum);
                callback.onMenuItemClick(activateButtonViewPager(itemEnum));
            }
        }
    };

    public void setDefaultHomeTab(){
        buttonActive(Item.HOME);
    }

    private void buttonActive(Item itemEnum){
        FrameLayout button = getButton(itemEnum);
        if (button != null) {
            if(currentTab != null){
                currentTab.setSelected(false);
            }
            button.setSelected(true);
            currentTab = button;
        }
    }

    /*public void hideColorTabMore(){
        if(more != null){
            more.setSelected(false);
        }
    }

    public void showColorTabMore(){
        if(more != null){
            more.setSelected(true);
        }
    }*/


    public int activateButtonViewPager(Item itemEnum) {
        switch (itemEnum) {
            case HOME:
                return 0;
            case REQUEST:
                return 1;
            case ACCOUNT:
                return 2;
            default: //tab more
                return 3;
        }
    }


    /**
     * Activates button as if it was clicked (presents fragment and sets its view as selected)
     *
     * @param itemEnum Button to activate
     */

    public void activateButton(Item itemEnum) {
        /*BottomMenuProvider.BottomMenuEntry entry = BottomMenuProvider.getEntry(itemEnum);
        try {
            Constructor constructor = entry.fragmentClass.getConstructor();
            BaseFragment fragment = (BaseFragment) constructor.newInstance();
            fragment.setArguments(entry.fragmentArguments);
            if (getContext() instanceof NavBottomActivity) {
                ((NavBottomActivity) getContext()).presentFragment(fragment);
                setButtonActive(itemEnum);
            }//--  do nothing here
        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }

    /**
     * Sets button's view as selected without presenting its fragment
     *
     * @param itemEnum Button to set as currentTab
     */
    public void setButtonActive(Item itemEnum) {
        if (currentTab != null) {
            currentTab.setSelected(false);
            currentTab.setEnabled(true);
        }
        FrameLayout button = getButton(itemEnum);
        if (button != null) {
            button.setSelected(true);
            button.setEnabled(false);
            //button.setEnabled(false);//uncomment to disable clicking on already selected bottom menu item
            currentTab = button;
        }
        hideBadges(itemEnum);
    }

    /**
     * Sets button's view as selected without presenting its fragment
     * case go in child fragment on set bottom menu
     *
     * @param itemEnum Button to set as currentTab
     */

    public void setButtonActiveWithListener(Item itemEnum, boolean enabled) {
        if (currentTab != null) {
            currentTab.setSelected(false);
            currentTab.setEnabled(true);
        }
        FrameLayout button = getButton(itemEnum);
        if (button != null) {
            button.setSelected(true);
            button.setEnabled(enabled);
            button.setEnabled(false);
            currentTab = button;
        }
        hideBadges(itemEnum);
    }

    public void hideBadges(Item itemEnum) {
        /*if (itemEnum == null){
            return;
        }

//         only check Bottom Item Request
        TextView tvRequest = getBadge(Item.REQUEST);
        if (tvRequest != null) {
            String numberRequest = tvRequest.getText().toString();
            if (itemEnum == Item.REQUEST)
                hideView(tvRequest);
            else if (!TextUtils.isEmpty(numberRequest) && !"0".equals(numberRequest)) {
                showView(tvRequest);
            }
        }*/
    }

    private void showView(View view) {
        view.setVisibility(View.VISIBLE);
    }

    private void hideView(View view) {
        view.setVisibility(View.GONE);
    }

    /**
     * Return button view by enum ID
     *
     * @param itemEnum Button enum ID
     * @return
     */
    private FrameLayout getButton(Item itemEnum) {
        if (itemEnum == null) {
            return null;
        }
        switch (itemEnum) {
            case HOME:
                return home;
            case REQUEST:
                return request;
            case ACCOUNT:
                return account;
            case MORE:
                return more;
        }
        return null;
    }

    public FrameLayout getRequest() {
        return request;
    }

    /**
     * Return button's badge by enum ID
     *
     * @param itemEnum Button enum ID
     * @return
     */
    /*public TextView getBadge(Item itemEnum) {
        if (itemEnum == null) {
            return null;
        }
        switch (itemEnum) {
            case HOME:
                return homeBadge;
            case REQUEST:
                return requestBadge;
            case ACCOUNT:
                return accountBadge;
            case MORE:
                return moreBadge;
        }
        return null;
    }*/

}
