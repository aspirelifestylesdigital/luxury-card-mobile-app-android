package com.luxurycard.androidapp.datalayer.datasource;

import com.luxurycard.androidapp.datalayer.preference.PreferencesStorage;
import com.luxurycard.androidapp.domain.model.Profile;

import org.json.JSONException;

import io.reactivex.Completable;
import io.reactivex.Single;

public class LocalProfileDataStore {

    private PreferencesStorage preferencesStorage;

    public LocalProfileDataStore(PreferencesStorage preferencesStorage) {
        this.preferencesStorage = preferencesStorage;
    }

    public Single<Profile> loadProfile() {
        return Single.create(emitter -> {
            try {
                Profile profile = preferencesStorage.profile();
                emitter.onSuccess(profile);
            } catch (JSONException e) {
                emitter.onError(e);
            }
        });
    }

    public Completable saveProfile(Profile profile) {
        return Completable.create(e -> {
            preferencesStorage.editor().profile(profile).build().save();
            e.onComplete();
        });
    }

    public ParamSaveLocation saveLocationProfileLocal(Profile profile, PreferenceData preferenceData){
        try {
            profile.locationToggle("Yes".equalsIgnoreCase(preferenceData.extValue));
            preferencesStorage.editor().profile(profile).build().save();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return new ParamSaveLocation(preferenceData, profile);
    }

    public class ParamSaveLocation{
        private PreferenceData preferenceData;
        private Profile profile;

        ParamSaveLocation(PreferenceData preferenceData, Profile profile) {
            this.preferenceData = preferenceData;
            this.profile = profile;
        }

        public PreferenceData getPreferenceData() {
            return preferenceData;
        }

        public Profile getProfile() {
            return profile;
        }
    }

}
