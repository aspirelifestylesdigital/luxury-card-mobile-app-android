package com.luxurycard.androidapp.presentation.more;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.luxurycard.androidapp.R;
import com.luxurycard.androidapp.common.constant.RequestCode;
import com.luxurycard.androidapp.presentation.base.BaseNavSubFragment;
import com.luxurycard.androidapp.presentation.changepass.ChangePasswordActivity;
import com.luxurycard.androidapp.presentation.home.HomeNavBottomActivity;
import com.luxurycard.androidapp.presentation.preferences.UserPreferencesActivity;
import com.luxurycard.androidapp.presentation.profile.MyProfileActivity;

import butterknife.OnClick;

/**
 * Created by vinh.trinh on 9/7/2017.
 */

public class MySettingsFragment extends BaseNavSubFragment {

    public static MySettingsFragment newInstance() {
        Bundle args = new Bundle();
        MySettingsFragment fragment = new MySettingsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public int setupCreateViewLayoutId() {
        return R.layout.fragment_profile_navigator;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setTitle(getString(R.string.more_my_settings));
        getButtonBack().setOnClickListener(v -> this.onBackPressed());
    }

    @OnClick(R.id.profile_nav_profile)
    public void openProfile(View view) {
        if(getActivity() instanceof HomeNavBottomActivity){
            Intent intent = new Intent(getActivity(), MyProfileActivity.class);
            getActivity().startActivityForResult(intent, RequestCode.PROFILE);
        }
    }

    @OnClick(R.id.profile_nav_preferences)
    public void openPreferences(View view) {
        if(getActivity() instanceof HomeNavBottomActivity) {
            Intent intent = new Intent(getActivity(), UserPreferencesActivity.class);
            getActivity().startActivityForResult(intent, RequestCode.USER_PREFERENCES);
        }
    }

    @OnClick(R.id.profile_nav_change_pwd)
    public void openChangePassword(View view) {
        if(getActivity() instanceof HomeNavBottomActivity) {
            Intent intent = new Intent(getActivity(), ChangePasswordActivity.class);
            getActivity().startActivityForResult(intent, RequestCode.CHANGE_PASSWORD);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}
