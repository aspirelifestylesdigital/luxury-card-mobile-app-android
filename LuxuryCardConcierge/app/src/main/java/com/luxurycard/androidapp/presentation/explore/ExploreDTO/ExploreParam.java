package com.luxurycard.androidapp.presentation.explore.ExploreDTO;

public final class ExploreParam {

    private String selectedCategory;
    /* get data from page Sub Category (City Guide)*/
    private boolean isPageSubCategory = false;
    private ExploreParamSubCategory subCategoryDTO = null;
    private ExploreParamSearch searchDTO = null;

    public ExploreParam(String selectedCategory) {
        this.selectedCategory = selectedCategory;
    }

    public ExploreParam(ExploreParamSubCategory subCategoryDTO) {
        this.subCategoryDTO = subCategoryDTO;
        //-- default handle follow sub category
        isPageSubCategory = true;
    }

    public ExploreParam(ExploreParamSearch searchDTO) {
        this.searchDTO = searchDTO;
    }

    public ExploreParamSearch getSearchDTO() {
        return searchDTO;
    }

    public String getSelectedCategory() {
        return selectedCategory;
    }

    public boolean isPageSubCategory() {
        return isPageSubCategory;
    }

    public ExploreParamSubCategory getSubCategoryDTO() {
        return subCategoryDTO;
    }
}
