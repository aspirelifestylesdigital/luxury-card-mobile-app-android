package com.luxurycard.androidapp.presentation.venuedetail;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.BaseTarget;
import com.bumptech.glide.request.target.SizeReadyCallback;
import com.luxurycard.androidapp.App;
import com.luxurycard.androidapp.R;
import com.luxurycard.androidapp.common.constant.IntentConstant;
import com.luxurycard.androidapp.common.constant.RequestCode;
import com.luxurycard.androidapp.common.glide.GlideHelper;
import com.luxurycard.androidapp.common.logic.HtmlTagHandler;
import com.luxurycard.androidapp.common.logic.HtmlUtils;
import com.luxurycard.androidapp.common.logic.PermissionUtils;
import com.luxurycard.androidapp.common.logic.ShareHelper;
import com.luxurycard.androidapp.common.logic.TextViewLinkHandler;
import com.luxurycard.androidapp.common.logic.urlimage.URLImageParser;
import com.luxurycard.androidapp.datalayer.preference.PreferencesStorage;
import com.luxurycard.androidapp.datalayer.repository.B2CDataRepository;
import com.luxurycard.androidapp.domain.model.explore.ExploreRView;
import com.luxurycard.androidapp.domain.model.explore.OtherExploreDetailItem;
import com.luxurycard.androidapp.domain.usecases.GetContentFull;
import com.luxurycard.androidapp.presentation.base.BaseNavSubFragment;
import com.luxurycard.androidapp.presentation.request.AskConciergeActivity;
import com.luxurycard.androidapp.presentation.widget.DialogHelper;
import com.support.mylibrary.widget.LetterSpacingTextView;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Den on 4/12/18.
 */

public class OtherExploreDetailFragment extends BaseDetailFragment implements OtherExploreDetail.View {


    @BindView(R.id.loading)
    ProgressBar pbLoading;

    @BindView(R.id.explore_detail_layout)
    View exploreDetailLayout;
    @BindView(R.id.tvStub)
    TextView tvStub;
    @BindView(R.id.explore_detail_image)
    ImageView ivDetailImage;
    @BindView(R.id.explore_action_book)
    ImageButton ivActionBook;
    @BindView(R.id.explore_action_share)
    ImageButton ivActionShare;
    @BindView(R.id.explore_name)
    TextView tvExploreName;
    @BindView(R.id.explore_your_benefit_root_layout)
    View benefitLayout;
    @BindView(R.id.iv_benefit)
    ImageView ivBenefit;
    @BindView(R.id.your_benefit_content)
    LetterSpacingTextView tvBenefitContent;
    @BindView(R.id.explore_description_layout)
    View exploreDescriptionLayout;
    @BindView(R.id.explore_description)
    LetterSpacingTextView tvExploreDescription;
    @BindView(R.id.explore_terms_of_use_layout)
    View termsOfUseLayout;
    @BindView(R.id.explore_terms_of_use)
    LetterSpacingTextView tvTermsOfUse;

    private OtherExploreDetailItem exploreRViewItem;
    OtherExploreDetailPresenter presenter;

    Bitmap detailBitmap;

    ExploreRView detailViewData;

    public static OtherExploreDetailFragment newInstance(String titleExplore, ExploreRView detailViewData) {
        Bundle args = new Bundle();
        OtherExploreDetailFragment fragment = new OtherExploreDetailFragment();
        args.putString("explore_title", titleExplore);
        args.putParcelable("explore_other_detail", detailViewData);
        fragment.setArguments(args);
        return fragment;
    }

    private OtherExploreDetailPresenter buildPresenter() {
        return new OtherExploreDetailPresenter(
                new PreferencesStorage(App.getInstance().getApplicationContext()),
                new GetContentFull(new B2CDataRepository()));
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = buildPresenter();
        presenter.attach(this);
        trackGA(savedInstanceState);
    }

    @Override
    public int setupCreateViewLayoutId() {
        return R.layout.activity_other_explore_detail;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getBtnCallConcierge().setVisibility(View.VISIBLE);
        getButtonBack().setOnClickListener(view1 -> this.onBackPressed());
        tvExploreDescription.setMovementMethod(textViewLinkHandler);
        tvBenefitContent.setMovementMethod(textViewLinkHandler);
        tvTermsOfUse.setMovementMethod(textViewLinkHandler);

        // Get data
        //setTitle(Html.fromHtml(detailViewData.getTitle()));

        exploreDetailLayout.setVisibility(View.INVISIBLE);

        if (getArguments() == null) {
            return;
        }
        detailViewData = getArguments().getParcelable("explore_other_detail");
        String title = getArguments().getString("explore_title", "");
        setTitle(title);
        //hold get category
        //ExploreDetailLogic.setTitleDetail(tvTitle, title);

        if (detailViewData == null) {
            return;
        }
//            setTitle(detailViewData.getTitle());
        presenter.getContent(detailViewData.getId());
    }

    @Override
    public void onDestroy() {
        presenter.detach();
        super.onDestroy();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PermissionUtils.EXTERNAL_STORAGE_REQUEST_CODE:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Call share again
                    ShareHelper.getInstance().share(getActivity(), exploreRViewItem, detailBitmap);
                }
                break;
        }
    }


//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if(requestCode == 9175 && resultCode == RESULT_OK) {
//            finish();
//        }
//    }
    private void renderUI(){
        if(exploreRViewItem != null){
            // Description and image
            tvExploreDescription.setText(Html.fromHtml(HtmlUtils.adjustSomeHtmlTag(exploreRViewItem.description), new URLImageParser(tvExploreDescription, getActivity(), false, new URLImageParser.IImageParserCallback() {
                boolean isGetImageAtFirstTime;
                @Override
                public void onImageParserDone(String url) {
                    if(!TextUtils.isEmpty(url) && url.contains("http") && !isGetImageAtFirstTime) {
                        isGetImageAtFirstTime = true;
                        GlideHelper.getInstance().loadImage(url,
                                0, ivDetailImage, 0, new BaseTarget<Bitmap>() {
                                    @Override
                                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                        detailBitmap = resource;
                                    }

                                    @Override
                                    public void getSize(SizeReadyCallback cb) {

                                    }
                                });
                    }
                }
            }), new HtmlTagHandler()));
            // Dining name
            if(TextUtils.isEmpty(exploreRViewItem.title)){
                tvExploreName.setVisibility(View.GONE);
            }else{
                tvExploreName.setVisibility(View.VISIBLE);
                tvExploreName.setText(Html.fromHtml(exploreRViewItem.title));
//                setTitle(exploreRViewItem.title);
            }

            if(TextUtils.isEmpty(exploreRViewItem.benefit)){
                benefitLayout.setVisibility(View.GONE);
            }else{
                // Benefit
                Spanned benefitSpanned = Html.fromHtml(HtmlUtils.adjustSomeHtmlTag(exploreRViewItem.benefit), new URLImageParser(tvBenefitContent, getActivity(), false, null),
                        new HtmlTagHandler());
                if(TextUtils.isEmpty(benefitSpanned.toString().trim())){
                    benefitLayout.setVisibility(View.GONE);
                }else{
                    benefitLayout.setVisibility(View.VISIBLE);
                    tvBenefitContent.setText(benefitSpanned);
                }
            }

            // Terms of Use
            tvTermsOfUse.setText(Html.fromHtml(HtmlUtils.adjustSomeHtmlTag(exploreRViewItem.getDisplayTermsOfUse()), null,
                    new HtmlTagHandler()));
            termsOfUseLayout.setVisibility( View.GONE);
//            tvTermsOfUse.setText(exploreRViewItem.getDisplayTermsOfUse());
        }
    }
    @OnClick({R.id.explore_action_book, R.id.explore_action_share})
    public void onClick(View view){
        switch (view.getId()){
            case R.id.explore_action_book:
                Bundle bundle = new Bundle();
                bundle.putString(IntentConstant.SELECTED_CATEGORY, exploreRViewItem.getRequestTypeName());
                bundle.putString(IntentConstant.AC_SUGGESTED_CONCIERGE, exploreRViewItem.getSuggestedToAC());
                openNewRequestDetailActivity(bundle);
                break;
            case R.id.explore_action_share:
                ShareHelper.getInstance().share(getActivity(), exploreRViewItem, detailBitmap);
                break;
        }
    }

    @Override
    public void onGetContentFullFinished(OtherExploreDetailItem exploreRViewItem) {
        this.exploreRViewItem = exploreRViewItem;
        hideLoadingFragment();
        exploreDetailLayout.setVisibility(View.VISIBLE);
        renderUI();
        /* Tooltip View */
        presenter.handleTooltip();
    }

    @Override
    public void hideLoadingFragment() {
        pbLoading.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showLoadingFragment() {
        pbLoading.setVisibility(View.VISIBLE);
    }

    @Override
    public void noInternetMessage() {
//        dialogHelper.networkUnavailability(ErrCode.CONNECTIVITY_PROBLEM, null);
        showErrorNoNetwork();

    }

    private TextViewLinkHandler textViewLinkHandler = new TextViewLinkHandler() {
        @Override
        public void onLinkClick(String url) {
            new DialogHelper(getActivity()).showLeavingAlert(url);
        }
    };

    @Override
    public View getButtonBook() {
        return ivActionBook;
    }
}
