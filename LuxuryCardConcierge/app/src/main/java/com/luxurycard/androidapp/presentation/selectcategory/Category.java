package com.luxurycard.androidapp.presentation.selectcategory;

import android.content.Context;

import com.luxurycard.androidapp.presentation.base.BasePresenter;
import com.luxurycard.androidapp.presentation.selectcategory.adapter.CategoryTextAdapter;


/**
 * Created by tung.phan on 5/31/2017.
 */

public interface Category {

    interface View {
        void proceedWithDiningCategory();
        void askForLocationSetting();
    }

    interface Presenter extends BasePresenter<Category.View> {

        void loadProfile();
        boolean isLocationProfileSettingOn();
        void locationServiceCheck(Context context);
    }
}
