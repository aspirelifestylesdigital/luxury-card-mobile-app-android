package com.luxurycard.androidapp.datalayer.repository;

import android.text.TextUtils;

import com.luxurycard.androidapp.datalayer.datasource.LocalProfileDataStore;
import com.luxurycard.androidapp.datalayer.datasource.PreferenceData;
import com.luxurycard.androidapp.datalayer.datasource.RemoteProfileDataStore;
import com.luxurycard.androidapp.datalayer.preference.PreferencesStorage;
import com.luxurycard.androidapp.datalayer.retro2client.AppHttpClient;
import com.luxurycard.androidapp.domain.model.Profile;
import com.luxurycard.androidapp.domain.repository.ProfileRepository;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

/**
 * Created by vinh.trinh on 5/11/2017.
 */

public class ProfileDataRepository implements ProfileRepository {

    private LocalProfileDataStore local;
    private RemoteProfileDataStore remote;

    public ProfileDataRepository(PreferencesStorage pref) {
        this.local = new LocalProfileDataStore(pref);
        this.remote = new RemoteProfileDataStore(pref);
    }

    @Override
    public Single<Profile> loadProfile(String accessToken) {
        if(TextUtils.isEmpty(accessToken)) {
            return local.loadProfile();
        } else {
            return remote.loadProfile(accessToken)
                    .flatMap(profile -> local.saveProfile(profile).andThen(local.loadProfile()));
        }
    }

    @Override
    public Completable saveProfile(Profile profile, String password) {
        return remote.saveProfile(profile, password)
                .andThen(local.saveProfile(profile));
    }

    @Override
    public LocalProfileDataStore.ParamSaveLocation saveLocationProfileLocal(Profile profile, PreferenceData preferenceData) {
        return local.saveLocationProfileLocal(profile, preferenceData);
    }
}
