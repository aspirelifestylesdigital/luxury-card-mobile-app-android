package com.luxurycard.androidapp.presentation.more;

import com.luxurycard.androidapp.presentation.base.BasePresenter;

public interface More {

    interface View {

        void showDate(String date);

    }

    interface Presenter extends BasePresenter<View> {

        void setUpDate();

    }
}
