package com.luxurycard.androidapp.domain.usecases;

import com.luxurycard.androidapp.domain.repository.ACRepository;

/**
 * Created by vinh.trinh on 8/30/2017.
 */

public class EditRequest extends CreateRequest {

    public EditRequest(ACRepository repository) {
        super(repository);
    }
}
