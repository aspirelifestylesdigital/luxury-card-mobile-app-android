package com.luxurycard.androidapp.presentation.profile;

import com.luxurycard.androidapp.App;
import com.luxurycard.androidapp.common.BackendException;
import com.luxurycard.androidapp.common.constant.ErrCode;
import com.luxurycard.androidapp.datalayer.datasource.LocalProfileDataStore;
import com.luxurycard.androidapp.domain.model.Profile;
import com.luxurycard.androidapp.domain.usecases.GetAccessToken;
import com.luxurycard.androidapp.domain.usecases.GetUserPreferences;
import com.luxurycard.androidapp.domain.usecases.LoadProfile;
import com.luxurycard.androidapp.domain.usecases.SaveProfile;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by vinh.trinh on 5/11/2017.
 */

public class EditProfilePresenter implements EditProfile.Presenter {
    private CompositeDisposable disposables;
    private LoadProfile loadProfile;
    private SaveProfile saveProfile;
    private GetAccessToken getAccessToken;
    private GetUserPreferences getUserPreferences;
    private EditProfile.View view;

    EditProfilePresenter(GetAccessToken getAccessToken, LoadProfile loadProfile, SaveProfile saveProfile,
                         GetUserPreferences getUserPreferences) {
        disposables = new CompositeDisposable();
        this.loadProfile = loadProfile;
        this.saveProfile = saveProfile;
        this.getAccessToken = getAccessToken;
        this.getUserPreferences = getUserPreferences;
    }

    @Override
    public void attach(EditProfile.View view) {
        this.view = view;
    }

    @Override
    public void detach() {
        disposables.dispose();
        view = null;
    }

    @Override
    public void getProfile() {
        view.showProgressDialog();
        getAccessToken.execute()
                      .flatMap(accessToken -> loadProfile.buildUseCaseSingle(accessToken).zipWith(getUserPreferences.buildUseCaseSingle(accessToken), (profile, preferenceData) -> {
                          LocalProfileDataStore.ParamSaveLocation param = loadProfile.saveLocationInProfile(profile, preferenceData);
                          return param.getProfile();
                      }))
                      .subscribeOn(Schedulers.io())
                      .observeOn(AndroidSchedulers.mainThread())
                      .subscribeWith(new LoadProfileObserver());

    }

    public void getProfileLocal(){
        loadProfile.buildUseCaseSingle(null).zipWith(getUserPreferences.buildUseCaseSingle(null), (profile, preferenceData) -> {
            LocalProfileDataStore.ParamSaveLocation param = loadProfile.saveLocationInProfile(profile, preferenceData);
            return param.getProfile();
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new LoadProfileLocalObserver());
    }

    @Override
    public void updateProfile(Profile profile) {
        if(!App.getInstance().hasNetworkConnection()) {
            view.showErrorDialog(ErrCode.CONNECTIVITY_PROBLEM, null);
            return;
        }
        view.showProgressDialog();
        disposables.add(saveProfile.buildUseCaseCompletable(new SaveProfile.Params(profile, null))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new SaveProfileObserver()));
    }

    @Override
    public void abort() {
        disposables.clear();
    }

    private class LoadProfileObserver extends DisposableSingleObserver<Profile> {

        @Override
        public void onSuccess(@NonNull Profile profile) {
            loadProfileOnUI(profile);
        }

        protected void loadProfileOnUI(Profile profile){
            if (view != null) {
                view.showProfile(profile, true);
                view.dismissProgressDialog();
            }
            dispose();
        }

        @Override
        public void onError(@NonNull Throwable e) {
            view.dismissProgressDialog();
            view.showErrorDialog(ErrCode.UNKNOWN_ERROR, e.getMessage());
            dispose();
        }
    }

    private final class LoadProfileLocalObserver extends LoadProfileObserver {

        @Override
        public void onSuccess(Profile profile) {
            super.onSuccess(profile);
        }

        @Override
        protected void loadProfileOnUI(Profile profile) {
            view.showProfile(profile,false);
        }

        @Override
        public void onError(Throwable e) {
            super.onError(e);
        }
    }

    private final class SaveProfileObserver extends DisposableCompletableObserver {

        @Override
        public void onComplete() {
            view.profileUpdated();
            view.dismissProgressDialog();
            dispose();
        }

        @Override
        public void onError(Throwable e) {
            view.dismissProgressDialog();
            if(e instanceof BackendException) {
                view.showErrorDialog(ErrCode.API_ERROR, e.getMessage());
            } else {
                view.showErrorDialog(ErrCode.UNKNOWN_ERROR, e.getMessage());
            }
            dispose();
        }
    }


}
