package com.luxurycard.androidapp.presentation.home;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;

import com.luxurycard.androidapp.R;
import com.luxurycard.androidapp.presentation.base.BaseFragment;
import com.support.mylibrary.widget.ClickGuard;

import butterknife.OnClick;

/**
 * Created by vinh.trinh on 5/3/2017.
 */

public class HomeFragment extends BaseFragment {

    private HomeEventsListener eventsListener;

    public static HomeFragment newInstance() {
        Bundle args = new Bundle();
        HomeFragment fragment = new HomeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof HomeEventsListener) {
            eventsListener = (HomeEventsListener) context;
        } else {
            throw new ClassCastException(context.toString() + " must implement HomeEventsListener.");
        }
    }

    @Override
    public int setupCreateViewLayoutId() {
        boolean hasMenuKey = ViewConfiguration.get(getContext()).hasPermanentMenuKey();
        boolean hasBackKey = KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_BACK);
        return (!hasMenuKey && !hasBackKey) ? R.layout.fragment_home_softkey:R.layout.fragment_home;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ClickGuard.guard(
                view.findViewById(R.id.btn_gallery),
                view.findViewById(R.id.btn_explore),
                view.findViewById(R.id.btn_ask));
    }

    @OnClick(R.id.btn_gallery)
    public void onGalleryButtonClick(View view) {
        eventsListener.navigateToInspirationGallery();
    }

    @OnClick(R.id.btn_explore)
    public void onExploreButtonClick(View view) {
        eventsListener.navigateToExplore();
    }

    @OnClick(R.id.btn_ask)
    public void onAskConciergeButtonClick(View view) {
//        ((HomeActivity)getActivity()).navigateToAskConcierge();
    }

    interface HomeEventsListener {
        void navigateToInspirationGallery();
        void navigateToExplore();
    }
}
