package com.luxurycard.androidapp.presentation.request;

import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.speech.RecognizerIntent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.luxurycard.androidapp.App;
import com.luxurycard.androidapp.R;
import com.luxurycard.androidapp.common.constant.AppConstant;
import com.luxurycard.androidapp.common.constant.ErrCode;
import com.luxurycard.androidapp.common.constant.IntentConstant;
import com.luxurycard.androidapp.common.constant.ResultCode;
import com.luxurycard.androidapp.datalayer.preference.PreferencesStorage;
import com.luxurycard.androidapp.datalayer.repository.ProfileDataRepository;
import com.luxurycard.androidapp.datalayer.repository.RequestRepository;
import com.luxurycard.androidapp.domain.model.RequestDetailData;
import com.luxurycard.androidapp.domain.usecases.CreateRequest;
import com.luxurycard.androidapp.domain.usecases.GetAccessToken;
import com.luxurycard.androidapp.domain.usecases.LoadProfile;
import com.luxurycard.androidapp.presentation.base.BaseActivity;
import com.luxurycard.androidapp.presentation.base.CommonActivity;
import com.luxurycard.androidapp.presentation.bottomsheet.BottomSheetCustomFragment;
import com.luxurycard.androidapp.presentation.home.HomeNavBottomActivity;
import com.luxurycard.androidapp.presentation.widget.DialogHelper;
import com.luxurycard.androidapp.presentation.widget.ViewUtils;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by vinh.trinh on 5/3/2017.
 */

public class AskConciergeActivity extends BaseActivity implements CreateNewConciergeCase.View,
        PlaceRequestFragment.ViewEventsListener,
        ThankYouFragment.ViewEventsListener,
        BottomSheetCustomFragment.OnBottomSheetListener {

    private final int REQ_CODE_SPEECH_INPUT = 701;

    private AskConciergePresenter presenter;
    private DialogHelper dialogHelper;


    private AskConciergePresenter presenter() {
        PreferencesStorage ps = new PreferencesStorage(getApplicationContext());
        ProfileDataRepository profileDataRepository = new ProfileDataRepository(ps);
        GetAccessToken getAccessToken = new GetAccessToken(ps);
        LoadProfile loadProfile = new LoadProfile(profileDataRepository);
        CreateRequest createRequest = new CreateRequest(new RequestRepository());
        return new AskConciergePresenter(ps, getAccessToken, loadProfile, createRequest);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
//        this.isSwipeBack = false;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment_holder);
        setTitle(R.string.request_title);
//        setToolbarColor(R.color.colorPrimary);
        dialogHelper = new DialogHelper(this);
//        toolbar.setVisibility(View.GONE);
//        back.setOnClickListener(null);
//        disableSwipe();

        // Get extra
        RequestDetailData requestDetailData = new RequestDetailData();
        requestDetailData =  getIntent().getParcelableExtra(IntentConstant.AC_REQUEST_DETAIL);

        String suggestedConcierge = getIntent().getStringExtra(IntentConstant.AC_SUGGESTED_CONCIERGE);
        String prefResponse = getIntent().getStringExtra(IntentConstant.AC_PREF_RESPONSE);
        String transactionId = getIntent().getStringExtra(IntentConstant.AC_TRANSACTION_ID);
        String requestSelected = getIntent().getStringExtra(IntentConstant.SELECTED_CATEGORY);
        boolean isDuplicate = getIntent().getBooleanExtra(IntentConstant.AC_BOOL_DUPLICATE, true);

        if (savedInstanceState == null) {
            Bundle bundle = new Bundle();
            if (requestDetailData != null) {
                bundle.putParcelable(IntentConstant.AC_REQUEST_DETAIL, requestDetailData);
                bundle.putBoolean(IntentConstant.AC_BOOL_DUPLICATE, isDuplicate);
                bundle.putString(IntentConstant.SELECTED_CATEGORY, requestDetailData.getNameRequestForEdit());
            } else {
                if (!TextUtils.isEmpty(requestSelected)) {
                    bundle.putString(IntentConstant.SELECTED_CATEGORY, requestSelected);
                }
            }
            if (!TextUtils.isEmpty(suggestedConcierge)) {
                bundle.putString(IntentConstant.AC_SUGGESTED_CONCIERGE, suggestedConcierge + "<br/>");
            }
            bundle.putString(IntentConstant.AC_PREF_RESPONSE, prefResponse);
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragment_place_holder, PlaceRequestFragment.newInstance(bundle),
                            PlaceRequestFragment.class.getSimpleName())
                    .commit();

            // Track GA
            App.getInstance().track(AppConstant.GA_TRACKING_SCREEN_NAME.ASK_CONCIERGE.getValue());
        }
        presenter = presenter();
        if (requestDetailData != null)
            presenter.setTransactionID(requestDetailData.transactionID);
        presenter.setDuplicate(isDuplicate);
        presenter.attach(this);

    }

    @Override
    protected void onDestroy() {
        presenter.detach();
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    private void callTheConcierge(String number) {
        BottomSheetCustomFragment bottomSheetFragment = new BottomSheetCustomFragment();
        bottomSheetFragment.show(this.getSupportFragmentManager(), BottomSheetCustomFragment.class.getSimpleName());
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQ_CODE_SPEECH_INPUT) {
            if (resultCode == ResultCode.RESULT_OK && null != data) {
                ArrayList<String> result = data
                        .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                String text = result.get(0);
                if (!TextUtils.isEmpty(text)) {
                    ((PlaceRequestFragment) getSupportFragmentManager().findFragmentByTag(
                            PlaceRequestFragment.class.getSimpleName()
                    )).speechToTextResult(text);
                }
            }
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override //CreateNewConciergeCase.View::
    public void onRequestSuccessfullySent() {
        if (getCurrentFocus() != null) {
            ViewUtils.hideSoftKey(getCurrentFocus());
        }
        setResult(RESULT_OK);
        finish();

        // Track GA with "create request successfully"
        App.getInstance().track(AppConstant.GA_TRACKING_CATEGORY.REQUEST.getValue(),
                AppConstant.GA_TRACKING_ACTION.SUBMIT.getValue(),
                AppConstant.GA_TRACKING_LABEL.REQUEST_ACCEPTED.getValue());
    }

    @Override //CreateNewConciergeCase.View::
    public void showErrorDialog(ErrCode errCode, String extraMsg) {
        if (!dialogHelper.networkUnavailability(errCode, extraMsg)) {
            String message = getString(R.string.ask_concierge_error);
            dialogHelper.action("",
                    message,
                    getString(R.string.text_ok),
                    getString(R.string.call_concierge),
                    (dialog, which) -> callTheConcierge("")
            );
        }

        // Track GA with "create request failed"
        App.getInstance().track(AppConstant.GA_TRACKING_CATEGORY.REQUEST.getValue(),
                AppConstant.GA_TRACKING_ACTION.SUBMIT.getValue(),
                AppConstant.GA_TRACKING_LABEL.REQUEST_DENIED.getValue());
    }

    @Override //CreateNewConciergeCase.View::
    public void showProgressDialog() {
        dialogHelper.showProgress();
    }

    @Override
    public void dismissProgressDialog() { //CreateNewConciergeCase.View::
        dialogHelper.dismissProgress();
    }

//    @Override // PlaceRequestFragment.ViewEventsListener::
//    public void record() {
//        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
//        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
//                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
//        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.ENGLISH);
//        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
//                getString(R.string.speech_prompt));
//        try {
//            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
//        } catch (ActivityNotFoundException a) {
//            Toast.makeText(getApplicationContext(),
//                    getString(R.string.speech_not_supported),
//                    Toast.LENGTH_SHORT).show();
//        }
//    }

    @Override // PlaceRequestFragment.ViewEventsListener::
    public void sendRequest(String content, boolean email, boolean phone) {
        presenter.sendRequest(content, email, phone);
    }

    @Override
    public void updateRequest(String content, boolean email, boolean phone, String transactionID) {

    }

    @Override // PlaceRequestFragment.ViewEventsListener::
    public void makePhoneCall(String number) {
        callTheConcierge(number);
    }

    @Override
    public void onClickButtonCancel() {
        onBackPressed();
    }

    @Override // ThankYouFragment.ViewEventsListener::
    public void anotherRequest() {
        Bundle bundle = new Bundle();
        bundle.putBoolean(IntentConstant.INVOKE_KEYBOARD, true);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_place_holder, PlaceRequestFragment.newInstance(),
                        PlaceRequestFragment.class.getSimpleName())
                .commit();
        setTitle(R.string.request_title);
    }

    @Override // ThankYouFragment.ViewEventsListener::
    public void toExplore() {
        setResult(ResultCode.RESULT_OK, null);
        Intent intent = new Intent(this, HomeNavBottomActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtra(IntentConstant.RESET_EXPLORE_PAGE, true);
        startActivity(intent);
    }


    @Override
    public void onBackPressed() {
        Log.e("TAG", "onBackPressed TEst" );
        if (placeRequestOnTop()) {
            return;
        }
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return true;
    }

    private boolean placeRequestOnTop() {
        boolean opTop = getSupportFragmentManager()
                .findFragmentByTag(PlaceRequestFragment.class.getSimpleName()) != null;
        if (opTop) {
            dialogHelper.action(null,
                    getString(R.string.ask_quit_confirm),
                    getString(R.string.text_no_all_caps), getString(R.string.text_yes_all_caps),
                    (dialog, which) -> {

                        super.onBackPressed();
                        overridePendingTransition(0,0);

                    },false);
        }
        return opTop;
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public void onBottomSheetClickCancel() {
        PlaceRequestFragment fragment = (PlaceRequestFragment) getSupportFragmentManager().findFragmentByTag(PlaceRequestFragment.class.getSimpleName());
        fragment.focusAskEditText();
    }
}