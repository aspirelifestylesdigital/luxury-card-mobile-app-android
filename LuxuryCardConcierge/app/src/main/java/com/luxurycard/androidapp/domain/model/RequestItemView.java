package com.luxurycard.androidapp.domain.model;

import com.luxurycard.androidapp.common.constant.RequestTypeConstant;
import com.luxurycard.androidapp.datalayer.entity.askconcierge.ACRequestItem;
import com.luxurycard.androidapp.presentation.requestdetail.RequestDetail;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by vinh.trinh on 8/30/2017.
 */

public class RequestItemView {

    public enum STATUS {PENDING, OPEN, CLOSED, OPEN_CANCELED}

    public final String title;
    public final String date;
    public final String s_status;
    public final int dataListIndex;
    public final STATUS status;
    public final String EPCCaseID;
    public final String requestType;
    public final String content;
    public final String transactionID;
    public final RequestDetailData requestDetailData;


    private SimpleDateFormat datePrinter;
    private SimpleDateFormat dateFormat;

    private List<SimpleDateFormat> simpleDateFormats;


    public RequestItemView(String title, String EPCCaseID, String date, int index, STATUS status,String requestType,
                           String content, String transactionID, RequestDetailData requestDetailData, String s_status) {
        this.title = title;
        this.EPCCaseID = EPCCaseID;
        this.date = date;
        this.dataListIndex = index;
        this.status = status;
        this.s_status = s_status;
//        switch (status) {
//            case PENDING:
//                s_status = "Pending";
//                break;
//            case CLOSED:
//                s_status = "Closed";
//                break;
//            default:
//                if (status == STATUS.OPEN) {
//                    s_status = "Open";
//                } else {
//                    s_status = "In Progress";
//                }
//                break;
//        }
        this.requestType = requestType;
        this.content = content;
        this.transactionID = transactionID;
        this.requestDetailData = requestDetailData;



        dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.US);
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT+0"));
//        compareFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.US);
//        compareFormat.setTimeZone(TimeZone.getTimeZone("GMT+0"));
        datePrinter = new SimpleDateFormat("MM/dd/yyyy | h:mm a", Locale.US);

        simpleDateFormats = new ArrayList<>();
        SimpleDateFormat firstDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.US);
        firstDateFormat.setTimeZone(TimeZone.getTimeZone("GMT+0"));
        SimpleDateFormat secondDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
        secondDateFormat.setTimeZone(TimeZone.getTimeZone("GMT+0"));
        simpleDateFormats.add(firstDateFormat);
        simpleDateFormats.add(secondDateFormat);


    }

//    public String getContent() {
//        String result = "";
//        String lines[] = this.content.split("\\r?\\n");
//        if (lines.length > 3) {
//            for (int i = 0; i < 3; i++) {
//                if (i == 2) {
//                    result = result + lines[i] + "\n...";
//                } else  {
//                    result = result + lines[i] + "\n";
//                }
//            }
//        } else {
//            result = this.content;
//        }
//        return result;
//    }

    public STATUS getStatus() {

        try {
            Calendar current = Calendar.getInstance(Locale.US);
//            Date date = dateFormat.parse(requestDetailData.createDate);
            Date date = tryParse(requestDetailData.createDate, simpleDateFormats);
            if (date == null) {
                throw new ParseException("Error parse date time", 0);
            }
            if ("closed".equalsIgnoreCase(requestDetailData.requestStatus)) {
                return RequestItemView.STATUS.CLOSED;
            }
            if (current.getTimeInMillis() - date.getTime()  <= (60 * 5)*1000) {
                if ("cancel".equalsIgnoreCase(requestDetailData.requestMode)) {
                    return RequestItemView.STATUS.OPEN_CANCELED;
                } else {
                    return  RequestItemView.STATUS.PENDING;
                }
            } else {

                if ("cancel".equalsIgnoreCase(requestDetailData.requestMode)) {
                    return  RequestItemView.STATUS.OPEN_CANCELED;
                } else {
                    return  RequestItemView.STATUS.OPEN;
                }

            }
        } catch (ParseException e) {
            e.printStackTrace();
            return STATUS.CLOSED;
        }
    }

    public String getRequestType() {
        try {
            Calendar current = Calendar.getInstance(Locale.US);
//            Date date = dateFormat.parse(requestDetailData.createDate);
            Date date = tryParse(requestDetailData.createDate, simpleDateFormats);
            if (date == null) {
                throw new ParseException("Error parse date time", 0);
            }
            if ("closed".equalsIgnoreCase(requestDetailData.requestStatus)) {
                return RequestTypeConstant.CLOSED;
            }
            if (current.getTimeInMillis() - date.getTime()  <= (60 * 5)*1000) {
                if ("cancel".equalsIgnoreCase(requestDetailData.requestMode)) {
                    return  RequestTypeConstant.OPEN;
                } else {
                    return  RequestTypeConstant.PENDING;
                }
            } else {
                if ("inprogress".equalsIgnoreCase(requestDetailData.requestStatus)) {
                    return RequestTypeConstant.IN_PROGRESS;
                } else {
                    return RequestTypeConstant.OPEN;
                }
            }
        } catch (ParseException e) {
            return "";
        }
    }


    Date tryParse(String dateString, List<SimpleDateFormat> formatStrings)
    {
        for (SimpleDateFormat formatString : formatStrings)
        {
            try
            {
                return formatString.parse(dateString);
            }
            catch (ParseException e) {}
        }

        return null;
    }
}
