package com.luxurycard.androidapp.presentation.cityguidecategory;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.luxurycard.androidapp.R;
import com.luxurycard.androidapp.common.glide.GlideHelper;
import com.luxurycard.androidapp.domain.model.SubCategoryItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by vinh.trinh on 5/10/2017.
 */

public class SubCategoryAdapter extends RecyclerView.Adapter<SubCategoryAdapter.SubCategoryViewHolder> {

    private OnCategoryItemClickListener listener;
    private List<SubCategoryItem> subCategoryItems;

    public SubCategoryAdapter(List<SubCategoryItem> subCategoryItems, OnCategoryItemClickListener listener) {
        this.subCategoryItems = subCategoryItems;
        this.listener = listener;
    }

    public void add(SubCategoryItem subCategoryItem) {
        this.subCategoryItems.add(subCategoryItem);
        notifyDataSetChanged();
    }

    public void swapData(List<SubCategoryItem> data) {
        this.subCategoryItems.clear();
        this.subCategoryItems.addAll(data);
        notifyDataSetChanged();
    }

    public SubCategoryItem getItem(int index) {
        return subCategoryItems.get(index);
    }

    @Override
    public SubCategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.explore_category_item, parent, false);
        return new SubCategoryViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SubCategoryViewHolder holder, int position) {
        final SubCategoryItem subCategoryItem = subCategoryItems.get(position);
        holder.subCategoryName.setText(subCategoryItem.getSubCategoryName());
    }

    @Override
    public int getItemCount() {
        return subCategoryItems.size();
    }

    void setListener(OnCategoryItemClickListener listener) {
        this.listener = listener;
    }

    class SubCategoryViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_name)
        AppCompatTextView subCategoryName;

        SubCategoryViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(v -> {
                if(listener != null) listener.onItemClick(getAdapterPosition());
            });
        }
    }

    public interface OnCategoryItemClickListener {
        void onItemClick(int index);
    }
}
