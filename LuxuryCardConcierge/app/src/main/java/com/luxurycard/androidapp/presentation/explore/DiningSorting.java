package com.luxurycard.androidapp.presentation.explore;

import android.location.Location;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.luxurycard.androidapp.domain.model.LatLng;
import com.luxurycard.androidapp.domain.model.explore.ExploreRViewItem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Admin on 7/30/2017.
 */

class DiningSorting {

    static List<ExploreRViewItem> sort(List<ExploreRViewItem> data, DiningSortingCriteria criteria) {
        sortByAlphabe(data);
        List<ExploreRViewItem> dataWithCuisine = new ArrayList<>();
        if(!TextUtils.isEmpty(criteria.cuisine)) {
            filterCuisineList(dataWithCuisine, data, criteria.cuisine.toLowerCase());
            data.removeAll(dataWithCuisine);
        }
        if(dataWithCuisine.isEmpty()) {
            dataWithCuisine.addAll(data);
            data.removeAll(dataWithCuisine);
        }
        //-- hold feature location
        /*if(criteria.location != null) {
            //data = sortByDistance(data, criteria.location);
            List<ExploreRViewItem> sortedDataWithCuisine = sortByDistance(dataWithCuisine, criteria.location, criteria.isRegion);
            dataWithCuisine.removeAll(sortedDataWithCuisine);
            dataWithCuisine.addAll(0, sortedDataWithCuisine);
            List<ExploreRViewItem> sortedData = sortByDistance(data, criteria.location, criteria.isRegion);
            data.removeAll(sortedData);
            data.addAll(0, sortedData);
        }*/
        dataWithCuisine.addAll(data);
        return dataWithCuisine;
    }

    private static void filterCuisineList(List<ExploreRViewItem> desireCuisineList, List<ExploreRViewItem> data, String cuisine) {
        for (int i = 0; i < data.size(); i++) {
            DiningSortingCriteria dsc = data.get(i).getSortingCriteria();
            if(dsc == null) continue;
            String itemCuisine = dsc.cuisine;
            if(itemCuisine != null && itemCuisine.toLowerCase().contains(cuisine)) {
                desireCuisineList.add(data.get(i));
            }
        }
    }

    private static List<ExploreRViewItem> sortByDistance(List<ExploreRViewItem> data, LatLng point, boolean isRegion) {
        float[] result = new float[1];
        int size = data.size();
        List<DistanceCriteria> sorted = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            DiningSortingCriteria dsc = data.get(i).getSortingCriteria();
            if(dsc == null) continue;
            LatLng target = dsc.location;
            Location.distanceBetween(target.latitude, target.longitude, point.latitude, point.longitude, result);
//            dsc.distance = result[0] / 1609.34f;
            sorted.add(new DistanceCriteria(result[0], i));
        }
        Collections.sort(sorted);
        final float allowedDistance = isRegion ? 16093.4f*5 : 16093.4f;
        size = sorted.size();
        for (int i = size-1; i >= 0; i--) {
            DistanceCriteria dc = sorted.get(i);
            if (dc.distance > allowedDistance) {
                sorted.remove(i);
            }
        }
        List<ExploreRViewItem> sortedData = new ArrayList<>();
        for (DistanceCriteria d : sorted) {
            sortedData.add(data.get(d.index));
        }
        return sortedData;
    }

    private static void sortByAlphabe(List<ExploreRViewItem> data) {
        Collections.sort(data);
    }

    private static class DistanceCriteria implements Comparable<DistanceCriteria> {
        Float distance;
        int index;

        DistanceCriteria(Float distance, int index) {
            this.distance = distance;
            this.index = index;
        }

        @Override
        public int compareTo(@NonNull DistanceCriteria another) {
            return distance.compareTo(another.distance);
        }
    }
}