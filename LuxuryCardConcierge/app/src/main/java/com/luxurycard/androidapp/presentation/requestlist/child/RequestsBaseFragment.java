package com.luxurycard.androidapp.presentation.requestlist.child;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.luxurycard.androidapp.R;
import com.luxurycard.androidapp.domain.model.RequestItemView;
import com.luxurycard.androidapp.presentation.base.BaseNavFragment;
import com.luxurycard.androidapp.presentation.home.HomeNavBottomActivity;
import com.luxurycard.androidapp.presentation.requestdetail.RequestDetailFragment;
import com.luxurycard.androidapp.presentation.requestlist.parent.RequestFragment;
import com.luxurycard.androidapp.presentation.widget.RecyclerViewScrollListener;

import java.util.List;

import butterknife.BindView;

import static com.luxurycard.androidapp.presentation.requestlist.child.RequestItemAdapter.TYPE.OPEN;

/**
 * Created by vinh.trinh on 9/11/2017.
 */

public class RequestsBaseFragment extends BaseNavFragment implements RequestItemAdapter.RequestItemListener {

    protected RequestItemAdapter requestItemAdapter;
    private RecyclerViewScrollListener scrollListener;

    @BindView(R.id.request_list_rcv)
    RecyclerView recyclerView;

    @BindView(R.id.request_list_empty)
    RelativeLayout relativeContainerEmpty;

    @BindView(R.id.request_text_empty)
    AppCompatTextView tvEmpty;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public int setupCreateViewLayoutId() {
        return R.layout.fragment_request_list;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    public RequestItemAdapter.TYPE getRequestsItemType(){
        return OPEN; // dummy here
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        relativeContainerEmpty.setVisibility(View.GONE);
        requestItemAdapter = new RequestItemAdapter(getContext(), getRequestsItemType());
        requestItemAdapter.setItemClickListener(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(requestItemAdapter);
        setupLoadMore();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void loadList(List<RequestItemView> data) {
        if (requestItemAdapter != null) {
            requestItemAdapter.append(data);
        }
    }

    public void refreshDataCache(List<RequestItemView> data) {
        if (requestItemAdapter != null) {
            requestItemAdapter.refreshDataCache(data);
        }
    }

    @Override
    public void onItemClick(int pos) {
        //-- open detail request from Activity
        RequestItemView item = requestItemAdapter.getItem(pos);
        addChildFragmentTabCurrent(RequestDetailFragment.newInstance(item.requestDetailData, item.EPCCaseID));
    }

    @Override
    public void onCancelClick(int pos) {
        if(getParentFragment() instanceof RequestFragment){
            ((RequestFragment) getParentFragment()).onCancelClick(requestItemAdapter.getItem(pos));
        }
    }

    private void setupLoadMore() {
        scrollListener = new RecyclerViewScrollListener() {
            @Override
            public void onScrollUp() {
            }

            @Override
            public void onScrollDown() {
            }

            @Override
            public void onLoadMore() {
                if (getParentFragment() instanceof RequestFragment) {
                    ((RequestFragment) getParentFragment()).handleLoadRequestsMore();
                }
            }
        };
        recyclerView.addOnScrollListener(scrollListener);
    }

    public void showLoadMore() {
        if (recyclerView == null) {
            return;
        }
        recyclerView.post(() -> {
            if (requestItemAdapter != null) {
                requestItemAdapter.showLoading(true);
            }
        });
    }

    public void stopLoadMore() {
        if (recyclerView == null) {
            return;
        }
        recyclerView.post(() -> {
            requestItemAdapter.showLoading(false);
            scrollListener.loadDone();
        });
    }

    public int itemCount() {
        return requestItemAdapter == null ? 0 : requestItemAdapter.getItemCount();
    }

    public void clear() {
         requestItemAdapter.clear();
    }

    public List<RequestItemView> getRequestItem(){
        return requestItemAdapter.getRequestItemView();
    }

    public void onViewEmptyData() {
        relativeContainerEmpty.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
    }

    public void onViewHaveData() {
        relativeContainerEmpty.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
    }
}
