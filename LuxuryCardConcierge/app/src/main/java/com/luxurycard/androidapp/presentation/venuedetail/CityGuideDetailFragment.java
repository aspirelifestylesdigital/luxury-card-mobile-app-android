package com.luxurycard.androidapp.presentation.venuedetail;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.BaseTarget;
import com.bumptech.glide.request.target.SizeReadyCallback;
import com.bumptech.glide.request.target.Target;
import com.luxurycard.androidapp.App;
import com.luxurycard.androidapp.R;
import com.luxurycard.androidapp.common.constant.AppConstant;
import com.luxurycard.androidapp.common.constant.IntentConstant;
import com.luxurycard.androidapp.common.constant.RequestCode;
import com.luxurycard.androidapp.common.glide.GlideHelper;
import com.luxurycard.androidapp.common.logic.HtmlTagHandler;
import com.luxurycard.androidapp.common.logic.HtmlUtils;
import com.luxurycard.androidapp.common.logic.ShareHelper;
import com.luxurycard.androidapp.common.logic.TextViewLinkHandler;
import com.luxurycard.androidapp.common.logic.urlimage.URLImageParser;
import com.luxurycard.androidapp.datalayer.preference.PreferencesStorage;
import com.luxurycard.androidapp.datalayer.repository.B2CDataRepository;
import com.luxurycard.androidapp.domain.model.explore.CityGuideDetailItem;
import com.luxurycard.androidapp.domain.model.explore.ExploreRView;
import com.luxurycard.androidapp.domain.model.explore.SearchDetailItem;
import com.luxurycard.androidapp.domain.usecases.GetCityGuideDetail;
import com.luxurycard.androidapp.presentation.base.BaseNavSubFragment;
import com.luxurycard.androidapp.presentation.home.HomeNavBottomActivity;
import com.luxurycard.androidapp.presentation.request.AskConciergeActivity;
import com.luxurycard.androidapp.presentation.venuedetail.mapview.VenueDetailMapFragment;
import com.luxurycard.androidapp.presentation.widget.DialogHelper;
import com.support.mylibrary.widget.LetterSpacingTextView;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Den on 4/13/18.
 */

public class CityGuideDetailFragment extends BaseDetailFragment implements CityGuideDetail.View {


    @BindView(R.id.city_guide_detail_layout)
    View cityGuideDetailLayout;
    @BindView(R.id.loading)
    ProgressBar pbLoading;
    @BindView(R.id.explore_detail_image)
    ImageView ivDetailImage;
    @BindView(R.id.explore_action_book)
    ImageButton ivActionBook;
    @BindView(R.id.explore_action_share)
    ImageButton ivActionShare;
    @BindView(R.id.explore_name)
    TextView tvCityGuideName;
    @BindView(R.id.explore_address)
    TextView tvCityGuideAddress;
    @BindView(R.id.explore_see_map)
    TextView tvCityGuideSeeMap;
    @BindView(R.id.explore_website)
    TextView tvCityGuideWebsite;
    @BindView(R.id.explore_insider_tip_root_layout)
    View insiderTipLayout;
    @BindView(R.id.insider_tip_content)
    LetterSpacingTextView tvInsiderTip;

    @BindView(R.id.explore_description_layout)
    View cityGuideDescriptionLayout;
    @BindView(R.id.explore_description)
    LetterSpacingTextView tvCityGuideDescription;
    @BindView(R.id.explore_description_title)
    LetterSpacingTextView descriptionTitle;

    private CityGuideDetailItem cityGuideDetailItem;
    private SearchDetailItem searchDetailItem;
    private CityGuideDetailPresenter presenter;
    Bitmap detailBitmap;
    private int imgIndex;

    ExploreRView detailViewData;


    public static CityGuideDetailFragment newInstance(String titleExplore, ExploreRView detailViewData) {
        Bundle args = new Bundle();
        CityGuideDetailFragment fragment = new CityGuideDetailFragment();
        args.putString("explore_title", titleExplore);
        args.putParcelable("explore_city_guide_detail", detailViewData);
        fragment.setArguments(args);
        return fragment;
    }

    private CityGuideDetailPresenter buildPresenter() {
        return new CityGuideDetailPresenter(
                new PreferencesStorage(App.getInstance().getApplicationContext()),
                new GetCityGuideDetail(new B2CDataRepository()));
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = buildPresenter();
        presenter.attach(this);
        trackGA(savedInstanceState);
    }

    @Override
    public int setupCreateViewLayoutId() {
        return R.layout.activity_city_guide_detail;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getBtnCallConcierge().setVisibility(View.VISIBLE);
        getButtonBack().setOnClickListener(view1 -> this.onBackPressed());
        tvInsiderTip.setMovementMethod(textViewLinkHandler);
        tvCityGuideDescription.setMovementMethod(textViewLinkHandler);

//        tvCityGuideSeeMap.setPaintFlags(tvCityGuideSeeMap.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
//        tvCityGuideWebsite.setPaintFlags(tvCityGuideWebsite.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        if (getArguments() == null) {
            return;
        }

        detailViewData = getArguments().getParcelable("explore_city_guide_detail");
        String title = getArguments().getString("explore_title", "");
        setTitle(title);

        if (detailViewData == null) {
            return;
        }

        // handle Get data
        if (detailViewData instanceof CityGuideDetailItem) {
            cityGuideDetailItem = (CityGuideDetailItem) detailViewData;
            renderUI();
        } else if(detailViewData instanceof SearchDetailItem){
            searchDetailItem = (SearchDetailItem) detailViewData;
//            setTitle(searchDetailItem.title);
            cityGuideDetailLayout.setVisibility(View.INVISIBLE);
            pbLoading.setVisibility(View.VISIBLE);
            imgIndex = searchDetailItem.cityGuideSubCategoryIndex;

            // Call API
            presenter.getCityGuideDetail(searchDetailItem.secondaryID, searchDetailItem.ID);
        }

    }

    @Override
    public void onDestroy() {
        presenter.detach();
        super.onDestroy();
    }

    private void renderUI(){
        pbLoading.setVisibility(View.GONE);
        if(cityGuideDetailItem != null){
            cityGuideDetailLayout.setVisibility(View.VISIBLE);
            Target<Bitmap> target = new BaseTarget<Bitmap>() {
                @Override
                public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                    detailBitmap = resource;
                }

                @Override
                public void getSize(SizeReadyCallback cb) {

                }
            };
            if(!TextUtils.isEmpty(cityGuideDetailItem.getImageUrl())){
                GlideHelper.getInstance().loadImage(cityGuideDetailItem.getImageUrl(),
                        0, ivDetailImage, 0, target);
            }else if(cityGuideDetailItem.imageResId != 0){
                GlideHelper.getInstance().loadImage(mapImageResIdForLarger(cityGuideDetailItem.imageResId),
                        0, ivDetailImage, 0, target);
            }
            // Dining name
            if(TextUtils.isEmpty(cityGuideDetailItem.title)){
                tvCityGuideName.setVisibility(View.GONE);
            }else{
                tvCityGuideName.setVisibility(View.VISIBLE);
                tvCityGuideName.setText(Html.fromHtml(cityGuideDetailItem.title));
//                setTitle(cityGuideDetailItem.title);
            }
            // Dining address
            String diningAddress = cityGuideDetailItem.getDisplayAddress();
            if(TextUtils.isEmpty(diningAddress)){
                tvCityGuideAddress.setVisibility(View.GONE);
            }else{
                tvCityGuideAddress.setVisibility(View.VISIBLE);
                tvCityGuideAddress.setText(diningAddress);
            }
            // See map --- later
            // Website
            if(TextUtils.isEmpty(cityGuideDetailItem.url) || !cityGuideDetailItem.url.contains("http")){
                tvCityGuideWebsite.setVisibility(View.GONE);
            }else{
                tvCityGuideWebsite.setVisibility(View.VISIBLE);
//                tvCityGuideWebsite.setText(cityGuideDetailItem.url);
            }
            // Insider tip
            if(TextUtils.isEmpty(cityGuideDetailItem.insiderTips)){
                insiderTipLayout.setVisibility(View.GONE);
            }else{
                insiderTipLayout.setVisibility(View.VISIBLE);
                tvInsiderTip.setText(Html.fromHtml(HtmlUtils.adjustSomeHtmlTag(cityGuideDetailItem.insiderTips), null,
                        new HtmlTagHandler()));
            }
            // Description
            if (isStringNullOrWhiteSpace(cityGuideDetailItem.description)){
                cityGuideDescriptionLayout.setVisibility(View.GONE);
                descriptionTitle.setVisibility(View.GONE);
            }else{
                cityGuideDescriptionLayout.setVisibility(View.VISIBLE);
                descriptionTitle.setVisibility(View.VISIBLE);
                tvCityGuideDescription.setText(Html.fromHtml(HtmlUtils.adjustSomeHtmlTag(cityGuideDetailItem.description), new URLImageParser(tvCityGuideDescription, getActivity(), false, null),
                        new HtmlTagHandler()));
            }
        }

        /* Tooltip View */
        presenter.handleTooltip();
    }

    private int mapImageResIdForLarger(int resId){
        if(resId == R.drawable.sub_category_accomodation){
            return R.drawable.sub_category_accomodation_1;
        }else if(resId == R.drawable.sub_category_bar_club){
            return R.drawable.sub_category_bar_club_1;
        }else if(resId == R.drawable.sub_category_culture){
            return R.drawable.sub_category_culture_1;
        }else if(resId == R.drawable.sub_category_dining){
            return R.drawable.sub_category_dining_1;
        }else if(resId == R.drawable.sub_category_shopping){
            return R.drawable.sub_category_shopping_1;
        }
        else if(resId == R.drawable.sub_category_spas){
            return R.drawable.sub_category_spas_1;
        }
        return 0;
    }

    private int imgResource(int index) {
        switch (index) {
            case 0:
                return R.drawable.sub_category_accomodation;
            case 1:
                return R.drawable.sub_category_bar_club;
            case 2:
                return R.drawable.sub_category_culture;
            case 3:
                return R.drawable.sub_category_dining;
            case 4:
                return R.drawable.sub_category_shopping;
            default:
                return  R.drawable.sub_category_spas;
        }
    }


    @OnClick({R.id.explore_see_map, R.id.explore_website, R.id.explore_action_book, R.id.explore_action_share})
    public void onClick(View view){
        switch (view.getId()){
            case R.id.explore_see_map:
                //new DialogHelper(CityGuideDetailActivity.this)
//                        .showLeavingAlert((dialogInterface, i) -> MapSearchHelper.getInstance().searchNameAndShowOnMap(CityGuideDetailActivity.this, cityGuideDetailItem.getQuerySearchOnGoogleMap(), cityGuideDetailItem.getTitle()));
                //        .showLeavingAlert((dialogInterface, i) ->
                if (!App.getInstance().hasNetworkConnection()) {
                    showErrorNoNetwork();
                } else {
                    addChildFragmentTabCurrent(VenueDetailMapFragment.newInstance(cityGuideDetailItem, null));
                }
                    //startActivity(new Intent(this, VenueDetailMapActivity.class).putExtra("cityGuide",cityGuideDetailItem));
                //        );
                break;
            case R.id.explore_website:
                new DialogHelper(getActivity()).showLeavingAlert(cityGuideDetailItem.url);
                break;
            case R.id.explore_action_book:
                Bundle bundle = new Bundle();
                bundle.putString(IntentConstant.SELECTED_CITY, cityGuideDetailItem.city);
                bundle.putString(IntentConstant.SELECTED_CATEGORY, cityGuideDetailItem.getRequestTypeName());
                bundle.putString(IntentConstant.AC_SUGGESTED_CONCIERGE, cityGuideDetailItem.getSuggestedToAC());
                openNewRequestDetailActivity(bundle);
                break;
            case R.id.explore_action_share:
                ShareHelper.getInstance().share(getActivity(), cityGuideDetailItem, detailBitmap);
                break;
        }
    }

    private TextViewLinkHandler textViewLinkHandler = new TextViewLinkHandler() {
        @Override
        public void onLinkClick(String url) {
            new DialogHelper(getActivity()).showLeavingAlert(url);
        }
    };

    @Override
    public void onGetCityGuideDetailFinished(CityGuideDetailItem cityGuideDetailItem) {
        this.cityGuideDetailItem = cityGuideDetailItem;
        this.cityGuideDetailItem.setImageResId(imgResource(imgIndex));
        renderUI();
    }

    @Override
    public void onUpdateFailed() {
        pbLoading.setVisibility(View.GONE);
    }

    @Override
    public View getButtonBook() {
        return ivActionBook;
    }
    private static boolean isStringNullOrWhiteSpace(String value) {
        if (TextUtils.isEmpty(value)){
            return  true;
        }
        String contentString  = Html.fromHtml(value).toString();
        String replaceString =   contentString.replaceAll("\\r\\n|\\r|\\n|<br/>", "").trim();
        if (replaceString.length() <=0){
            return  true;
        }
        return false;
    }

}
