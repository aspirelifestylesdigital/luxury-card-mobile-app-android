package com.luxurycard.androidapp.presentation.profile;

import com.luxurycard.androidapp.App;
import com.luxurycard.androidapp.common.BackendException;
import com.luxurycard.androidapp.common.constant.ErrCode;
import com.luxurycard.androidapp.domain.model.Profile;
import com.luxurycard.androidapp.domain.usecases.CreatePlainUserPreferences;
import com.luxurycard.androidapp.domain.usecases.GetAccessToken;
import com.luxurycard.androidapp.domain.usecases.GetUserPreferences;
import com.luxurycard.androidapp.domain.usecases.SaveProfile;

import io.reactivex.Completable;
import io.reactivex.CompletableEmitter;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by vinh.trinh on 4/28/2017.
 */

public class CreateProfilePresenter implements CreateProfile.Presenter {

    private CompositeDisposable disposables;
    private CreateProfile.View view;
    private SaveProfile saveProfile;
    // just for location setting, all of craps below this line appear
    private GetAccessToken getAccessToken;
    private CreatePlainUserPreferences createPlainUserPreferences;
    private GetUserPreferences getUserPreferences;

    CreateProfilePresenter(SaveProfile saveProfile, GetAccessToken getAccessToken,
                           CreatePlainUserPreferences createPlainUserPreferences,
                           GetUserPreferences getUserPreferences) {
        disposables = new CompositeDisposable();
        this.saveProfile = saveProfile;
        this.getAccessToken = getAccessToken;
        this.createPlainUserPreferences = createPlainUserPreferences;
        this.getUserPreferences = getUserPreferences;
    }

    @Override
    public void attach(CreateProfile.View view) {
        this.view = view;
    }

    @Override
    public void detach() {
        disposables.dispose();
        view = null;
    }

    @Override
    public void createProfile(Profile profile, String password) {
        if (!App.getInstance().hasNetworkConnection()) {
            view.showErrorDialog(ErrCode.CONNECTIVITY_PROBLEM, null);
            return;
        }
        view.showProgressDialog();
        disposables.add(saveProfile.buildUseCaseCompletable(new SaveProfile.Params(profile, password))
                .andThen(getAccessToken.execute().flatMapCompletable(accessToken -> {
                    Completable gup = getUserPreferences.buildUseCaseSingle(accessToken).flatMapCompletable(preferenceData -> Completable.create(CompletableEmitter::onComplete));
                    //create account -> selectCity empty
                    return createPlainUserPreferences.buildUseCaseCompletable(new CreatePlainUserPreferences.Params(accessToken, profile.isLocationOn(), "")).andThen(gup);
                }))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new SaveProfileObserver()));
    }

    @Override
    public void abort() {
        disposables.clear();
    }

    private final class SaveProfileObserver extends DisposableCompletableObserver {

        @Override
        public void onComplete() {
            view.dismissProgressDialog();
            view.showProfileCreatedDialog();
            dispose();
        }

        @Override
        public void onError(Throwable e) {
            if (view != null) {
                view.dismissProgressDialog();
                if(e instanceof BackendException) {
                    view.showErrorDialog(ErrCode.API_ERROR, e.getMessage());
                } else {
                    view.showErrorDialog(ErrCode.UNKNOWN_ERROR, e.getMessage());
                }
            }
            dispose();
        }
    }

}
