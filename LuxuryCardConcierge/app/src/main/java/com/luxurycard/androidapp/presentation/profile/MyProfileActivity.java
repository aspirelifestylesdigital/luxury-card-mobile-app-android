package com.luxurycard.androidapp.presentation.profile;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;

import com.luxurycard.androidapp.App;
import com.luxurycard.androidapp.R;
import com.luxurycard.androidapp.common.GPSChecker;
import com.luxurycard.androidapp.common.constant.AppConstant;
import com.luxurycard.androidapp.common.constant.ErrCode;
import com.luxurycard.androidapp.common.constant.RequestCode;
import com.luxurycard.androidapp.common.constant.ResultCode;
import com.luxurycard.androidapp.datalayer.preference.PreferencesStorage;
import com.luxurycard.androidapp.datalayer.repository.PreferencesDataRepository;
import com.luxurycard.androidapp.datalayer.repository.ProfileDataRepository;
import com.luxurycard.androidapp.domain.model.Profile;
import com.luxurycard.androidapp.domain.repository.UserPreferencesRepository;
import com.luxurycard.androidapp.domain.usecases.GetAccessToken;
import com.luxurycard.androidapp.domain.usecases.GetUserPreferences;
import com.luxurycard.androidapp.domain.usecases.LoadProfile;
import com.luxurycard.androidapp.domain.usecases.SaveProfile;
import com.luxurycard.androidapp.presentation.LocationPermissionHandler;
import com.luxurycard.androidapp.presentation.base.BaseActivity;
import com.luxurycard.androidapp.presentation.base.CommonActivity;
import com.luxurycard.androidapp.presentation.widget.DialogHelper;
import com.luxurycard.androidapp.presentation.widget.ViewUtils;

public class MyProfileActivity extends BaseActivity implements ProfileFragment.ProfileEventsListener,
        EditProfile.View{

    private EditProfilePresenter presenter;
    public DialogHelper dialogHelper;
    private ProfileFragment myFragment;
//    LocationPermissionHandler locationPermissionHandler = new LocationPermissionHandler();

    private EditProfilePresenter editProfilePresenter() {
        PreferencesStorage preferencesStorage = new PreferencesStorage(getApplicationContext());
        final ProfileDataRepository profileDataRepository = new ProfileDataRepository(preferencesStorage);
        UserPreferencesRepository preferencesRepository = new PreferencesDataRepository(preferencesStorage);
        GetUserPreferences getUserPreferences = new GetUserPreferences(preferencesRepository);

        return new EditProfilePresenter(new GetAccessToken(preferencesStorage),
                new LoadProfile(profileDataRepository),
                new SaveProfile(profileDataRepository),
                getUserPreferences);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
//        this.isSwipeBack = false;
        super.onCreate(savedInstanceState);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_fragment_holder);
        dialogHelper = new DialogHelper(this);
        presenter = editProfilePresenter();
        presenter.attach(this);
//        setTitle("CONTACT INFO");
//        appBar.setVisibility(View.GONE);
        if(savedInstanceState == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragment_place_holder
                            , ProfileFragment.newInstance(ProfileFragment.PROFILE.EDIT)
                            , ProfileFragment.class.getSimpleName())
                    .commit();
        }
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
//        toolbar.setClickable(true);
//        toolbar.setOnClickListener(view -> {
//            if(getCurrentFocus() != null) {
//                ViewUtils.hideSoftKey(getCurrentFocus());
//            }
//        });

        if(savedInstanceState == null){
            // Track GA
            App.getInstance().track(AppConstant.GA_TRACKING_SCREEN_NAME.MY_PROFILE.getValue());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    protected void onDestroy() {
        presenter.detach();
        super.onDestroy();
    }

    @Override
    public void onFragmentCreated() {
        presenter.getProfileLocal();
        presenter.getProfile();
    }

    @Override
    public void onLocationSwitchOn() {
        /*if(!locationPermissionHandler.hasPermission(this)) {
            locationPermissionHandler.requestPermission(this);
        } else {
            if(!GPSChecker.GPSEnable(getApplicationContext())) {
                dialogHelper.action(null,"To enable, please go to Settings and turn on Location Service for this app.",
                        "Setting","Cancel",
                        (dialogInterface, i) -> GPSChecker.openGPSSetting(this));
            }
        }*/
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
//        boolean granted = locationPermissionHandler.handlingPermissionResult(requestCode, grantResults);
//        if(!granted) myFragment.switchOff();
//        else if (!GPSChecker.GPSEnable(getApplicationContext())) {
//            dialogHelper.action(null, "To enable, please go to Settings and turn on Location Service for this app.",
//                    "Setting", "Cancel",
//                    (dialogInterface, i) -> GPSChecker.openGPSSetting(this));
//        }
    }

    @Override
    public void onProfileSubmit(Profile profile, String password) {
        setResult(ResultCode.RESULT_OK);
        presenter.updateProfile(profile);
    }

    @Override
    public void onProfileCancel() {
        presenter.getProfileLocal();
    }

    @Override
    public void showProfile(Profile profile, boolean fromRemote) {
        myFragment = (ProfileFragment) getSupportFragmentManager()
                .findFragmentByTag(ProfileFragment.class.getSimpleName());
        myFragment.loadProfile(profile);
    }

    @Override
    public void profileUpdated() {
        dialogHelper.alert(null, getString(R.string.profile_updated_message));
        if(myFragment != null){
            myFragment.reloadProfileAfterUpdated();
        }
    }

    @Override
    public void showErrorDialog(ErrCode errCode, String extraMsg) {
        if(dialogHelper.networkUnavailability(errCode, extraMsg)) return;
        if(errCode == ErrCode.API_ERROR) dialogHelper.alert("ERROR!", extraMsg);
        else dialogHelper.showGeneralError();
    }

    @Override
    public void showProgressDialog() {
        dialogHelper.showProgress();
    }

    @Override
    public void dismissProgressDialog() {
        dialogHelper.dismissProgress();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onProfileInputError(String message) {
        dialogHelper.profileDialog(message, dialog -> {
            if(myFragment != null) {
                myFragment.getView().postDelayed(() -> myFragment.invokeSoftKey(), 100);
            }
        });
    }
}
