package com.luxurycard.androidapp.presentation.requestlist;

import com.luxurycard.androidapp.App;
import com.luxurycard.androidapp.common.constant.AppConstant;
import com.luxurycard.androidapp.datalayer.entity.askconcierge.ACResponse;
import com.luxurycard.androidapp.datalayer.preference.PreferencesStorage;
import com.luxurycard.androidapp.domain.model.Metadata;
import com.luxurycard.androidapp.domain.model.Profile;
import com.luxurycard.androidapp.domain.model.RequestDetailData;
import com.luxurycard.androidapp.domain.model.RequestItemView;
import com.luxurycard.androidapp.domain.usecases.CreateRequest;
import com.luxurycard.androidapp.domain.usecases.GetAccessToken;
import com.luxurycard.androidapp.domain.usecases.GetRequestList;
import com.luxurycard.androidapp.domain.usecases.LoadProfile;
import com.luxurycard.androidapp.presentation.widget.tooltip.TooltipView;
import com.luxurycard.androidapp.presentation.widget.tooltip.data.TooltipConstant;
import com.luxurycard.androidapp.presentation.widget.tooltip.data.TooltipData;

import org.json.JSONException;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by vinh.trinh on 8/30/2017.
 */

public class RequestListPresenter implements RequestList.Presenter {

    private RequestList.View view;
    private CompositeDisposable compositeDisposable;
    private GetAccessToken getAccessToken;
    private GetRequestList getRequestList;
    private CreateRequest askConcierge;
    private LoadProfile loadProfile;
    private RequestDetailData data;
    private Metadata metadata;
    private PreferencesStorage preferencesStorage;
    private int page = 1;
    private boolean reachEnd = false;
    /** true: tab Open, otherwise tab Close */
    private boolean openLoad = true;

    private boolean isTooltipCreated = false;

    private CacheAccessToken cache = new CacheAccessToken();

    public RequestListPresenter(PreferencesStorage ps, GetAccessToken at, GetRequestList gql,
                                CreateRequest cr, LoadProfile lp) {
        this.compositeDisposable = new CompositeDisposable();
        this.getAccessToken = at;
        this.getRequestList = gql;
        this.loadProfile = lp;
        this.askConcierge = cr;
        this.preferencesStorage = ps;
        try {
            this.metadata = ps.metadata();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public RequestDetailData getDetail(int index) {
        if(!App.getInstance().hasNetworkConnection()) {
            view.onFetchError();
            return null;
        }
        try {
            return getRequestList.getDatum(index);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void switchList(boolean open) {
        openLoad = open;
    }

    @Override
    public void fetchList() {
        if(reachEnd) return;
        if(!openLoad && view.openCount() > 0) page++;
        if(view.openCount() < 10 || view.closeCount() < 10) view.showProgressDialog();
        load();
    }

    @Override
    public void loadMore() {
        if(reachEnd) {
            dismissViewLoader();
            //-- finish load data check empty show view empty
            handleEmptyRequestData();
            return;
        }
        page++;
        view.showListLoadMore(openLoad);
        load();
    }

    @Override
    public void refresh() {
        getRequestList.reset();
        page = 1;
        reachEnd = false;
//        openLoad = true;
        fetchList();
    }



    @Override
    public void attach(RequestList.View view) {
        this.view = view;
    }

    @Override
    public void detach() {
        compositeDisposable.dispose();
        this.view = null;
    }

    private void load() {
        if(!App.getInstance().hasNetworkConnection()) {
            view.dismissProgressDialog();
            view.onFetchError();
            return;
        }

        if(!openLoad && view.closeCount() == 0 && view.openCount() > 0){
            view.resetViewRequestsData();
        }

        handleRequestApi();

    }

    private void handleRequestApi() {
        //cache access token
        Single<String> accessTokenLocal;
        if(cache.accessToken == null){
            accessTokenLocal = getAccessToken.execute();
        }else {
            accessTokenLocal = cache.accessToken;
        }

        compositeDisposable.add(accessTokenLocal
                .flatMap(accessToken -> {
                    //-- save cache access token
                    if (cache.accessToken == null) {
                        this.cache.accessToken = Single.just(accessToken);
                    }
                    GetRequestList.Params params = new GetRequestList.Params(
                            accessToken,
                            metadata,
                            page*20-19,
                            page*20
                    );
                    return getRequestList.buildUseCaseSingle(params);
                })
                .map(requestItemViews -> {
                    if(requestItemViews.size() == 0) reachEnd = true;
                    return separate(requestItemViews);
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new RequestDataObserver()));
    }

    private RequestData separate(List<RequestItemView> data) {
        List<RequestItemView> openList = new ArrayList<>();
        List<RequestItemView> closeList = new ArrayList<>();
        for (RequestItemView item : data) {
            if(item.status == RequestItemView.STATUS.CLOSED) closeList.add(item);
            else openList.add(item);
        }
        return new RequestData(openList, closeList);
    }

    public class RequestData {
        final List<RequestItemView> openList;
        final List<RequestItemView> closeList;

        RequestData(List<RequestItemView> openList, List<RequestItemView> closeList) {
            this.openList = openList;
            this.closeList = closeList;
        }

        public List<RequestItemView> getOpenList() {
            return openList;
        }

        public List<RequestItemView> getCloseList() {
            return closeList;
        }
    }

    private class RequestDataObserver extends DisposableSingleObserver<RequestData> {

        @Override
        public void onSuccess(RequestData data) {
            if (view == null) {
                return;
            }
            dismissViewLoader();
            view.loadData(data);
            if (shouldLoadMore(data)) {
                view.showListLoadMore(openLoad);
                loadMore();
            } else {
                //-- finish load data check empty show view empty
                handleEmptyRequestData();
            }

            if (cache != null) {
                cache.resetCache();
            }
            resetHomeLoadDuplicate();
            dispose();
        }

        @Override
        public void onError(Throwable e) {
            if (view == null) {
                return;
            }
            boolean handleError = false;
            if(cache != null){
                try {
                    if(e != null && e.getMessage() != null){
                        RequestListResponse responseError = new RequestListResponse(e.getMessage());
                        if(responseError.isExpireAccessToken() && cache.isLoadNewAccessToken()){
                            cache.loadNewAccessToken();
                            handleRequestApi();
                            handleError = true;
                        }else{
                            handleError = false;
                        }
                    }
                } catch (Exception e1) {
                    handleError = false;
                }
            }

            if(!handleError){
                dismissViewLoader();
                try {
                    view.onFetchError();
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
                //-- finish load data check empty show view empty
                handleEmptyRequestData();
                resetHomeLoadDuplicate();
                dispose();
            }//else true do nothing
        }
    }

    private void handleEmptyRequestData(){
        view.isRequestsEmpty();
    }

    private void resetHomeLoadDuplicate() {
        if (view != null) {
            view.resetLoadDuplicate();
        }
    }

    private boolean shouldLoadMore(RequestData data) {
        int fetchedAmount = 0, odd = 0;
        if(openLoad) {
            fetchedAmount = data.openList.size();
            if(fetchedAmount == 0 && view.openCount() == 0)
                return true;
            odd = view.openCount() > 10 ? view.openCount() % 10 : 0;
        }
        if(!openLoad) {
            fetchedAmount = data.closeList.size();
            if(fetchedAmount == 0 && view.closeCount() == 0)
                return true;
            odd = view.closeCount() > 10 ? view.closeCount() % 10 : 0;
        }
        return odd + fetchedAmount < 10;
    }

    private void dismissViewLoader() {
        if(view.openCount() > 0 && openLoad) {
            view.hideListLoadMore(true);
        }
        if(view.closeCount() > 0 && !openLoad) {
            view.hideListLoadMore(false);
        }
        view.dismissProgressDialog();
    }


    @Override
    public void cancelRequest(RequestDetailData data) {
        this.data = data;
        view.showProgressDialog();
        compositeDisposable.add(Single.zip(getAccessToken.execute(), loadProfile.buildUseCaseSingle(null), this::buildParams)
                .flatMap(params -> askConcierge.execute(params)).subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<ACResponse>() {
                    @Override
                    public void onSuccess(ACResponse acResponse) {
                        view.dismissProgressDialog();
                        if(acResponse.getStatus() != null && acResponse.getStatus().isSuccess()) {
                            view.doneCancel();
                        } else {
                            view.onFetchError();
                            trackFail();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.dismissProgressDialog();
                        view.onFetchError();
                        trackFail();
                    }

                    private void trackFail(){
                        // Track GA with "create request failed"
                        App.getInstance().track(AppConstant.GA_TRACKING_CATEGORY.REQUEST.getValue(),
                                AppConstant.GA_TRACKING_ACTION.SUBMIT.getValue(),
                                AppConstant.GA_TRACKING_LABEL.REQUEST_DENIED.getValue());
                    }

                }));

    }


    private CreateRequest.Params buildParams(String accessToken, Profile profile) {
        CreateRequest.RequestContent requestContent = new CreateRequest.RequestContent(
                data.detail,
                data.responseMode.contains("Email"),
                data.responseMode.contains("Mobile")
        );
        CreateRequest.Params params = new CreateRequest.Params(
                accessToken,
                profile,
                metadata,
                requestContent
        );
        params.transactionID(data.transactionID);
        params.editType(CreateRequest.EDIT_TYPE.CANCEL);
        return params;
    }


    @Override
    public void handleTooltip() {
        if (view != null && !isTooltipCreated) {
            isTooltipCreated = true;
            if(TooltipData.isShouldShow(preferencesStorage, TooltipConstant.REQUESTS_BUTTON_NEW_REQUEST)){
                TooltipView tooltipView = new TooltipView();
                tooltipView.showViewBottom(view.getButtonNewRequest(), TooltipConstant.REQUESTS_BUTTON_NEW_REQUEST);
            }//else do nothing here
        }
    }


    private final class CacheAccessToken{
        Single<String> accessToken = null;
        /** value == 0: is use old access token,
         * value == 1: called get new access token */
        int numberLoadNewAccessToken = 0;

        void resetCache(){
            numberLoadNewAccessToken = 0;
        }

        void loadNewAccessToken(){
            accessToken = null;
            numberLoadNewAccessToken = 1;
        }

        boolean isLoadNewAccessToken(){
            return numberLoadNewAccessToken == 0;
        }

    }
}
