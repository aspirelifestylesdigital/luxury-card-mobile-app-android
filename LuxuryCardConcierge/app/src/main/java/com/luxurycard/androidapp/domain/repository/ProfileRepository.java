package com.luxurycard.androidapp.domain.repository;

import android.hardware.usb.UsbRequest;

import com.luxurycard.androidapp.datalayer.datasource.LocalProfileDataStore;
import com.luxurycard.androidapp.datalayer.datasource.PreferenceData;
import com.luxurycard.androidapp.domain.model.Profile;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

/**
 * Created by vinh.trinh on 5/12/2017.
 */

public interface ProfileRepository {

    /**
     * access token is empty -> load from local
     */
    Single<Profile> loadProfile(String accessToken);
    Completable saveProfile(Profile profile, String password);
    LocalProfileDataStore.ParamSaveLocation saveLocationProfileLocal(Profile profile, PreferenceData preferenceData);

}
