package com.luxurycard.androidapp.presentation.infoweb;

import com.luxurycard.androidapp.common.constant.AppConstant;
import com.luxurycard.androidapp.common.constant.ErrCode;
import com.luxurycard.androidapp.datalayer.entity.GetClientCopyResult;
import com.luxurycard.androidapp.presentation.base.BasePresenter;

/**
 * Created by vinh.trinh on 6/13/2017.
 */

public interface InfoWeb {

    interface View {
        void showErrorMessage(ErrCode errCode);
    }

    interface Presenter extends BasePresenter<View> {

        String getTitleOfType(AppConstant.MASTERCARD_COPY_UTILITY type);

        String getLinkUrl(AppConstant.MASTERCARD_COPY_UTILITY type);

    }

}
