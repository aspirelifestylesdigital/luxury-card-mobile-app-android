package com.luxurycard.androidapp.presentation.requestdetail;

import android.text.TextUtils;
import android.util.Pair;

import com.luxurycard.androidapp.common.constant.RequestTypeConstant;
import com.luxurycard.androidapp.common.logic.StringUtils;
import com.luxurycard.androidapp.domain.model.RequestDetailData;
import com.luxurycard.androidapp.domain.model.explore.RequestDetailContent;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by vinh.trinh on 9/19/2017.
 */

public class TextDetailBuilder {

    private RequestDetailData data;
    private JSONObject jsonDetail;
    private SimpleDateFormat dateParser;
    private SimpleDateFormat datePrinter;
    private SimpleDateFormat simpleDatePrinter;
    private List<String> nameKeys;
    private boolean ignoreDate;
    private String lastUpdate;
    private List<SimpleDateFormat> simpleDateFormats;
    TimeZone tz;

    public TextDetailBuilder() {
        nameKeys = Arrays.asList("restaurantname", "hotelname", "golfcoursename",
                "eventname", "tourname", "cruisename");
    }

    public TextDetailBuilder(RequestDetailData data, JSONObject jsonData, boolean ignoreDate) {
        this.data = data;
        this.jsonDetail = jsonData;
        Calendar cal = Calendar.getInstance();
        tz = cal.getTimeZone();
        dateParser = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        datePrinter = new SimpleDateFormat("MM/dd/yyyy | h:mm a");
        simpleDatePrinter = new SimpleDateFormat("MMMM dd, yyyy");


        simpleDateFormats = new ArrayList<>();
        SimpleDateFormat firstDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.US);
        SimpleDateFormat secondDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
        simpleDateFormats.add(firstDateFormat);
        simpleDateFormats.add(secondDateFormat);

        this.ignoreDate = ignoreDate;
    }

    String dining() {
        List<Pair<String, String>> map = new ArrayList<>();

        String name = getDetailValue("restaurantname");
        if(!TextUtils.isEmpty(name)) map.add(new Pair<>("Name", name));
        eventDate(map);
        childrenAndAdults(map);
        location(map);
        comments(map);
        return buildString(map);
    }

    String hotel() {
        List<Pair<String, String>> map = new ArrayList<>();

        String name = getDetailValue("hotelname");
        if(!TextUtils.isEmpty(name)) map.add(new Pair<>("Name", name));
        map.add(new Pair<>("Check in", simpleDate(data.startDate)));
        map.add(new Pair<>("Check out", simpleDate(data.endDate)));
        childrenAndAdults(map);
        location(map);
        comments(map);
        return buildString(map);
    }

    String golf() {
        List<Pair<String, String>> map = new ArrayList<>();

        String name = getDetailValue("golfcoursename");
        if(!TextUtils.isEmpty(name)) map.add(new Pair<>("Name", name));
        map.add(new Pair<>("Date", formatDate(data.startDate)));
        map.add(new Pair<>("# of golfers", String.valueOf(data.numberOfAdults)));
        location(map);
        comments(map);
        return buildString(map);
    }

    String carRental() {
        List<Pair<String, String>> map = new ArrayList<>();

        String name = getDetailValue("drivername");
        if(!TextUtils.isEmpty(name)) map.add(new Pair<>("Driver", name));
        map.add(new Pair<>("Pickup date", formatDate(data.pickupDate)));
        map.add(new Pair<>("Pick up location", getDetailValue("picklocation")));
        map.add(new Pair<>("Drop off location", getDetailValue("droplocation")));
        numberInParty(map);
        comments(map);
        return buildString(map);
    }

    String entertainment() {
        List<Pair<String, String>> map = new ArrayList<>();

        map.add(new Pair<>("Name", getDetailValue("eventname")));
        map.add(new Pair<>("Date", formatDate(data.eventDate)));
        numberOfTickets(map);
        location(map);
        comments(map);
        return buildString(map);
    }

    String tour() {
        List<Pair<String, String>> map = new ArrayList<>();

        String name = getDetailValue("tourname");
        if(!TextUtils.isEmpty(name)) map.add(new Pair<>("Name", name));
        map.add(new Pair<>("Start date", formatDate(data.startDate)));
        map.add(new Pair<>("End date", formatDate(data.endDate)));
        map.add(new Pair<>("Start location", getDetailValue("startlocation")));
        map.add(new Pair<>("End location", getDetailValue("endlocation")));
        childrenAndAdults(map);
        map.add(new Pair<>("# of infants", getNumberValue("numberofinfants")));

        comments(map);
        return buildString(map);
    }

    String flight() {
        List<Pair<String, String>> map = new ArrayList<>();
        map.add(new Pair<>("Start date", formatDate(data.startDate)));
        map.add(new Pair<>("End date", formatDate(data.endDate)));
        map.add(new Pair<>("Start location", getDetailValue("startlocation")));
        map.add(new Pair<>("End location", getDetailValue("endlocation")));
        childrenAndAdults(map);
        map.add(new Pair<>("# of infants", getNumberValue("numberofinfants")));
        comments(map);
        return buildString(map);
    }

    String flower() {
        List<Pair<String, String>> map = new ArrayList<>();

        map.add(new Pair<>("Arrangement", getDetailValue("flowertype")));
        map.add(new Pair<>("Delivery date", formatDate(data.eventDate)));
        map.add(new Pair<>("Delivery address", getDetailValue("daddress")));
        map.add(new Pair<>("Quantity", getNumberValue("bouquetquantity")));
        comments(map);
        return buildString(map);
    }

    String privateJet() {
        List<Pair<String, String>> map = new ArrayList<>();
        map.add(new Pair<>("Start date", formatDate(data.startDate)));
        map.add(new Pair<>("Start location", getDetailValue("startlocation")));
        map.add(new Pair<>("End location", getDetailValue("endlocation")));
        map.add(new Pair<>("# of passengers", getNumberValue("noticket")));
        comments(map);
        return buildString(map);
    }

    String cruise() {
        List<Pair<String, String>> map = new ArrayList<>();

        String name = getDetailValue("cruisename");
        if(!TextUtils.isEmpty(name)) map.add(new Pair<>("Name", name));
        map.add(new Pair<>("Start date", simpleDate(data.startDate)));
        map.add(new Pair<>("End date", simpleDate(data.endDate)));
        map.add(new Pair<>("Start location", getDetailValue("startlocation")));
        map.add(new Pair<>("End location", getDetailValue("endlocation")));
        childrenAndAdults(map);
        map.add(new Pair<>("# of infants", getNumberValue("numberofinfants")));
        comments(map);
        return buildString(map);
    }

    String others() {
        /*List<Pair<String, String>> map = new ArrayList<>();
        comments(map);*/
        return "Other comments: Common Message";
    }



    public String detailText() throws JSONException {
        switch (data.requestType.toUpperCase()) {
            case "D RESTAURANT":
                return dining();
            case "T HOTEL AND B&B":
                return hotel();
            case "S GOLF":
                return golf();
            case "T LIMO AND SEDAN":
                return carRental();
            case "E CONCERT/THEATER":
                return entertainment();
            case "C SIGHTSEEING/TOURS":
                return tour();
            case "I AIRPORT SERVICES":
                return flight();
            case "T CRUISE":
                return cruise();
            case "G FLOWERS/GIFT BASKET":
                return flower();
            case "T OTHER":
                return privateJet();
            case "O CLIENT SPECIFIC":
                return others();
        }
        return "";
    }



    String vacationPackage() {
        return "";
    }

    private String buildString(List<Pair<String, String>> map) {
        StringBuilder sb = new StringBuilder();
        for (Pair<String, String> val : map) {
            sb.append(val.first).append(": ").append(val.second).append("\n");
        }
        return sb.toString();
    }

    private String build(List<Pair<String, String>> map) {
        StringBuilder sb = new StringBuilder();
        for (Pair<String, String> val : map) {
            sb.append(val.first).append(val.second).append("\n");
        }
        return sb.toString();
    }

    public String nameMapping(String type) {
        switch (type.toUpperCase()) {
            case "D RESTAURANT":
                return "Restaurant Request";
            case "T HOTEL AND B&B":
                return "HOTEL AND B&B Request";
            case "S GOLF":
                return "Golf Request";
            case "T LIMO AND SEDAN":
                return "Limo and Sedan Request";
            case "E CONCERT/THEATER":
                return "Entertainment";
            case "C SIGHTSEEING/TOURS":
                return "Sightseeing/Tours Request";
            case "I AIRPORT SERVICES":
                return "Airport Services Request";
            case "T CRUISE":
                return "Cruise Request";
            case "G FLOWERS/GIFT BASKET":
                return "Flowers/Gift Basket Request";
            case "T OTHER":
                return "Private Jet Request";
        }
        return "Other Request";
    }

    public String getName(String type, String detail) throws JSONException {
        JSONObject jsonDetail = new JSONObject();
        String[] data = detail.split("\\|");
        for (String d : data) {
            String datum[] = d.split(":", 2);
            if (datum.length == 2) {
                jsonDetail.put(datum[0].toLowerCase().trim(), datum[1]);
            }
        }
        String nameSuffix = null;
        Iterator<String> jsonKeys = jsonDetail.keys();
        while (jsonKeys.hasNext()) {
            String key = jsonKeys.next();
            if(nameKeys.contains(key)) {
                nameSuffix = jsonDetail.getString(key);
                break;
            }
        }

        return TextUtils.isEmpty(nameSuffix) ? nameMapping(type) :
                nameMapping(type) + " - " + nameSuffix;
    }

    private void childrenAndAdults(List<Pair<String, String>> map) {
        map.add(new Pair<>("# of adults", String.valueOf(data.numberOfAdults)));
        map.add(new Pair<>("# of children", String.valueOf(data.numberOfChildren)));
    }

    private void location(List<Pair<String, String>> map) {
        map.add(new Pair<>("City", data.city));
        map.add(new Pair<>("State", data.state));
        map.add(new Pair<>("Country", data.country));
    }

    private void numberInParty(List<Pair<String, String>> map) {
        map.add(new Pair<>("# in party", String.valueOf(data.numberOfAdults)));
    }

    private void numberOfTickets(List<Pair<String, String>> map) {
        map.add(new Pair<>("# in tickets", String.valueOf(data.numberOfAdults)));
    }

    private void eventDate(List<Pair<String, String>> map) {
        map.add(new Pair<>("Date/time", ignoreDate ? "" : formatDate(data.eventDate)));
    }

    private void eventDate(String prefix, List<Pair<String, String>> map, SimpleDateFormat format) {
        map.add(new Pair<>(prefix, ignoreDate ? "" : formatDate(data.eventDate, format)));
    }

    private void comments(List<Pair<String, String>> map) {
        map.add(new Pair<>("Other comments", getDetailValue("specialreq")));
    }

    private String getDetailValue(String key) {
        return jsonDetail.optString(key, "");
    }

    private String getNumberValue(String key) {
        return jsonDetail.optString(key, "0");
    }

    private String formatDate(String val) {
        if(ignoreDate) return "";
        Date date;
        try {
            date = dateParser.parse(val);
            return datePrinter.format(date);
        } catch (ParseException e) {
            return "";
        }
    }

    private String formatDate(String val, SimpleDateFormat format) {
        if(ignoreDate) return "";
        Date date;
        try {
//            return parseTimeZoneUserCurrent(val, format, dateParser);
            date = dateParser.parse(val);
            return format.format(date);
        } catch (ParseException e) {
            return "";
        }
    }

    private String formatCreateDate(String val, SimpleDateFormat format) {
        if(ignoreDate) return "";
        Date date;
        try {
//            2017-10-04T09:26:31.057
            format.setTimeZone(TimeZone.getDefault());
//            SimpleDateFormat firstDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sss");
//            SimpleDateFormat secondDateDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sss");

            return parseTimeZoneUserCurrent(val, format, simpleDateFormats);
//            date = dateFormat.parse(val);
//            return format.format(date);
        } catch (ParseException e) {
            return "";
        }
    }




    private String simpleDate(String val) {
        if(ignoreDate) return "";
        Date date;
        try {
            return parseTimeZoneUserCurrent(val, simpleDatePrinter, simpleDateFormats);
//            date = dateParser.parse(val);
//            return simpleDatePrinter.format(date);
        } catch (ParseException e) {
            return "";
        }
    }

    public String getContentRequestList() throws JSONException {
        data.buildJsonDetail();
        switch (data.requestType.toUpperCase()) {
            case "D RESTAURANT":
                return diningRequestList();
            case "T HOTEL AND B&B":
                return hotelRequestList();
            case "S GOLF":
                return golfRequestList();
            case "T LIMO AND SEDAN":
                return carRequestList();
            case "E CONCERT/THEATER":
                return eventRequestList();
            case "C SIGHTSEEING/TOURS":
                return tourRequestList();
            case "I AIRPORT SERVICES":
                return airportRequestList();
            case "T CRUISE":
                return cruiseRequestList();
            case "G FLOWERS/GIFT BASKET":
                return flowerRequestList();
            case "T OTHER":
                return privateJetRequestList();
//            case "Client Specific":
//                return textDetail.others();
        }
        return "";
    }



    //Get content for request list
    private String diningRequestList() {
        List<Pair<String, String>> map = new ArrayList<>();
        String name = getDetailValue("restaurantname");
        if(!TextUtils.isEmpty(name)) map.add(new Pair<>("", name));
        SimpleDateFormat format = new SimpleDateFormat("MMMM dd, yyyy @ h:mm a");
        eventDate("",map, format);
        return build(map);
    }

    private String hotelRequestList() {
        List<Pair<String, String>> map = new ArrayList<>();
        String name = getDetailValue("hotelname");
        if(!TextUtils.isEmpty(name)) map.add(new Pair<>("", name));
        map.add(new Pair<>("", simpleDate(data.startDate) + " - " + simpleDate(data.endDate)));
        return build(map);
    }

    private String golfRequestList() {
        List<Pair<String, String>> map = new ArrayList<>();
        String name = getDetailValue("reservationname");
        if(!TextUtils.isEmpty(name)) map.add(new Pair<>("", name));
//        SimpleDateFormat format = new SimpleDateFormat("MMMM/dd/yyyy @ h:mm a", Locale.US);
        map.add(new Pair<>("", simpleDate(data.startDate)));
        return build(map);
    }

    private String carRequestList() {
        List<Pair<String, String>> map = new ArrayList<>();
        String name = getDetailValue("drivername");
        if(!TextUtils.isEmpty(name)) map.add(new Pair<>("", name));
        SimpleDateFormat format = new SimpleDateFormat("MMMM dd, yyyy @ h:mm a");
        map.add(new Pair<>("", formatDate(data.pickupDate, format)));
        return build(map);
    }

    private String eventRequestList() {
        List<Pair<String, String>> map = new ArrayList<>();
        String name = getDetailValue("eventname");
        if(!TextUtils.isEmpty(name)) map.add(new Pair<>("", name));
        SimpleDateFormat format = new SimpleDateFormat("MMMM dd, yyyy @ h:mm a");
        eventDate("",map, format);
        return build(map);
    }

    private String tourRequestList() {
        List<Pair<String, String>> map = new ArrayList<>();
        String name = getDetailValue("tourname");
        if(!TextUtils.isEmpty(name)) map.add(new Pair<>("", name));
        map.add(new Pair<>("", simpleDate(data.startDate) + " - " + simpleDate(data.endDate)));
        return build(map);
    }

    private String airportRequestList() {
        List<Pair<String, String>> map = new ArrayList<>();
        SimpleDateFormat format = new SimpleDateFormat("MMMM dd, yyyy @ h:mm a");
        map.add(new Pair<>("", formatDate(data.startDate, format) + " - " + formatDate(data.endDate, format)));
        return build(map);
    }

    private String cruiseRequestList() {
        List<Pair<String, String>> map = new ArrayList<>();
        SimpleDateFormat format = new SimpleDateFormat("MMMM dd, yyyy @ h:mm a");
        map.add(new Pair<>("", formatDate(data.startDate, format) + " - " + formatDate(data.endDate, format)));
        return build(map);
    }

    private String flowerRequestList() {
        String deliverydate = getDetailValue("deliverydate");
        List<Pair<String, String>> map = new ArrayList<>();
        SimpleDateFormat format = new SimpleDateFormat("MMMM dd, yyyy @ h:mm a");
        map.add(new Pair<>("",  formatDate(deliverydate, format)));
        return build(map);
    }

    private String privateJetRequestList() {
        List<Pair<String, String>> map = new ArrayList<>();
        SimpleDateFormat format = new SimpleDateFormat("MMMM dd, yyyy @ h:mm a");
        map.add(new Pair<>("", formatDate(data.startDate, format)));
        return build(map);
    }


    //Get Content For Detail

    public List<RequestDetailContent> getContentRequestDetail() throws JSONException {
        data.buildJsonDetail();
        lastUpdate = data.requestStatus.equals(RequestTypeConstant.CLOSED) ? "CLOSED" : "LAST UPDATED";
        return getRequestDetail();
//        switch (data.requestType.toUpperCase()) {
//            case "D RESTAURANT":
//                return diningRequesDetail();
//            case "T HOTEL AND B&B":
//                return hotelRequesDetail();
//            case "S GOLF":
//                return golfRequesDetail();
//            case "T LIMO AND SEDAN":
//                return carRequesDetail();
//            case "E CONCERT/THEATER":
//                return entertainmentRequesDetail();
//            case "C SIGHTSEEING/TOURS":
//                return tourRequesDetail();
//            case "I AIRPORT SERVICES":
//                return flightRequesDetail();
//            case "T CRUISE":
//                return cruiseRequesDetail();
//            case "G FLOWERS/GIFT BASKET":
//                return flowerRequesDetail();
//            case "T OTHER":
//                return privateJetRequesDetail();
//            case "O CLIENT SPECIFIC":
//                return otherRequesDetail();
//        }
//        return new ArrayList<>();
    }


    private String getAddress() {
        String result = "";
        if (!TextUtils.isEmpty(data.country) && (data.country.toLowerCase().equals("united states")
                || data.country.toLowerCase().equals("canada"))) {
            if(!TextUtils.isEmpty(data.city)) result = result + data.city + ", ";
            if(!TextUtils.isEmpty(data.state)) result = result + data.state;
        } else {
            if(!TextUtils.isEmpty(data.country)) result = result + data.country;
        }

        return (TextUtils.isEmpty(result) ? "": result).replace("null", "") ;
    }

    private String getNumberAdult() {
        if(!TextUtils.isEmpty(String.valueOf(data.numberOfAdults))) {
            if (data.numberOfAdults > 1) {
                return String.valueOf(data.numberOfAdults) + " adults";
            } else {
                return String.valueOf(data.numberOfAdults) + " adult";
            }
        } else {
            return  "";
        }
    }

    private String getChildren() {
        if(!TextUtils.isEmpty(String.valueOf(data.numberOfChildren))) {
            if (data.numberOfChildren > 1) {
                return String.valueOf(data.numberOfChildren) + " children";
            } else {
                return String.valueOf(data.numberOfChildren) + " child";
            }
        } else {
            return  "";
        }
    }

    private String getNumberInfants() {
        if(!TextUtils.isEmpty(getNumberValue("numberofinfants"))) {
            try {
                if (Integer.valueOf(getNumberValue("numberofinfants")) > 1 ) {
                    return getNumberValue("numberofinfants") + " infants";
                } else  {
                    return getNumberValue("numberofinfants") + " infant";
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
                return  "";
            }
        } else {
            return "";
        }
    }

    private String getBouquetQuantity() {
        if(!TextUtils.isEmpty(getNumberValue("bouquetquantity"))) {
            try {
                if (Integer.valueOf(getNumberValue("bouquetquantity")) > 1 ) {
                    return getNumberValue("bouquetquantity") + " bouquets";
                } else  {
                    return getNumberValue("bouquetquantity") + " bouquet";
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
                return  "";
            }
        } else {
            return "";
        }
    }

    private String getPassenger() {
        if(!TextUtils.isEmpty(getNumberValue("noticket"))) {
            try {
                if (Integer.valueOf(getNumberValue("noticket")) > 1 ) {
                    return getNumberValue("noticket") + " passengers";
                } else  {
                    return getNumberValue("noticket") + " passenger";
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
                return  "";
            }
        } else {
            return "";
        }

    }


    private List<RequestDetailContent>  diningRequesDetail() {

        List<RequestDetailContent> result = new ArrayList<>();


        String requestType = nameMapping(data.requestType);
        String firstContent = "";
        String name = getDetailValue("restaurantname");
        if(!TextUtils.isEmpty(name)) firstContent = name + "\n";
        firstContent = firstContent + getAddress();
        result.add(new RequestDetailContent(requestType, firstContent));


        SimpleDateFormat format = new SimpleDateFormat("MMMM dd, yyyy h:mm a");
        result.add(new RequestDetailContent("WHEN", formatDate(data.eventDate, format)));

        List<String> stringList = new ArrayList<>();
        stringList.add(getNumberAdult());
        stringList.add(getChildren());
        String sPartySize = "";

        result.add(new RequestDetailContent("PARTY SIZE",  joinString(stringList, ", ")));

        result.add(new RequestDetailContent(lastUpdate, formatCreateDate(data.createDate, format)));

        return result;
    }

    private List<RequestDetailContent>  hotelRequesDetail() {

        List<RequestDetailContent> result = new ArrayList<>();


        String requestType = nameMapping(data.requestType);
        String firstContent = "";
        String name = getDetailValue("hotelname");
        if(!TextUtils.isEmpty(name)) firstContent = name + "\n";
        firstContent = firstContent + getAddress();
        result.add(new RequestDetailContent(requestType, firstContent));


        SimpleDateFormat format = new SimpleDateFormat("MMMM dd, yyyy h:mm a");
        result.add(new RequestDetailContent("WHEN", formatDate(data.startDate, format) + " - " + formatDate(data.endDate, format)));


        List<String> stringList = new ArrayList<>();
        stringList.add(getNumberAdult());
        stringList.add(getChildren());
//        if(!TextUtils.isEmpty(String.valueOf(data.numberOfAdults)))
//            stringList.add(String.valueOf(data.numberOfAdults) + " adult(s)");
//        if(!TextUtils.isEmpty(String.valueOf(data.numberOfChildren)))
//            stringList.add(String.valueOf(data.numberOfChildren) + " child/children");
        result.add(new RequestDetailContent("PARTY SIZE", joinString(stringList, ", ")));
        result.add(new RequestDetailContent(lastUpdate, formatCreateDate(data.createDate, format)));

        return result;
    }

    private List<RequestDetailContent>  golfRequesDetail() {

        List<RequestDetailContent> result = new ArrayList<>();


        String requestType = nameMapping(data.requestType);
        String firstContent = "";
        String name = getDetailValue("reservationname");
        if(!TextUtils.isEmpty(name)) firstContent = name + "\n";
        firstContent = firstContent + getAddress();
        result.add(new RequestDetailContent(requestType, firstContent));


        SimpleDateFormat format = new SimpleDateFormat("MMMM dd, yyyy h:mm a");
        result.add(new RequestDetailContent("WHEN", formatDate(data.startDate, format)));

        String sPartySize = "";
        if(!TextUtils.isEmpty(String.valueOf(data.numberOfAdults)))
            sPartySize = getNumberAdult();
        result.add(new RequestDetailContent("PARTY SIZE", sPartySize));

        result.add(new RequestDetailContent(lastUpdate, formatCreateDate(data.createDate, format)));

        return result;
    }


    private List<RequestDetailContent>  carRequesDetail() {

        List<RequestDetailContent> result = new ArrayList<>();


        String requestType = nameMapping(data.requestType);
        String firstContent = "";
        String name = getDetailValue("drivername");
        if(!TextUtils.isEmpty(name)) firstContent = name + "\n";
        firstContent = getDetailValue("picklocation") + " - " + getDetailValue("droplocation");

        result.add(new RequestDetailContent(requestType, firstContent));


        SimpleDateFormat format = new SimpleDateFormat("MMMM dd, yyyy h:mm a");
        result.add(new RequestDetailContent("WHEN", formatDate(data.pickupDate, format)));

        String sPartySize = "";
        if(!TextUtils.isEmpty(String.valueOf(data.numberOfAdults)))
            sPartySize = getNumberAdult();
        result.add(new RequestDetailContent("PARTY SIZE", sPartySize));

        result.add(new RequestDetailContent(lastUpdate, formatCreateDate(data.createDate, format)));

        return result;
    }


    private List<RequestDetailContent>  entertainmentRequesDetail() {

        List<RequestDetailContent> result = new ArrayList<>();


        String requestType = nameMapping(data.requestType);
        String firstContent = "";
        String name = getDetailValue("eventname");
        if(!TextUtils.isEmpty(name)) firstContent = name + "\n";
        firstContent = firstContent + getAddress();
        result.add(new RequestDetailContent(requestType, firstContent));


        SimpleDateFormat format = new SimpleDateFormat("MMMM dd, yyyy h:mm a");
        result.add(new RequestDetailContent("WHEN", formatDate(data.eventDate, format)));

        String sPartySize = "";
        if(!TextUtils.isEmpty(String.valueOf(data.numberOfAdults)))
            sPartySize = getNumberAdult();
        result.add(new RequestDetailContent("PARTY SIZE", sPartySize));

        result.add(new RequestDetailContent(lastUpdate, formatCreateDate(data.createDate, format)));

        return result;
    }


    private List<RequestDetailContent>  tourRequesDetail() {

        List<RequestDetailContent> result = new ArrayList<>();


        String requestType = nameMapping(data.requestType);
        String firstContent = "";
        String name = getDetailValue("tourname");
        firstContent = getDetailValue("startlocation") + " - " + getDetailValue("endlocation");
        result.add(new RequestDetailContent(requestType, firstContent));


        SimpleDateFormat format = new SimpleDateFormat("MMMM dd, yyyy h:mm a");
        result.add(new RequestDetailContent("WHEN", formatDate(data.startDate, format) + " - "
                + formatDate(data.endDate, format)));


        List<String> stringList = new ArrayList<>();
        stringList.add(getNumberAdult());
        stringList.add(getChildren());
        stringList.add(getNumberInfants());
//        if(!TextUtils.isEmpty(String.valueOf(data.numberOfAdults)))
//            stringList.add(String.valueOf(data.numberOfAdults) + " adult(s)");
//        if(!TextUtils.isEmpty(String.valueOf(data.numberOfChildren)))
//            stringList.add(String.valueOf(data.numberOfChildren) + " child/children");
//        if(!TextUtils.isEmpty(getNumberValue("numberofinfants")))
//            stringList.add(getNumberValue("numberofinfants") + " infant(s)");

        result.add(new RequestDetailContent("PARTY SIZE", joinString(stringList, " ,")));

        result.add(new RequestDetailContent(lastUpdate, formatCreateDate(data.createDate, format)));

        return result;
    }


    private List<RequestDetailContent>  flightRequesDetail() {

        List<RequestDetailContent> result = new ArrayList<>();


        String requestType = nameMapping(data.requestType);
        String firstContent = "";
        firstContent = getDetailValue("startlocation") + " - " + getDetailValue("endlocation");
        result.add(new RequestDetailContent(requestType, firstContent));


        SimpleDateFormat format = new SimpleDateFormat("MMMM dd, yyyy h:mm a");
        result.add(new RequestDetailContent("WHEN", formatDate(data.startDate, format) + " - " + formatDate(data.endDate, format)));

        List<String> stringList = new ArrayList<>();
        stringList.add(getNumberAdult());
        stringList.add(getChildren());
        stringList.add(getNumberInfants());
//        if(!TextUtils.isEmpty(String.valueOf(data.numberOfAdults)))
//            stringList.add(String.valueOf(data.numberOfAdults) + " adult(s)");
//        if(!TextUtils.isEmpty(String.valueOf(data.numberOfChildren)))
//            stringList.add(String.valueOf(data.numberOfChildren) + " child/children");
//        if(!TextUtils.isEmpty(getNumberValue("numberofinfants")))
//            stringList.add(getNumberValue("numberofinfants") + " infant(s)");
        result.add(new RequestDetailContent("PARTY SIZE", joinString(stringList, ", ")));

        result.add(new RequestDetailContent(lastUpdate, formatCreateDate(data.createDate, format)));

        return result;
    }

    private List<RequestDetailContent>  cruiseRequesDetail() {

        List<RequestDetailContent> result = new ArrayList<>();


        String requestType = nameMapping(data.requestType);
        String firstContent = "";
        firstContent = getDetailValue("startlocation") + " - " + getDetailValue("endlocation");
        result.add(new RequestDetailContent(requestType, firstContent));


        SimpleDateFormat format = new SimpleDateFormat("MMMM dd, yyyy h:mm a");
        result.add(new RequestDetailContent("WHEN", formatDate(data.startDate, format) + " - " + formatDate(data.endDate, format)));

        List<String> stringList = new ArrayList<>();
        stringList.add(getNumberAdult());
        stringList.add(getChildren());
        stringList.add(getNumberInfants());
//        if(!TextUtils.isEmpty(String.valueOf(data.numberOfAdults)))
//            stringList.add(String.valueOf(data.numberOfAdults) + " adult(s)");
//        if(!TextUtils.isEmpty(String.valueOf(data.numberOfChildren)))
//            stringList.add(String.valueOf(data.numberOfChildren) + " child/children");
//        if(!TextUtils.isEmpty(getNumberValue("numberofinfants")))
//            stringList.add(getNumberValue("numberofinfants") + " infant(s)");
        result.add(new RequestDetailContent("PARTY SIZE", joinString(stringList, ", ")));

        result.add(new RequestDetailContent(lastUpdate, formatCreateDate(data.createDate, format)));

        return result;
    }


    private List<RequestDetailContent>  flowerRequesDetail() {

        List<RequestDetailContent> result = new ArrayList<>();


        String requestType = nameMapping(data.requestType);
        String firstContent = "";
        String name = getDetailValue("flowertype");
        if(!TextUtils.isEmpty(name)) firstContent = name + "\n";
        if (!TextUtils.isEmpty(getNumberValue("bouquetquantity")))
            firstContent = firstContent + getBouquetQuantity() +"\n";

        if (!TextUtils.isEmpty(getDetailValue("daddress")))
            firstContent = firstContent + getDetailValue("daddress");

        firstContent = firstContent + getAddress();
        result.add(new RequestDetailContent(requestType, firstContent));


        SimpleDateFormat format = new SimpleDateFormat("MMMM dd, yyyy h:mm a");
        result.add(new RequestDetailContent("WHEN", getDetailValue("deliverydate")));


        List<String> stringList = new ArrayList<>();
        stringList.add(getNumberAdult());
        stringList.add(getChildren());
//        if(!TextUtils.isEmpty(String.valueOf(data.numberOfAdults)))
//            stringList.add(String.valueOf(data.numberOfAdults) + " adult(s)");
//        if(!TextUtils.isEmpty(String.valueOf(data.numberOfChildren)))
//            stringList.add(String.valueOf(data.numberOfChildren) + " child/children");
        result.add(new RequestDetailContent("PARTY SIZE", joinString(stringList, ", ")));

        result.add(new RequestDetailContent(lastUpdate, formatCreateDate(data.createDate, format)));

        return result;
    }


    private List<RequestDetailContent>  privateJetRequesDetail() {

        List<RequestDetailContent> result = new ArrayList<>();


        String requestType = nameMapping(data.requestType);
        String firstContent = "";
        firstContent = getDetailValue("startlocation") + " - " + getDetailValue("endlocation");
        result.add(new RequestDetailContent(requestType, firstContent));

        SimpleDateFormat format = new SimpleDateFormat("MMMM dd, yyyy h:mm a");
        result.add(new RequestDetailContent("WHEN", formatDate(data.startDate, format)));

        String sPartySize = "";
        if(!TextUtils.isEmpty(getNumberValue("noticket")))
            sPartySize = sPartySize + getPassenger();
        result.add(new RequestDetailContent("PARTY SIZE", sPartySize));

        result.add(new RequestDetailContent(lastUpdate, formatCreateDate(data.createDate, format)));

        return result;
    }


    private List<RequestDetailContent>  otherRequesDetail() {

        List<RequestDetailContent> result = new ArrayList<>();


        String requestType = nameMapping(data.requestType);
        result.add(new RequestDetailContent(requestType, ""));

        SimpleDateFormat format = new SimpleDateFormat("MMMM dd, yyyy h:mm a");
        result.add(new RequestDetailContent(lastUpdate, formatCreateDate(data.createDate, format)));

        return result;
    }


    private String joinString(List<String> list, String delimiter) {
        String result = "";
        for (int i = 0; i < list.size(); i++) {
            if (i == list.size() - 1) {
                result = result + list.get(i);
            } else {
                if (!list.get(i).isEmpty()) {
                    result = result + list.get(i) + delimiter;
                }
            }
        }
        return result;

    }

    Date tryParseTime(String dateString, List<SimpleDateFormat> formatStrings)
    {
        for (SimpleDateFormat formatString : formatStrings)
        {
            try
            {
                formatString.setTimeZone(TimeZone.getTimeZone("UTC"));
                return formatString.parse(dateString);
            }
            catch (ParseException e) {}
        }

        return null;
    }


    public String parseTimeZoneUserCurrent(String dateTime, SimpleDateFormat format, List<SimpleDateFormat> formatStrings) throws ParseException {
        //final String dateFormat = format;

        //SimpleDateFormat fullIso = new SimpleDateFormat(dateFormat);
        //-- Fix card #368
        //fullIso.setTimeZone(TimeZone.getTimeZone("UTC"));
//        serverFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
//        long timeParse = serverFormat.parse(dateTime).getTime();
        Date date = tryParseTime(dateTime, formatStrings);
        if (date == null) {
            throw new ParseException("Error parse date time", 0);
        }
        long timeParse = date.getTime();
        SimpleDateFormat fullTimeCurrent = format;
        fullTimeCurrent.setTimeZone(TimeZone.getDefault());
        String dateTimeCurrent = fullTimeCurrent.format(timeParse);
        return dateTimeCurrent;
    }


    public String getRequestName() {
        String requestType = "Other Request";
        String requestContent = data.detail.replace("##", "\n");
        String[] lines = requestContent.split("\r\n|\r|\n");
        if (lines.length > 0) {
            for (int i = 0; i <lines.length; i++) {
                if (lines[i].contains("CategoryName:")) {
                    requestType = lines[i].replace("CategoryName:", "").trim();
                    break;
                }
            }
        }
        return requestType;
    }

    private List<RequestDetailContent> getRequestDetail() {
        List<RequestDetailContent> result = new ArrayList<>();


        String requestType = "Other Request";
        String requestContent = data.detail.replace("##", "\n");
        String[] lines = requestContent.split("\r\n|\r|\n");
        if (lines.length > 0) {
            for (int i = 0; i <lines.length; i++) {
                if (lines[i].contains("CategoryName:")) {
                    requestType = lines[i].replace("CategoryName:", "").trim();
                    requestContent = data.detail.replace(lines[i], "").replace("##", "\n").trim();
                    break;
                }
            }
        }

        result.add(new RequestDetailContent("Request type", requestType));
        result.add(new RequestDetailContent("Request details", requestContent));



        SimpleDateFormat format = new SimpleDateFormat("MMMM dd, yyyy h:mm a");
        result.add(new RequestDetailContent(lastUpdate, formatCreateDate(data.createDate, format)));

        return result;
    }




    public String getDetailContent() {
        String requestContent = data.detail.replace("##", "\n");
        String[] lines = requestContent.split("\r\n|\r|\n");
        if (lines.length > 0) {
            for (int i = 0; i <lines.length; i++) {
                if (lines[i].contains("CategoryName:")) {
                    requestContent = data.detail.replace(lines[i], "").replace("##", "\n").trim();
                    break;
                }
            }
        }
        return requestContent;

    }

}
