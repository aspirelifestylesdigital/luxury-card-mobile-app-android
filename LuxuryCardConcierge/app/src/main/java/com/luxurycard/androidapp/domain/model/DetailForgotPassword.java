package com.luxurycard.androidapp.domain.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Den on 3/23/18.
 */

public class DetailForgotPassword implements Parcelable {
    String email;
    long time;

    public DetailForgotPassword(String email, long time) {
        this.email = email;
        this.time = time;
    }

    protected DetailForgotPassword(Parcel in) {
        email = in.readString();
        time = in.readLong();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public DetailForgotPassword(String jsonString) throws JSONException {
        JSONObject json = new JSONObject(jsonString);
        this.email = json.getString("email");
        this.time = json.getLong("time");

    }


    public JSONObject toJSON() throws JSONException {
        JSONObject json = new JSONObject();
        json.put("email", email);
        json.put("time", time);
        return json;
    }

    public static Creator<DetailForgotPassword> getCREATOR() {
        return CREATOR;
    }

    public static final Creator<DetailForgotPassword> CREATOR = new Creator<DetailForgotPassword>() {
        @Override
        public DetailForgotPassword createFromParcel(Parcel in) {
            return new DetailForgotPassword(in);
        }

        @Override
        public DetailForgotPassword[] newArray(int size) {
            return new DetailForgotPassword[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(email);
        dest.writeLong(time);
    }
}
