package com.luxurycard.androidapp.presentation.request;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.text.Editable;
import android.text.Html;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.luxurycard.androidapp.App;
import com.luxurycard.androidapp.R;
import com.luxurycard.androidapp.common.constant.IntentConstant;
import com.luxurycard.androidapp.domain.model.RequestDetailData;
import com.luxurycard.androidapp.presentation.base.BaseFragment;
import com.luxurycard.androidapp.presentation.widget.ViewKeyboardListener;
import com.luxurycard.androidapp.presentation.widget.ViewUtils;
import com.support.mylibrary.widget.ClickGuard;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by vinh.trinh on 5/24/2017.
 */

public class PlaceRequestFragment extends BaseFragment{

    private static final String STATE_RECORD_TEXT = "text";

    private Spanned hintText;

    @BindView(R.id.layout_container)
    View layoutContainer;
    @BindView(R.id.edt_ask_content)
    AppCompatEditText edtAskContent;
    @BindView(R.id.chb_reply_phone)
    RadioButton checkBoxPhone;
    @BindView(R.id.chb_reply_email)
    RadioButton checkBoxEmail;
    @BindView(R.id.btn_send_request)
    Button btnSendRequest;
    @BindView(R.id.title)
    TextView tvTitle;
    @BindView(R.id.tvCancel)
    TextView tvCancel;
//    @BindView(R.id.tv_roaming)
//    AppCompatTextView tvRoaming;

    @BindView(R.id.linearTitleRequestContainer)
    LinearLayout linearTitleRequestContainer;
    @BindView(R.id.linearRespondContainer)
    LinearLayout linearRespondContainer;
//    @BindView(R.id.linearCallContainer)
//    LinearLayout linearCallContainer;
//    @BindView(R.id.viewBottomFooter)
//    View viewBottomFooter;

    @BindView(R.id.tvErrorSelect)
    AppCompatTextView tvErrorSelect;

    @BindColor(R.color.color_hint_new_rq_1)
    int requestHintP1;
    @BindColor(R.color.color_hint_new_rq_2)
    int requestHintP2;

    /** View -> Activity */
    ViewEventsListener listener;

    private String requestTypeName;

    public static PlaceRequestFragment newInstance() {
        Bundle args = new Bundle();
        PlaceRequestFragment fragment = new PlaceRequestFragment();
        fragment.setArguments(args);
        return fragment;
    }
    public static PlaceRequestFragment newInstance(Bundle bundle) {
        PlaceRequestFragment fragment = new PlaceRequestFragment();
        fragment.setArguments(bundle);
        return fragment;
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ViewEventsListener) {
            listener = (ViewEventsListener) context;
        } else {
            throw new ClassCastException(context.toString()
                    + " must implement PlaceRequestFragment.ViewEventsListener.");
        }
    }

    @Override
    public int setupCreateViewLayoutId() {
        return R.layout.fragment_ask_concierge;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getActivity() != null && getActivity().getWindow() != null) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }


        setHintText();
        ClickGuard.guard(btnSendRequest, tvCancel);
        handleArgumentsData();
        handleInteract();
    }

    /**  config hint text */
    private void setHintText() {
        int flag = Spanned.SPAN_EXCLUSIVE_EXCLUSIVE;
        SpannableString s1 = new SpannableString("How can we help you today?\n\n");
        s1.setSpan(new ForegroundColorSpan(requestHintP1), 0, s1.length(), flag);
        //-------------------------------
        SpannableString s2 = new SpannableString("Ideas?\n");
        s2.setSpan(new ForegroundColorSpan(requestHintP1), 0, s2.length(), flag);
        s2.setSpan(new RelativeSizeSpan(0.5f), 0, s2.length(), flag);
        //-------------------------------
        SpannableString s3 = new SpannableString("Arrange dinner reservations for tonight\n" +
                "Book a hotel near me \n" +
                "Arrange a car to pick me up \n" +
                "Book tickets to a local concert");
        s3.setSpan(new ForegroundColorSpan(requestHintP2), 0, s3.length(), flag);
        s3.setSpan(new RelativeSizeSpan(0.7f), 0, s3.length(), flag);

        SpannableStringBuilder builder = new SpannableStringBuilder();
        builder.append(s1);
        builder.append(s2);
        builder.append(s3);

        //hintText = Html.fromHtml(getString(R.string.ask_hint));
        hintText = builder;
        edtAskContent.setHint(builder);
    }

    private void handleInteract() {
        //-- default off button Send


//        edtAskContent.setOnTouchListener((view, event) -> {
//            view.performClick();
//            if (event.getAction() == MotionEvent.ACTION_DOWN) {
//                handleEditTextFocus(true);
//            }
//            return false;
//        });
        edtAskContent.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                boolean hasText = s.length() > 0;
                boolean trimText = s.toString().trim().length() > 0;
                setViewBtnSend(hasText && trimText);
                if(!hasText){
                    hideErrorSelectResponse();
                }
            }
        });

        keyboardInteractListener();
    }

    /** handle click on back hide keyboard close cursor */
    private void keyboardInteractListener() {
        ViewKeyboardListener.KeyboardEvent event = new ViewKeyboardListener.KeyboardEvent() {
            @Override
            public void showKeyboard() {
            }

            @Override
            public void hideKeyboard() {
                //hideCursorFocus();
            }

            @Override
            public View getCurrentFocus() {
                return getActivity().getCurrentFocus();
            }
        };
        //-- add listen keyboard
        ViewKeyboardListener keyboardListener = new ViewKeyboardListener(layoutContainer, event);
        keyboardListener.setRootViewFocusChangeListener();
    }

    /** process receive data argument */
    private void handleArgumentsData() {
        if(getArguments() != null){
            String suggestedConcierge = getArguments().getString(IntentConstant.AC_SUGGESTED_CONCIERGE);
            if (suggestedConcierge!=null) {
                suggestedConcierge = suggestedConcierge.replace("\n", "<br/>");
                if (!TextUtils.isEmpty(suggestedConcierge + "<br/>")) {
                    edtAskContent.setText(Html.fromHtml(suggestedConcierge));
                    edtAskContent.setSelection(edtAskContent.getText().length() - 1);
                }
            }

            String requestName = getArguments().getString(IntentConstant.SELECTED_CATEGORY);
            if (!TextUtils.isEmpty(requestName)) {
                this.requestTypeName = requestName;
            }

            String prefResponse = getArguments().getString(IntentConstant.AC_PREF_RESPONSE);
            if(!TextUtils.isEmpty(prefResponse)){
                if(prefResponse.toLowerCase().contains("email")){
                    checkBoxEmail.setChecked(true);
                }
                if(prefResponse.toLowerCase().contains("mobile")){
                    checkBoxPhone.setChecked(true);
                }
            }
            boolean invokeKeyboard = getArguments().getBoolean(IntentConstant.INVOKE_KEYBOARD, false);
            if(invokeKeyboard) {
                edtAskContent.post(() -> {
                    setEdtFocusOn();
                    ViewUtils.invoSoftKey(edtAskContent);
                });
            }
            RequestDetailData requestDetailData = getArguments().getParcelable(IntentConstant.AC_REQUEST_DETAIL);
            if (requestDetailData != null) {
//                edtAskContent.setText(requestDetailData.detail.replace("##", "\n"));
                edtAskContent.setText(requestDetailData.getStringDetailContent());
                edtAskContent.setSelection(edtAskContent.getText().length());
                setViewBtnSend(false);
                boolean isDuplicate =  getArguments().getBoolean(IntentConstant.AC_BOOL_DUPLICATE, false);

                if (!isDuplicate) {
                    tvTitle.setText(getString(R.string.request_edit) + " " + requestDetailData.getEPCCaseIDCCaseID());
                } else {
                    tvTitle.setText(getString(R.string.request_duplicate));
                }
                if (!requestDetailData.prefResponse.isEmpty()) {
                    if (requestDetailData.prefResponse.equals("Email")) {
                        checkBoxEmail.setChecked(true);
                    } else {
                        checkBoxPhone.setChecked(true);
                    }
                }


            } else {
                setViewBtnSend(false);
            }
        }
    }

    @Override
    public void onResume() {
        //handleEditTextFocus(true);
//        setEdtFocusOn();
        super.onResume();
        focusAskEditText();
    }

    public void focusAskEditText() {

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
//                if (!edtAskContent.hasFocus()) {
                    edtAskContent.requestFocus();
                    ViewUtils.invoSoftKey(edtAskContent);
//                }
            }
        }, 500);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(STATE_RECORD_TEXT, edtAskContent.getText().toString());
        super.onSaveInstanceState(outState);
    }

    //<editor-fold desc="On Click">
    @OnClick(R.id.btn_send_request)
    public void recordOrSend(View view) {
        //hide error
        if(isShowErrorSelectResponse()){
            hideErrorSelectResponse();
        }



        String text = edtAskContent.getText().toString().trim();
        boolean statusPhone = checkBoxPhone.isChecked();
        boolean statusEmail = checkBoxEmail.isChecked();

        if(text.length() > 0){
            if(!statusPhone && !statusEmail){
                showErrorSelectResponse();
            }else {
                if (!TextUtils.isEmpty(requestTypeName)) {
                    if (!requestTypeName.contains("CategoryName")) {
                        text = "CategoryName: " + requestTypeName + " ##" + text;
                    }
                }
                listener.sendRequest(text.replace("\n", "##"), statusEmail, statusPhone);
//                ViewUtils.hideSoftKey(edtAskContent);
            }
        }// else do nothing
    }

//    /

    @OnClick(R.id.tvCancel)
    public void onClickCancel(View view) {
        listener.onClickButtonCancel();
    }

    @OnClick(R.id.btn_call)
    public void onClickCall(View view) {
        listener.makePhoneCall("");
    }
//
//    @OnClick({R.id.viewBottomFooter, R.id.layout_container})
//    public void onClickOutsideEdt(View view) {
//        handleEditTextFocus(false);
//    }

    /*@OnClick(R.id.title)
    public void orCallTheAssistant(View view) {
        if(getActivity() instanceof AskConciergeActivity){
            ((AskConciergeActivity) getActivity()).onRequestSuccessfullySent();
        }
    }*/

    //</editor-fold>

    void speechToTextResult(String text) {
        edtAskContent.setText(text);
        edtAskContent.setSelection(edtAskContent.getText().length());
    }

    /** show/hide cursor */
    private void handleEditTextFocus(boolean hasFocus) {
        String text = edtAskContent.getText().toString().trim();
        int textSize = text.length();
        boolean stsSend = textSize > 0;

        if(hasFocus) {
            setEdtFocusOn();
        } else {
            setEdtFocusOff();
        }

        setViewBtnSend(stsSend);
    }

    //<editor-fold desc="View">
    private void setEdtFocusOn() {
        edtAskContent.requestFocus();
//        edtAskContent.setHint("");
        edtAskContent.setCursorVisible(true);
    }

    private void setEdtFocusOff(){
        hideCursorFocus();
        if(getActivity() != null && getActivity().getCurrentFocus() instanceof EditText){
            ViewUtils.hideSoftKey(getActivity().getCurrentFocus());
        }
    }

    private void hideCursorFocus() {
//        edtAskContent.setHint(hintText);
        edtAskContent.setCursorVisible(false);
    }

    private void setViewBtnSend(final boolean statusEnable){
        btnSendRequest.setEnabled(statusEnable);
        btnSendRequest.setClickable(statusEnable);
    }

    private void showErrorSelectResponse(){
        tvErrorSelect.setVisibility(View.VISIBLE);
    }

    private void hideErrorSelectResponse(){
        tvErrorSelect.setVisibility(View.INVISIBLE);
    }

    private boolean isShowErrorSelectResponse(){
        return tvErrorSelect.getVisibility() == View.VISIBLE;
    }

    //</editor-fold>

    interface ViewEventsListener {
        //void record();
        void sendRequest(String content, boolean email, boolean phone);
        void updateRequest(String content, boolean email, boolean phone, String transactionID);
        void makePhoneCall(String number);
        void onClickButtonCancel();
    }
}
