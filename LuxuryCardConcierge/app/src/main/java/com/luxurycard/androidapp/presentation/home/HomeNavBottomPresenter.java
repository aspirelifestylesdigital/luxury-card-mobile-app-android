package com.luxurycard.androidapp.presentation.home;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;

import com.luxurycard.androidapp.common.constant.CityData;
import com.luxurycard.androidapp.common.constant.IntentConstant;
import com.luxurycard.androidapp.common.constant.RequestCode;
import com.luxurycard.androidapp.datalayer.datasource.AppGeoCoder;
import com.luxurycard.androidapp.datalayer.datasource.PreferenceData;
import com.luxurycard.androidapp.datalayer.preference.PreferencesStorage;
import com.luxurycard.androidapp.domain.model.LatLng;
import com.luxurycard.androidapp.domain.model.RequestDetailData;
import com.luxurycard.androidapp.domain.usecases.GetUserPreferences;
import com.luxurycard.androidapp.domain.usecases.LoadProfile;
import com.luxurycard.androidapp.presentation.bottomsheet.BottomSheetCustomFragment;
import com.luxurycard.androidapp.presentation.explore.ExploreFragment;
import com.luxurycard.androidapp.presentation.home.adapter.BaseParentFragment;
import com.luxurycard.androidapp.presentation.request.AskConciergeActivity;
import com.luxurycard.androidapp.presentation.widget.tooltip.TooltipView;
import com.luxurycard.androidapp.presentation.widget.tooltip.data.TooltipConstant;
import com.luxurycard.androidapp.presentation.widget.tooltip.data.TooltipData;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by vinh.trinh on 7/28/2017.
 */

public class HomeNavBottomPresenter implements HomeNavBottom.Presenter {

    private CompositeDisposable disposables;
    private HomeNavBottom.View view;

    private HomeNavBottomActivity homeNavBottomActivity;
    private GetUserPreferences getUserPreferences;
    private LoadProfile loadProfile;
    private AppGeoCoder geoCoder;
    private Single<LatLng> getLocation;
    private PreferencesStorage preferencesStorage;
    private boolean isTooltipCreated = false;

    //<editor-fold desc="data cache">

    private LatLng location = null;
    private String cuisine = null;

    //</editor-fold>

    HomeNavBottomPresenter(HomeNavBottomActivity HomeNavBottomActivity, GetUserPreferences getUserPreferences,
                           LoadProfile loadProfile, AppGeoCoder appGeoCoder,
                           PreferencesStorage preferencesStorage) {
        this.homeNavBottomActivity = HomeNavBottomActivity;
        this.getUserPreferences = getUserPreferences;
        this.loadProfile = loadProfile;
        this.geoCoder = appGeoCoder;
        this.preferencesStorage = preferencesStorage;
//        provideMockLocations();
    }

    @Override
    public void attach(HomeNavBottom.View view) {
        this.view = view;
    }

    @Override
    public void detach() {
        disposables.dispose();
        view = null;
    }

    //<editor-fold desc="HomeNav To Screen">

    void openAskConcierge(Bundle bundle){
        Intent intent = new Intent(homeNavBottomActivity, AskConciergeActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        homeNavBottomActivity.startActivityForResult(intent, RequestCode.REQUEST_CODE_ASK_CONCIERGE);
    }

    @Override
    public void openCallConcierge() {
        BottomSheetCustomFragment bottomSheetFragment = new BottomSheetCustomFragment();
        bottomSheetFragment.show(homeNavBottomActivity.getSupportFragmentManager(), BottomSheetCustomFragment.class.getSimpleName());
    }

    //</editor-fold>

    //-- hold feature location
    //<editor-fold desc="reload location">
    /*void loadPreferences(Location lastKnownLocation) {
        *//*LatLng mockLocation = mockLocationProvider.get(CityData.cityName());
        if(mockLocation == null) {
            getLocation = Single.just(lastKnownLocation == null ? LatLng.INVALID :
                    new LatLng(lastKnownLocation.getLatitude(), lastKnownLocation.getLongitude()));
        } else {
            getLocation = Single.just(mockLocation);
        }*//*
        getLocation = Single.just(lastKnownLocation == null ? LatLng.INVALID :
                new LatLng(lastKnownLocation.getLatitude(), lastKnownLocation.getLongitude()));
        Single.zip(getLocation(), getCuisinePreference(), (location, cuisine) -> new Result(
                hasLocation(location),
                hasCuisine(cuisine)
        ))
                .subscribeWith(new DisposableSingleObserver<Result>() {
                    @Override
                    public void onSuccess(Result result) {
                        location = result.location;
                        cuisine = result.cuisine;
                        reloadChangedExploreFragment();
                    }

                    @Override
                    public void onError(Throwable e) {
                    }
                });
    }

    void loadLocationCriteria() {
        getLocation()
                .subscribeWith(new DisposableSingleObserver<LatLng>() {
                    @Override
                    public void onSuccess(LatLng latLng) {
                        location = hasLocation(latLng);
                        reloadChangedExploreFragment();
                    }

                    @Override
                    public void onError(Throwable e) {
                    }
                });
    }*/
    //</editor-fold>

    void loadCuisineCriteria(final boolean isReloadPageExplore) {
        getUserPreferences.param(null)
                .on(Schedulers.io(), AndroidSchedulers.mainThread())
                .execute(new DisposableSingleObserver<PreferenceData>() {
                    @Override
                    public void onSuccess(PreferenceData preferenceData) {
                        //cache data
                        cuisine = hasCuisine(preferenceData.cuisine);
                        if(isReloadPageExplore){
                            reloadChangedExploreFragment();
                        }//-- else do nothing here
                    }

                    @Override
                    public void onError(Throwable e) {
                    }
                });
    }

    //-- hold feature location
    //<editor-fold desc="reload location">
    /*private Single<LatLng> getLocation() {
        if (getLocation == null) return Single.just(LatLng.INVALID);
        return Single.zip(loadProfile.buildUseCaseSingle(null), getLocation,
                (profile, location) -> {
                    if (!location.isValid() || !profile.isLocationOn() || !CityData.citySelected())
                        return LatLng.INVALID;
                    String cityName = CityData.cityName();
                    if (CityData.regions.contains(cityName)) {
                        return location;
                    }
                    String currentAddress = geoCoder.getAddress(location);
                    return currentAddress.contains(CityData.cityName()) ? location : LatLng.INVALID;
                }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    private Single<String> getCuisinePreference() {
        return getUserPreferences.buildUseCaseSingle(null)
                .flatMap(preferenceData -> Single.just(preferenceData.cuisine));
    }

    private LatLng hasLocation(LatLng location) {
        return location.isValid() ? location : null;
    }*/
    //</editor-fold>

    private String hasCuisine(String cuisine) {
        return cuisine.equalsIgnoreCase(PreferenceData.NA_VALUE) ? null : cuisine;
    }

    private void reloadChangedExploreFragment() {
        if (homeNavBottomActivity.vpAdapter != null) {
            BaseParentFragment baseParentFragment = homeNavBottomActivity.vpAdapter.getTopFragmentOnTabHome();
            Fragment fr = baseParentFragment.getChildFragmentManager().findFragmentByTag(ExploreFragment.class.getSimpleName());
            if(fr instanceof ExploreFragment){
                ((ExploreFragment) fr).preferencesChange(location, cuisine);
            }
        }
    }

    private class Result {
        LatLng location;
        String cuisine;

        Result(LatLng location, String cuisine) {
            this.location = location;
            this.cuisine = cuisine;
        }
    }

    //<editor-fold desc="Data cache">
    public LatLng getLocationCache() {
        return location;
    }

    public String getCuisineCache() {
        return cuisine;
    }
    //</editor-fold>

    @Override
    public void handleTooltip() {
        if (view != null && !isTooltipCreated) {
            isTooltipCreated = true;
            if(TooltipData.isShouldShow(preferencesStorage, TooltipConstant.BOTTOM_MENU_REQUESTS)){
                View bottomMenuRequestView = view.getBottomMenuRequests();
                if(bottomMenuRequestView != null){
                    TooltipView tooltipView = new TooltipView();
                    tooltipView.showViewTop(bottomMenuRequestView, TooltipConstant.BOTTOM_MENU_REQUESTS);
                }

            }//else do nothing here
        }
    }

    /*private Map<String, LatLng> mockLocationProvider;
    private void provideMockLocations() {
        mockLocationProvider = new HashMap<>();
        mockLocationProvider.put("London", new LatLng(51.511981f,-0.0693553f));
        mockLocationProvider.put("Boston", new LatLng(42.3020856f,-71.0554553f));
        mockLocationProvider.put("Los Angeles", new LatLng(34.1442319f,-118.3975795f));
        mockLocationProvider.put("Vancouver", new LatLng(49.261055f,-123.1289526f));
    }*/
}
