package com.luxurycard.androidapp.presentation.bottomsheet;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.luxurycard.androidapp.App;
import com.luxurycard.androidapp.R;
import com.luxurycard.androidapp.datalayer.preference.PreferencesStorage;
import com.luxurycard.androidapp.presentation.home.HomeNavBottomActivity;
import com.luxurycard.androidapp.presentation.widget.DialogHelper;

import android.Manifest;

import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.content.Context.MODE_PRIVATE;

public class BottomSheetCustomFragment extends BottomSheetDialogFragment {

    private OnBottomSheetListener listener;

    public static final int MY_PERMISSIONS_REQUEST_CALL_PHONE = 1;
    private String numberToCall;

    public BottomSheetCustomFragment() {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnBottomSheetListener) {
            listener = (OnBottomSheetListener) context;
        } else {
            throw new ClassCastException(context.toString() + " must implement OnBottomSheetListener.");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.CustomBottomSheetDialogTheme);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bottom_sheet_dialog, container, false);
        ButterKnife.bind(this, view);
        return view;
    }



    @OnClick(R.id.bottom_sheet_number_domestic)
    public void onClickDomestic(View view) {
        numberToCall = App.getInstance().getString(R.string.bottom_sheet_number_domestic_value);
        makeCall(numberToCall);
    }

    @OnClick(R.id.bottom_sheet_number_international)
    public void onClickCallInternational(View view) {
        numberToCall = App.getInstance().getString(R.string.bottom_sheet_number_international_value);
        makeCall(numberToCall);
    }

    @OnClick(R.id.sheet_btn_cancel)
    public void onClickCancel(View view) {
        dismiss();
        listener.onBottomSheetClickCancel();
    }



    private void makeCall(String number) {
        //check permission
        int permissionCheck = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, MY_PERMISSIONS_REQUEST_CALL_PHONE);
        } else {
            call(number);
        }

    }

    private void call(String number) {
        try {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:" + number));
            startActivity(callIntent);
            dismissAllowingStateLoss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        PreferencesStorage preferencesStorage = new PreferencesStorage(getContext());
        boolean firstTimeAccount = preferencesStorage.hasCheckPermission();

        if (requestCode == MY_PERMISSIONS_REQUEST_CALL_PHONE || grantResults.length > 0) {
            switch (grantResults[0]) {
                case PackageManager.PERMISSION_GRANTED:
                    makeCall(numberToCall);
                    break;
                case PackageManager.PERMISSION_DENIED:
                    boolean showRationale = shouldShowRequestPermissionRationale(permissions[0]);
                    if (showRationale) {
//                        dismiss();
                    } else {
                        if (firstTimeAccount) {
                            // 1. first time, never asked
                            preferencesStorage.editor().hasCheckPermission(false).build().saveAsync();
                        } else {
                            showDialogActionSetting();
                        }
                    }
                    break;
                default:
                    return;
            }
        }

    }


    private void openSetting() {
        Intent i = new Intent();
        i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        i.addCategory(Intent.CATEGORY_DEFAULT);
        i.setData(Uri.parse("package:" + getActivity().getPackageName()));
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        startActivity(i);
    }

    public interface OnBottomSheetListener {
        void onBottomSheetClickCancel();
    }


    public void showDialogActionSetting() {
        if (getActivity() instanceof HomeNavBottomActivity) {
            DialogHelper dialogHelper = ((HomeNavBottomActivity) getActivity()).mDialogHelper;
            if (dialogHelper != null) {
                DialogInterface.OnClickListener listener = (dialogInterface, i) -> {
                    switch (i) {
                        case DialogInterface.BUTTON_NEGATIVE:
                            dismiss();
                            break;
                        case DialogInterface.BUTTON_POSITIVE:
                            openSetting();
                            break;
                    }
                };

                dialogHelper.action("",
                        getString(R.string.call_permission_dialog),
                        getString(R.string.text_cancel),
                        getString(R.string.action_settings), listener, listener
                );
            }

        }
    }

}