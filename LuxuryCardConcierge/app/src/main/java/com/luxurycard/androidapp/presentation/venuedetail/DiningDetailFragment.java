package com.luxurycard.androidapp.presentation.venuedetail;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.BaseTarget;
import com.bumptech.glide.request.target.SizeReadyCallback;
import com.luxurycard.androidapp.App;
import com.luxurycard.androidapp.R;
import com.luxurycard.androidapp.common.constant.AppConstant;
import com.luxurycard.androidapp.common.constant.IntentConstant;
import com.luxurycard.androidapp.common.constant.RequestCode;
import com.luxurycard.androidapp.common.glide.GlideHelper;
import com.luxurycard.androidapp.common.logic.HtmlTagHandler;
import com.luxurycard.androidapp.common.logic.HtmlUtils;
import com.luxurycard.androidapp.common.logic.ShareHelper;
import com.luxurycard.androidapp.common.logic.TextViewLinkHandler;
import com.luxurycard.androidapp.common.logic.urlimage.URLImageParser;
import com.luxurycard.androidapp.datalayer.preference.PreferencesStorage;
import com.luxurycard.androidapp.datalayer.repository.B2CDataRepository;
import com.luxurycard.androidapp.domain.model.explore.DiningDetailItem;
import com.luxurycard.androidapp.domain.model.explore.ExploreRView;
import com.luxurycard.androidapp.domain.model.explore.SearchDetailItem;
import com.luxurycard.androidapp.domain.usecases.GetCityGuideDetail;
import com.luxurycard.androidapp.domain.usecases.GetDiningDetail;
import com.luxurycard.androidapp.presentation.base.BaseNavSubFragment;
import com.luxurycard.androidapp.presentation.request.AskConciergeActivity;
import com.luxurycard.androidapp.presentation.venuedetail.mapview.VenueDetailMapFragment;
import com.luxurycard.androidapp.presentation.widget.DialogHelper;
import com.support.mylibrary.widget.LetterSpacingTextView;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Den on 4/13/18.
 */

public class DiningDetailFragment extends BaseDetailFragment implements DiningDetail.View {


    @BindView(R.id.dining_detail_layout)
    View diningDetailLayout;
    @BindView(R.id.loading)
    ProgressBar pbLoading;

    @BindView(R.id.explore_detail_image)
    ImageView ivDetailImage;
    @BindView(R.id.explore_action_book)
    ImageButton ivActionBook;
    @BindView(R.id.explore_action_share)
    ImageButton ivActionShare;
    @BindView(R.id.explore_name)
    TextView tvDiningName;
    @BindView(R.id.explore_address)
    TextView tvDiningAddress;
    @BindView(R.id.explore_see_map)
    TextView tvDiningSeeMap;
    @BindView(R.id.explore_website)
    TextView tvDiningWebsite;
    @BindView(R.id.explore_insider_tip_root_layout)
    View insiderTipLayout;
    @BindView(R.id.insider_tip_content)
    LetterSpacingTextView tvInsiderTip;
    @BindView(R.id.explore_your_benefit_root_layout)
    View benefitLayout;
    @BindView(R.id.your_benefit_content)
    LetterSpacingTextView tvBenefitContent;
    @BindView(R.id.explore_price_cuisine_layout)
    View priceCuisineLayout;
    @BindView(R.id.explore_price_range_layout)
    View priceLayout;
    @BindView(R.id.explore_price_range)
    TextView tvPriceRange;
    @BindView(R.id.explore_cuisine_layout)
    View cuisineLayout;
    @BindView(R.id.explore_cuisine)
    TextView tvCuisine;
    @BindView(R.id.explore_description_layout)
    View diningDescriptionLayout;
    @BindView(R.id.explore_description)
    LetterSpacingTextView tvDiningDescription;
    @BindView(R.id.explore_hour_of_operation_layout)
    View hourOfOperationLayout;
    @BindView(R.id.explore_hour_of_operation)
    LetterSpacingTextView tvHourOfOperation;
    @BindView(R.id.explore_terms_and_conditions_layout)
    View termsAndConditionsLayout;
    @BindView(R.id.explore_terms_and_conditions)
    LetterSpacingTextView tvTermsAndConditions;

    private SearchDetailItem searchDetailItem;
    private DiningDetailItem diningDetailItem;
    private Bitmap detailBitmap;

    private DiningDetailPresenter presenter;

    ExploreRView detailViewData;

    public static DiningDetailFragment newInstance(String titleExplore, ExploreRView detailViewData) {
        Bundle args = new Bundle();
        DiningDetailFragment fragment = new DiningDetailFragment();
        args.putString("explore_title", titleExplore);
        args.putParcelable("explore_dining_detail", detailViewData);
        fragment.setArguments(args);
        return fragment;
    }

    private DiningDetailPresenter buildPresenter() {
        return new DiningDetailPresenter(
                new PreferencesStorage(App.getInstance().getApplicationContext()),
                new GetDiningDetail(new B2CDataRepository()));
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = buildPresenter();
        presenter.attach(this);
        trackGA(savedInstanceState);
    }

    @Override
    public int setupCreateViewLayoutId() {
        return R.layout.activity_dining_detail;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getBtnCallConcierge().setVisibility(View.VISIBLE);
        getButtonBack().setOnClickListener(view1 -> this.onBackPressed());
        tvInsiderTip.setMovementMethod(textViewLinkHandler);
        tvBenefitContent.setMovementMethod(textViewLinkHandler);
        tvDiningDescription.setMovementMethod(textViewLinkHandler);
        tvHourOfOperation.setMovementMethod(textViewLinkHandler);
        tvTermsAndConditions.setMovementMethod(textViewLinkHandler);

//        tvDiningSeeMap.setPaintFlags(tvDiningSeeMap.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
//        tvDiningWebsite.setPaintFlags(tvDiningWebsite.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);


//        ExploreRView exploreRView = getIntent().getParcelableExtra(IntentConstant.EXPLORE_DETAIL);

        if (getArguments() == null) {
            return;
        }

        detailViewData = getArguments().getParcelable("explore_dining_detail");
        String title = getArguments().getString("explore_title", "");
        setTitle(title);

        if (detailViewData == null) {
            return;
        }

        if (detailViewData instanceof DiningDetailItem) { // Load UI directly from bundle
            diningDetailItem = (DiningDetailItem) detailViewData;
            renderUI();
        } else if(detailViewData instanceof SearchDetailItem){
            searchDetailItem = (SearchDetailItem) detailViewData;
//                setTitle(searchDetailItem.title);

            pbLoading.setVisibility(View.VISIBLE);
            diningDetailLayout.setVisibility(View.GONE);

            // Load from API
            presenter.getDiningDetail(searchDetailItem.secondaryID, searchDetailItem.ID);
        }

    }

    @Override
    public void onDestroy() {
        presenter.detach();
        super.onDestroy();
    }

    private void renderUI(){
        pbLoading.setVisibility(View.GONE);
        if(diningDetailItem != null){
            diningDetailLayout.setVisibility(View.VISIBLE);
            // Dining avatar
            if(!TextUtils.isEmpty(diningDetailItem.imageURL) && diningDetailItem.imageURL.contains("http")) {
                /*Picasso.with(this)
                        .load(diningDetailItem.imageURL)
                        .into(new Target() {
                            @Override
                            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                                detailBitmap = bitmap;
                                ivDetailImage.setImageBitmap(bitmap);
                            }

                            @Override
                            public void onBitmapFailed(Drawable errorDrawable) {

                            }

                            @Override
                            public void onPrepareLoad(Drawable placeHolderDrawable) {

                            }
                        });*/
                GlideHelper.getInstance().loadImage(diningDetailItem.imageURL,
                        0, ivDetailImage, 0, new BaseTarget<Bitmap>() {
                            @Override
                            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                detailBitmap = resource;
                            }

                            @Override
                            public void getSize(SizeReadyCallback cb) {

                            }
                        });
            }
            // Dining name
            if(TextUtils.isEmpty(diningDetailItem.title)){
                tvDiningName.setVisibility(View.GONE);
            }else{
                tvDiningName.setVisibility(View.VISIBLE);
                tvDiningName.setText(Html.fromHtml(diningDetailItem.title));
//                setTitle(diningDetailItem.title);
            }
            // Dining address
            String diningAddress = diningDetailItem.getDisplayAddress();
            if(TextUtils.isEmpty(diningAddress)){
                tvDiningAddress.setVisibility(View.GONE);
            }else{
                tvDiningAddress.setVisibility(View.VISIBLE);
                tvDiningAddress.setText(diningAddress);
            }
            // See map --- later
            // Website
            if(TextUtils.isEmpty(diningDetailItem.url) || !diningDetailItem.url.contains("http")){
                tvDiningWebsite.setVisibility(View.GONE);
            }else{
                tvDiningWebsite.setVisibility(View.VISIBLE);
//                tvDiningWebsite.setText(diningDetailItem.url);
            }
            // Insider tip
            String insiderTip = diningDetailItem.insiderTips;
            if(TextUtils.isEmpty(insiderTip)){
                insiderTipLayout.setVisibility(View.GONE);
            }else{
                insiderTipLayout.setVisibility(View.VISIBLE);
                tvInsiderTip.setText(Html.fromHtml(HtmlUtils.adjustSomeHtmlTag(insiderTip), new URLImageParser(tvInsiderTip, getActivity(), false, null),
                        new HtmlTagHandler()));
            }
            // Benefit
            if(TextUtils.isEmpty(diningDetailItem.benefits)){
                benefitLayout.setVisibility(View.GONE);
            }else{
                benefitLayout.setVisibility(View.VISIBLE);
                tvBenefitContent.setText(Html.fromHtml(HtmlUtils.adjustSomeHtmlTag(diningDetailItem.benefits), new URLImageParser(tvBenefitContent, getActivity(), false, null),
                        new HtmlTagHandler()));
            }
            priceCuisineLayout.setVisibility(View.GONE);
            // Price range
            if(TextUtils.isEmpty(diningDetailItem.priceRange)){
                priceLayout.setVisibility(View.GONE);
            }else{
                priceCuisineLayout.setVisibility(View.VISIBLE);
                priceLayout.setVisibility(View.VISIBLE);
                tvPriceRange.setText(diningDetailItem.priceRange);
            }
            // Cuisine
            if(TextUtils.isEmpty(diningDetailItem.cuisine)){
                cuisineLayout.setVisibility(View.GONE);
            }else{
                priceCuisineLayout.setVisibility(View.VISIBLE);
                cuisineLayout.setVisibility(View.VISIBLE);
                tvCuisine.setText(diningDetailItem.cuisine);
            }
            // Description
            if(TextUtils.isEmpty(diningDetailItem.description)){
                diningDescriptionLayout.setVisibility(View.GONE);
            }else{
                diningDescriptionLayout.setVisibility(View.VISIBLE);
                tvDiningDescription.setText(Html.fromHtml(HtmlUtils.adjustSomeHtmlTag(diningDetailItem.description), new URLImageParser(tvDiningDescription, getActivity(), false, null),
                        new HtmlTagHandler()));
            }
            // Hour of operation
            if(TextUtils.isEmpty(diningDetailItem.hoursOfOperation)){
                hourOfOperationLayout.setVisibility(View.GONE);
            }else{
                hourOfOperationLayout.setVisibility(View.VISIBLE);
                tvHourOfOperation.setText(Html.fromHtml(HtmlUtils.adjustSomeHtmlTag(diningDetailItem.hoursOfOperation), new URLImageParser(tvHourOfOperation, getActivity(), false, null),
                        new HtmlTagHandler()));
            }
            // Terms of Condition
            termsAndConditionsLayout.setVisibility(TextUtils.isEmpty(diningDetailItem.benefits) ? View.GONE : View.VISIBLE);
        }

        /* Tooltip View */
        presenter.handleTooltip();

    }

    @OnClick({R.id.explore_see_map, R.id.explore_website, R.id.explore_action_book, R.id.explore_action_share})
    public void onClick(View view){
        switch (view.getId()){
            case R.id.explore_see_map:
                //new DialogHelper(DiningDetailActivity.this)
//                        .showLeavingAlert((dialogInterface, i) -> MapSearchHelper.getInstance().searchNameAndShowOnMap(DiningDetailActivity.this, diningDetailItem.getQuerySearchOnGoogleMap(), diningDetailItem.getTitle()));
                //      .showLeavingAlert((dialogInterface, i) ->
                if(!App.getInstance().hasNetworkConnection()) {
//                    dialogHelper = new DialogHelper(this);
//                    dialogHelper.networkUnavailability(ErrCode.CONNECTIVITY_PROBLEM, null);
                    showErrorNoNetwork();
                }
                else {
                    addChildFragmentTabCurrent(VenueDetailMapFragment.newInstance(null, diningDetailItem));
                }
                    //startActivity(new Intent(this, VenueDetailMapActivity.class).putExtra("dining",diningDetailItem));
                //);
                break;
            case R.id.explore_website:
                new DialogHelper(getActivity()).showLeavingAlert(diningDetailItem.url);
                break;
            case R.id.explore_action_book:
                Bundle bundle = new Bundle();
                bundle.putString(IntentConstant.SELECTED_CATEGORY, "Restaurant Request");
                bundle.putString(IntentConstant.AC_SUGGESTED_CONCIERGE, diningDetailItem.getSuggestedToAC());
                openNewRequestDetailActivity(bundle);
                break;
            case R.id.explore_action_share:
                ShareHelper.getInstance().share(getActivity(), diningDetailItem, detailBitmap);
                break;
        }
    }

    private TextViewLinkHandler textViewLinkHandler = new TextViewLinkHandler() {
        @Override
        public void onLinkClick(String url) {
            new DialogHelper(getActivity()).showLeavingAlert(url);
        }
    };

    @Override
    public void onGetDiningDetailFinished(DiningDetailItem diningDetailItem) {
        this.diningDetailItem = diningDetailItem;
        renderUI();
    }

    @Override
    public void onUpdateFailed() {
        pbLoading.setVisibility(View.GONE);
    }

    @Override
    public View getButtonBook() {
        return ivActionBook;
    }
}
