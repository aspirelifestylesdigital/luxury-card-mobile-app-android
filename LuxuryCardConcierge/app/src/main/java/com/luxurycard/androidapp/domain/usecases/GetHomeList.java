package com.luxurycard.androidapp.domain.usecases;

import android.text.TextUtils;

import com.luxurycard.androidapp.datalayer.entity.Tiles;
import com.luxurycard.androidapp.domain.model.HomeViewItem;
import com.luxurycard.androidapp.domain.repository.B2CRepository;
import com.luxurycard.androidapp.presentation.homelist.dto.HomeContent;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;


/**
 * Created by vinh.trinh on 6/22/2017.
 */

public class GetHomeList extends UseCase<List<HomeViewItem>, GetHomeList.Params> {

    private B2CRepository b2CRepository;
    private String cityAll = "all";

    public GetHomeList(B2CRepository b2CRepository) {
        this.b2CRepository = b2CRepository;
    }

    @Override
    Observable<List<HomeViewItem>> buildUseCaseObservable(Params params) {
        return null;
    }

    @Override
    public Single<List<HomeViewItem>> buildUseCaseSingle(Params params) {
        return b2CRepository.getHomeList().map(dataList -> filterHomeListData(dataList, params))
                .map(dataListURL ->{
                    //-- sort follow TitleText
                    Collections.sort(dataListURL);
                   return dataListURL;
                });
    }

    @Override
    Completable buildUseCaseCompletable(Params params) {
        return null;
    }

    private List<HomeViewItem> filterHomeListData(List<Tiles> dataList, Params params) {
        List<HomeViewItem> homeItems = new ArrayList<>();
        int length = dataList.size();
        for (int i = 0; i < length; i++) {
            Tiles tiles = dataList.get(i);
            String filterCity = tiles.shortDescription();

            if(!TextUtils.isEmpty(filterCity)){
                if(filterCity.toLowerCase().contains(cityAll)){
                    homeItems.add(homeViewItemUrl(tiles));
                }else if(filterCity.contains(params.cityName)){
                    homeItems.add(homeViewItemUrl(tiles));
                }//-- else don't add to list view
            }
        }
        return homeItems;
    }

    /*getColor(tiles.shortDescription()*/
    private HomeViewItem homeViewItemUrl(Tiles tiles) {
        String id = "0"; //-- default id 0
        String titleContent = "";
        String textSort = "";
        String imgURL = "";
        String linkCategory = "";
        try {
            id = String.valueOf(tiles.ID().intValue());
            if(!TextUtils.isEmpty(tiles.title())){
                titleContent = tiles.title();
            }
            if(!TextUtils.isEmpty(tiles.image())){
                imgURL = tiles.image();
            }
            if(!TextUtils.isEmpty(tiles.text())){
                textSort = tiles.text();
            }

            if(!TextUtils.isEmpty(tiles.link())){
                linkCategory = tiles.link();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        return new HomeViewItem(id, HomeContent.TYPE_API, imgURL,0, titleContent, textSort, linkCategory);
    }


    public static final class Params {
        private String cityName;

        public Params(String cityName) {
            this.cityName = cityName;
        }
    }

}
