package com.luxurycard.androidapp.presentation.search;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatCheckBox;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.luxurycard.androidapp.App;
import com.luxurycard.androidapp.R;
import com.luxurycard.androidapp.common.constant.AppConstant;
import com.luxurycard.androidapp.common.constant.RequestCode;
import com.luxurycard.androidapp.presentation.base.BaseNavSubFragment;
import com.luxurycard.androidapp.presentation.explore.ExploreDTO.ExploreParam;
import com.luxurycard.androidapp.presentation.explore.ExploreDTO.ExploreParamSearch;
import com.luxurycard.androidapp.presentation.explore.ExploreFragment;
import com.luxurycard.androidapp.presentation.widget.DialogHelper;
import com.luxurycard.androidapp.presentation.widget.ViewKeyboardListener;
import com.luxurycard.androidapp.presentation.widget.ViewUtils;

import java.util.Objects;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by vinh.trinh on 5/3/2017.
 */

public class SearchFragment extends BaseNavSubFragment {

    @BindView(R.id.main_search_view)
    View mainSearchView;
    @BindView(R.id.edt_search)
    EditText edtSearchBox;
    @BindView(R.id.checkBox_offers)
    AppCompatCheckBox checkBoxOffers;
    @BindView(R.id.btn_search_submit)
    AppCompatButton buttonSubmit;
    @BindView(R.id.view_dummy_keyboard)
    View viewKeyboard;

    @BindView(R.id.linearCheckBoxContainer)
    LinearLayout linearCheckBoxContainer;

    ViewKeyboardListener keyboardListener;

    public static SearchFragment newInstance() {
        return SearchFragment.newInstance("", false);
    }

    public static SearchFragment newInstance(String term, boolean withOffers) {
        Bundle args = new Bundle();
        SearchFragment fragment = new SearchFragment();
        args.putString("explore_search_term", term);
        args.putBoolean("explore_search_with_offers", withOffers);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(savedInstanceState == null){
            // Track GA
            App.getInstance().track(AppConstant.GA_TRACKING_SCREEN_NAME.SEARCH.getValue());
        }
    }

    @Override
    public int setupCreateViewLayoutId() {
        return R.layout.fragment_search;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setTitle(R.string.text_search);
        getBtnCallConcierge().setVisibility(View.VISIBLE);
        getButtonBack().setOnClickListener(view1 -> {
            if (getParentFragment() != null) {
                ExploreFragment exploreFragment = getExploreFragment();
                if (exploreFragment != null) {
                    exploreFragment.reloadData(RequestCode.SEARCH_INPUT, null);

                    //previous page explore fragment
                    onBackPreviousFragment();
                }
            }
        });

        ViewUtils.drawableStart(edtSearchBox, R.drawable.ic_search_small);
        linearCheckBoxContainer.setEnabled(false);
        linearCheckBoxContainer.setClickable(false);
        checkBoxOffers.setChecked(false);
        buttonSubmit.setEnabled(false);
        buttonSubmit.setClickable(false);

        edtSearchBox.setImeActionLabel(getString(R.string.text_search), EditorInfo.IME_ACTION_SEARCH);
        edtSearchBox.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                searchSubmit(edtSearchBox);
                return true;
            }
            return false;
        });
        edtSearchBox.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (edtSearchBox.getText().toString().trim().length() == 0) {
                    checkBoxOffers.setChecked(false);
                    linearCheckBoxContainer.setEnabled(false);
                    linearCheckBoxContainer.setClickable(false);
                    buttonSubmit.setEnabled(false);
                    buttonSubmit.setClickable(false);
                } else {
                    linearCheckBoxContainer.setEnabled(true);
                    linearCheckBoxContainer.setClickable(true);
                    buttonSubmit.setEnabled(true);
                    buttonSubmit.setClickable(true);
                }
            }
        });

        keyboardInteractListener();

        // Suggested keyword
        if(getArguments() != null){
            String suggestedKeyword = getArguments().getString("explore_search_term", "");
            boolean isWithOffers = getArguments().getBoolean("explore_search_with_offers", false);
            if(!TextUtils.isEmpty(suggestedKeyword)){
                edtSearchBox.setText(suggestedKeyword);
                edtSearchBox.setSelection(suggestedKeyword.length());
            }
            checkBoxOffers.setChecked(isWithOffers);
        }

    }

    private void onBackPreviousFragment() {
        if (keyboardListener != null) {
            keyboardListener.resetListenHeightKeyboard();
        }

        if(edtSearchBox != null){
            ViewUtils.hideSoftKey(edtSearchBox);
            edtSearchBox.postDelayed(this::onBackPressed, 100);
        }
    }

    /**
     * handle click on back hide keyboard close cursor
     */
    private void keyboardInteractListener() {
        ViewKeyboardListener.KeyboardEvent event = new ViewKeyboardListener.KeyboardEvent() {
            @Override
            public void showKeyboard() {
                if(viewKeyboard != null)
                    viewKeyboard.setVisibility(View.VISIBLE);
            }

            @Override
            public void hideKeyboard() {
                if(viewKeyboard != null)
                    viewKeyboard.setVisibility(View.GONE);
            }

            @Override
            public View getCurrentFocus() {
                try {
                    return Objects.requireNonNull(getActivity()).getCurrentFocus();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }
        };

        //-- add listen keyboard
        keyboardListener = new ViewKeyboardListener(mainSearchView, event);
        keyboardListener.setRootViewFocusChangeListener();

    }

    @OnClick(R.id.btn_search_submit)
    void searchSubmit(View view) {
        String term = edtSearchBox.getText().toString().trim();
        boolean withOffers = checkBoxOffers.isChecked();
        if(TextUtils.isEmpty(term) && !withOffers) {
            edtSearchBox.setText("");
            DialogHelper dialogHelper = getActionDialog();
            if(dialogHelper != null){
                dialogHelper.alert(view.getResources().getString(R.string.text_alert), getString(R.string.search_input_required));
            }
            return;
        }

        //-- reload page ExploreFragment
        ExploreParam exploreParam = new ExploreParam(new ExploreParamSearch(term, withOffers));

        ExploreFragment exploreFragment = getExploreFragment();
        if (exploreFragment != null) {
            exploreFragment.reloadData(RequestCode.SEARCH_INPUT, exploreParam);

            //previous page explore fragment
            onBackPreviousFragment();
        }
    }

    @OnClick({R.id.main_search_view, R.id.toolbar, R.id.frameSearchContainer})
    void onMainViewClick(View v){
        ViewUtils.hideSoftKey(edtSearchBox);
    }


    @OnClick(R.id.linearCheckBoxContainer)
    void onClickCheckBox(View v) {
        checkBoxOffers.setChecked(!checkBoxOffers.isChecked());
    }

    private ExploreFragment getExploreFragment() {
        if (getParentFragment() != null) {
            Fragment baseExploreFr = getParentFragment().getChildFragmentManager().findFragmentByTag(ExploreFragment.class.getSimpleName());
            if (baseExploreFr instanceof ExploreFragment) {
                return ((ExploreFragment) baseExploreFr);
            }
        }
        return null;
    }
}
