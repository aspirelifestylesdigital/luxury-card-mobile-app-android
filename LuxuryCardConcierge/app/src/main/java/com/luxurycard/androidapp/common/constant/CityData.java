package com.luxurycard.androidapp.common.constant;

import android.text.TextUtils;
import android.util.SparseArray;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by vinh.trinh on 6/2/2017.
 */

public final class CityData {
    /* warring change array cities */
    private static LinkedHashMap<String, City> cities;
    static final SparseArray<CityGuide> cityGuide;
    private static String SELECTED_CITY = "";
    public static final String DEFAULT_CITY = "All";
    public static final String VALUE_GEOGRAPHIC_REGION_ALL = "all";
    public static final List<String> regions = Arrays.asList("Africa/Middle East", "Asia Pacific", "Caribbean",
            "Europe", "Latin America", "Canada");

    private static final List<Integer> cityGuideCodeList = Arrays.asList(
            6357, 6358, 6359, 6360, 6362,
            6339, 6340, 6341, 6342, 6344,
            6452, 6453, 6454, 6455, 6457,
            6351, 6352, 6353, 6354, 6356,
            6375, 6376, 6377, 6378, 6380,
            6363, 6364, 6365, 6366, 6368,
            6333, 6334, 6335, 6336, 6338,
            6436, 6438, 6439, 6440, 6437,
            6326, 6327, 6328, 6329, 6331,
            6395, 6398, 6405, 6408, 6418,
            6381, 6382, 6383, 6384, 6386,
            6460, 6461, 6462, 6471, 6472,
            6369, 6370, 6371, 6372, 6374,
            6446, 6447, 6448, 6449, 6451,
            6430, 6432, 6433, 6434, 6431,
            6464, 6468, 6470, 6465, 6469,
            6345, 6346, 6347, 6348, 6350
    );
    static {
        cityGuide = new SparseArray<>();

        cities = new LinkedHashMap<>();

        //<editor-fold desc="Config city name">
        final String cityNameAtlanta = "Atlanta";
        final String cityNameBoston = "Boston";
        final String cityNameCancun = "Cancun";
        final String cityNameChicago = "Chicago";
        final String cityNameCleveland = "Cleveland";
        final String cityNameDallas = "Dallas";
        final String cityNameHawaii = "Hawaii";
        final String cityNameLasVegas = "Las Vegas";
        final String cityNameLondon = "London";
        final String cityNameLosAngles = "Los Angeles";
        final String cityNameMiami = "Miami";
        final String cityNameMilwaukee = "Milwaukee";
        final String cityNameMontreal = "Montreal";
        final String cityNameNapa = "Napa/Sonoma";
        final String cityNameOrleans = "New Orleans";
        final String cityNameNewYork = "New York";
        final String cityNameOrlando = "Orlando";
        final String cityNameParis = "Paris";
        final String cityNamePhiladelphia = "Philadelphia";
        final String cityNameRome = "Rome";
        final String cityNameSan = "San Francisco";
        final String cityNameSeattle = "Seattle";
        final String cityNameToronto = "Toronto";
        final String cityNameVancouver = "Vancouver";
        final String cityNameWashington = "Washington, D.C.";
        final String cityNameAfrica = "Africa/Middle East";
        final String cityNameAsia = "Asia Pacific";
        final String cityNameCanada = "Canada";
        final String cityNameCaribbean = "Caribbean";
        final String cityNameEurope = "Europe";
        final String cityNameLatin = "Latin America";
        //</editor-fold>

        cities.put(cityNameAtlanta,new City(cityNameAtlanta,"North America.Georgia",true,6514,0));
        cities.put(cityNameBoston,new City(cityNameBoston,"North America.Vermont.Maine.Rhode Island.New Hampshire.Massachusetts",true,6422,6311,6357,6358,6359,6360,6362));
        cities.put(cityNameCancun,new City(cityNameCancun,"North America.Mexico",false,6586,0));
        cities.put(cityNameChicago,new City(cityNameChicago,"North America.Illinois",true,6317,6308,6339,6340,6341,6342,6344));
        cities.put(cityNameCleveland,new City(cityNameCleveland,"",true,6318,0));
        cities.put(cityNameDallas,new City(cityNameDallas,"North America.Texas",true,6423,6445,6452,6453,6454,6455,6457));
        cities.put(cityNameHawaii,new City(cityNameHawaii,"North America.Hawaii",true,6473,0));
        cities.put(cityNameLasVegas,new City(cityNameLasVegas,"North America.Arizona.Nevada",true,6319,6310,6351,6352,6353,6354,6356));
        cities.put(cityNameLondon,new City(cityNameLondon,"Europe.United Kingdom",false,6443,6314,6375,6376,6377,6378,6380));
        cities.put(cityNameLosAngles,new City(cityNameLosAngles,"North America.California",true,6320,6312,6363,6364,6365,6366,6368));
        cities.put(cityNameMiami,new City(cityNameMiami,"North America.Florida",true,6321,6307,6333,6334,6335,6336,6338));
        cities.put(cityNameMilwaukee,new City(cityNameMilwaukee,"North America.Wisconsin",true,6322,0));
        cities.put(cityNameMontreal, new City(cityNameMontreal,"North America.Quebec",true,6424,6429,6436,6438,6439,6440,6437));
        cities.put(cityNameNapa, new City(cityNameNapa,"North America.California",true,6483,0));
        cities.put(cityNameOrleans, new City(cityNameOrleans,"North America.Lousiana",true,6515,0));
        cities.put(cityNameNewYork, new City(cityNameNewYork,"North America.Connecticut.New York",true,6323,6306,6326,6327,6328,6329,6331));
        cities.put(cityNameOrlando, new City(cityNameOrlando,"North America.Florida",true,6477,6389,6395,6398,6405,6408,6418));
        cities.put(cityNameParis, new City(cityNameParis,"Europe.France",false,6476,6315,6381,6382,6383,6384,6386));
        cities.put(cityNamePhiladelphia, new City(cityNamePhiladelphia,"North America.Delaware",true,6425,0));
        cities.put(cityNameRome, new City(cityNameRome,"Europe.Italy",false,6475,6458,6460,6461,6462,6471,6472));
        cities.put(cityNameSan, new City(cityNameSan,"North America.California",true,6324,6313,6369,6370,6371,6372,6374));
        cities.put(cityNameSeattle, new City(cityNameSeattle,"North America.Washington",true,6426,6444,6446,6447,6448,6449,6451));
        cities.put(cityNameToronto, new City(cityNameToronto,"North America.Ontario",true,6427,6428,6430,6432,6433,6434,6431));
        cities.put(cityNameVancouver, new City(cityNameVancouver,"North America.British Columbia",true,6442,6459,6464,6468,6470,6465,6469));
        cities.put(cityNameWashington, new City(cityNameWashington,"North America.Virgina.District of Columbia",true,6325,6309,6345,6346,6347,6348,6350));

        //Region
        cities.put(cityNameAfrica, new City(cityNameAfrica,"Africa/Middle East",false,0,0));
        cities.put(cityNameAsia, new City(cityNameAsia,"Asia Pacific",false,6523,0));
        cities.put(cityNameCanada, new City(cityNameCanada,"North America.Quebec.Ontario.British Columbia",false,0,0));
        cities.put(cityNameCaribbean, new City(cityNameCaribbean,"Caribbean",false,0,0));
        cities.put(cityNameEurope, new City(cityNameEurope,"Europe.United Kingdom.France.Italy.all",false,0,0)); //- add value VALUE_GEOGRAPHIC_REGION_ALL = 'all'
        cities.put(cityNameLatin, new City(cityNameLatin,"Latin America",false,0,0));
    }

    public static void reset() {
        SELECTED_CITY = "";
    }

    /** check and save city name cache local*/
    public static boolean setSelectedCity(String cityName) {
        if (!TextUtils.isEmpty(cityName) && cities.get(cityName) != null) {
            SELECTED_CITY = cityName;
            return true;
        }//-- do nothing save select city
        return false;
    }

    /** check city name exist in array city */
    public static boolean isCityInApp(String cityName) {
        if (!TextUtils.isEmpty(cityName) && cities.get(cityName) != null) {
            return true;
        }
        return false;
    }

    public static int selectedCity() {
        int pos = 0;
        for (Map.Entry<String, City> entry : cities.entrySet()){
            if(SELECTED_CITY.equalsIgnoreCase(entry.getKey())){
                break;
            }
            pos++;
        }
        return pos;
    }

    public static boolean citySelected() {
        return (!TextUtils.isEmpty(SELECTED_CITY) && cities.get(SELECTED_CITY) != null);
    }

    public static int diningCode() {
        if(!citySelected()) return 0;
        return cities.get(SELECTED_CITY).diningCode;
    }

    public static int guideCode() {
        if(!citySelected()) return 0;
        return cities.get(SELECTED_CITY).cityGuideCode;
    }

    public static String cityName() {
        if(!citySelected()) return DEFAULT_CITY;
        return cities.get(SELECTED_CITY).name;
    }

    public static boolean isUSCity() {
        return citySelected() && cities.get(SELECTED_CITY).usCity;
    }

    public static String geographicRegion() {
        return cities.get(SELECTED_CITY).geographic;
    }

    public static CityGuide cityGuide(int code) {
        return cityGuide.get(code);
        /*if(result == null) {
            throw new IllegalStateException("passed city code not found");
        }*/
    }

    /**
     * @param cityGuideCategoryIndex either position of spa | bar | shopping...
     * @return int value of
     */
    public static int specificCityGuideCode(int cityGuideCategoryIndex) {
        if(!citySelected())
            return -1;
        int cityCode = cities.get(SELECTED_CITY).cityGuideCode;
        if(cityCode == 0) return -1;
        CityGuide cityGuide = cityGuide(cityCode);
        if(cityGuide == null || cityGuide.size() <= cityGuideCategoryIndex) return -1;
        return cityGuide(cityCode).at(cityGuideCategoryIndex);
    }

    public static boolean isDiningItem(int subCategoryID) {
        for (Map.Entry<String, City> entry : cities.entrySet()){
            City city = entry.getValue();
            if(city.diningCode == subCategoryID) return true;
        }
        return false;
    }

    public static boolean isCityGuideItem(int catID) {
        return catID > 0 && cityGuideCodeList.contains(catID);
    }
}
