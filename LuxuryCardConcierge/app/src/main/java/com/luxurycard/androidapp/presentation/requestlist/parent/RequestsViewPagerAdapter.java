package com.luxurycard.androidapp.presentation.requestlist.parent;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;

import com.luxurycard.androidapp.App;
import com.luxurycard.androidapp.R;
import com.luxurycard.androidapp.presentation.requestlist.child.RequestsBaseFragment;
import com.luxurycard.androidapp.presentation.requestlist.child.RequestsCloseFragment;
import com.luxurycard.androidapp.presentation.requestlist.child.RequestsOpenFragment;

import java.util.LinkedHashMap;

public class RequestsViewPagerAdapter extends FragmentStatePagerAdapter {
    private final LinkedHashMap<String, RequestsBaseFragment> mFragments = new LinkedHashMap<>();

    private final String TAB_OPEN = "open";
    private final String TAB_CLOSE = "close";

    RequestsViewPagerAdapter(FragmentManager fm) {
        super(fm);
        mFragments.put(TAB_OPEN, RequestsOpenFragment.newInstance());
        mFragments.put(TAB_CLOSE, RequestsCloseFragment.newInstance());
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public Fragment getItem(int position) {
        return position == 0 ? mFragments.get(TAB_OPEN) : mFragments.get(TAB_CLOSE);
    }

    @Override
    public int getCount() {
        return mFragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return position == 0 ? App.getInstance().getString(R.string.request_tab_title_open) : App.getInstance().getString(R.string.request_tab_title_close);
    }


    public RequestsOpenFragment getRequestOpen() {
        if (mFragments.get(TAB_OPEN) instanceof RequestsOpenFragment) {
            return (RequestsOpenFragment) mFragments.get(TAB_OPEN);
        }
        return null;
    }

    public RequestsCloseFragment getRequestClose() {
        if (mFragments.get(TAB_CLOSE) instanceof RequestsCloseFragment) {
            return (RequestsCloseFragment) mFragments.get(TAB_CLOSE);
        }
        return null;
    }
}
