package com.luxurycard.androidapp.presentation.venuedetail;

import android.os.Bundle;

import com.luxurycard.androidapp.App;
import com.luxurycard.androidapp.common.constant.AppConstant;
import com.luxurycard.androidapp.presentation.base.BaseNavSubFragment;
import com.luxurycard.androidapp.presentation.explore.ExploreConstant;

public class BaseDetailFragment extends BaseNavSubFragment implements ExploreConstant {

    protected void trackGA(Bundle savedInstanceState){
        if(savedInstanceState == null){
            // Track GA
            App.getInstance().track(AppConstant.GA_TRACKING_SCREEN_NAME.VENUE_DETAIL.getValue());
        }
    }

    @Override
    public void setTitle(String title) {
        super.setTitle(title);
        tvTitle.postDelayed(() -> {
            if(tvTitle.getLineCount() >= 2 && title.contains(CONCAT_TITLE)){
                String titleShowLogic = title.replace(CONCAT_TITLE, CONCAT_LINE_TITLE);
                tvTitle.setText(titleShowLogic);
            }//-- do nothing here
        },20);
    }
}
