package com.luxurycard.androidapp.datalayer.entity.b2ctile;

import com.google.gson.annotations.SerializedName;
import com.luxurycard.androidapp.datalayer.entity.GetTilesResult;

/**
 * Created by vinh.trinh on 6/6/2017.
 */

public class B2CTilesResponse {
    @SerializedName("GetTilesResult")
    private GetTilesResult result;

    public GetTilesResult getResult() {
        return result;
    }

    public void setResult(GetTilesResult result) {
        this.result = result;
    }
}
