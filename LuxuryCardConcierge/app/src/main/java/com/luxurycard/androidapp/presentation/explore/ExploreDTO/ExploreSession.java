package com.luxurycard.androidapp.presentation.explore.ExploreDTO;

import com.luxurycard.androidapp.presentation.explore.ExplorePresenter;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created on 4/12/18.
 */
public final class ExploreSession {

    //<editor-fold desc="Cache Data">
    // for other categories
    public String selectedCategory = ExplorePresenter.TYPE_CATEGORY_ALL;
    public String lastSelectedCity = "";
    // for city guide
    public int cityGuideIndex = -1;
    public String selectedSubCategory = "";
    public int selectedSubCategoryResId;
    // for search
    public String term;
    public boolean withOffers;

    //</editor-fold>

    public ExploreSession() {

    }

    public ExploreSession(ExploreSession session) {
        this.selectedCategory = session.selectedCategory;
        this.lastSelectedCity = session.lastSelectedCity;
        this.cityGuideIndex = session.cityGuideIndex;
        this.selectedSubCategory = session.selectedSubCategory;
        this.selectedSubCategoryResId = session.selectedSubCategoryResId;
        //don't save data search
        term = "";
        withOffers = false;
    }

    public ExploreSession(String jsonString) throws JSONException {
        JSONObject json = new JSONObject(jsonString);
        selectedCategory = json.getString("selectedCategory");
        //lastSelectedCity save in CityData => don't need handle lastCity
        cityGuideIndex = json.getInt("cityGuideIndex");
        selectedSubCategory = json.getString("selectedSubCategory");
        selectedSubCategoryResId = json.getInt("selectedSubCategoryResId");
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject json = new JSONObject();
        json.put("selectedCategory", selectedCategory);
        json.put("cityGuideIndex", cityGuideIndex);
        json.put("selectedSubCategory", selectedSubCategory);
        json.put("selectedSubCategoryResId", selectedSubCategoryResId);
        return json;
    }
}
