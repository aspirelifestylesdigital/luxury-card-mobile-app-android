package com.luxurycard.androidapp.presentation.home;

import com.luxurycard.androidapp.presentation.base.BasePresenter;

public interface HomeNavBottom {

    interface View {

        /*Tooltip view*/
        android.view.View getBottomMenuRequests();
    }

    interface Presenter extends BasePresenter<View> {

        void openCallConcierge();

        void handleTooltip();
    }
}
