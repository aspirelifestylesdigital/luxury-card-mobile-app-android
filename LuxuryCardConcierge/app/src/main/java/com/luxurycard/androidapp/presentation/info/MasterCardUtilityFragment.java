package com.luxurycard.androidapp.presentation.info;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.luxurycard.androidapp.App;
import com.luxurycard.androidapp.R;
import com.luxurycard.androidapp.common.constant.AppConstant;
import com.luxurycard.androidapp.common.constant.ErrCode;
import com.luxurycard.androidapp.common.constant.IntentConstant;
import com.luxurycard.androidapp.datalayer.entity.GetClientCopyResult;
import com.luxurycard.androidapp.datalayer.repository.B2CDataRepository;
import com.luxurycard.androidapp.domain.usecases.GetMasterCardCopy;
import com.luxurycard.androidapp.presentation.base.BaseNavFragment;
import com.luxurycard.androidapp.presentation.base.BaseNavSubFragment;
import com.luxurycard.androidapp.presentation.widget.DialogHelper;
import com.support.mylibrary.widget.JustifiedTextView;

import java.io.Serializable;

import butterknife.BindView;

/** special fragment show in MasterCardUtilityActivity and HomeNavActivity
 * warning careful when use func in parent fragment {@link BaseNavSubFragment}, {@link BaseNavFragment}
 * don't open dialog of func BaseNavFragment */
public class MasterCardUtilityFragment extends BaseNavSubFragment implements MasterCardUtility.View {

    @BindView(R.id.pbLoading)
    ProgressBar pbLoading;
    @BindView(R.id.content)
    JustifiedTextView tvContent;

    private DialogHelper mDialogHelper;
    MasterCardUtilityPresenter presenter;

    public MasterCardUtilityFragment() {
    }

    public static MasterCardUtilityFragment newInstance(AppConstant.MASTERCARD_COPY_UTILITY utilityType) {
        Bundle args = new Bundle();
        args.putSerializable(IntentConstant.MASTERCARD_COPY_UTILITY, utilityType);
        MasterCardUtilityFragment fragment = new MasterCardUtilityFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private MasterCardUtilityPresenter buildPresenter() {
        return new MasterCardUtilityPresenter(new GetMasterCardCopy(new B2CDataRepository()));
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = buildPresenter();
        presenter.attach(this);
    }

    @Override
    public int setupCreateViewLayoutId() {
        return R.layout.fragment_mastercard_utility;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvContent.setJustifyTextViewListener(defaultJustifyTextViewListener);
        tvContent.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
        mDialogHelper = new DialogHelper(getActivity());
        getButtonBack().setOnClickListener(v -> {
            this.onBackPressed();
        });

        // Get mastercard utility type
        if (getArguments() != null) {
            Serializable serializable = getArguments().getSerializable(IntentConstant.MASTERCARD_COPY_UTILITY);
            if (serializable instanceof AppConstant.MASTERCARD_COPY_UTILITY) {
                AppConstant.MASTERCARD_COPY_UTILITY type = (AppConstant.MASTERCARD_COPY_UTILITY) getArguments().getSerializable(IntentConstant.MASTERCARD_COPY_UTILITY);
                if (type != null) {

                    String titleVal = presenter.getTitleOfType(type);
                    setTitle(titleVal);

                    presenter.getMasterCardCopy(type.getValue());

                    if (savedInstanceState == null) {
                        // Track GA
                        trackGA(type);
                    }
                }
            }
        }
    }

    // Track GA
    private void trackGA(AppConstant.MASTERCARD_COPY_UTILITY type) {
        if (type == null) {
            return;
        }
        switch (type) {
            case TermsOfUse:
                App.getInstance().track(AppConstant.GA_TRACKING_SCREEN_NAME.TERMS_OF_USE.getValue());
                break;
            case Privacy:
                App.getInstance().track(AppConstant.GA_TRACKING_SCREEN_NAME.PRIVACY_POLICY.getValue());
                break;
            case About:
                App.getInstance().track(AppConstant.GA_TRACKING_SCREEN_NAME.ABOUT_THIS_APP.getValue());
                break;
            default:
                break;
        }
    }

    @Override
    public void onResume() {
        hideBottomMenuOnFragment();
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        showBottomMenuOnFragment();
        super.onStop();
    }

    @Override
    public void onDestroy() {
        presenter.detach();
        super.onDestroy();

    }

    public void renderContent(String content) {
        hideViewLoading();
//        content = "MAKING EACH EXPERIENCE A PRICELESS ONE, NO MATTER WHERE YOU ARE.<br/><br/>With the Mastercard Concierge app, we’ll help you book and arrange those memorable experiences that you won’t get anywhere else.  Whether you’re interested in travel planning, shopping services, dining reservations, or access to a once-in-a-lifetime event, Mastercard Concierge’s dedicated team will work diligently to fulfill your every request.<br/><br/>We look forward to making your experiences priceless, 24\\/7\\/365, everywhere and anywhere.  Just call us toll free at 1-877-288-6503 or email us at MastercardAppConcierge@vipdeskservice.com to book your experience now.  This is a free service for all Mastercard World or World Elite card holders.";
        if (!TextUtils.isEmpty(content)) {
//            tvContent.setText(Html.fromHtml(HtmlUtils.adjustSomeHtmlTag(content), new URLImageParser(tvContent, getContext(), true, null),
//                    new HtmlTagHandler(tvContent.getFont())));
            tvContent.setText(content);
        }
    }

    private void showViewLoading() {
        pbLoading.setVisibility(View.VISIBLE);
    }

    private void hideViewLoading() {
        pbLoading.setVisibility(View.GONE);
    }

    JustifiedTextView.IJustifyTextViewListener defaultJustifyTextViewListener = new JustifiedTextView.IJustifyTextViewListener() {
        @Override
        public void onContentScroll(boolean hitToBottom) {
        }

        @Override
        public void onHyperLinkClicked(String url) {
            if (getActivity() != null && mDialogHelper != null) {
                mDialogHelper.showLeavingAlert(url);
            }
        }
    };


    /* MasterCard View */

    @Override
    public void onGetMasterCardCopy(GetClientCopyResult result) {
        renderContent(result.getText());
    }

    @Override
    public void loadEmptyContent() {
        renderContent("");
    }

    @Override
    public void showProcessLoading() {
        showViewLoading();
    }

    @Override
    public void showErrorMessage(ErrCode errCode) {
        if (mDialogHelper == null)
            return;

        if (errCode == ErrCode.CONNECTIVITY_PROBLEM) {
            mDialogHelper.networkUnavailability(ErrCode.CONNECTIVITY_PROBLEM, null);
        } else {
            mDialogHelper.showGeneralError();
        }
    }
}
