package com.luxurycard.androidapp.presentation.requestlist;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by vinh.trinh on 7/18/2017.
 */

public class RequestListResponse {

    private final boolean isExpireAccessToken;

    public RequestListResponse(String rawJson){
        if(rawJson.contains("\"ENR61-1\"")){
            isExpireAccessToken = true;
        }else{
            isExpireAccessToken = false;
        }
    }

    boolean isExpireAccessToken() {
        return isExpireAccessToken;
    }
}
