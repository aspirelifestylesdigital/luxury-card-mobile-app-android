package com.luxurycard.androidapp.domain.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.luxurycard.androidapp.domain.model.explore.RequestDetailContent;
import com.luxurycard.androidapp.presentation.requestdetail.TextDetailBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vinh.trinh on 9/13/2017.
 */

public class RequestDetailData implements Parcelable{

    public  String EPCCaseID;
    public  String transactionID;
    public  String date;
    public  String requestType;
    public  String functionality;
    public  String eventDate;
    public  String pickupDate;
    public  String startDate;
    public  String endDate;
    public  String createDate;
    public  String prefResponse;
    public String city;
    public String state;
    public String country;
    public  String detail;
    public  int numberOfAdults;
    public  int numberOfChildren;
    public  String responseMode;
    public  boolean open;
    private JSONObject jsonDetail;
    public  String status;
    public String requestMode;
    public String requestStatus;
    public RequestDetailData(){

    }



    public RequestDetailData(String EPCCaseID, String transactionID, String requestType, String functionality, String date,
                             String eventDate, String pickupDate, String startDate, String endDate, String createDate,
                             Integer numberOfAdults, Integer numberOfChildren, String detail,
                             String responseMode, boolean open, String status, String prefResponse, String requestMode, String requestStatus) {
        this.EPCCaseID = EPCCaseID;
        this.transactionID = transactionID;
        this.requestType = requestType;
        this.functionality = functionality;
        this.date = date;
        this.eventDate = eventDate;
        this.pickupDate = pickupDate;
        this.startDate = startDate;
        this.endDate = endDate;
        this.createDate = createDate;
        this.numberOfAdults = numberOfAdults;
        this.numberOfChildren = numberOfChildren;
        this.detail = detail;
        this.responseMode = responseMode;
        this.open = open;
        this.status = status;
        this.prefResponse = prefResponse;
        this.requestMode = requestMode;
        this.requestStatus = requestStatus;
    }


    protected RequestDetailData(Parcel in) {
        EPCCaseID = in.readString();
        transactionID = in.readString();
        date = in.readString();
        requestType = in.readString();
        functionality = in.readString();
        eventDate = in.readString();
        pickupDate = in.readString();
        startDate = in.readString();
        endDate = in.readString();
        createDate = in.readString();
        prefResponse = in.readString();
        city = in.readString();
        state = in.readString();
        country = in.readString();
        detail = in.readString();
        numberOfAdults = in.readInt();
        numberOfChildren = in.readInt();
        responseMode = in.readString();
        open = in.readByte() != 0;
        status = in.readString();
        requestMode = in.readString();
        requestStatus = in.readString();
    }

    public static final Creator<RequestDetailData> CREATOR = new Creator<RequestDetailData>() {
        @Override
        public RequestDetailData createFromParcel(Parcel in) {
            return new RequestDetailData(in);
        }

        @Override
        public RequestDetailData[] newArray(int size) {
            return new RequestDetailData[size];
        }
    };

    public RequestDetailData setCity(String city){
        this.city = city;
        return this;
    }
    public RequestDetailData setState(String state){
        this.state = state;
        return this;
    }
    public RequestDetailData setCountry(String country){
        this.country = country;
        return this;
    }


    public JSONObject detail() {
        return jsonDetail;
    }

    public void buildJsonDetail() throws JSONException {
        jsonDetail = new JSONObject();
        String[] data = detail.split("\\|");
        for (String d : data) {
            String datum[] = d.split(":", 2);
            if (datum.length == 2) {
                jsonDetail.put(datum[0].toLowerCase().trim(), datum[1]);
            }
        }
    }


    public String getEPCCaseIDCCaseID() {
        if (this.EPCCaseID.equals("null")) {
            return "#";
        } else {
            return  "#" + this.EPCCaseID;
        }
    }




    public String getNameRequestType() {
        TextDetailBuilder textDetailBuilder = new TextDetailBuilder(this, this.detail(), false);
        return textDetailBuilder.nameMapping(this.requestType);
    }


    public String getNameRequestForEdit() {
        TextDetailBuilder textDetailBuilder = new TextDetailBuilder(this, this.detail(), false);
        return textDetailBuilder.getRequestName();
    }



    public String getContentEdit() {
        try {
            buildJsonDetail();
            TextDetailBuilder textDetailBuilder = new TextDetailBuilder(this, this.detail(), false);
            return textDetailBuilder.detailText();
        }  catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }


    public String getContentRequestItem()  {
        try {
            buildJsonDetail();
            return this.detail.replace("##", "\n");
//            TextDetailBuilder textDetailBuilder = new TextDetailBuilder(this, this.detail(), false);
//            return textDetailBuilder.getContentRequestList();
        }  catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }


    public List<RequestDetailContent> getDetailContent() {
        try {
            buildJsonDetail();
            TextDetailBuilder textDetailBuilder = new TextDetailBuilder(this, this.detail(), false);
            return textDetailBuilder.getContentRequestDetail();
//            return textDetailBuilder.getContentRequestList();
        }  catch (JSONException e) {
//            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    public String getStringDetailContent() {
        try {
            buildJsonDetail();
            TextDetailBuilder textDetailBuilder = new TextDetailBuilder(this, this.detail(), false);
            return textDetailBuilder.getDetailContent();
//            return textDetailBuilder.getContentRequestList();
        }  catch (JSONException e) {
//            e.printStackTrace();
            return "";
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(EPCCaseID);
        dest.writeString(transactionID);
        dest.writeString(date);
        dest.writeString(requestType);
        dest.writeString(functionality);
        dest.writeString(eventDate);
        dest.writeString(pickupDate);
        dest.writeString(startDate);
        dest.writeString(endDate);
        dest.writeString(createDate);
        dest.writeString(prefResponse);
        dest.writeString(city);
        dest.writeString(state);
        dest.writeString(country);
        dest.writeString(detail);
        dest.writeInt(numberOfAdults);
        dest.writeInt(numberOfChildren);
        dest.writeString(responseMode);
        dest.writeByte((byte) (open ? 1 : 0));
        dest.writeString(status);
        dest.writeString(requestMode);
        dest.writeString(requestStatus);
    }
}