package com.luxurycard.androidapp.presentation.venuedetail.mapview;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.luxurycard.androidapp.App;
import com.luxurycard.androidapp.R;
import com.luxurycard.androidapp.common.logic.HtmlUtils;
import com.luxurycard.androidapp.domain.model.explore.CityGuideDetailItem;
import com.luxurycard.androidapp.domain.model.explore.DiningDetailItem;
import com.luxurycard.androidapp.domain.model.explore.ExploreRView;
import com.luxurycard.androidapp.presentation.base.BaseNavSubFragment;
import com.luxurycard.androidapp.presentation.widget.DialogHelper;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import butterknife.BindView;

/**
 * Created by Den on 4/13/18.
 */

public class VenueDetailMapFragment extends BaseNavSubFragment {

    @Nullable
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.wvMap)
    WebView wvMap;
    CityGuideDetailItem cityGuideDetailItem;
    DiningDetailItem diningDetailItem;

    public static VenueDetailMapFragment newInstance(@Nullable CityGuideDetailItem cityGuideDetailItem, @Nullable DiningDetailItem diningDetailItem) {
        Bundle args = new Bundle();
        VenueDetailMapFragment fragment = new VenueDetailMapFragment();
        args.putParcelable("explore_city_guide_map", cityGuideDetailItem);
        args.putParcelable("explore_dining_map", diningDetailItem);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int setupCreateViewLayoutId() {
        return R.layout.activity_venue_detail_map;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getBtnCallConcierge().setVisibility(View.VISIBLE);
        getButtonBack().setOnClickListener(view1 -> this.onBackPressed());

        wvMap.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view,
                                      String url,
                                      Bitmap favicon) {
                showLoading();
                super.onPageStarted(view,
                        url,
                        favicon);
            }

            @Override
            public void onPageFinished(WebView view,
                                       String url) {
                hideLoading();
                super.onPageFinished(view,
                        url);
            }
        });
        wvMap.getSettings().setJavaScriptEnabled(true);
        String query = "";

        if(getArguments() == null)
            return;

        cityGuideDetailItem = getArguments().getParcelable("explore_city_guide_map");
        diningDetailItem = getArguments().getParcelable("explore_dining_map");

        if (cityGuideDetailItem != null) {
            setTitle((HtmlUtils.adjustSomeHtmlTag(cityGuideDetailItem.getTitle())));
            //<editor-fold desc="dummy code">
            //                setupWebView(cityGuideDetailItem.getTitle());
//                if (cityGuideDetailItem.city.contains(Beijing)||cityGuideDetailItem.city.contains(beijing)){
//                    try {
//                        query = URLEncoder.encode(cityGuideDetailItem.getQueryBeiJingSearchOnGoogleMap().replaceAll("&amp;", "&"), "utf-8");
//                    } catch (UnsupportedEncodingException e) {
//                        e.printStackTrace();
//                    }
//                    wvMap.loadUrl("http://maps.google.com/maps?" + "q=" + query);
//                    Log.v("VenueDetailMapActivity:", "Beijing:"+ query);
//                }else {
            //</editor-fold>
            try {
                query = URLEncoder.encode(cityGuideDetailItem.getQuerySearchOnGoogleMap().replaceAll("&amp;", "&"), "utf-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            wvMap.loadUrl("http://maps.google.com/maps?" + "q=" + query);
            Log.v("VenueDetailMapActivity:", query);
        }else if (diningDetailItem != null) {
            setTitle(Html.fromHtml(diningDetailItem.getTitle()).toString());
            //<editor-fold desc="dummy code">
            //                setupWebView(cityGuideDetailItem.getTitle());
//                if (cityGuideDetailItem.city.contains(Beijing)||cityGuideDetailItem.city.contains(beijing)){
//                    try {
//                        query = URLEncoder.encode(cityGuideDetailItem.getQuerySearchOnGoogleMap().replaceAll("&amp;", "&"), "utf-8");
//                    } catch (UnsupportedEncodingException e) {
//                        e.printStackTrace();
//                    }
//                    wvMap.loadUrl("http://maps.google.com/maps?" + "q=" + query);
//                    Log.v("VenueDetailMapActivity:", "Beijing:"+ query);
//                }else {
            //</editor-fold>
            try {
                query = URLEncoder.encode(diningDetailItem.getQuerySearchOnGoogleMap().replaceAll("&amp;", "&"), "utf-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            wvMap.loadUrl("http://maps.google.com/maps?" + "q=" + query);
            Log.v("VenueDetailMapActivity:", "http://maps.google.com/maps?" + "q=" + query);
        }

        tvTitle.post(new Runnable() {
            @Override
            public void run() {
                int lineCount = tvTitle.getLineCount();
                if (lineCount > 1) {
                    tvTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, App.getInstance().getResources().getDimension(R.dimen.font_size_xxlarge));
                }
            }
        });

    }

}
