package com.luxurycard.androidapp.domain.mapper.profile;

import com.api.aspire.common.utils.CommonUtils;
import com.api.aspire.data.entity.profile.CreateProfileAspireRequest;
import com.api.aspire.data.entity.profile.ProfileAspireResponse;
import com.api.aspire.domain.model.ProfileAspire;
import com.api.aspire.domain.usecases.CreateProfileCase;
import com.luxurycard.androidapp.BuildConfig;
import com.luxurycard.androidapp.domain.model.Profile;

public class MapProfile {

    public MapProfile() {
    }

    public CreateProfileCase.Params createProfile(Profile profile, String password, String passCode) {
        ProfileAspire profileAspire = new ProfileAspire();
        //set password in profile
        profileAspire.setSecretKey(password);

        //- map profile
        profileAspire.setFirstName(profile.getFirstName());
        profileAspire.setLastName(profile.getLastName());
        profileAspire.setEmail(profile.getEmail());
        profileAspire.setPhone(profile.getPhone());
        profileAspire.setSalutation(profile.getSalutation());
        profileAspire.locationToggle(profile.isLocationOn());
//        profileAspire.setCountryCode(profile.getCountryCode());
        //- special
        profileAspire.setPassCodeRemote(passCode);

        ProfileAspireResponse profileAspireResponse = MapDataProfile.mapRequest(profileAspire);

        //-- set bin verifyMetadata
        int binNumber = CommonUtils.convertStringToInt(passCode);

        CreateProfileAspireRequest createProfileRequest = new CreateProfileAspireRequest(profile.getEmail(),
                password,
                profileAspireResponse,
                binNumber);

        return new CreateProfileCase.Params(profileAspire,
                createProfileRequest,
                BuildConfig.AS_TOKEN_USERNAME,
                BuildConfig.AS_TOKEN_SECRET);
    }


    public Profile getProfile(ProfileAspire profileAspire) {
        Profile profile = new Profile();

        //- map profile
        profile.setFirstName(profileAspire.getFirstName());
        profile.setLastName(profileAspire.getLastName());
        profile.setEmail(profileAspire.getEmail());
        profile.setPhone(profileAspire.getPhone());
        profile.setSalutation(profileAspire.getSalutation());
        profile.locationToggle(profileAspire.isLocationOn());
//        profile.setCountryCode(profileAspire.getCountryCode());
        //- special
//        profile.setPassCode(profileAspire.getPassCodeLocal());

        return profile;
    }

    public ProfileAspire updateProfile(ProfileAspire profileAspire, Profile profileUpdated) {

        //- map profile
        profileAspire.setFirstName(profileUpdated.getFirstName());
        profileAspire.setLastName(profileUpdated.getLastName());
        profileAspire.setEmail(profileUpdated.getEmail());
        profileAspire.setPhone(profileUpdated.getPhone());
        profileAspire.setSalutation(profileUpdated.getSalutation());
        profileAspire.locationToggle(profileUpdated.isLocationOn());
//        profileAspire.setCountryCode(profileUpdated.getCountryCode());
        //- special
//        profileAspire.setPassCodeLocal(profileUpdated.getPassCode());
        return profileAspire;
    }
}
