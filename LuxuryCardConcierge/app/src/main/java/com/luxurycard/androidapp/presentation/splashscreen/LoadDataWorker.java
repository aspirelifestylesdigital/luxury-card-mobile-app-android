package com.luxurycard.androidapp.presentation.splashscreen;

import com.luxurycard.androidapp.datalayer.preference.PreferencesStorage;
import com.luxurycard.androidapp.datalayer.repository.PreferencesDataRepository;
import com.luxurycard.androidapp.domain.repository.UserPreferencesRepository;
import com.luxurycard.androidapp.domain.usecases.GetAccessToken;
import com.luxurycard.androidapp.domain.usecases.GetUserPreferences;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by vinh.trinh on 8/31/2017.
 */

class LoadDataWorker {

    void load(PreferencesStorage ps) {
        GetAccessToken getAccessToken = new GetAccessToken(ps);
        UserPreferencesRepository upRepository = new PreferencesDataRepository(ps);
        GetUserPreferences getUserPreferences = new GetUserPreferences(upRepository);
        getAccessToken.execute()
                .flatMap(getUserPreferences::buildUseCaseSingle)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe();
    }

}
