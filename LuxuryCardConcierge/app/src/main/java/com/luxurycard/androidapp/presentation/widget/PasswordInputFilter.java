package com.luxurycard.androidapp.presentation.widget;

import android.text.InputFilter;
import android.text.Spanned;

/**
 * Created by vinh.trinh on 10/2/2017.
 */

public class PasswordInputFilter implements InputFilter {
    @Override
    public CharSequence filter(CharSequence source, int start, int end,
                               Spanned dest, int dstart, int dend) {
        for (int i = start; i < end; i++) {
            if (source.charAt(i) == ' ') {
                return "";
            }
        }
        return null;
    }
}
