package com.luxurycard.androidapp.datalayer.preference;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.text.TextUtils;

import com.luxurycard.androidapp.common.constant.SharedPrefConstant;
import com.luxurycard.androidapp.datalayer.datasource.PreferenceData;
import com.luxurycard.androidapp.domain.model.Metadata;
import com.luxurycard.androidapp.domain.model.Profile;
import com.luxurycard.androidapp.presentation.explore.ExploreDTO.ExploreSession;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by vinh.trinh on 4/26/2017.
 */

public class PreferencesStorage {

    private Context context;
    private String jsonMetadata;
    private String jsonProfile;
    private String jsonPreferences;
    private String selectedCity;
    private Boolean hasForgotPwd;
    private Boolean firstTimeCheckPermission;
    private String jsonExploreSession;
    private Set<String> tooltipCreated;

    public PreferencesStorage(Context context) {
        this.context = context;
    }

    private PreferencesStorage(Context context, String metadata, String profile, String preferences,
                               String selectedCity, Boolean hasForgotPwd , Boolean firstTimeCheckPermission,
                               String jsonExploreSession,
                               Set<String> tooltipCreated) {
        this.context = context;
        this.jsonMetadata = metadata;
        this.jsonProfile = profile;
        this.jsonPreferences = preferences;
        this.selectedCity = selectedCity;
        this.hasForgotPwd = hasForgotPwd;
        this.firstTimeCheckPermission = firstTimeCheckPermission;
        this.jsonExploreSession = jsonExploreSession;
        this.tooltipCreated = tooltipCreated;
    }

    public boolean profileCreated() {
        SharedPreferences preferences = getInstance();
        return preferences.contains(SharedPrefConstant.PROFILE);
    }

    public boolean preferencesCreated() {
        SharedPreferences preferences = getInstance();
        return preferences.contains(SharedPrefConstant.PREFERENCES);
    }

    public Metadata metadata() throws JSONException {
        SharedPreferences preferences = getInstance();
        String jsonStringMetadata = preferences.getString(SharedPrefConstant.METADATA, "");
        return (TextUtils.isEmpty(jsonStringMetadata)) ? null : new Metadata(jsonStringMetadata);
    }

    public Profile profile() throws JSONException {
        SharedPreferences preferences = getInstance();
        return new Profile(
                preferences.getString(SharedPrefConstant.PROFILE, "")
        );
    }

    public PreferenceData userPreferences() throws JSONException {
        SharedPreferences preferences = getInstance();
        return new PreferenceData(
                preferences.getString(SharedPrefConstant.PREFERENCES, "")
        );
    }

    public String selectedCity() {
        SharedPreferences preferences = getInstance();
        return preferences.getString(SharedPrefConstant.SELECTED_CITY, "");
    }

    public boolean hasForgotPwd() {
        SharedPreferences preferences = getInstance();
        return preferences.getBoolean(SharedPrefConstant.FORGOT_SECRET_KEY, false);
    }

    public boolean hasCheckPermission() {
        SharedPreferences preferences = getInstance();
        return preferences.getBoolean(SharedPrefConstant.FIRST_TIME_CHECK_PERMISSION, true);
    }

    public ExploreSession exploreSession() throws JSONException {
        SharedPreferences preferences = getInstance();
        String jsonStringExploreSession = preferences.getString(SharedPrefConstant.EXPLORE_SESSION, "");
        return TextUtils.isEmpty(jsonStringExploreSession) ? null : new ExploreSession(jsonStringExploreSession);
    }

    public ArrayList<String> tooltipCreated(){
        SharedPreferences preferences = getInstance();
        Set<String> strings = preferences.getStringSet(SharedPrefConstant.TOOLTIP_CREATED, null);
        return strings == null ? new ArrayList<>() : new ArrayList<>(strings);
    }


    public void save() {
        if(Looper.myLooper() == Looper.getMainLooper()) {
            throw new IllegalStateException("operation shouldn't have executed on main thread");
        }
        SharedPreferences preferences = getInstance();
        SharedPreferences.Editor editor = preferences.edit();
        if(jsonMetadata != null) {
            editor.putString(SharedPrefConstant.METADATA, jsonMetadata);
        }
        if(jsonProfile != null) {
            editor.putString(SharedPrefConstant.PROFILE, jsonProfile);
        }
        if(!TextUtils.isEmpty(selectedCity)) {
            editor.putString(SharedPrefConstant.SELECTED_CITY, selectedCity);
        }
        if(jsonPreferences != null) {
            editor.putString(SharedPrefConstant.PREFERENCES, jsonPreferences);
        }
        if(hasForgotPwd != null) {
            editor.putBoolean(SharedPrefConstant.FORGOT_SECRET_KEY, hasForgotPwd);
        }
        if(firstTimeCheckPermission != null) {
            editor.putBoolean(SharedPrefConstant.FIRST_TIME_CHECK_PERMISSION, firstTimeCheckPermission);
        }
        if(jsonExploreSession != null) {
            editor.putString(SharedPrefConstant.EXPLORE_SESSION, jsonExploreSession);
        }
        if(tooltipCreated != null){
            editor.putStringSet(SharedPrefConstant.TOOLTIP_CREATED, tooltipCreated);
        }
        editor.commit();
    }

    public void saveAsync() {
        SharedPreferences preferences = getInstance();
        SharedPreferences.Editor editor = preferences.edit();
        if(jsonMetadata != null) {
            editor.putString(SharedPrefConstant.METADATA, jsonMetadata);
        }
        if(jsonProfile != null) {
            editor.putString(SharedPrefConstant.PROFILE, jsonProfile);
        }
        if(!TextUtils.isEmpty(selectedCity)) {
            editor.putString(SharedPrefConstant.SELECTED_CITY, selectedCity);
        }
        if(jsonPreferences != null) {
            editor.putString(SharedPrefConstant.PREFERENCES, jsonPreferences);
        }
        if(hasForgotPwd != null) {
            editor.putBoolean(SharedPrefConstant.FORGOT_SECRET_KEY, hasForgotPwd);
        }
        if (firstTimeCheckPermission != null){
            editor.putBoolean(SharedPrefConstant.FIRST_TIME_CHECK_PERMISSION, firstTimeCheckPermission);
        }
        if(jsonExploreSession != null) {
            editor.putString(SharedPrefConstant.EXPLORE_SESSION, jsonExploreSession);
        }
        if(tooltipCreated != null){
            editor.putStringSet(SharedPrefConstant.TOOLTIP_CREATED, tooltipCreated);
        }
        editor.apply();
    }

    public void clear() {
        SharedPreferences preferences = getInstance();
        preferences.edit().remove(SharedPrefConstant.METADATA).apply();
        preferences.edit().remove(SharedPrefConstant.PROFILE).apply();
        preferences.edit().remove(SharedPrefConstant.SELECTED_CITY).apply();
        preferences.edit().remove(SharedPrefConstant.PREFERENCES).apply();
        preferences.edit().remove(SharedPrefConstant.FORGOT_SECRET_KEY).apply();
        preferences.edit().remove(SharedPrefConstant.EXPLORE_SESSION).apply();
    }

    private SharedPreferences  getInstance() {
        return PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
    }

    public Editor editor() {
        return new Editor(context);
    }

    public static class Editor {
        private Context context;
        private String jsonMetadata;
        private String jsonProfile;
        private String jsonPreferences;
        private Boolean hasForgotPwd;
        private String selectedCity = "";
        private Boolean firstTimeCheckPermission;
        private String jsonExploreSession;
        private Set<String> tooltipCreated;

        public Editor (Context context) {
            this.context = context;
        }

        public Editor profile(Profile val) throws JSONException {
            this.jsonProfile = val.toJSON().toString();
            return this;
        }

        public Editor preferences(PreferenceData val) throws JSONException {
            this.jsonPreferences = val.toJSON().toString();
            return this;
        }

        public Editor metadata(Metadata val) throws JSONException {
            this.jsonMetadata = val.toJSON().toString();
            return this;
        }

        public Editor selectedCity(String val) {
            this.selectedCity = val;
            return this;
        }

        public Editor hasForgotPwd(boolean val) {
            hasForgotPwd = val;
            return this;
        }
        public Editor hasCheckPermission(boolean val) {
            firstTimeCheckPermission = val;
            return this;
        }

        public Editor exploreSession(ExploreSession session) throws JSONException {
            this.jsonExploreSession = session.toJSON().toString();
            return this;
        }

        public Editor tooltipCreated(ArrayList<String> tooltipCreated){
            this.tooltipCreated = new LinkedHashSet<>(tooltipCreated);
            return this;
        }

        public PreferencesStorage build() {
            return new PreferencesStorage(context, jsonMetadata, jsonProfile, jsonPreferences,
                    selectedCity, hasForgotPwd , firstTimeCheckPermission, jsonExploreSession,
                    tooltipCreated);
        }
    }
}
