package com.luxurycard.androidapp.presentation.changepass;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatButton;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.luxurycard.androidapp.App;
import com.luxurycard.androidapp.R;
import com.luxurycard.androidapp.common.constant.AppConstant;
import com.luxurycard.androidapp.common.constant.ErrCode;
import com.luxurycard.androidapp.common.logic.Validator;
import com.luxurycard.androidapp.datalayer.preference.PreferencesStorage;
import com.luxurycard.androidapp.datalayer.repository.ProfileDataRepository;
import com.luxurycard.androidapp.domain.usecases.LoadProfile;
import com.luxurycard.androidapp.domain.usecases.ResetPassword;
import com.luxurycard.androidapp.presentation.base.CommonActivity;
import com.luxurycard.androidapp.presentation.home.HomeNavBottomActivity;
import com.luxurycard.androidapp.presentation.widget.DialogHelper;
import com.luxurycard.androidapp.presentation.widget.PasswordInputFilter;
import com.luxurycard.androidapp.presentation.widget.ViewUtils;
import com.support.mylibrary.widget.ErrorIndicatorEditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChangePasswordActivity extends CommonActivity implements ChangePassword.View {

    @BindView(R.id.edt_old_pwd)
    ErrorIndicatorEditText edtOldSecret;
    @BindView(R.id.edt_new_pwd)
    ErrorIndicatorEditText edtNewSecret;
//    @BindView(R.id.edt_retype_pwd)
//    ErrorIndicatorEditText edtRetypeSecret;
    @BindView(R.id.btn_submit)
    AppCompatButton btnSubmit;
    @BindView(R.id.btn_cancel)
    AppCompatButton btnCancel;
    @BindView(R.id.content_wrapper)
    LinearLayout contentWrapper;
    @BindView(R.id.scroll_view)
    ScrollView scroll;
    @BindView(R.id.contentSubmit)
    LinearLayout contentSubmit;
    DialogHelper dialogHelper;
    ChangePasswordPresenter presenter;
    Validator validator;
    String errMessage;
    private boolean hasForgotSecret;

    private ChangePasswordPresenter presenter() {
        PreferencesStorage preferencesStorage = new PreferencesStorage(getApplicationContext());
        final ProfileDataRepository profileDataRepository = new ProfileDataRepository(preferencesStorage);
        LoadProfile loadProfile = new LoadProfile(profileDataRepository);
        ResetPassword resetPassword = new ResetPassword(preferencesStorage);
        return new ChangePasswordPresenter(resetPassword, loadProfile);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        ButterKnife.bind(this);
        setTitle(R.string.title_change_password);
        hasForgotSecret = !TextUtils.isEmpty(getIntent().getStringExtra(Intent.EXTRA_REFERRER));
        if (hasForgotSecret) {
            back.setVisibility(View.GONE);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        }
        disableSwipe();
        dialogHelper = new DialogHelper(this);
        presenter = presenter();
        presenter.attach(this);
        presenter.loadProfile();
        validator = new Validator();

//        edtRetypeSecret.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(final CharSequence charSequence,
//                                          final int i,
//                                          final int i1,
//                                          final int i2) {}
//
//            @Override
//            public void onTextChanged(final CharSequence charSequence,
//                                      final int i,
//                                      final int i1,
//                                      final int i2) {}
//
//            @Override
//            public void afterTextChanged(final Editable editable) {
//                enableButton();
//            }
//        });
        edtNewSecret.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(final CharSequence charSequence,
                                          final int i,
                                          final int i1,
                                          final int i2) {}

            @Override
            public void onTextChanged(final CharSequence charSequence,
                                      final int i,
                                      final int i1,
                                      final int i2) {}

            @Override
            public void afterTextChanged(final Editable editable) {
                enableButton();
            }
        });
        edtOldSecret.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(final CharSequence charSequence,
                                          final int i,
                                          final int i1,
                                          final int i2) {}

            @Override
            public void onTextChanged(final CharSequence charSequence,
                                      final int i,
                                      final int i1,
                                      final int i2) {}

            @Override
            public void afterTextChanged(final Editable editable) {
                enableButton();
            }
        });
        InputFilter[] filtersPassword = new InputFilter[]{new PasswordInputFilter(), new InputFilter.LengthFilter(getResources().getInteger(R.integer.password_max_length_characters))};
        edtNewSecret.setFilters(filtersPassword);
        edtOldSecret.setFilters(filtersPassword);
//        edtRetypeSecret.setFilters(filtersPassword);

        if(savedInstanceState == null){
            // Track GA
            App.getInstance().track(AppConstant.GA_TRACKING_SCREEN_NAME.CHANGE_PASSWORD.getValue());
        }
    }

    @Override
    protected void onDestroy() {
        presenter.detach();
        super.onDestroy();
    }

    @OnClick(R.id.btn_submit)
    public void submitClick(View view) {
        ViewUtils.hideSoftKey(view);
        if (validateFields(edtOldSecret.getText().toString(), edtNewSecret.getText().toString()/*, edtRetypeSecret.getText().toString() */)) {
            edtOldSecret.setError(null);
            edtNewSecret.setError(null);
//            edtRetypeSecret.setError(null);
            presenter.doChangePassword(edtOldSecret.getText().toString(), edtNewSecret.getText().toString());
        } else {
            dialogHelper.profileDialog(errMessage, dialog -> {
                if (view != null) {
                    view.postDelayed(() -> ViewUtils.invoSoftKey(getCurrentFocus()), 100);
                }
            });
        }
    }

    @OnClick(R.id.btn_cancel)
    public void cancelClick(View view) {
//        ViewUtils.hideSoftKey(view);
        resetView();
    }


    private void enableButton() {
        if (!TextUtils.isEmpty(edtOldSecret.getText().toString()) || /*!TextUtils.isEmpty(edtRetypeSecret.getText().toString()) ||*/ !TextUtils.isEmpty(edtNewSecret.getText().toString())) {
            btnCancel.setEnabled(true);
            btnSubmit.setEnabled(true);
            btnSubmit.setClickable(true);
            btnSubmit.setClickable(true);
        } else {
            btnCancel.setEnabled(false);
            btnSubmit.setEnabled(false);
        }
    }


    @OnClick({R.id.profile_rootview, R.id.content_wrapper, R.id.contentSubmit, R.id.toolbar})
    public void onClick(View view) {
        ViewUtils.hideSoftKey(view);
    }

    private void resetView() {
        edtOldSecret.setText("");
        edtNewSecret.setText("");
//        edtRetypeSecret.setText("");
        edtOldSecret.setError(null);
        edtNewSecret.setError(null);
//        edtRetypeSecret.setError(null);
    }

    private void proceedToHome() {
        Intent intent = new Intent(this, HomeNavBottomActivity.class);
        startActivity(intent);
    }

    // presenter
    @Override
    public void showErrorDialog(ErrCode errCode, String extraMsg) {
        if (dialogHelper.networkUnavailability(errCode, extraMsg)) return;
        if ("Login unsuccessfull!".equals(extraMsg)) {
            dialogHelper.alert(getString(R.string.input_err_fields), getString(R.string.input_err_old_pwd_failed));
        } else if (errCode == ErrCode.API_ERROR) {
            dialogHelper.alert(getString(R.string.dialogTitleError), extraMsg);
        } else {
            dialogHelper.showGeneralError();
        }
    }

    // presenter
    @Override
    public void showProgressDialog() {
        dialogHelper.showProgress();
    }

    // presenter
    @Override
    public void dismissProgressDialog() {
        dialogHelper.dismissProgress();
    }

    @Override
    public void showSuccessMessage() {
        dialogHelper.alert(null, getString(R.string.change_password_message), dialogInterface -> {
            if (hasForgotSecret) proceedToHome();
            finish();
        });
    }

    private boolean validateFields(String oldPassword, String newPassword) {
        boolean valid = true;
        StringBuilder stringBuilder = new StringBuilder();
        if (TextUtils.isEmpty(oldPassword.trim()) || TextUtils.isEmpty(newPassword.trim()) /*|| TextUtils.isEmpty(retypePassword.trim())*/) {
            if (TextUtils.isEmpty(oldPassword)) edtOldSecret.setError("");
            else edtOldSecret.setError(null);
            if (TextUtils.isEmpty(newPassword)) edtNewSecret.setError("");
            else edtNewSecret.setError(null);
//            if (TextUtils.isEmpty(retypePassword)) edtRetypeSecret.setError("");
//            else edtRetypeSecret.setError(null);
            stringBuilder.append(getString(R.string.input_err_required));
            valid = false;
        }
        if (TextUtils.isEmpty(oldPassword)) {
            edtOldSecret.setError("");
            if (stringBuilder.length() > 0) stringBuilder.append("\n");
            stringBuilder.append(getString(R.string.input_err_old_pwd_failed));
            valid = false;
        }
        if (!TextUtils.isEmpty(newPassword) && !validator.secretValidator(newPassword)) {
            edtNewSecret.setError("");
            if (stringBuilder.length() > 0) stringBuilder.append("\n");
            stringBuilder.append(getString(R.string.input_err_invalid_password));
            valid = false;
        } /*else {
            if (!TextUtils.isEmpty(newPassword) && !TextUtils.isEmpty(retypePassword) && !validator.password(newPassword, retypePassword)) {
                edtRetypeSecret.setError("");
                if (stringBuilder.length() > 0) stringBuilder.append("\n");
                stringBuilder.append(getString(R.string.input_err_pwd_confirmation_failed));
                valid = false;
            }
        }*/
        errMessage = stringBuilder.toString();
        return valid;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            super.onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (!hasForgotSecret) super.onBackPressed();
    }
}
