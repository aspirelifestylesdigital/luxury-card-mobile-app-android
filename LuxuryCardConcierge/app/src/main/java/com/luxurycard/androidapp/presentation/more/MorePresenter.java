package com.luxurycard.androidapp.presentation.more;

import android.content.Intent;
import android.support.v4.app.FragmentActivity;

import com.luxurycard.androidapp.R;
import com.luxurycard.androidapp.common.constant.AppConstant;
import com.luxurycard.androidapp.common.constant.CityData;
import com.luxurycard.androidapp.common.constant.IntentConstant;
import com.luxurycard.androidapp.common.logic.TimeFormats;
import com.luxurycard.androidapp.datalayer.preference.PreferencesStorage;
import com.luxurycard.androidapp.presentation.home.HomeNavBottomActivity;
import com.luxurycard.androidapp.presentation.info.MasterCardUtilityActivity;
import com.luxurycard.androidapp.presentation.welcome.WelcomeActivity;

public class MorePresenter implements More.Presenter{

    private More.View view;

    MorePresenter() {

    }

    @Override
    public void attach(More.View view) {
        this.view = view;
    }

    @Override
    public void detach() {
        this.view = null;
    }

    @Override
    public void setUpDate() {
        String dateCurrent = "";
        try {
            dateCurrent = TimeFormats.timeCurrentOfUser();
        } catch (Exception e) {
            e.printStackTrace();
        }

        view.showDate(dateCurrent);
    }

    void toSignOut(HomeNavBottomActivity activity) {
        if (activity != null) {
            PreferencesStorage preferencesStorage = new PreferencesStorage(activity.getApplicationContext());
            preferencesStorage.clear();
            CityData.reset();
            Intent intent = new Intent(activity, WelcomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            activity.startActivity(intent);
            activity.overridePendingTransition(R.anim.fadein, R.anim.fadeout);
            activity.finish();
        }
    }
}
