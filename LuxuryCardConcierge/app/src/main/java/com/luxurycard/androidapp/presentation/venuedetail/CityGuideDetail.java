package com.luxurycard.androidapp.presentation.venuedetail;

import android.view.View;

import com.luxurycard.androidapp.domain.model.explore.CityGuideDetailItem;
import com.luxurycard.androidapp.presentation.base.BasePresenter;

/**
 * Created by ThuNguyen on 6/13/2017.
 */

public interface CityGuideDetail {

    interface View {
        void onGetCityGuideDetailFinished(CityGuideDetailItem cityGuideDetailItem);
        void onUpdateFailed();

        android.view.View getButtonBook();
    }

    interface Presenter extends BasePresenter<View> {
        void getCityGuideDetail(Integer categoryId, Integer itemId);
        void handleTooltip();
    }

}
