package com.luxurycard.androidapp.presentation.widget.tooltip.data;


import android.util.ArrayMap;

import com.luxurycard.androidapp.datalayer.preference.PreferencesStorage;

import java.util.ArrayList;

/**
 * Created on 4/15/18.
 */
public final class TooltipData implements TooltipConstant {

    private static ArrayMap<String, String> mTooltipList;

    static {

        mTooltipList = new ArrayMap<>();
        mTooltipList.put(BOTTOM_MENU_REQUESTS, "Manage your requests here");
        mTooltipList.put(EXPLORE_BUTTON_CATEGORY, "Browse by category");
        mTooltipList.put(EXPLORE_DETAIL_BUTTON_BOOK, "Want to book this?");
        mTooltipList.put(REQUESTS_BUTTON_NEW_REQUEST, "Tap here to create a new request");

    }


    /** @param keyConstant: value {@link TooltipConstant} */
    public static String getMessage(String keyConstant){
        return mTooltipList.containsKey(keyConstant) ? mTooltipList.get(keyConstant) : "";
    }

    /**
     * save tooltip view if don'f create before that
     * @param tooltipConstant: value {@link TooltipConstant} */
    public static boolean isShouldShow(PreferencesStorage storage, final String tooltipConstant) {
        ArrayList<String> tooltipList = storage.tooltipCreated();
        if (tooltipList.contains(tooltipConstant)) {

            return false;
        } else {

            ArrayList<String> initList = new ArrayList<>(tooltipList);
            initList.add(tooltipConstant);
            storage.editor().tooltipCreated(initList).build().saveAsync();
            return true;
        }
    }

}
