package com.luxurycard.androidapp.presentation.preferences;


import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.luxurycard.androidapp.App;
import com.luxurycard.androidapp.R;
import com.luxurycard.androidapp.common.constant.AppConstant;
import com.luxurycard.androidapp.common.constant.ErrCode;
import com.luxurycard.androidapp.common.constant.ResultCode;
import com.luxurycard.androidapp.datalayer.datasource.PreferenceData;
import com.luxurycard.androidapp.datalayer.preference.PreferencesStorage;
import com.luxurycard.androidapp.datalayer.repository.PreferencesDataRepository;
import com.luxurycard.androidapp.domain.repository.UserPreferencesRepository;
import com.luxurycard.androidapp.domain.usecases.GetAccessToken;
import com.luxurycard.androidapp.domain.usecases.GetUserPreferences;
import com.luxurycard.androidapp.domain.usecases.UpdateUserPreferences;
import com.luxurycard.androidapp.presentation.base.CommonActivity;
import com.luxurycard.androidapp.presentation.widget.DialogHelper;

public class UserPreferencesActivity extends CommonActivity implements UserPreferences.View,
        UserPreferencesFragment.PreferencesListener {

    private DialogHelper dialogHelper;
    private UserPreferencesFragment preferencesFragment;
    private UserPreferencesPresenter presenter;
    PreferenceData original;

    private UserPreferencesPresenter presenter() {
        PreferencesStorage preferencesStorage = new PreferencesStorage(getApplicationContext());
        UserPreferencesRepository repository = new PreferencesDataRepository(preferencesStorage);
        GetUserPreferences getUserPreferences = new GetUserPreferences(repository);
        UpdateUserPreferences updateUserPreferences = new UpdateUserPreferences(repository);
        GetAccessToken getAccessToken = new GetAccessToken(preferencesStorage);
        return new UserPreferencesPresenter(getUserPreferences, updateUserPreferences, getAccessToken);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acticity_dummy_content);
        setTitle(R.string.title_my_preferences);
        dialogHelper = new DialogHelper(this);
        presenter = presenter();
        presenter.attach(this);
        disableSwipe();
        if(savedInstanceState == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragment_place_holder
                            , UserPreferencesFragment.newInstance()
                            , UserPreferencesFragment.class.getSimpleName())
                    .commit();
            // Track GA
            App.getInstance().track(AppConstant.GA_TRACKING_SCREEN_NAME.PREFERENCES.getValue());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void loadPreferencesOnUI(PreferenceData data) {
        original = data;
        preferencesFragment.load(data);
    }

    @Override
    public void showPreferenceSavedDialog() {
        dialogHelper.alert(null, getString(R.string.preference_updated_message));
        presenter.loadPreferences();
    }

    @Override
    public void showErrorDialog(ErrCode errCode, String extraMsg) {
        if(dialogHelper.networkUnavailability(errCode, extraMsg)) return;
        if(errCode == ErrCode.API_ERROR) dialogHelper.alert("ERROR!", extraMsg);
        else dialogHelper.showGeneralError();
    }

    @Override
    public void hideLoading() {
        dialogHelper.dismissProgress();
    }

    @Override
    public void showLoading() {
        dialogHelper.showProgress();
    }

    @Override
    public void onFragmentCreated() {
        preferencesFragment = (UserPreferencesFragment) getSupportFragmentManager()
                .findFragmentByTag(UserPreferencesFragment.class.getSimpleName());
        presenter.loadPreferences();
    }

    @Override
    public void onPreferencesSubmitted(String cuisine, String hotel, String transportation) {
        setResult(ResultCode.RESULT_OK);
        presenter.savePreferences(original.applyChanges(cuisine, hotel, transportation));
    }

    @Override
    public void onPreferencesCancel() {
        presenter.loadPreferences();
    }
}
