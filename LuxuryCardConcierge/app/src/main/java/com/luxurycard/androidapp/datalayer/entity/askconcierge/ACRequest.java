package com.luxurycard.androidapp.datalayer.entity.askconcierge;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.luxurycard.androidapp.BuildConfig;
import com.luxurycard.androidapp.domain.model.Profile;

/**
 * Created by vinh.trinh on 5/16/2017.
 */

public class ACRequest {

    @SerializedName("AccessToken")
    String accessToken;
    @SerializedName("TransactionId")
    String transactionID;
    @SerializedName("ConsumerKey")
    String consumerKey;
    @SerializedName("EPCClientCode")
    String EPCClientCode;
    @SerializedName("EPCProgramCode")
    String EPCProgramCode;
    @SerializedName("Source")
    String source;
    @SerializedName("EditType")
    String editType;
    @SerializedName("RequestType")
    String requestType;
    @SerializedName("Functionality")
    String functionality;
    @SerializedName("OnlineMemberId")
    String onlineMemberId;
    @SerializedName("VeriCode")
    String verificationCode;
    @SerializedName("Salutation")
    String salutation;
    @SerializedName("FirstName")
    String firstName;
    @SerializedName("LastName")
    String lastName;
    @SerializedName("EmailAddress1")
    String emailAddress;
    @SerializedName("PhoneNumber")
    String phoneNumber;
    @SerializedName("PrefResponse")
    String prefResponse;
    @SerializedName("requestDetails")
    String requestDetails;

    public ACRequest(String accessToken, String onlineMemberId, Profile profile, String requestDetails,
                     String responseType) {
        this.accessToken = accessToken;
        this.consumerKey = BuildConfig.WS_BCD_CONSUMER_KEY;
        this.EPCClientCode = BuildConfig.WS_BCD_EPC_CLIENT_CODE;
        this.EPCProgramCode = BuildConfig.WS_BCD_EPC_PROGRAM_CODE;
        this.verificationCode = BuildConfig.WS_BCD_VERIFICATION_CODE;
        this.source = "MobileApp";
        this.editType = "ADD";
        this.requestType = "O Client Specific";
        this.functionality = "Others";
        this.onlineMemberId = onlineMemberId;
        this.salutation = TextUtils.isEmpty(profile.getSalutation()) ? "N/A" : profile.getSalutation();
        this.firstName = profile.getFirstName();
        this.lastName = profile.getLastName();
        this.emailAddress = profile.getEmail();
        if(!TextUtils.isEmpty(profile.getPhone())) {
            this.phoneNumber = profile.getPhone().replace("+", "").replace("-", "");
        }
        this.prefResponse = responseType;
        this.requestDetails = requestDetails;
    }

    public ACRequest(String accessToken, String transactionID, String onlineMemberId, Profile profile, String requestDetails,
                     String responseType, String editType) {
        this(accessToken, onlineMemberId, profile, requestDetails, responseType);
        if(!TextUtils.isEmpty(transactionID)) {
            this.transactionID = transactionID;
            this.editType = editType;
        }
    }
}
