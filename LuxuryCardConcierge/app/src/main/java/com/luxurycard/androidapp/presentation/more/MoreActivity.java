package com.luxurycard.androidapp.presentation.more;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.luxurycard.androidapp.R;
import com.luxurycard.androidapp.presentation.base.BaseActivity;

public class MoreActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acticity_dummy_content);
        if(savedInstanceState == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragment_place_holder
                            , MoreFragment.newInstance()
                            , MoreFragment.class.getSimpleName())
                    .commit();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

}
