package com.luxurycard.androidapp.domain.model.explore;

/**
 * Created by Den on 4/4/18.
 */

public class RequestDetailContent {
    final private String title;
    final private String description;

    public RequestDetailContent(String title, String description) {
        this.title = title;
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }
}
