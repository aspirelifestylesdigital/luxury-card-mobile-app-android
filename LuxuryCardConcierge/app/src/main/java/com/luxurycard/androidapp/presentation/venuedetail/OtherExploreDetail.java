package com.luxurycard.androidapp.presentation.venuedetail;

import android.view.View;

import com.luxurycard.androidapp.domain.model.explore.OtherExploreDetailItem;
import com.luxurycard.androidapp.presentation.base.BasePresenter;

/**
 * Created by vinh.trinh on 6/13/2017.
 */

public interface OtherExploreDetail {

    interface View {
        void onGetContentFullFinished(OtherExploreDetailItem exploreRViewItem);
        void hideLoadingFragment();
        void noInternetMessage();

        void showLoadingFragment();

        android.view.View getButtonBook();
    }

    interface Presenter extends BasePresenter<View> {
        void getContent(Integer contentId);

        void handleTooltip();
    }

}
