package com.luxurycard.androidapp.presentation.requestlist;

import com.luxurycard.androidapp.domain.model.RequestItemView;

import java.util.List;

public final class RequestCacheData {
    List<RequestItemView> openList;
    List<RequestItemView> closeList;

    public RequestCacheData(List<RequestItemView> openList, List<RequestItemView> closeList) {
        this.openList = openList;
        this.closeList = closeList;
    }

    public List<RequestItemView> getOpenList() {
        return openList;
    }

    public List<RequestItemView> getCloseList() {
        return closeList;
    }
}
