package com.luxurycard.androidapp.datalayer.retro2client;

import com.luxurycard.androidapp.BuildConfig;
import com.luxurycard.androidapp.datalayer.restapi.ACApi;
import com.luxurycard.androidapp.datalayer.restapi.B2CContentApi;
import com.luxurycard.androidapp.datalayer.restapi.B2CInstantApi;
import com.luxurycard.androidapp.datalayer.restapi.B2CUtilityApi;
import com.luxurycard.androidapp.datalayer.restapi.CCAApi;
import com.luxurycard.androidapp.datalayer.restapi.GeoCoderApi;
import com.luxurycard.androidapp.datalayer.restapi.OAuthApi;
import com.luxurycard.androidapp.datalayer.restapi.UserManagementApi;

import java.util.concurrent.TimeUnit;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by tung.phan on 5/11/2017.
 * Singleton AppHttpClient
 */

public class AppHttpClient extends Retro2Client {

    private ACApi acApi;
    private B2CInstantApi b2cInstantApi;
    private B2CContentApi b2CContentApi;
    private B2CUtilityApi b2CUtilityApi;
    private CCAApi b2CTilesApi;
    private OAuthApi oAuthApi;
    private UserManagementApi umAPI;
    private GeoCoderApi geoCoderApi;

    private static class AppHttpClientHelper {
        private static final AppHttpClient INSTANCE = new AppHttpClient();
    }

    public static AppHttpClient getInstance() {
        return AppHttpClientHelper.INSTANCE;
    }

    public B2CInstantApi getB2CInstantApi() {
        return b2cInstantApi;
    }

    public B2CContentApi getB2CContentApi() {
        return b2CContentApi;
    }

    public B2CUtilityApi getB2CUtilityApi() {
        return b2CUtilityApi;
    }

    public CCAApi getB2CTilesApi() {
        return b2CTilesApi;
    }

    public ACApi getAcApi() {
        return acApi;
    }

    public OAuthApi oAuthApi() {
        return oAuthApi;
    }

    public UserManagementApi userManagementApi() {
        return umAPI;
    }

    public GeoCoderApi geoCoderApi() {
        return geoCoderApi;
    }

    private AppHttpClient() {
        final Retrofit b2cRetrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.WS_B2C_ROOT_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(provideOkHttpClientWithoutCache())
                .build();
        Retrofit b2cCCARetrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.WS_B2C_ROOT_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(provideOkHttpClient(10, TimeUnit.MINUTES))
                .build();
        Retrofit acRetrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.WS_BCD_ROOT_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(provideOkHttpClientWithoutCache())
                .build();
        Retrofit oauthRetrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.WS_BCD_ROOT_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(provideOkHttpClientWithoutCache())
                .build();
        Retrofit geoCoderApiRetrofit = new Retrofit.Builder()
                .baseUrl("http://maps.google.com/maps/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(provideOkHttpClient(7, TimeUnit.DAYS))
                .build();

        acApi = acRetrofit.create(ACApi.class);
        b2cInstantApi = b2cRetrofit.create(B2CInstantApi.class);
        b2CContentApi = b2cRetrofit.create(B2CContentApi.class);
        b2CUtilityApi = b2cRetrofit.create(B2CUtilityApi.class);
        b2CTilesApi = b2cCCARetrofit.create(CCAApi.class);
        oAuthApi = oauthRetrofit.create(OAuthApi.class);
        umAPI = oauthRetrofit.create(UserManagementApi.class);
        geoCoderApi = geoCoderApiRetrofit.create(GeoCoderApi.class);
    }

}
