package com.luxurycard.androidapp.presentation.profile;

import android.support.annotation.Nullable;
import android.support.v7.widget.SwitchCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.EditText;

import com.luxurycard.androidapp.domain.model.Profile;
import com.luxurycard.androidapp.presentation.widget.CustomSpinner;
import com.support.mylibrary.widget.LetterSpacingTextView;

/**
 * Created by vinh.trinh on 7/11/2017.
 */

class ProfileComparator {

    Profile original;

    private EditText edtFirstName, edtLastName, edtEmail, edtPhone, edtZipCode, edtFirstSixDigits;
    //private SwitchCompat locationSwitch;
    /*private Spinner salutationSpinner;*/
    private CustomSpinner salutationSpinner, countryCodeSpinner;
    private Button btnSubmit/*, btnCancel*/, btnSave;
    private boolean firstName, lastName, email, phone, salutation, countryCode, zipCode, firstSixDigits;

    ProfileComparator(EditText edtFirstName, EditText edtLastName, EditText edtEmail, EditText edtPhone,
                      Button btnSubmit/*,Button btnCancel*//*, SwitchCompat locationSwitch*/, CustomSpinner salutationSpinner,
                      CustomSpinner countryCodeSpinner, EditText edtZipCode, EditText edtFirstSixDigits, @Nullable Button btnSave) {
        this.edtFirstName = edtFirstName;
        this.edtLastName = edtLastName;
        this.edtEmail = edtEmail;
        this.edtPhone = edtPhone;
        this.btnSubmit = btnSubmit;
//        this.btnCancel = btnCancel;
//        this.locationSwitch = locationSwitch;
        this.salutationSpinner = salutationSpinner;
        this.countryCodeSpinner = countryCodeSpinner;
        this.edtZipCode = edtZipCode;
        this.edtFirstSixDigits = edtFirstSixDigits;
        if (btnSave != null) this.btnSave = btnSave;
        setUp();
    }

    private void setUp() {
        edtFirstName.addTextChangedListener(new TextWatcher() {
            @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                String string = s.toString();
                if(string.contains("*")) return ;
                firstName = !string.equals(original.getFirstName());
                onChanged();
            }
        });
        edtLastName.addTextChangedListener(new TextWatcher() {
            @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                String string = s.toString();
                if(string.contains("*")) return;
                lastName = !string.equals(original.getLastName());
                onChanged();
            }
        });
      /*  edtEmail.addTextChangedListener(new TextWatcher() {
            @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                String string = s.toString();
                if(string.contains("*")) return;
                email = !string.equals(original.getEmail());
                onChanged();
            }
        });*/
        edtPhone.addTextChangedListener(new TextWatcher() {
            @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override public void onTextChanged(CharSequence s, int start, int before, int count) {

            }
            @Override
            public void afterTextChanged(Editable s) {
                ProfileFragment.FocusChangeListener focusChangeListener = (ProfileFragment.FocusChangeListener) (edtPhone.getOnFocusChangeListener());
                String string = focusChangeListener.hasFocus ? s.toString() : focusChangeListener.original;
//                if(string.contains("*")) return;
                phone = !string.equals(original.getPhone());
                onChanged();
            }
        });
        salutationSpinner.setmSpinnerListener(new CustomSpinner.SpinnerListener() {
            @Override
            public void onArchorViewClick() {

            }

            @Override
            public void onItemSelected(final int index) {
                salutation = !(salutationSpinner.getDropdownAdapter().getItem(index)).equals(original.getSalutation());
                onChanged();

            }
        });
      /*  salutationSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                salutation = !(salutationSpinner.getSelectedItem()).equals(original.getSalutation());
                onChanged();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });*/

      countryCodeSpinner.setmSpinnerListener(new CustomSpinner.SpinnerListener() {
          @Override
          public void onArchorViewClick() {

          }

          @Override
          public void onItemSelected(int index) {
              countryCode = !(countryCodeSpinner.getDropdownAdapter().getItem(index).equals(original.getCountryCode()));
              onChanged();
          }
      });

      edtZipCode.addTextChangedListener(new TextWatcher() {
          @Override
          public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
          @Override
          public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
          @Override
          public void afterTextChanged(Editable s) {
              String string = s.toString();
//              if(string.contains("*")) return;
              zipCode = !string.equals(original.getZipCode());
              onChanged();
          }
      });

        edtFirstSixDigits.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void afterTextChanged(Editable s) {
                String string = s.toString();
                if(string.contains("*")) return ;
                firstSixDigits = !string.equals(original.getFirstSixDigits());
                onChanged();
            }
        });
    }

    void onChanged() {
        boolean changed = salutation || firstName || lastName ||email || countryCode || phone ||  zipCode || firstSixDigits /*||
                locationSwitch.isChecked() == original.isLocationOn()*/;
        btnSubmit.setEnabled(changed);
        btnSubmit.setClickable(changed);
        if (btnSave != null) {
            btnSave.setEnabled(changed);
            btnSave.setClickable(changed);
        }
        //btnCancel.setEnabled(changed);
        //btnCancel.setClickable(changed);
    }
}
