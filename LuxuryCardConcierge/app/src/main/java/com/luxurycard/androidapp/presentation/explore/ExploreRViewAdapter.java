package com.luxurycard.androidapp.presentation.explore;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.luxurycard.androidapp.R;
import com.luxurycard.androidapp.common.glide.GlideHelper;
import com.luxurycard.androidapp.domain.model.explore.ExploreRViewItem;
import com.luxurycard.androidapp.presentation.selectcategory.CategoryPresenter;
import com.luxurycard.androidapp.presentation.widget.FooterLoaderAdapter;
import com.luxurycard.androidapp.presentation.widget.OnItemClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;

/**
 * Created by tung.phan on 5/9/2017.
 */

public class ExploreRViewAdapter
        extends FooterLoaderAdapter<ExploreRViewAdapter.ExploreItemViewHolder> {
    private final int expectedImageWidth;
    private final List<ExploreRViewItem> data;
    private OnItemClickListener listener;
    private int selectedPosition = -1;
    private boolean isSearchAction;
    private Resources resources;
    private boolean isCategoryAll = true;
//    private DecimalFormat df = new DecimalFormat("#.##");

    ExploreRViewAdapter(Context context, List<ExploreRViewItem> data, OnItemClickListener listener) {
        super(context);
        resources = context.getResources();
        this.data = new ArrayList<>(data);
        this.listener = listener;
        setItemCount(this.data.size());
        expectedImageWidth = context.getResources().getDimensionPixelSize(R.dimen.text_explore_align_image);
    }

    public void setSearchAction(boolean searchAction) {
        isSearchAction = searchAction;
    }

    public ExploreRViewItem getItem(int pos) {
        return data.get(pos);
    }

    public List<ExploreRViewItem> getData() {
        return new ArrayList<>(data);
    }

    public boolean isEmpty() {
        return this.data.size() == 0;
    }

    public void clear() {
        if(this.data.size() == 0) return;
        selectedPosition = -1;
        this.data.clear();
        setItemCount(0);
        notifyDataSetChanged();
    }

    public void add(ExploreRViewItem datum) {
        this.data.add(datum);
        setItemCount(this.data.size());
        notifyDataSetChanged();
    }

    public void append(List<ExploreRViewItem> data) {
        this.data.addAll(data);
        setItemCount(this.data.size());
        notifyDataSetChanged();
    }

    public void swapData(List<ExploreRViewItem> data) {
        this.data.clear();
        this.data.addAll(data);
        setItemCount(this.data.size());
        notifyDataSetChanged();
    }

    private void bindViewTypeNormal(ExploreItemViewHolder holder, ExploreRViewItem exploreRViewItem) {
        holder.title.setText(Html.fromHtml(exploreRViewItem.title));
        if(TextUtils.isEmpty(exploreRViewItem.description.trim())) {
//            holder.description.setVisibility(View.GONE);
            holder.description.setText("");
        } else {
//            holder.description.setVisibility(View.VISIBLE);
            holder.description.setText(exploreRViewItem.description);
            holder.description.setTextColor(Color.parseColor("#C7D4D9"));
        }
        holder.specialDescription.setText(Html.fromHtml(exploreRViewItem.summary));

        if (holder.imageContainer.getVisibility() != View.VISIBLE) {
            holder.imageContainer.setVisibility(View.VISIBLE);
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) holder.contentContainer.getLayoutParams();
            params.setMarginStart(expectedImageWidth);
            holder.contentContainer.setLayoutParams(params);
        }
        ImageView iv;
        switch (exploreRViewItem.getItemType()) {
            case DINING:
                iv = holder.diningImage;
                holder.diningImage.setVisibility(View.VISIBLE);
                holder.image.setVisibility(View.GONE);
                break;
            default:
                iv = holder.image;
                holder.image.setVisibility(View.VISIBLE);
                holder.diningImage.setVisibility(View.GONE);
                break;
        }
        iv.setImageResource(0);
        if(TextUtils.isEmpty(exploreRViewItem.imageURL)) {
            GlideHelper.getInstance().loadImage(R.drawable.img_placeholder, R.drawable.img_placeholder, iv, expectedImageWidth);
        } else {
            GlideHelper.getInstance().loadImage(exploreRViewItem.imageURL, R.drawable.img_placeholder, iv, expectedImageWidth);
        }
        if(isCategoryAll) {
            holder.showMask(exploreRViewItem.getDisplayCategory());
        } else {
            holder.hideMask();
        }
        holder.searchStart.setVisibility(View.GONE);
        if (exploreRViewItem.hasStar) {
            holder.star.setVisibility(View.VISIBLE);
            holder.benefit.setVisibility(View.VISIBLE);
        } else {
            holder.star.setVisibility(View.GONE);
            holder.benefit.setVisibility(View.GONE);
        }
    }

    public void setShowCategoryAll(){
        isCategoryAll = true;
    }

    public void setShowCategoryType(){
        isCategoryAll = false;
    }

    private void bindViewTypeNoImage(ExploreItemViewHolder holder, ExploreRViewItem exploreRViewItem) {

        if (holder.imageContainer.getVisibility() != View.GONE) {
            holder.imageContainer.setVisibility(View.GONE);
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) holder.contentContainer.getLayoutParams();
            params.setMarginStart(0);
            holder.contentContainer.setLayoutParams(params);
        }
        holder.title.setText(Html.fromHtml(exploreRViewItem.title));
        if(TextUtils.isEmpty(exploreRViewItem.description.trim())) {
//            holder.description.setVisibility(View.GONE);
            holder.description.setText("");
        } else {
//            holder.description.setVisibility(View.VISIBLE);
            holder.description.setText(exploreRViewItem.description);
            holder.description.setTextColor(Color.parseColor("#ffffff"));
        }
        holder.specialDescription.setText((Html.fromHtml(exploreRViewItem.summary)));
        if (exploreRViewItem.getItemType() == ExploreRViewItem.ItemType.CITY_GUIDE ||
                exploreRViewItem.getItemType() == ExploreRViewItem.ItemType.SEARCH || isSearchAction) {
            if(exploreRViewItem.hasStar) {
                holder.searchStart.setVisibility(View.VISIBLE);
                holder.star.setVisibility(View.GONE);
                holder.benefit.setVisibility(View.GONE);
            } else {
                holder.searchStart.setVisibility(View.GONE);
                holder.star.setVisibility(View.GONE);
                holder.benefit.setVisibility(View.GONE);
            }
        } else {
            if(exploreRViewItem.hasStar) {
                holder.searchStart.setVisibility(View.GONE);
                holder.star.setVisibility(View.VISIBLE);
                holder.benefit.setVisibility(View.VISIBLE);
            } else {
                holder.searchStart.setVisibility(View.GONE);
                holder.star.setVisibility(View.GONE);
                holder.benefit.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public long getYourItemId(int position) {
        return position;
    }

    @Override
    public RecyclerView.ViewHolder getYourItemViewHolder(LayoutInflater inflater, ViewGroup parent) {
        View itemView = inflater.inflate(R.layout.explore_rview_item, parent, false);
        return new ExploreItemViewHolder(itemView);
    }

    @Override
    public void bindYourViewHolder(RecyclerView.ViewHolder holder, int position) {
        ExploreItemViewHolder viewHolder = (ExploreItemViewHolder) holder;
//        viewHolder.itemView.setSelected(position == selectedPosition);
        final ExploreRViewItem commonRViewItem = data.get(position);
        if (commonRViewItem.getItemType() == ExploreRViewItem.ItemType.CITY_GUIDE ||
                commonRViewItem.getItemType() == ExploreRViewItem.ItemType.SEARCH || isSearchAction) {
            ViewTreeObserver.OnGlobalLayoutListener onGlobalLayoutListener = () -> {
                if (((ExploreItemViewHolder) holder).title.getWidth() > 0 && ((ExploreItemViewHolder) holder).title.getLayout() != null) {
                    int lineOfTitle = ((ExploreItemViewHolder) holder).title.getLayout().getLineCount();
                    int descriptionMaxLine = 5 - (lineOfTitle + 1);
                    ((ExploreItemViewHolder) holder).specialDescription.setMaxLines(descriptionMaxLine);
                }
            };
            ((ExploreItemViewHolder) holder).title.getViewTreeObserver().addOnGlobalLayoutListener(onGlobalLayoutListener);

            bindViewTypeNoImage(viewHolder, commonRViewItem);
        } else {

            ViewTreeObserver.OnGlobalLayoutListener onGlobalLayoutListener = () -> {
                if (((ExploreItemViewHolder) holder).title.getWidth() > 0 && ((ExploreItemViewHolder) holder).title.getLayout() != null) {
                    int lineOfTitle = ((ExploreItemViewHolder) holder).title.getLayout().getLineCount();
                    int descriptionMaxLine = 5 - (lineOfTitle + 1) - 1;
                    ((ExploreItemViewHolder) holder).specialDescription.setMaxLines(descriptionMaxLine);
                }
            };
            ((ExploreItemViewHolder) holder).title.getViewTreeObserver().addOnGlobalLayoutListener(onGlobalLayoutListener);

            bindViewTypeNormal(viewHolder, commonRViewItem);
        }
    }


    private long watch = 0;
    class ExploreItemViewHolder extends RecyclerView.ViewHolder {
        private View imageContainer;
        private LinearLayout contentContainer;
        private View mask;
        private TextView tvMask;
        private ImageView image, star, diningImage, searchStart;
        private TextView title, description, specialDescription, benefit;
//        TextView textView;

        ExploreItemViewHolder(View view) {
            super(view);
            imageContainer = ButterKnife.findById(view, R.id.image_container);
            contentContainer = ButterKnife.findById(view, R.id.content_container);
            mask = ButterKnife.findById(view, R.id.img_mask);
            tvMask = ButterKnife.findById(view, R.id.img_mask_text);
            image = ButterKnife.findById(view, R.id.image);
            diningImage = ButterKnife.findById(view, R.id.diningImage);
            star = ButterKnife.findById(view, R.id.star);
            title = ButterKnife.findById(view, R.id.title);
            description = ButterKnife.findById(view, R.id.short_description);
            specialDescription = ButterKnife.findById(view, R.id.special_description);
            benefit = ButterKnife.findById(view, R.id.text_benefit);
            searchStart = ButterKnife.findById(view, R.id.search_star);
//            textView = ButterKnife.findById(view, R.id.textView);
            view.setOnClickListener(v -> {
                long start = System.currentTimeMillis();
                if(start - watch > 1000) {
                    selectedPosition = getAdapterPosition();
                    listener.onItemClick(selectedPosition);
                }
                watch = start;
            });
            /*title.getViewTreeObserver().addOnGlobalLayoutListener(() -> {
                if(title.getWidth() > 0 && title.getLayout() != null){
                    int firstLineWidth = (int)title.getLayout().getLineWidth(0);
                    RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    layoutParams.leftMargin = firstLineWidth;
                    layoutParams.topMargin = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 2, App.getInstance().getResources().getDisplayMetrics());
                    star.setLayoutParams(layoutParams);
                }
            });*/

        }

        void showMask(String text) {
            // Transportation, Entertainment
            if("TransportationEntertainment".contains(text)) {
                tvMask.setTextSize(TypedValue.COMPLEX_UNIT_PX, resources.getDimension(R.dimen.font_size_small));
            } else {
                tvMask.setTextSize(TypedValue.COMPLEX_UNIT_PX, resources.getDimension(R.dimen.font_size_normal));
            }
            tvMask.setVisibility(View.VISIBLE);
            tvMask.setText(text);
            mask.setVisibility(View.VISIBLE);
        }

        void hideMask() {
            tvMask.setVisibility(View.GONE);
            mask.setVisibility(View.GONE);
        }
    }
}