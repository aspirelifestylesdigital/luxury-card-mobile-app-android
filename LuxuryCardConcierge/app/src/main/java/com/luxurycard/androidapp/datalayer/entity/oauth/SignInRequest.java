package com.luxurycard.androidapp.datalayer.entity.oauth;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.luxurycard.androidapp.BuildConfig;

/**
 * Created by vinh.trinh on 7/18/2017.
 */

public class SignInRequest {
    @Expose
    @SerializedName("ConsumerKey")
    private final String consumerKey;
    @Expose
    @SerializedName("Functionality")
    private final String functionality;
    @Expose
    @SerializedName("MemberDeviceId")
    private final String memberDeviceId;
    @Expose
    @SerializedName("Email2")
    private final String email;
    @Expose
    @SerializedName("Password")
    private final String secretKey;

    public SignInRequest(String email, String secretKey) {
        this.consumerKey = BuildConfig.WS_BCD_CONSUMER_KEY;
        this.functionality = "Login";
        this.memberDeviceId = BuildConfig.WS_BCD_MEMBER_DEVICE_ID;
        this.email = email;
        this.secretKey = secretKey;
    }
}
