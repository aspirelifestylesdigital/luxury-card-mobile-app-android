package com.luxurycard.androidapp.presentation.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.View;
import android.view.WindowManager;

import com.luxurycard.androidapp.App;
import com.luxurycard.androidapp.R;
import com.luxurycard.androidapp.common.constant.CityData;
import com.luxurycard.androidapp.common.constant.ErrCode;
import com.luxurycard.androidapp.common.constant.RequestCode;
import com.luxurycard.androidapp.common.constant.ResultCode;
import com.luxurycard.androidapp.common.logic.Utils;
import com.luxurycard.androidapp.datalayer.datasource.AppGeoCoder;
import com.luxurycard.androidapp.datalayer.preference.PreferencesStorage;
import com.luxurycard.androidapp.datalayer.repository.PreferencesDataRepository;
import com.luxurycard.androidapp.datalayer.repository.ProfileDataRepository;
import com.luxurycard.androidapp.domain.model.RequestDetailData;
import com.luxurycard.androidapp.domain.model.RequestItemView;
import com.luxurycard.androidapp.domain.repository.ProfileRepository;
import com.luxurycard.androidapp.domain.repository.UserPreferencesRepository;
import com.luxurycard.androidapp.domain.usecases.GetUserPreferences;
import com.luxurycard.androidapp.domain.usecases.LoadProfile;
import com.luxurycard.androidapp.presentation.base.BaseNavFragment;
import com.luxurycard.androidapp.presentation.base.BaseNavSubFragment;
import com.luxurycard.androidapp.presentation.base.NavBottomActivity;
import com.luxurycard.androidapp.presentation.bottomsheet.BottomSheetCustomFragment;
import com.luxurycard.androidapp.presentation.explore.ExploreFragment;
import com.luxurycard.androidapp.presentation.home.adapter.BaseParentFragment;
import com.luxurycard.androidapp.presentation.home.adapter.HomeNavViewPagerAdapter;
import com.luxurycard.androidapp.presentation.homelist.HomeListFragment;
import com.luxurycard.androidapp.presentation.infoweb.InfoWebFragment;
import com.luxurycard.androidapp.presentation.more.MoreFragment;
import com.luxurycard.androidapp.presentation.requestdetail.RequestDetailFragment;
import com.luxurycard.androidapp.presentation.requestlist.parent.RequestFragment;
import com.luxurycard.androidapp.presentation.selectcity.SelectCityFragment;
import com.luxurycard.androidapp.presentation.widget.bottommenu.BottomMenu;

import butterknife.BindView;
import butterknife.ButterKnife;

/*
 * Activity will handle all fragment on View
 * on SubFragment return data to Activity after that
 * from activity send for BaseParent Fragment
 */

public class HomeNavBottomActivity extends NavBottomActivity implements
        HomeNavBottom.View
        , BottomSheetCustomFragment.OnBottomSheetListener
        /*, OnSuccessListener<Location>*/ {

    private static final String STATE_LAST_SELECTED = "last_selected";

    @Nullable
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.bottom_menu)
    public BottomMenu navBottomMenu;

    @Nullable
    @BindView(R.id.appbar)
    AppBarLayout actionBar;

    @BindView(R.id.view_pager_main)
    ViewPager viewPager;

    private HomeNavBottomPresenter presenter;
    protected HomeNavViewPagerAdapter vpAdapter;

    //-- hold feature location
    //private LocationProvider locationProvider;
    private PreferencesStorage preferencesStorage;

    private HomeNavBottomPresenter buildPresenter() {
        this.preferencesStorage = new PreferencesStorage(App.getInstance().getApplicationContext());
        UserPreferencesRepository preferencesRepository = new PreferencesDataRepository(preferencesStorage);
        ProfileRepository profileRepository = new ProfileDataRepository(preferencesStorage);
        GetUserPreferences getUserPreferences = new GetUserPreferences(preferencesRepository);
        LoadProfile loadProfile = new LoadProfile(profileRepository);
        return new HomeNavBottomPresenter(this, getUserPreferences,
                loadProfile,
                new AppGeoCoder(),
                preferencesStorage);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nav_bottom_menu);
        ButterKnife.bind(this);
        setStatusBarColor();

        presenter = buildPresenter();
        presenter.attach(this);

        setupView();

        if (savedInstanceState == null) {
            //first time load cuisine in preference
            presenter.loadCuisineCriteria(false);
        }

        if (!CityData.citySelected()) {
            String selectedCity = preferencesStorage.selectedCity();
            CityData.setSelectedCity(selectedCity);
        }
        //-- hold feature location
        //locationProvider = new LocationProvider(this);
        //requestLocation();
    }

    private void restartMainActivity(){
        Intent intent = getIntent();
        overridePendingTransition(0, 0);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        finish();
        overridePendingTransition(0, 0);
        startActivity(intent);
    }

    private void setupView() {
        try {

            if (vpAdapter == null && getSupportFragmentManager().getFragments().size() > 0) {
                restartMainActivity();
            }

            vpAdapter = new HomeNavViewPagerAdapter(getSupportFragmentManager());
            viewPager.setAdapter(vpAdapter);
            viewPager.setOffscreenPageLimit(vpAdapter.getCount());
            viewPager.setCurrentItem(0, false);
            viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                }

                @Override
                public void onPageSelected(int position) {
                    if(vpAdapter == null)
                        return;

                    if(vpAdapter.isPageRequest(position)){
                        RequestFragment requestFragment = vpAdapter.getRequestFragment();
                        if (requestFragment != null) {
                            //reload data My Request
                            toRootClearAllChildFragment(true);
                            requestFragment.onHomeActivityRequestFragment();
                        }
                    } else if (vpAdapter.isPageMore(position)) {
                        MoreFragment moreFragment = vpAdapter.getMoreFragment();
                        if (moreFragment != null) {
                            toRootClearAllChildFragment(true);
                        }
                    } else if (vpAdapter.isPageHome(position)) {
                        HomeListFragment homeListFragment = vpAdapter.getHomeListFragment();
                        if (homeListFragment != null) {
                            homeListFragment.showViewToolbarExpand();
                        }
                    }

                    //track GA
                    if(viewPager == null){
                        return;
                    }
                    Fragment fragment = vpAdapter.getItem(viewPager.getCurrentItem());
                    trackGATopFragment(fragment);

                }

                @Override
                public void onPageScrollStateChanged(int state) {
                }
            });
            navBottomMenu.setCallback(new BottomMenu.OnMenuItemClickListener() {
                @Override
                public void onMenuItemClick(int position) {
                    if (viewPager != null) {
                        viewPager.setCurrentItem(position, false);
                    }
                }

                @Override
                public void onBackRootFragment() {
                    toRootClearAllChildFragment(false);
                }
            });
            //init setup default
            navBottomMenu.setDefaultHomeTab();
            presenter.handleTooltip();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void trackGATopFragment(Fragment fragment){
        //-- click on Top Root Fragment
        if (fragment instanceof BaseParentFragment) {
            if(fragment.getChildFragmentManager().getFragments().size() == 1){
                Fragment baseNavFragment = fragment.getChildFragmentManager().getFragments().get(0);
                if(baseNavFragment instanceof BaseNavFragment){
                    ((BaseNavFragment)baseNavFragment).track();
                }
            }
        }
    }

    private void toRootClearAllChildFragment(boolean popBackStackImmediate) {
        if(vpAdapter == null && viewPager == null)
            return;
        Fragment fragment = vpAdapter.getItem(viewPager.getCurrentItem());
        if (fragment instanceof BaseParentFragment && fragment.getChildFragmentManager().getBackStackEntryCount() > 0) {
            if(popBackStackImmediate){
                fragment.getChildFragmentManager().popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }else {
                fragment.getChildFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }
        }
    }


    //<editor-fold desc="show/hide ActionBar View">
    public void hideActionBar() {
        if (actionBar != null) {
            actionBar.setVisibility(View.GONE);
        }
    }

    public void showActionBar() {
        if (actionBar != null) {
            actionBar.setVisibility(View.VISIBLE);
        }
    }

    public void setStatusBarColor() {
        if (Utils.isHigherThanLolipop()) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorToolbar));
        }
    }

    //</editor-fold>

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

    @Override
    public void onBackPressed() {
        if (viewPager != null) {
            Fragment fragment = vpAdapter.getItem(viewPager.getCurrentItem());
            if (fragment instanceof BaseParentFragment
                    && fragment.getChildFragmentManager() != null
                    && fragment.getChildFragmentManager().getBackStackEntryCount() > 0) {

                //track GA
                //<editor-fold desc="in case 2 fragment: current fragment and top root fragment">
                //skip fragment HomeListFragment ( run lifecycle)
                if(!vpAdapter.isPageHome(viewPager.getCurrentItem())
                        && fragment.getChildFragmentManager().getFragments().size() == 2){
                    Fragment fragmentTopTab = fragment.getChildFragmentManager().getFragments().get(0);
                    if(fragmentTopTab instanceof BaseNavFragment){
                        ((BaseNavFragment)fragmentTopTab).track();
                    }
                }
                //</editor-fold>

                fragment.getChildFragmentManager().popBackStack();
            } else {
                super.onBackPressed();
            }
        } else {
            super.onBackPressed();
        }
    }
//
//    public void resetViewCurrentTabFragment(){
//        if (viewPager != null && vpAdapter != null) {
//            int idCurrentItem = viewPager.getCurrentItem();
//            boolean isTabMore = vpAdapter.isPageMore(idCurrentItem);
////            boolean isTabHome = vpAdapter.isPageHome(idCurrentItem);
//            if(isTabMore){
//                MoreFragment moreFragment = vpAdapter.getMoreFragment();
//                if(moreFragment != null){
//                    moreFragment.showColorTabMore();
//                }
//            }/*else if(isTabHome){
//                HomeListFragment homeFragment = vpAdapter.getHomeListFragment();
//                if(homeFragment != null){
//                }
//            }*/
//        }
//    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
//        outState.putInt(STATE_LAST_SELECTED, lastSelected);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == RequestCode.SELECT_CITY) {
            if (resultCode == ResultCode.RESULT_OK) {
                //navigateToExplore();
            }
        } else if(requestCode == RequestCode.PROFILE) {
            if (resultCode == ResultCode.RESULT_OK) {
//                presenter.loadLocationCriteria();
                if (vpAdapter != null) {
                    HomeListFragment homeListFragment = vpAdapter.getHomeListFragment();
                    if (homeListFragment != null) {
                        homeListFragment.reloadProfileLocal();
                    }
                }
            }
        } else if(requestCode == RequestCode.USER_PREFERENCES) {
            if (resultCode == ResultCode.RESULT_OK) {
                presenter.loadCuisineCriteria(true);
            }
        } else if (requestCode == RequestCode.REQUEST_CODE_ASK_CONCIERGE) {
            handleResultNewRequest(resultCode);
        }
    }

    private void handleResultNewRequest(int resultCode){
        if (viewPager != null && vpAdapter != null) {
            int idCurrentItem = viewPager.getCurrentItem();
            boolean isTabRequest = vpAdapter.isPageRequest(idCurrentItem);
            boolean isTabHome = vpAdapter.isPageHome(idCurrentItem);
            if(isTabRequest){
                if(resultCode == ResultCode.RESULT_OK){
                    handleRequestsFragment(true);
                }else if(resultCode == ResultCode.RESULT_CANCELED){
                    handleRequestsFragment(false);
                }
            }else if(isTabHome){
                if(resultCode == ResultCode.RESULT_OK) {
                    //-- Explore detail open book new request
                    showDialogNewRequestSuccess();
                }
            }
        }//--do nothing here
    }

    //<editor-fold desc="Activity result">
    private void handleRequestsFragment(boolean isSuccess) {
        if (vpAdapter == null) {
            return;
        }
        RequestFragment requestFragment = vpAdapter.getRequestFragment();
        if (requestFragment != null) {
            toRootClearAllChildFragment(true);
            if (isSuccess) {
                //-- Requests Ok
                requestFragment.showDialogNewRequestSuccess();
            } else {
                //-- Requests Cancel
                requestFragment.resetLoadDuplicate();
            }
        }
    }
    //</editor-fold>

    public void onPageColorChanged(int color) {
        /*Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.fragment_place_holder);
        if(currentFragment != null && currentFragment instanceof InspireGalleryFragment){
            setStatusBarColor(color);
            setToolbarColor(color);
        }else{
//            setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
//            setToolbarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
            setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimary));
            setToolbarColor(ContextCompat.getColor(this, R.color.colorPrimary));
        }*/
    }

    //<editor-fold desc="general dialog">
    /* dialog loading */

    public void showProgressDialog(){
        if (mDialogHelper != null) {
            mDialogHelper.showProgress();
        }
    }

    public void hideProgressDialog(){
        if (mDialogHelper != null) {
            mDialogHelper.dismissProgress();
        }
    }

    public void showErrorDialog(ErrCode errCode, String extraMsg) {
        if (ErrCode.GENERAL_ERROR == errCode) {
            mDialogHelper.showGeneralError();
        } else {
            boolean isErrorNetwork = mDialogHelper.networkUnavailability(errCode, extraMsg);
            if (!isErrorNetwork) {
//                String message = getString(R.string.ask_concierge_error);
                mDialogHelper.alert(null, TextUtils.isEmpty(extraMsg) ? "" : extraMsg);
            }
        }
    }

    //</editor-fold>

    //<editor-fold desc="show child fragment tab current">
    public void replaceChildFragment(BaseNavSubFragment fragment) {
        if (vpAdapter != null && viewPager != null && fragment != null) {
            vpAdapter.presentReplaceChildFragment(viewPager.getCurrentItem(), fragment);
        }
    }

    public void addChildFragment(BaseNavSubFragment fragment){
        if (vpAdapter != null && viewPager != null && fragment != null) {
            vpAdapter.presentAddChildFragment(viewPager.getCurrentItem(), fragment);
        }
    }
    //</editor-fold>

    //-- hold feature location
    /*@Override
    public void onSuccess(Location location) {
        presenter.loadPreferences(location);
    }*/

    //-- hold feature location
    /*public void requestLocation() {
        locationProvider.getLocation();
    }*/

    //<editor-fold desc="Screen More View">

    /* open web view follow bottom menu current -> with subFragment */
    public void openWebViewTabCurrent(InfoWebFragment infoWebfragment) {
        replaceChildFragment(infoWebfragment);
    }

    //</editor-fold>


    //<editor-fold desc="HomeListFragment">
    /** @param typeCategory: empty ("") is select all category explore, otherwise filter follow type category */
    public void toExploreFragment(String typeCategory){
        /*
        1. selected city
        2. no select city
        */
        if(!CityData.citySelected()) {
            replaceChildFragment(SelectCityFragment.newInstance(true));
            return;
        }

        replaceChildFragment(ExploreFragment.newInstance(
                typeCategory,
                presenter.getLocationCache(),
                presenter.getCuisineCache()));
    }

    public void onCallConcierge() {
        presenter.openCallConcierge();
    }

    @Override
    public View getBottomMenuRequests() {
        if(navBottomMenu != null){
            return navBottomMenu.getRequest();
        }
        return null;
    }

    //</editor-fold>


    //<editor-fold desc="My Requests">

    public void showDialogNewRequestSuccess(){
        if(mDialogHelper != null){
            mDialogHelper.showDialogNewRequestSuccess();
        }//-- do nothing here
    }

    public void openAskConciergeActivity(Bundle bundle){
        presenter.openAskConcierge(bundle);
    }

    //</editor-fold>


    //<editor-fold desc="ButtonSheetCustom">
    @Override
    public void onBottomSheetClickCancel() {
        //dummy code here, do nothing here
    }
    //</editor-fold>


    public void showBottomMenu(){
        if(navBottomMenu != null){
            navBottomMenu.setVisibility(View.VISIBLE);
        }
    }

    public void hideBottomMenu(){
        if(navBottomMenu != null){
            navBottomMenu.setVisibility(View.GONE);
        }
    }


}
