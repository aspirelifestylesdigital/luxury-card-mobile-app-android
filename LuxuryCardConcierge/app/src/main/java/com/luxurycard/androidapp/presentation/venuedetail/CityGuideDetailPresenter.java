package com.luxurycard.androidapp.presentation.venuedetail;

import com.luxurycard.androidapp.datalayer.preference.PreferencesStorage;
import com.luxurycard.androidapp.domain.model.explore.CityGuideDetailItem;
import com.luxurycard.androidapp.domain.usecases.GetCityGuideDetail;
import com.luxurycard.androidapp.presentation.widget.tooltip.TooltipView;
import com.luxurycard.androidapp.presentation.widget.tooltip.data.TooltipConstant;
import com.luxurycard.androidapp.presentation.widget.tooltip.data.TooltipData;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by ThuNguyen on 6/13/2017.
 */

public class CityGuideDetailPresenter implements CityGuideDetail.Presenter {

    private GetCityGuideDetail getCityGuideDetail;
    private CityGuideDetail.View view;
    private CompositeDisposable disposables;
    private PreferencesStorage preferencesStorage;

    CityGuideDetailPresenter(PreferencesStorage preferencesStorage, GetCityGuideDetail getCityGuideDetail) {
        disposables = new CompositeDisposable();
        this.getCityGuideDetail = getCityGuideDetail;
        this.preferencesStorage = preferencesStorage;
    }


    @Override
    public void attach(CityGuideDetail.View view) {
        this.view = view;
    }

    @Override
    public void detach() {
        disposables.dispose();
        this.view = null;
    }

    @Override
    public void getCityGuideDetail(Integer categoryId, Integer itemId) {
        disposables.add(getCityGuideDetail.param(new GetCityGuideDetail.Params(categoryId, itemId))
                .on(Schedulers.io(), AndroidSchedulers.mainThread())
                .execute(new CityGuideDetailPresenter.GetCityGuideDetailObserver()));
    }

    private final class GetCityGuideDetailObserver extends DisposableSingleObserver<CityGuideDetailItem> {

        @Override
        public void onSuccess(CityGuideDetailItem cityGuideDetailItem) {
            view.onGetCityGuideDetailFinished(cityGuideDetailItem);
            dispose();
        }

        @Override
        public void onError(Throwable e) {
            view.onUpdateFailed();
            dispose();
        }
    }

    @Override
    public void handleTooltip() {
        if (view != null) {
            if(TooltipData.isShouldShow(preferencesStorage, TooltipConstant.EXPLORE_DETAIL_BUTTON_BOOK)){
                TooltipView tooltipView = new TooltipView();
                tooltipView.showViewTop(view.getButtonBook(), TooltipConstant.EXPLORE_DETAIL_BUTTON_BOOK);
            }//else do nothing here
        }
    }
}
