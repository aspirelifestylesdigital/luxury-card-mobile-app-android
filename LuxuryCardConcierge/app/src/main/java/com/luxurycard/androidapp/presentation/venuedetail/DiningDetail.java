package com.luxurycard.androidapp.presentation.venuedetail;

import android.view.View;

import com.luxurycard.androidapp.domain.model.explore.DiningDetailItem;
import com.luxurycard.androidapp.presentation.base.BasePresenter;

/**
 * Created by ThuNguyen on 6/13/2017.
 */

public interface DiningDetail {

    interface View {
        void onGetDiningDetailFinished(DiningDetailItem diningDetailItem);
        void onUpdateFailed();

        android.view.View getButtonBook();
    }

    interface Presenter extends BasePresenter<View> {
        void getDiningDetail(Integer categoryId, Integer itemId);
        void handleTooltip();
    }

}
