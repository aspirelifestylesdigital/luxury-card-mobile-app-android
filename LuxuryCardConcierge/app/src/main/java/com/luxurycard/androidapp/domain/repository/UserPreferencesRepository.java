package com.luxurycard.androidapp.domain.repository;

import com.luxurycard.androidapp.datalayer.datasource.PreferenceData;

import io.reactivex.Completable;
import io.reactivex.Single;

/**
 * Created by vinh.trinh on 7/26/2017.
 */

public interface UserPreferencesRepository {

    Single<PreferenceData> load(String accessToken);
    Completable save(PreferenceData preferenceData, String accessToken);

}
