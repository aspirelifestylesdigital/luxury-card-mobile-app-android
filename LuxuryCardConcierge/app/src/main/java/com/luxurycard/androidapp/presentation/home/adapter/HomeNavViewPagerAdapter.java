package com.luxurycard.androidapp.presentation.home.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.text.TextUtils;


import com.luxurycard.androidapp.presentation.base.BaseNavSubFragment;
import com.luxurycard.androidapp.presentation.homelist.HomeListFragment;
import com.luxurycard.androidapp.presentation.more.MoreFragment;
import com.luxurycard.androidapp.presentation.requestlist.parent.RequestFragment;

import java.util.LinkedHashMap;

public class HomeNavViewPagerAdapter extends FragmentStatePagerAdapter {
    private final LinkedHashMap<String, BaseParentFragment> mFragments = new LinkedHashMap<>();

    private final String PAGE_HOME = "home";
    private final String PAGE_REQUEST = "request";
    private final String PAGE_INFO = "info";
    private final String PAGE_MORE = "more";

    public HomeNavViewPagerAdapter(FragmentManager fm) {
        super(fm);
        if (mFragments.isEmpty()) {
            mFragments.put(PAGE_HOME, BaseParentFragment.newInstance(0));
            mFragments.put(PAGE_REQUEST, BaseParentFragment.newInstance(1));
            mFragments.put(PAGE_INFO, BaseParentFragment.newInstance(2));
            mFragments.put(PAGE_MORE, BaseParentFragment.newInstance(3));
        }
    }

    @Override
    public int getItemPosition(Object object) {
        return super.getItemPosition(object);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment;
        switch (position) {
            case 0:
                fragment = mFragments.get(PAGE_HOME);
                break;
            case 1:
                fragment = mFragments.get(PAGE_REQUEST);
                break;
            case 2:
                fragment = mFragments.get(PAGE_INFO);
                break;
            case 3:
                fragment = mFragments.get(PAGE_MORE);
                break;
            default:
                fragment = mFragments.get(PAGE_HOME);
                break;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return mFragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "";
    }

    private String getCurrentPage(int position) {
        String rs;
        switch (position) {
            case 0:
                rs = PAGE_HOME;
                break;
            case 1:
                rs = PAGE_REQUEST;
                break;
            case 2:
                rs = PAGE_INFO;
                break;
            case 3:
                rs = PAGE_MORE;
                break;
            default:
                rs = PAGE_HOME;
                break;
        }
        return rs;
    }

    public boolean isPageRequest(int position){
        return getCurrentPage(position).equalsIgnoreCase(PAGE_REQUEST);
    }

    public boolean isPageMore(int position){
        return getCurrentPage(position).equalsIgnoreCase(PAGE_MORE);
    }

    public boolean isPageHome(int position){
        return getCurrentPage(position).equalsIgnoreCase(PAGE_HOME);
    }

    public boolean isPageInfo(int position){
        return getCurrentPage(position).equalsIgnoreCase(PAGE_INFO);
    }

    //<editor-fold desc=" open child fragment ">
    /** use when add child fragment
     * - reload lifecycle from parent fragment (BaseParentFragment)
     * - no cache data */
    public void presentReplaceChildFragment(int currentBottomMenu, BaseNavSubFragment childFragment){
        presentChildFragment(true, currentBottomMenu, childFragment);
    }

    /** use when add child fragment
     * - hold lifecycle from parent fragment (BaseParentFragment)
     * - hold cache data */
    public void presentAddChildFragment(int currentBottomMenu, BaseNavSubFragment childFragment){
        presentChildFragment(false, currentBottomMenu, childFragment);
    }

    private void presentChildFragment(boolean isReplace, int currentBottomMenu, BaseNavSubFragment childFragment){
        if(currentBottomMenu >= 0 && currentBottomMenu < mFragments.size()){
            Fragment fragment = getItem(currentBottomMenu);
            if (fragment instanceof BaseParentFragment) {
                BaseParentFragment baseParentFragment = (BaseParentFragment) fragment;
                baseParentFragment.showFragment(isReplace, childFragment, true);
            }
        }//- do nothing here
    }
    //</editor-fold>

    public BaseParentFragment getTopFragmentOnTabHome() {
        if (mFragments != null) {
            return mFragments.get(PAGE_HOME);
        }
        return null;
    }

    /** @return null: can not get HomeListFragment */
    public HomeListFragment getHomeListFragment(){
        BaseParentFragment baseFragment = mFragments.get(PAGE_HOME);
        HomeListFragment homeListFragment = null;
        try {
            Fragment fragmentCheck = baseFragment.getChildFragmentManager().findFragmentByTag(HomeListFragment.class.getSimpleName());
            if (fragmentCheck instanceof HomeListFragment) {
                homeListFragment = (HomeListFragment) fragmentCheck;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return homeListFragment;
    }

    /** @return null: can not get RequestFragment */
    public RequestFragment getRequestFragment(){
        BaseParentFragment baseFragment = mFragments.get(PAGE_REQUEST);
        RequestFragment requestFragment = null;
        try {
            Fragment fragmentCheck = baseFragment.getChildFragmentManager().findFragmentByTag(RequestFragment.class.getSimpleName());
            if (fragmentCheck instanceof RequestFragment) {
                requestFragment = (RequestFragment) fragmentCheck;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return requestFragment;
    }

    public MoreFragment getMoreFragment(){
        BaseParentFragment baseFragment = mFragments.get(PAGE_MORE);
        MoreFragment moreFragment = null;
        try {
            Fragment fragmentCheck = baseFragment.getChildFragmentManager().findFragmentByTag(MoreFragment.class.getSimpleName());
            if (fragmentCheck instanceof MoreFragment) {
                moreFragment = (MoreFragment) fragmentCheck;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return moreFragment;
    }
}
