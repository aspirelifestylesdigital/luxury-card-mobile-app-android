package com.luxurycard.androidapp.domain.usecases;

import android.text.TextUtils;

import com.luxurycard.androidapp.datalayer.datasource.PreferenceData;
import com.luxurycard.androidapp.domain.repository.UserPreferencesRepository;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;

/**
 * Created by vinh.trinh on 8/8/2017.
 */

public class CreatePlainUserPreferences extends UseCase<Void, CreatePlainUserPreferences.Params> {

    private UserPreferencesRepository repository;

    public CreatePlainUserPreferences(UserPreferencesRepository repository) {
        this.repository = repository;
    }

    @Override
    Observable<Void> buildUseCaseObservable(Params s) {
        return null;
    }

    @Override
    Single<Void> buildUseCaseSingle(Params s) {
        return null;
    }

    @Override
    public Completable buildUseCaseCompletable(Params params) {
        PreferenceData preferenceData = PreferenceData.plain();
        preferenceData.extValue = params.locationOn ? "Yes" : "No";
        preferenceData.selectCity = TextUtils.isEmpty(params.citySelected) ? "" : params.citySelected;
        return repository.save(preferenceData, params.accessToken);
    }

    public static class Params {
        public final String accessToken;
        public final boolean locationOn;
        public final String citySelected;

        public Params(String accessToken, boolean locationOn, String citySelected) {
            this.accessToken = accessToken;
            this.locationOn = locationOn;
            this.citySelected = citySelected;
        }
    }
}
