package com.luxurycard.androidapp.domain.repository;

import com.luxurycard.androidapp.common.BackendException;
import com.luxurycard.androidapp.datalayer.entity.askconcierge.ACRequest;
import com.luxurycard.androidapp.datalayer.entity.askconcierge.ACRequestDetail;
import com.luxurycard.androidapp.datalayer.entity.askconcierge.ACRequestItem;
import com.luxurycard.androidapp.datalayer.entity.askconcierge.ACResponse;
import com.luxurycard.androidapp.datalayer.entity.askconcierge.GetACRequest;

import org.json.JSONException;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;

/**
 * Created by vinh.trinh on 5/17/2017.
 */

public interface ACRepository {
    
    Call<ACResponse> createNewConciergeCase(ACRequest request);
    Call<ACResponse> updateConciergeCase(ACRequest request);
    List<ACRequestItem> getRequestList(GetACRequest request) throws IOException, BackendException, JSONException;
    ACRequestDetail getRequestDetail(GetACRequest request);
}