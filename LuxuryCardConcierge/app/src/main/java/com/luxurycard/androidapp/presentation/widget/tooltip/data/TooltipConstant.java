package com.luxurycard.androidapp.presentation.widget.tooltip.data;

public interface TooltipConstant {

    /* bottom menu */
    String BOTTOM_MENU_REQUESTS = "bottom_menu_requests";

     /* my requests */
    String REQUESTS_BUTTON_NEW_REQUEST = "requests_button_new_request";

    /* page explore */
    String EXPLORE_BUTTON_CATEGORY = "explore_button_category";

    /* page explore -> detail */
    String EXPLORE_DETAIL_BUTTON_BOOK = "explore_detail_button_book";

}
