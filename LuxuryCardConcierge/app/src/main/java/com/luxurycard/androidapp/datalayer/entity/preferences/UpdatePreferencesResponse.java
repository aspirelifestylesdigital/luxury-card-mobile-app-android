package com.luxurycard.androidapp.datalayer.entity.preferences;


import com.google.gson.annotations.SerializedName;

public class UpdatePreferencesResponse {

    @SerializedName("success")
    private Boolean success;
    @SerializedName("message")
    private String message;

    public Boolean isSuccess() {
        return success;
    }

    public String message() {
        return message;
    }
}
