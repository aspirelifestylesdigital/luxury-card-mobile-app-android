package com.support.mylibrary.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.util.AttributeSet;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class CustomWebView extends WebView {

    public interface OnListenerWebView{
        void showDialogLoading();
        void hideDialogLoading();
    }
    OnListenerWebView listener;
    boolean isResponseData = false;

    public CustomWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public CustomWebView(Context context, AttributeSet attrs, int i) {
        super(context, attrs, i);
        init(attrs);
    }

    @SuppressLint("NewApi")
    public CustomWebView(Context context, AttributeSet attrs, int i, boolean b) {
        super(context, attrs, i, b);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        try {
            this.getSettings().setJavaScriptEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.setWebViewClient(new WebViewClient(){
            /*@Override
            public void onReceivedSslError(final WebView view,
                                           final SslErrorHandler handler,
                                           final SslError error) {
                handler.proceed();
            }*/


            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                if (listener != null) {
                    listener.showDialogLoading();
                }
                super.onPageStarted(view, url, favicon);
            }

            @Override
            public void onPageFinished(WebView view,
                                       String url) {

                if (listener != null) {
                    listener.hideDialogLoading();
                }
                super.onPageFinished(view,
                        url);
            }

        });
        this.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                if(!isResponseData && progress >= 15){
                    try{
                        if (listener != null) {
                            listener.hideDialogLoading();
                            isResponseData = true;
                            setWebChromeClient(new WebChromeClient(){});
                        }
                    }catch(Exception exception){
                        exception.printStackTrace();
                    }
                }
            }
        });
    }


    public void setListenerWebView(OnListenerWebView listener) {
        this.listener = listener;
    }
}