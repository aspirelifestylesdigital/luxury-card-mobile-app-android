package com.support.mylibrary.widget;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.os.Build;
import android.support.annotation.AnimatorRes;
import android.support.annotation.DrawableRes;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.animation.Interpolator;
import android.widget.LinearLayout;

import com.support.mylibrary.R;
import com.support.mylibrary.common.Utils;

import static android.support.v4.view.ViewPager.OnPageChangeListener;

public class CircleIndicator extends LinearLayout {

    private final static String TAG = CircleIndicator.class.getSimpleName();
    private final static int DEFAULT_INDICATOR_WIDTH = 7;
    private final static int DEFAULT_INDICATOR_MARGIN = 3;
    private ViewPager viewPager;
    private int indicatorMargin = -1;
    private int indicatorWidth = -1;
    private int indicatorHeight = -1;
    private int animatorResId = R.animator.scale_with_alpha;
    private int animatorReverseResId = 0;
    private int indicatorBackgroundResId = R.drawable.white_radius;
    private int indicatorUnselectedBackgroundResId = R.drawable.gray_radius;
    private Animator animationOut;
    private Animator animationIn;
    private Animator immediateAnimatorOut;
    private Animator immediateAnimatorIn;
    private int lastPosition = -1;

    public CircleIndicator(Context context) {
        super(context);
        init(context, null);
    }

    public CircleIndicator(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public CircleIndicator(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public CircleIndicator(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        handleTypedArray(context, attrs);
        checkIndicatorConfig(context);
    }

    private void handleTypedArray(Context context, AttributeSet attrs) {
        if (attrs == null) {
            return;
        }
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.CircleIndicator);
        indicatorWidth =
                typedArray.getDimensionPixelSize(R.styleable.CircleIndicator_ci_width, -1);
        indicatorHeight =
                typedArray.getDimensionPixelSize(R.styleable.CircleIndicator_ci_height, -1);
        indicatorMargin =
                typedArray.getDimensionPixelSize(R.styleable.CircleIndicator_ci_margin, -1);
        animatorResId = typedArray.getResourceId(R.styleable.CircleIndicator_ci_animator,
                R.animator.scale_with_alpha);
        animatorReverseResId =
                typedArray.getResourceId(R.styleable.CircleIndicator_ci_animator_reverse, 0);
        indicatorBackgroundResId =
                typedArray.getResourceId(R.styleable.CircleIndicator_ci_drawable,
                        R.drawable.white_radius);
        indicatorUnselectedBackgroundResId =
                typedArray.getResourceId(R.styleable.CircleIndicator_ci_drawable_unselected,
                        indicatorBackgroundResId);
        int orientation = typedArray.getInt(R.styleable.CircleIndicator_ci_orientation, -1);
        setOrientation(orientation == VERTICAL ? VERTICAL : HORIZONTAL);
        int gravity = typedArray.getInt(R.styleable.CircleIndicator_ci_gravity, -1);
        setGravity(gravity >= 0 ? gravity : Gravity.CENTER);
        typedArray.recycle();
    }

    /**
     * Create and configure Indicator in Java code.
     */
//    AppCompatResources.getDrawable(mContext, mImageTitleResId);
    public void configureIndicator() {
        configureIndicator(indicatorWidth, indicatorHeight, indicatorMargin,
                R.animator.scale_with_alpha, 0, R.drawable.white_radius, R.drawable.gray_radius);
    }

    public void configureIndicator(int indicatorWidth, int indicatorHeight, int indicatorMargin,
                                   @AnimatorRes int animatorId, @AnimatorRes int animatorReverseId,
                                   @DrawableRes int indicatorBackgroundId,
                                   @DrawableRes int indicatorUnselectedBackgroundId) {
        this.indicatorWidth = indicatorWidth;
        this.indicatorHeight = indicatorHeight;
        this.indicatorMargin = indicatorMargin;
        animatorResId = animatorId;
        animatorReverseResId = animatorReverseId;
        indicatorBackgroundResId = indicatorBackgroundId;
        indicatorUnselectedBackgroundResId = indicatorUnselectedBackgroundId;
        checkIndicatorConfig(getContext());
    }

    private void checkIndicatorConfig(Context context) {
        indicatorWidth = (indicatorWidth < 0) ? Utils
                .dip2px(context, DEFAULT_INDICATOR_WIDTH) : indicatorWidth;
        indicatorHeight = (indicatorHeight < 0) ? Utils
                .dip2px(context, DEFAULT_INDICATOR_WIDTH) : indicatorHeight;
        indicatorMargin = (indicatorMargin < 0) ? Utils
                .dip2px(context, DEFAULT_INDICATOR_MARGIN) : indicatorMargin;
        animatorResId = (animatorResId == 0) ? R.animator.scale_with_alpha : animatorResId;
        animationOut = createAnimatorOut(context);
        immediateAnimatorOut = createAnimatorOut(context);
        immediateAnimatorOut.setDuration(0);
        animationIn = createAnimatorIn(context);
        immediateAnimatorIn = createAnimatorIn(context);
        immediateAnimatorIn.setDuration(0);
        indicatorBackgroundResId = (indicatorBackgroundResId == 0) ? R.drawable.white_radius
                : indicatorBackgroundResId;
        indicatorUnselectedBackgroundResId =
                (indicatorUnselectedBackgroundResId == 0) ? indicatorBackgroundResId
                        : indicatorUnselectedBackgroundResId;
    }

    private Animator createAnimatorOut(Context context) {
        return AnimatorInflater.loadAnimator(context, animatorResId);
    }

    private Animator createAnimatorIn(Context context) {
        Animator animatorIn;
        if (animatorReverseResId == 0) {
            animatorIn = AnimatorInflater.loadAnimator(context, animatorResId);
            animatorIn.setInterpolator(new ReverseInterpolator());
        } else {
            animatorIn = AnimatorInflater.loadAnimator(context, animatorReverseResId);
        }
        return animatorIn;
    }

    public void setViewPager(ViewPager viewPager) {
        this.viewPager = viewPager;
        if (this.viewPager != null && this.viewPager.getAdapter() != null) {
            lastPosition = -1;
            createIndicators();
            this.viewPager.removeOnPageChangeListener(mInternalPageChangeListener);
            this.viewPager.addOnPageChangeListener(mInternalPageChangeListener);
            mInternalPageChangeListener.onPageSelected(this.viewPager.getCurrentItem());
        }
    }

    private final OnPageChangeListener mInternalPageChangeListener = new OnPageChangeListener() {

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        }

        @Override
        public void onPageSelected(int position) {
            if (viewPager.getAdapter() == null || viewPager.getAdapter().getCount() <= 0) {
                return;
            }
            if (animationIn.isRunning()) {
                animationIn.end();
                animationIn.cancel();
            }
            if (animationOut.isRunning()) {
                animationOut.end();
                animationOut.cancel();
            }
            View currentIndicator;
            if (lastPosition >= 0 && (currentIndicator = getChildAt(lastPosition)) != null) {
                currentIndicator.setBackgroundResource(indicatorUnselectedBackgroundResId);
                animationIn.setTarget(currentIndicator);
                animationIn.start();
            }
            View selectedIndicator = getChildAt(position % viewPager.getAdapter().getCount());
            if (selectedIndicator != null) {
                selectedIndicator.setBackgroundResource(indicatorBackgroundResId);
                animationOut.setTarget(selectedIndicator);
                animationOut.start();
            }
            lastPosition = position % viewPager.getAdapter().getCount();
        }

        @Override
        public void onPageScrollStateChanged(int state) {
        }
    };

    public DataSetObserver getDataSetObserver() {
        return mInternalDataSetObserver;
    }

    private DataSetObserver mInternalDataSetObserver = new DataSetObserver() {
        @Override
        public void onChanged() {
            super.onChanged();
            if (viewPager == null) {
                return;
            }
            int newCount = viewPager.getAdapter().getCount();
            int currentCount = getChildCount();
            if (newCount == currentCount) {  // No change
                return;
            } else if (lastPosition < newCount) {
                lastPosition = viewPager.getCurrentItem();
            } else {
                lastPosition = -1;
            }
            createIndicators();
        }
    };

    /**
     * @deprecated User ViewPager addOnPageChangeListener
     */
    @Deprecated
    public void setOnPageChangeListener(OnPageChangeListener onPageChangeListener) {
        if (viewPager == null) {
            throw new NullPointerException("can not find Viewpager , setViewPager first");
        }
        viewPager.removeOnPageChangeListener(onPageChangeListener);
        viewPager.addOnPageChangeListener(onPageChangeListener);
    }

    private void createIndicators() {
        removeAllViews();
        int count = viewPager.getAdapter().getCount();
        if (count <= 0) {
            return;
        }
        int currentItem = viewPager.getCurrentItem();
        int orientation = getOrientation();
        for (int i = 0; i < count; i++) {
            if (currentItem == i) {
                addIndicator(orientation, indicatorBackgroundResId, immediateAnimatorOut, i);
            } else {
                addIndicator(orientation, indicatorUnselectedBackgroundResId,
                        immediateAnimatorIn, i);
            }
        }
    }

    private void addIndicator(int orientation, @DrawableRes int backgroundDrawableId,
                              Animator animator, final int position) {
        if (animator.isRunning()) {
            animator.end();
            animator.cancel();
        }
        LinearLayout indicator = new LinearLayout(getContext());
        indicator.setBackgroundResource(backgroundDrawableId);
//        indicator.setOnClickListener(v -> viewPager.setCurrentItem(position));
        indicator.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(position);
            }
        });
        addView(indicator, indicatorWidth, indicatorHeight);
        LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) indicator.getLayoutParams();
        if (orientation == HORIZONTAL) {
            lp.leftMargin = indicatorMargin;
            lp.rightMargin = indicatorMargin;
        } else {
            lp.topMargin = indicatorMargin;
            lp.bottomMargin = indicatorMargin;
        }
        indicator.setLayoutParams(lp);
        animator.setTarget(indicator);
        animator.start();
    }

    private class ReverseInterpolator implements Interpolator {
        @Override
        public float getInterpolation(float value) {
            return Math.abs(1.0f - value);
        }
    }
}
