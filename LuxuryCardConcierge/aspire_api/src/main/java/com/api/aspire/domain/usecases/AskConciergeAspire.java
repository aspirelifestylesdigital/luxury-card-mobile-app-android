package com.api.aspire.domain.usecases;

import android.content.Context;
import android.text.TextUtils;

import com.api.aspire.common.constant.ConciergeCaseType;
import com.api.aspire.data.entity.askconcierge.ConciergeCaseAspireRequest;
import com.api.aspire.data.entity.askconcierge.ConciergeCaseAspireResponse;
import com.api.aspire.data.repository.AspireConciergeRepository;
import com.api.aspire.domain.model.ProfileAspire;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.Response;

public class AskConciergeAspire extends UseCase<ConciergeCaseAspireResponse, AskConciergeAspire.Params> implements ConciergeCaseType {

    private AspireConciergeRepository repository;
    private GetToken getTokenRemote;
    private ProfileAspire cache;
    private LoadProfile loadProfile;

    public AskConciergeAspire(Context c, MapProfileBase mapLogic) {
        this.repository = new AspireConciergeRepository();
        this.getTokenRemote = new GetToken(c);
        this.loadProfile = new LoadProfile(c, mapLogic);

    }

    @Override
    Observable<ConciergeCaseAspireResponse> buildUseCaseObservable(Params params) {
        return null;
    }

    @Override
    Single<ConciergeCaseAspireResponse> buildUseCaseSingle(final Params params) {
        return Single.create(e -> {

            String accessToken = params.getAccessToken();

            if (TextUtils.isEmpty(accessToken) || params.profile == null) {
                e.onError(new Exception("Error Get Token"));
                return;
            }

            Call<ConciergeCaseAspireResponse> requestApi = setRequestAPI(accessToken, params);

            if (requestApi == null) {

                e.onError(new Exception("Error Request Type"));

            } else {
                Response<ConciergeCaseAspireResponse> response = requestApi.execute();

                if (response.isSuccessful()) {
                    e.onSuccess(response.body());
                } else {
                    final String errorMessage = response.errorBody().string();
                    e.onError(new Exception(errorMessage));
                }
            }
        });
    }

    /**
     * data request api
     */
    private Call<ConciergeCaseAspireResponse> setRequestAPI(String accessToken, Params params) {

        Call<ConciergeCaseAspireResponse> requestApi = null;

        String requestType = params.requestTypeApi;

        //Update create request case Aspire API: dont send Salutation to API
        /*String salutation = mapLogic.mapSalutation(params.profile.getSalutation());*/

        ConciergeCaseAspireRequest requestData = new ConciergeCaseAspireRequest(
                buildMessage(params),
                params.profile.getFirstName(),
                params.profile.getLastName(),
                params.profile.getPhone(),
                params.profile.getEmail(),
                null,
                params.bin,
                params.program,
                params.conciergeRequestType
        );

        if (ConciergeCaseType.CREATE.equals(requestType)) {

            requestApi = repository.createConciergeCase(accessToken, requestData);
        } else if (ConciergeCaseType.UPDATE.equals(requestType)) {

            requestApi = repository.updateConciergeCase(accessToken, requestData);

        } else if (ConciergeCaseType.DELETE.equals(requestType)) {

            requestApi = repository.deleteConciergeCase(accessToken, requestData);
        }

        return requestApi;
    }

    @Override
    Completable buildUseCaseCompletable(Params params) {
        return null;
    }

    /**
     * flow load get bin (PassCode) cache Profile
     * load second time get data Profile
     * */
    public  Single<ProfileAspire> loadStorage(){
        Single<ProfileAspire> getProfile = cache == null ? loadProfile.loadStorage() : Single.just(cache);

        return getProfile.flatMap(profileAspire -> {
            if (cache == null) cache = profileAspire;
            return Single.just(profileAspire);
        });
    }

    public Single<ConciergeCaseAspireResponse> execute(Params params) {

        return loadStorage().flatMap(profile -> {

            params.setProfile(profile);

            Single<ConciergeCaseAspireResponse> askApi;
            if (profile == null) {
                ConciergeCaseAspireResponse emptyResponse = new ConciergeCaseAspireResponse();
                emptyResponse.empty();
                askApi = Single.just(emptyResponse);
            } else {
                askApi = getTokenRemote.refreshToken(profile.getEmail(), profile.getSecretKeyDecrypt())
                        .flatMap(accessToken -> {
                            params.setAccessToken(accessToken);
                            return buildUseCaseSingle(params);
                        });
            }
            return askApi;
        });


//        Single<ConciergeCaseResponse> askApi;
//        if (params.profile == null) {
//            ConciergeCaseResponse emptyResponse = new ConciergeCaseResponse();
//            emptyResponse.empty();
//            askApi = Single.just(emptyResponse);
//        } else {
//            askApi = getTokenRemote.refreshToken(params.profile.getEmail(), params.profile.getSecretKeyDecrypt())
//                    .flatMap(accessToken -> {
//                        params.setAccessToken(accessToken);
//                        return buildUseCaseSingle(params);
//                    });
//        }
//        return askApi;
    }

    private String buildMessage(Params params) {
        StringBuilder stringBuilder = new StringBuilder();

        if (!params.email && params.phone)
            stringBuilder.append("Respond by ").append(params.profile.getPhone());
        if (!params.phone && params.email)
            stringBuilder.append("Respond by ").append(params.profile.getEmail());
        if (params.email && params.phone)
            stringBuilder.append("Respond by ").append(params.profile.getPhone()).append(" or ").append(params.profile.getEmail());

        // Append category name or city name
        if (!TextUtils.isEmpty(params.city)) {
            stringBuilder.append("  \t\r\n").append(params.city);
        }
        // Append sub-category or category name
        if (!TextUtils.isEmpty(params.category)) {
            stringBuilder.append("  \t\r\n").append(params.category);
        }
        // Append original content finally
        stringBuilder.append("  \t\r\n").append(params.content);


        return stringBuilder.toString();
    }

    public static final class Params {
        private ProfileAspire profile;
        private final String content;
        private final String city;
        private final String category;

        private final boolean email;
        private final boolean phone;

        private final String requestTypeApi;
        private String accessToken = "";

        private int bin;
        private String program;
        private String conciergeRequestType;

        public Params(String requestTypeApi, String content, String city, String category,
                      boolean email, boolean phone, int bin, String program, String conciergeRequestType) {
            this.requestTypeApi = requestTypeApi;
            this.content = content;
            this.city = city;
            this.category = category;
            this.email = email;
            this.phone = phone;
            this.bin = bin;
            this.program = program;
            this.conciergeRequestType = conciergeRequestType;
        }

        public void setProfile(ProfileAspire profile) {
            this.profile = profile;
        }

        public String getAccessToken() {
            return accessToken;
        }

        public void setAccessToken(String accessToken) {
            this.accessToken = accessToken;
        }


    }
}