package com.api.aspire.domain.usecases;

import android.content.Context;

import com.api.aspire.data.preference.PreferencesStorageAspire;


public class SignOut {

    private PreferencesStorageAspire preferencesStorageAspire;

    public SignOut(Context context) {
        this.preferencesStorageAspire = new PreferencesStorageAspire(context);
    }

    public void setSignOutProfile(OnSignOutListener onSignOut) {
        preferencesStorageAspire.clear();
        if (onSignOut != null) {
            onSignOut.onSignOut();
        }
    }

    /**
     * Callback to reset CityData: remove select city data
     * Use this line: CityData.reset();
     */
    public interface OnSignOutListener {
        void onSignOut();
    }
}
