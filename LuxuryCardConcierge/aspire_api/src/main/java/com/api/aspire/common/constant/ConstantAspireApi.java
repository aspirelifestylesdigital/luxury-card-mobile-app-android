package com.api.aspire.common.constant;

public class ConstantAspireApi {

    public interface APP_USER_PREFERENCES {

        String USER_LOCATION_STATUS_KEY = "User Location Status";
        String PASS_CODE_KEY = "Passcode";
        String SELECTED_CITY_KEY = "Selected City";
        String NEWSLETTER_STATUS_KEY = "Newsletter Status";// MC
        String POLICY_VERSION_KEY = "Policy Version";// MC
        String PLATFORM_KEY = "Device OS";
    }
}
