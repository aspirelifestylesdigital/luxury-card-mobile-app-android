package com.api.aspire.data.entity.profile;

import com.google.gson.annotations.SerializedName;

public class VerificationMetadata {
    @SerializedName("bin")
    private int bin;

    public VerificationMetadata(int bin) {
        this.bin = bin;
    }

    public void setBin(int bin) {
        this.bin = bin;
    }
}
