package com.api.aspire.data.entity.askconcierge;


import com.api.aspire.data.entity.profile.VerificationMetadata;
import com.google.gson.annotations.SerializedName;

public class ConciergeCaseAspireRequest {

    @SerializedName("request")
    private Request request;

    @SerializedName("preferredResponse")
    private String preferredResponse;

    @SerializedName("verificationMetadata")
    private VerificationMetadata verificationMetadata;

    @SerializedName("profile")
    private Profile profile;

    @SerializedName("program")
    private String program;

    /**
     * Use this salutation: MapDataApi.mapSalutationRequest(salutation)
     *
     * @param requestDetails
     * @param firstName
     * @param lastName
     * @param phone
     * @param email
     * @param salutation
     */
    public ConciergeCaseAspireRequest(String requestDetails,
                                      String firstName,
                                      String lastName,
                                      String phone,
                                      String email,
                                      String salutation,
                                      int bin,
                                      String program,
                                      String conciergeRequestType) {

        request = new Request(requestDetails, conciergeRequestType);
        profile = new Profile(firstName,
                lastName,
                phone,
                email,
                salutation);

        this.preferredResponse = "Email"; // enum param
        this.verificationMetadata = new VerificationMetadata(bin);
//        this.program = "DEMO SINGAPORE";
        this.program = program;
    }

    class Profile {

        @SerializedName("firstName")
        private String firstName;

        @SerializedName("lastName")
        private String lastName;

        @SerializedName("phoneNumber")
        private String phoneNumber;

        @SerializedName("emailAddress1")
        private String emailAddress1;

        @SerializedName("salutation")
        private String salutation;

        public Profile(String firstName, String lastName, String phoneNumber, String emailAddress1, String salutation) {
            this.firstName = firstName;
            this.lastName = lastName;
            this.phoneNumber = phoneNumber;
            this.emailAddress1 = emailAddress1;
            this.salutation = salutation;
        }
    }

    class Request {

        @SerializedName("requestType")
        private String requestType;

        @SerializedName("requestDetails")
        private String requestDetails;

        Request(String requestDetails, String conciergeRequestType) {
            //-- hardCode
//            this.requestType = "O Client Specific";
            this.requestType = conciergeRequestType;
            this.requestDetails = requestDetails;
        }
    }

}