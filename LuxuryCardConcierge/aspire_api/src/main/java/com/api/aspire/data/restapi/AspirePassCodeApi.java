package com.api.aspire.data.restapi;

import com.api.aspire.data.entity.passcode.PassCodeRequest;
import com.api.aspire.data.entity.passcode.PassCodeResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface AspirePassCodeApi {
    @Headers({"Content-Type: application/json"})
    @POST("verifications/")
    Call<PassCodeResponse> checkPassCode(@Header("Authorization") String auth, @Body PassCodeRequest request);
}
