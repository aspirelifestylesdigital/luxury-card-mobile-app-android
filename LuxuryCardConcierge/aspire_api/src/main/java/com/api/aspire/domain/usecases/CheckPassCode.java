package com.api.aspire.domain.usecases;

import android.content.Context;
import android.text.TextUtils;

import com.api.aspire.common.constant.ConstantAspireApi;
import com.api.aspire.common.constant.ErrCode;
import com.api.aspire.common.utils.CommonUtils;
import com.api.aspire.data.entity.profile.ProfileAspireResponse;
import com.api.aspire.data.preference.PreferencesStorageAspire;
import com.api.aspire.domain.model.ProfileAspire;
import com.api.aspire.domain.repository.AuthAspireRepository;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;

public class CheckPassCode extends UseCase<Boolean, CheckPassCode.Params> {

    private PreferencesStorageAspire preferencesStorage;
    private AuthAspireRepository repository;
    private GetAccessToken mGetAccessToken;
    private LoadProfile mLoadProfile;
    private SaveProfile saveProfile;
    private String accessToken;
    private GetToken getToken;
    private String defaultUsername;
    private String defaultSecretKey;

    public CheckPassCode(Context context,
                         AuthAspireRepository repository,
                         GetAccessToken mGetAccessToken,
                         LoadProfile mLoadProfile,
                         SaveProfile saveProfile,
                         GetToken getToken,
                         String defaultUsername,
                         String defaultSecretKey) {

        this.repository = repository;
        this.mGetAccessToken = mGetAccessToken;
        this.mLoadProfile = mLoadProfile;
        this.saveProfile = saveProfile;
        this.getToken = getToken;
        this.defaultUsername = defaultUsername;
        this.defaultSecretKey = defaultSecretKey;
        this.preferencesStorage = new PreferencesStorageAspire(context);
    }

    @Override
    Observable<Boolean> buildUseCaseObservable(Params params) {
        return null;
    }

    @Override
    public Single<Boolean> buildUseCaseSingle(Params params) {
        final int passCodeNumber = CommonUtils.convertStringToInt(params.passCode);
        if (passCodeNumber == -1) {
            return Single.error(new Exception(ErrCode.PASS_CODE_ERROR.name()));
        }
        return getToken.getTokenService(params.defaultUsername, params.defaultSecretKey)
                .flatMap(serviceToken ->
                        repository.checkPassCode(serviceToken, passCodeNumber)
                );
    }

    @Override
    Completable buildUseCaseCompletable(Params params) {
        return null;
    }

    public Completable saveUserProfile(String newPassCode) {

        final int passCodeNumber = CommonUtils.convertStringToInt(newPassCode);
        if (passCodeNumber == -1) {
            return Completable.error(new Exception(ErrCode.PASS_CODE_ERROR.name()));
        }

        return mGetAccessToken.execute()
                .flatMap(accessToken -> {
                    this.accessToken = accessToken;
                    return mLoadProfile.buildUseCaseSingle(accessToken);
                })
                .flatMapCompletable(profileAspire -> {
                    if (profileAspire != null
                            && profileAspire.getAppUserPreferences() != null) {
                        //update passCode into profile
                        List<ProfileAspireResponse.AppUserPreferences> appUserPreferences =
                                profileAspire.getAppUserPreferences();

                        for (int i = 0; i < appUserPreferences.size(); i++) {
                            if (ConstantAspireApi.APP_USER_PREFERENCES.PASS_CODE_KEY
                                    .equalsIgnoreCase(appUserPreferences.get(i).getPreferenceKey())) {
                                appUserPreferences.get(i).setPreferenceValue(newPassCode);
                                break;
                            }
                        }

                        profileAspire.setAppUserPreferences(appUserPreferences);
                        profileAspire.getPreferenceData().setPassCodeValue(newPassCode);
                    }

                    return saveProfile.buildUseCaseCompletable(new SaveProfile.Params(profileAspire, accessToken));
                });
    }

    public Single<ResultPassCode> handleCheckPassCode(ProfileAspire profileAspire) {
        if (profileAspire == null) {
            return Single.error(new Exception(ErrCode.PASS_CODE_ERROR.name()));
        }

        String passCodeInProfile = getAppUserPreference(profileAspire.getAppUserPreferences(),
                ConstantAspireApi.APP_USER_PREFERENCES.PASS_CODE_KEY);

        if (TextUtils.isEmpty(passCodeInProfile)) {
            return Single.error(new Exception(ErrCode.PASS_CODE_ERROR.name()));
        }

        Params params = new Params(defaultUsername, defaultSecretKey, passCodeInProfile);

        return buildUseCaseSingle(params).flatMap(statusPassCode -> {
            if (statusPassCode == null || !statusPassCode) {
                return Single.just(new ResultPassCode(PassCodeStatus.INVALID));
            } else {
                return Single.just(new ResultPassCode(PassCodeStatus.VALID));
            }
        });
    }

    public Single<ResultPassCode> handleCheckPassCode() {
        return mGetAccessToken.execute()
                .flatMap(accessToken -> mLoadProfile.buildUseCaseSingle(accessToken).flatMap(Single::just))
                .flatMap(this::handleCheckPassCode);
    }

    /**
     * Wrapper enum data type
     */
    public static class ResultPassCode {
        public PassCodeStatus passCodeStatus;

        public ResultPassCode(PassCodeStatus passCodeStatus) {
            this.passCodeStatus = passCodeStatus;
        }

        public PassCodeStatus getPassCodeStatus() {
            return passCodeStatus;
        }
    }

    public enum PassCodeStatus {VALID, INVALID, NONE}

    public static class Params {
        String passCode;

        String defaultUsername;
        String defaultSecretKey;

        public Params(String defaultUsername, String defaultSecretKey, String passCode) {
            this.defaultUsername = defaultUsername;
            this.defaultSecretKey = defaultSecretKey;
            this.passCode = passCode;
        }

        public void setPassCode(String passCode) {
            this.passCode = passCode;
        }
    }

    private String getAppUserPreference(List<ProfileAspireResponse.AppUserPreferences> appUserPreferences, String key) {
        if (TextUtils.isEmpty(key)) {
            return "";
        }
        if (appUserPreferences == null || appUserPreferences.isEmpty()) return "";


        for (ProfileAspireResponse.AppUserPreferences preference : appUserPreferences) {
            if (key.equals(preference.getPreferenceKey())) {
                return preference.getPreferenceValue();
            }
        }
        return "";
    }


    //<editor-fold desc="flow for UDA: check passCode before login">
    public Single<Boolean> checkOutPassCode(Params params) {
        return buildUseCaseSingle(params)
                .map((result) -> {
                    if (result != null && result) {
                        preferencesStorage.editor().passCode(params.passCode).build().save();
                    }
                    return result;
                });
    }

    public String getPassCodeStorage() {
        return preferencesStorage.getPassCode();
    }

    public Single<String> passCodeStorage() {
        return Single.create(e -> {
            String passCode = getPassCodeStorage();
            if (TextUtils.isEmpty(passCode)) {
                e.onError(new Exception(ErrCode.PASS_CODE_ERROR.name()));
            } else {
                e.onSuccess(passCode);
            }
        });
    }
    //</editor-fold>
}
