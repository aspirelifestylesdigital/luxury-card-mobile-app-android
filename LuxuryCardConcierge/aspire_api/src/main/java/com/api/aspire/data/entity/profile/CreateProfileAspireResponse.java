package com.api.aspire.data.entity.profile;

import com.google.gson.annotations.SerializedName;

public class CreateProfileAspireResponse {

    @SerializedName("message")
    public String message;

}
