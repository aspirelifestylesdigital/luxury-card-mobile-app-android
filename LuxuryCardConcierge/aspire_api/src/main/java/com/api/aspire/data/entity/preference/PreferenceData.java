package com.api.aspire.data.entity.preference;


import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PreferenceData implements Parcelable {

    public static final String NA_VALUE = "na";

    @Expose
    @SerializedName("cuisine")
    public String cuisine;
    @Expose
    @SerializedName("hotel")
    public String hotel;
    @Expose
    @SerializedName("vehicle")
    public String vehicle;
    @Expose
    @SerializedName("cuisinePrefID")
    String cuisinePrefID;
    @Expose
    @SerializedName("hotelPrefID")
    String hotelPrefID;
    @Expose
    @SerializedName("vehiclePrefID")
    String vehiclePrefID;
    @Expose
    @SerializedName("passCode")
    public String passCode;
    @Expose
    @SerializedName("passCodeID")
    String passCodeID;

    public PreferenceData(String cuisine, String hotel, String vehicle) {
        this.cuisine = cuisine;
        this.hotel = hotel;
        this.vehicle = vehicle;
    }

    public PreferenceData() {
        String temp = "";
        cuisine = temp;
        hotel = temp;
        vehicle = temp;
    }

    void preferenceIDs(String cuisine, String hotel, String vehicle) {
        this.cuisinePrefID = cuisine;
        this.hotelPrefID = hotel;
        this.vehiclePrefID = vehicle;
    }

    public boolean hasToBeCreated() {
        return TextUtils.isEmpty(hotelPrefID) || (!NA_VALUE.equals(cuisine) && TextUtils.isEmpty(cuisinePrefID))
                || (!NA_VALUE.equals(vehicle) && TextUtils.isEmpty(vehiclePrefID));
    }

    public static PreferenceData plain() {
        PreferenceData preferenceData = new PreferenceData(NA_VALUE, NA_VALUE, NA_VALUE);
        preferenceData.setPassCodeValue(NA_VALUE);
        return preferenceData;
    }

    public PreferenceData applyChanges(String cuisine, String hotel, String vehicle) {
        PreferenceData change = new PreferenceData(cuisine, hotel, vehicle);
        change.passCode = passCode;
        change.passCodeID = passCodeID;
        change.preferenceIDs(cuisinePrefID, hotelPrefID, vehiclePrefID);
        return change;
    }


    public void setHotel(String hotel, String hotelID){
        this.hotel = hotel;
        this.hotelPrefID = hotelID;
    }

    public void setCuisine(String cuisine, String cuisineID){
        this.cuisine = cuisine;
        this.cuisinePrefID = cuisineID;
    }

    public void setVehicle(String vehicle, String vehiclePrefID){
        this.vehicle = vehicle;
        this.vehiclePrefID = vehiclePrefID;
    }

    public void setPassCode(String passCode, String passCodeID){
        this.passCode = passCode;
        this.passCodeID = passCodeID;
    }

    public void setPassCodeValue(String passCode){
        this.passCode = passCode;
    }

    public String getCuisine() {
        return TextUtils.isEmpty(cuisine) ? NA_VALUE : cuisine;
    }

    public String getHotel() {
        return TextUtils.isEmpty(hotel) ? NA_VALUE : hotel;
    }

    public String getVehicle() {
        return TextUtils.isEmpty(vehicle) ? NA_VALUE : vehicle;
    }

    public String getCuisinePrefID() {
        return cuisinePrefID == null ? "" : cuisinePrefID;
    }

    public String getHotelPrefID() {
        return hotelPrefID == null ? "" : hotelPrefID;
    }

    public String getVehiclePrefID() {
        return vehiclePrefID == null ? "" : vehiclePrefID;
    }

    public String getPassCodeID() {
        return passCodeID == null ? "" : passCodeID;
    }

    public String getPassCode() {
        return TextUtils.isEmpty(passCode) ? NA_VALUE : passCode;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.cuisine);
        dest.writeString(this.hotel);
        dest.writeString(this.vehicle);
        dest.writeString(this.cuisinePrefID);
        dest.writeString(this.hotelPrefID);
        dest.writeString(this.vehiclePrefID);
        dest.writeString(this.passCode);
        dest.writeString(this.passCodeID);
    }

    protected PreferenceData(Parcel in) {
        this.cuisine = in.readString();
        this.hotel = in.readString();
        this.vehicle = in.readString();
        this.cuisinePrefID = in.readString();
        this.hotelPrefID = in.readString();
        this.vehiclePrefID = in.readString();
        this.passCode = in.readString();
        this.passCodeID = in.readString();
    }

    public static final Creator<PreferenceData> CREATOR = new Creator<PreferenceData>() {
        @Override
        public PreferenceData createFromParcel(Parcel source) {
            return new PreferenceData(source);
        }

        @Override
        public PreferenceData[] newArray(int size) {
            return new PreferenceData[size];
        }
    };
}
